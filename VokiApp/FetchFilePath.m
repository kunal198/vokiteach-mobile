//
//  FetchFilePath.m
//  VokiApp
//
//  Created by brst on 1/25/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "FetchFilePath.h"

@implementation FetchFilePath

- (id) init
{
    _jsNamespace = @"jsbridge";
    return self;
}

+ (FetchFilePath *)sharedMethod
{
    static FetchFilePath *sharedInstance;
    if (!sharedInstance)
    {
        sharedInstance = [[super alloc]init];
    }
    return sharedInstance;
}

-(NSArray*)FetchAssestData:(NSString*)path
{
    
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:path];
    NSError* err;
    NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&err];
    
    
   NSArray *sortedArray = [directoryContents sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    
   
//    NSArray *sortedArray = [directoryContents sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
// 
//        if ([obj1 integerValue] > [obj2 integerValue])
//        {
//            return (NSComparisonResult)NSOrderedDescending;
//        }
//        if ([obj1 integerValue] < [obj2 integerValue])
//        {
//            return (NSComparisonResult)NSOrderedAscending;
//        }
//        return (NSComparisonResult)NSOrderedSame;
//    }];
//    NSLog(@"Sorted Service Array is ::%@",sortedArray);
    
    
    return sortedArray;
}

-(NSString*)GetBundlePath
{
    NSString* MainBundlePath = [[NSBundle mainBundle] resourcePath];
    NSLog(@"called");
    return MainBundlePath;
}

@end
