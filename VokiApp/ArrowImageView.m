//
//  ArrowImageView.m
//  VokiApp
//
//  Created by Brst981 on 25/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "ArrowImageView.h"

@implementation ArrowImageView

-(instancetype)init{
    self = [super init];
    if (self) {
        imageView = [[UIImageView alloc]init];
        [self addSubview:imageView];
        //[imageView  setUserInteractionEnabled:false];
      //  self.backgroundColor = [UIColor redColor];
    }
    return self;
}
-(void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    imageView.frame = self.bounds;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.delegate touchChanges:touches withEvent:event];
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.delegate touchChanges:touches withEvent:event];
}
-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
     [self.delegate touchChanges:touches withEvent:event];
}

-(void)setImage:(UIImage *)image {
    imageView.image = image;
}
@end
