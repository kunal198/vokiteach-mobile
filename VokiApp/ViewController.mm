#import <AVFoundation/AVAudioPlayer.h>
#import <AVKit/AVKit.h>
#import "ViewController.h"
#import "SIPhoto.h"
#import "AppDelegate.h"
#import "NKOColorPickerView.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <GooglePlusShareActivity/GPPShareActivity.h>
#import "textTableCell.h"
#import "voiceIDCell.h"
#import "FXmenucell.h"
#import "ObjcObject.h"
#import "ServerConnection.h"
#import "ParseObject.h"
#import "FetchFilePath.h"
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ConnectionToCoreData.h"
#import "UialertViewViewController.h"
#import "Reachability.h"
#import "GalleryViewController.h"
#import "XMLReader.h"
#import "AlertControllerUtility.h"
#import  "AFHTTPSessionManager.h"
#import <sys/sysctl.h>
#import <StoreKit/StoreKit.h>
#import "viewCollectionViewCell.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ZipArchive.h"
#import "UCZProgressView.h"
#import <Crashlytics/Crashlytics.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import "TwitterVideoUpload.h"
#import  <ZipArchive/ZipArchive.h>
#import <TwitterKit/TwitterKit.h>
#import "SocialVideoHelper.h"

#define IS_IOS11orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)


BOOL isCostumeChanged = NO;
BOOL isHairChanged = NO;
BOOL isTextLangTableVisible = NO;
BOOL isVoiceTableVisible = NO;
BOOL isFxTableVisible = NO;
BOOL isCloathSelected = NO;
BOOL isHAirStyleSelected = NO;
BOOL isCapSelected = NO;
BOOL isGlassesSelected = NO;
BOOL isBlingSelected = NO;
BOOL isPropsSelected = NO;
BOOL isHairSelected = NO;
BOOL isfHairSelected = NO;
BOOL isMouthSelected = NO;
BOOL inAppPurchaseCount_bool = NO;
BOOL isChangeRegin = false;

#define BundleID @"com.Gil.VokiEd.buyall"
#define GOOGLE_PLUS_CLIENT_ID @"944406190708-9h5fd0gof4nf6mo3g01glbm1g51rjfsu.apps.googleusercontent.com"


@interface ViewController ()<NSURLConnectionDelegate, NSURLConnectionDataDelegate ,NSURLSessionDelegate,NSURLSessionDownloadDelegate,UICollectionViewDataSource,UICollectionViewDelegate,FBSDKSharingContent>

{
     
     
     IBOutlet UIView *shareDialogView;
     
     IBOutlet UITextView *txtShareText;
     
     IBOutlet UIImageView *imgVideoThumbnail;
     
     IBOutlet UIView *accountsBGView;
     
     IBOutlet UITableView *tableViewAccounts;
     
     IBOutlet NSLayoutConstraint *constraintHeightOfAccountsTable;
     
     IBOutlet UILabel *lblCharaterCount;
     IBOutlet UILabel *lblSharingHeading;
     
     int time;
     NSURLSessionDownloadTask *download;
     NSMutableString *RUNNING_CHARACTER;
     BOOL changebutton;
     NSRange range;
     NSRange termsrange;
     NSRange Privacyrange;
     NSString *xpos;
     NSString *yposvalue;
     NSString *scalevalue;
     NSMutableArray *splashArray;
     float cellWidth;
     UIActivityIndicatorView *audioActivity;
     UIActivityIndicatorView *Activityindicator;
     ObjcObject *_demoObject;
     NSMutableArray *saveCharacter;
     NSString *LibPath;
     UISwipeGestureRecognizer *gestureRecognizer;
     UISwipeGestureRecognizer *gestureRecognizerRight;
     UITapGestureRecognizer *TapGesture;
     UITapGestureRecognizer *CloseImageTapGesture;
     UIActivityIndicatorView *inappprogressview ;
     UIActivityIndicatorView *Restoreprogressview ;
     UIView *LoaderView;
     bool thirdView;
     NSMutableArray *temparr;
     BOOL menuViewOpenClose;
     BOOL shareViewOpenClose;
     BOOL isalphable;
     bool sharewithemail;
     bool sharewithfacebook;
     int finaltextvoiceID;
     UialertViewViewController *alertVc;
     UialertViewViewController *alertVc1;
     UialertViewViewController *alertVc2;
     UIView*  blurViewCalled;
     UIView*  blurViewCalled1;
     UIView*  blurViewCalled2;
     UIView*  blurViewCalled3;
     UIView*  blurViewCalledbuy;
     AppDelegate *appDelegate_obj;
     int deleteImageIndex;
     UIActionSheet *actnSht;
     int recordCounting;
     NSTimer *recordTimer;
     NSTimer *checklogin;
     NSTimer *sliding;
     int rotateDiceCount;
     int randomArrayCount;
     BOOL gesturePanBool;
     float buttonHeightSecondScroller;
     int thirdScrollerCharacterIndex;
     NSString *SessionID;
     NSString *characterCSURL;
     NSString *chsm;
     NSString *Scid;
     NSString *saveSceneUrl;
     int sharingCount;
     NSString *backgroundID;
     NSString *TTSText;
     UIAlertController *_alertController;
     UIButton *playCharacterAud1;
     CGFloat xvalueChar;
     NSInteger accSelectedCount;
     int buttonTagSelected;
     NSString *genderID;
     NSMutableArray *fxmenuItems;
     NSMutableArray *fxTypes;
     NSMutableArray *fxLevels;
     NSString *fxlevel;
     NSString *fxtype;
     NSString *spriteSheetSelectedChar;
     BOOL saveCharAudio;
     NSString *selectedCategory;
     BOOL ttsAudioRecord;
     NSString *recordFXtypeLevel;
     NSString *recordWithFXPath;
     NSData *ownBackround;
     NSMutableArray *solditems;
     int selectedcharid;
     bool sharealertopen;
     bool isrightshare;
     bool isright;
     bool issecopen;
     bool issavescean;
     bool isvoicepanopen;
     NSString *SoundLink;
     bool sharingwithTwitter;
     int usagecount;
     int imageCount;
     BOOL isdownloading;
     NSString *downloadedsize;
     NSString *downloadingtotalsize;
     
     //prabhjot
     NSMutableArray *TwitterAccountArray ;
     NSArray *TwitterDeviceArray;
     ACAccount *selectedAccount;
     UIImage *thumbnailImage;
     NSURL *videoURL;
     int isTwitterFirstTimeCount;
     NSURL * fileNameFirst;
}
@property (nonatomic, strong) NSURLSessionDownloadTask *backgroundTask;
@property (nonatomic, assign) float downloadSize;
@property (nonatomic, strong) NSMutableData *dataToDownload;

@end

@implementation ViewController
@synthesize transparentIndicator;

- (void)viewDidLoad
{
    

     isTwitterFirstTimeCount = 0;
     recordprocess = false;
     //     TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
     //     NSString *userID = store.session.userID;
     //     [store logOutUserID:userID];
     [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"sharecount"];
     _downloadview.hidden = true;
     splashArray = [[NSMutableArray alloc]initWithObjects:@"character-image",@"history",@"celebrate",@"animals",@"epic-stories",@"holidays",@"leaders",@"symbol", nil];
     
     _backgroundTask = nil;
     Pagecontrol.pageIndicatorTintColor = [UIColor lightGrayColor];
     Pagecontrol.currentPageIndicatorTintColor = [UIColor blackColor];
     Pagecontrol.backgroundColor = [UIColor clearColor];
     [Pagecontrol setNumberOfPages:[splashArray count]];
     
     login1 = [[FBSDKLoginManager alloc] init];
     [sliding_collectionview setPagingEnabled:YES];
     [login1 logOut];
     videofbshare = false;
     Main_videoPath = @"";
     sameaudio = false;
     outurltweet = nil;
     login1 = [[FBSDKLoginManager alloc]init];
     AVAudioSession *audioSession = [AVAudioSession sharedInstance];
     NSError *err = nil;
     [audioSession setCategory :AVAudioSessionCategoryPlayback error:&err];
     [self.TermsandPrivacyteextview setContentOffset:CGPointZero animated:NO];
     [self.Privacytextview setContentOffset:CGPointZero animated:NO];
     NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
     [center addObserver:self selector:@selector(appDidEnterBackground) name:@"appDidEnterbackground" object:nil];
     NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
     [center1 addObserver:self selector:@selector(appDidEnterForeground) name:@"appDidEnterForeground" object:nil];
     selectbtn = [[UIButton alloc]init];
     playTime = 0;
     [__saveToLibBtn setEnabled:true];
     [self fetchAvailableProducts];
     isenable = false;
     playCharacterAud1.alpha = 0;
     NSString *buy = [[NSUserDefaults standardUserDefaults]valueForKey:@"isbuyall"];
     memorywarningcount = 0;
     memorywarningcount = [[NSUserDefaults standardUserDefaults] integerForKey:@"memorywarningcount"];
     
     if ([buy isEqualToString:@"yes"])
     {
          isbuyall = true;
     }
     
     Googleshared = false;
     Mailshared = false;
     self.progressview.hidden = YES;
     self.progressview.trackColor = [UIColor clearColor];
     self.progressview.barColor = [UIColor colorWithRed:244.0f/255.0f green:165.0f/255.0f blue:83.0f/255.0f alpha:1.0f];
     self.progressview.barThickness = self.progressview.frame.size.height;
     self.progressview.showPercentageText = NO;
     
     [self.screencapture addSubview:___CharacterWebView];
     [self.screencapture addSubview:vokilogo_img];
     
     playCharacterAud1.hidden = true;
     recording = false;
     usagecount = 0;
     constant = 0;
     size_t size;
     sysctlbyname("hw.machine", NULL, &size, NULL, 0);
     char *machine = (char*)malloc(size);
     sysctlbyname("hw.machine", machine, &size, NULL, 0);
     NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
     [[NSUserDefaults standardUserDefaults]setObject:[self platformType:platform] forKey:@"DEVICE"];
     free(machine);
     fxTypes = [[NSMutableArray alloc]init];
     fxLevels = [[NSMutableArray alloc]init];
     _previousImageTag = 1000;
     selectedCategory = @"assets/img/char/Anime/";
     spriteSheetSelectedChar = @"img/char/Anime/Aiko_217/ohv2.png";
     
     LibPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",@"/assets/img/char/Anime/Aiko_217"] ofType:@"png" inDirectory:@""];
     [[NSUserDefaults standardUserDefaults]setValue:selectedCategory forKey:@"selectedCategory"];
     xpos = @"0.010";
     yposvalue=@"-0.15";
     scalevalue=@"0.60";
     genderID = @"1";
     __mainFirstStrip.alpha = 1;
     __shareView.alpha = 0;
     _deleteGalleryBtn.alpha = 0;
     __thirdView.alpha = 0;
     _CloeseThirdStripBtn.alpha = 0;
     __shareView.alpha = 0;
     __colorPickerView.alpha = 0;
     __clothingView.alpha =0;
     __vokiView.alpha = 0;
     [[NSUserDefaults standardUserDefaults]setValue:selectedCategory forKey:@"selectedCategory"];
     [[NSUserDefaults standardUserDefaults]setValue:spriteSheetSelectedChar forKey:@"spriteSheetSelectedChar"];
     backgroundID = @"1464235";
     buttonHeightSecondScroller = 0.0;
     gesturePanBool = true;
     [super viewDidLoad];
     [self ShowLoader];
     saveCharacter = [NSMutableArray new];
     partofBody = [[NSString alloc]initWithFormat:@"skin"];
     
     LibPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",@"/assets/img/char/Anime/Aiko_217"] ofType:@"png" inDirectory:@""];
     
     temparr =[[NSMutableArray alloc]init];
     //--------  getting data from xml file ----------//
     int parseObj = [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_XML_PARSED"];
     if(parseObj == 0)
     {
          _XMLDataArray = [[NSMutableArray alloc]init];
          _XMLDataArray = [[ParseObject sharedMethod] ParseFile];
          [[ConnectionToCoreData sharedMethod] SaveIntoCoreData:_XMLDataArray];
     }
     
     CoreDataArray = [[ConnectionToCoreData sharedMethod] FetchDataFromLocalDB];
     //------------------------------------------------------//
     appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     //--------  initializing variables ----------//
     [self InitializeVariables];
     [self AddWebView];
     checklogin = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkLogin) userInfo:nil repeats:YES];
     [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AudioPath"];
     [[NSUserDefaults standardUserDefaults] synchronize];
     [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
     [[NSUserDefaults standardUserDefaults] synchronize];
     [self CalculateViewSize];
     [self setup];
     [self HideAllViews];
     [self SetSelectedImageOfButtons];
     
     if (_canvasWidth <= 570  )
     {
          [self performSelector:@selector(StopLoader) withObject:nil afterDelay:4.0f];
          
     }
     else if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          [self performSelector:@selector(StopLoader) withObject:nil afterDelay:6.0f];
     }
     else
     {
          [self performSelector:@selector(StopLoader) withObject:nil afterDelay:4.0f];
          
     }
     
     _brightNessPicker.delegate = self;
     NSOperationQueue *queue = [[NSOperationQueue alloc] init];
     _motionManager = [[CMMotionManager alloc] init];
     _motionManager.accelerometerUpdateInterval = 1;
     
     
     [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(deviceOrientation:)
                                                  name:UIDeviceOrientationDidChangeNotification
                                                object:nil];
     
     
     [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryZVertical
                                                             toQueue:queue
                                                         withHandler:^(CMDeviceMotion *motion, NSError *error)
      {
           if (error == nil)
           {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                     
                     
                     CGFloat y = motion.gravity.y;
                     
                     CGFloat z = motion.gravity.z;
                     float angle = atan2(y, z);
                     
                     if (gesturePanBool == true)
                     {
                          int angl2 = angle;
                          if (angl2 == angle1)
                          {
                               m++;
                          }
                          else
                          {
                               angle1 = angle;
                               m=0;
                          }
                          if (m<25)
                          {
                               if (  oritentationLeft == true)
                               {
                                    if (angle1 == 2)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"90",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                    }
                                    
                                    if (angle1 == -1)
                                    {
                                         
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"309",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                    }
                                    
                                    if (angle1 == -2)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"273",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                         
                                    }
                                    if (angle1 == 1)
                                    {
                                         
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"90",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                         
                                    }
                                    if (angle1 == 0)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"359",@"1",@"180"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                    }
                               }
                               else
                               {
                                    if (angle1 == 2)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"271",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                    }
                                    
                                    if (angle1 == -1)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"53",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                    }
                                    
                                    if (angle1 == -2)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"90",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                         
                                    }
                                    if (angle1 == 1)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"312",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                    }
                                    if (angle1 == 0)
                                    {
                                         NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",@"2",@"1",@"200"];
                                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                    }
                               }
                          }
                          else
                          {
                               
                          }
                     }
                     
                     
                }];
                
           }
           else
           {
                NSLog(@"here is motion error%@",error);
           }
      }];
     
     
     
     [self loadpurchasescrooler];
     
     javascriptContext  = [[JSContext alloc] init]; // Create a standalone javascript VM and context.
     
     //********
     javascriptContext[@"myObjC_callback"] = ^(NSString *arg) {
          NSLog(@"Calling someObjectiveCMethod from javascript.  Passed arg is:%@", arg);
          // [self someObjectiveCMethod:arg];
     };
     [self setDialogFrame]; //prabhjot
     
}

//prabhjot

-(void) setDialogFrame{
     
     shareDialogView.frame = self.view.frame;
     accountsBGView.frame = self.view.frame;
     TwitterAccountArray = [[NSMutableArray alloc] init];
     TwitterDeviceArray = [[NSArray alloc] init];
     selectedAccount = [[ACAccount alloc] init];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
     NSInteger currentIndex1 = sliding_collectionview.contentOffset.x / sliding_collectionview.frame.size.width;
     Pagecontrol.currentPage=currentIndex1;
}


-(void)myObjectiveCFunction
{
     NSLog(@"Audio Started");
     
     NSString *str = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"testFunction()"]; // ThisGive issue when come From Background...
     
     if (str == nil)
     {
          
     }
     else
     {
          sameaudio = false;
          if ([str  isEqualToString: @"-1"])
          {
               if (sameaudio == false)
               {
                    
                    if (stop == true && recording == true)
                    {
                         stop = false;
                         play = false;
                         
                         appDelegate.isRecording = NO;
                         Main_videoPath = @"";
                         outurltweet = nil;
                         isrecording = false;
                         recordprocess = false;
                         if (ishalfrecording == true)
                         {
                              sameaudio = false;
                              
                         }
                         else
                         {
                              sameaudio = TRUE;
                              sameaudio = YES;
                         }
                         [_screencapture performSelector:@selector(stopRecording) withObject:nil afterDelay:0.00];
                         sameaudio = TRUE;
                         sameaudio = YES;
                         [self performSelector:@selector(enabledbtn) withObject:self afterDelay:0.00];
                         
                    }
               }
               
               Main_videoPath = @"";
               outurltweet = nil;
               
               [playCharacterAud1 setSelected:NO];
               [_PlayCharAud1dup setSelected:NO];
          }
          else
          {
               if (sameaudio == false)
               {
                    if (play == false && recording == true)
                    {
                         
                         play = true;
                         stop = true;
                         isgroupopen = false;
                         // __rightShareBtn.enabled = false;
                         
                         _video_downloadbtn.enabled = false;
                         __facebookBtn.enabled = false;
                         __twitterBtn.enabled = false;
                         appDelegate.isRecording = YES;
                         Main_videoPath = @"";
                         outurltweet = nil;
                         // isrecording = true;
                         __mailBtn.enabled = false;
                         _Whatsbtn.enabled = false;
                         __googleBtn.enabled = false;
                         ishalfrecording = false;
                         recordprocess = true;
                         [_screencapture performSelector:@selector(startRecording:) withObject:self afterDelay:0.0];
                         
                    }
               }
               
               Main_videoPath = @"";
               outurltweet = nil;
               [playCharacterAud1 setSelected:YES];
               [_PlayCharAud1dup setSelected:YES];
          }
          
          
     }
}
-(BOOL)silenced {
#if TARGET_IPHONE_SIMULATOR
     // return NO in simulator. Code causes crashes for some reason.
     return NO;
#endif
     
     CFStringRef state;
     UInt32 propertySize = sizeof(CFStringRef);
     AudioSessionInitialize(NULL, NULL, NULL, NULL);
     AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &propertySize, &state);
     if(CFStringGetLength(state) > 0)
          return NO;
     else
          return YES;
     
}
-(void)appDidEnterForeground{
     
     
     if (isChangeRegin == true) {
          isitunesconnect = false;
          [self restorecharacters];
     }
}
-(void)appDidEnterBackground
{
     
     NSString *str = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"testFunction()"]; // ThisGive issue when come From Background...
     
     
     if (str == nil)
     {
          
     }
     else
     {
          
          if ([str  isEqualToString: @"-1"])
          {
               
               
          }
          else
               
          {
               sameaudio = false;
               ishalfrecording = true;
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(0)"];
          }
          
          
     }
     sameaudio = false;
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(0)"];
     if (_isRecording == YES)
     {
          [self StopProgress];
          [self stopRecording];
          _isRecording = NO;
          __uptoTimeLbl.text = @"upto 60 sec long";
     }
     
    
     
}


-(void)enabledbtn
{
     
     __rightShareBtn.enabled = true;
     __voiceButton.enabled = true;
     __colorButton.enabled = true;
     __facebookBtn.enabled = true;
     __twitterBtn.enabled = true;
     __mailBtn.enabled = true;
     _Whatsbtn.enabled = true;
     __googleBtn.enabled = true;
     _video_downloadbtn.enabled = true;
}


-(void)setup
{
#pragma mark ---- set  Attributed string
     /**  HEre is code of Attributed String  – */
     
     if ([self silenced]) {
          NSLog(@"silenced");
     }
     else {
          NSLog(@"not silenced");
     }
     
     NSMutableAttributedString *touchattributed = [[NSMutableAttributedString alloc]initWithString:self.purchaselogintextview.text];
     NSString *url = @"http://www.voki.com/";
     NSString *labelText = @"login";
     range = [[self.purchaselogintextview text]rangeOfString:labelText];
     [touchattributed addAttribute:NSLinkAttributeName value:url range:range];
     
     self.purchaselogintextview.delegate = self;
     UITapGestureRecognizer *textTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
     textTapGesture.numberOfTapsRequired = 1;
     textTapGesture.delegate =  self;
     [self.purchaselogintextview addGestureRecognizer:textTapGesture];
     [self clearCookies];
     [self clearSavedData];
     [self ClearCacheDirectory];
     CGFloat borderWidth = 3.0f;
     _whyloginbackview.layer.borderColor = [UIColor whiteColor].CGColor;
     _whyloginbackview.layer.borderWidth = borderWidth;
     _whyloginbackview.layer.cornerRadius = 12;
     _whyloginbackview.layer.masksToBounds = YES;
     _purchaseloginbackview.layer.borderColor = [UIColor whiteColor].CGColor;
     _purchaseloginbackview.layer.borderWidth = borderWidth;
     _purchaseloginbackview.layer.cornerRadius = 12;
     _purchaseloginbackview.layer.masksToBounds = YES;
     _multipleloginborderview.layer.borderColor = [UIColor whiteColor].CGColor;
     _multipleloginborderview.layer.borderWidth = borderWidth;
     _multipleloginborderview.layer.cornerRadius = 12;
     _multipleloginborderview.layer.masksToBounds = YES;
     ImageScrollerArray = [[NSMutableArray alloc]init];
     Dontaskagain = false;
     _sidedollar.alpha = 0;
     random = 0;
     _Purchasedollerimg.hidden = true;
     _purchasemainscroolview.contentSize = CGSizeMake(self.Purchaseview.frame.size.width, self.Purchaseview.frame.size.height+100);
     allcatagoryview.hidden = true;
     playCharacterAud1.hidden = false;
     playCharacterAud1.alpha = 1;
     issecopen = false;
     isright = false;
     isvoicepanopen = false;
     _txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0);
     _twxtUserName.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0);
     solditems = [[NSMutableArray alloc]init];
     selectedcharid = 777;
     myframe = self.Accountview.frame;
     _txtPassword.layer.borderColor =[[UIColor whiteColor] CGColor];
     _twxtUserName.layer.borderColor = [[UIColor whiteColor] CGColor];
     _txtPassword.layer.borderWidth = 2.0;
     _twxtUserName.layer.borderWidth = 2.0;
     UIColor *color = [UIColor whiteColor];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide) name:UIKeyboardWillHideNotification object:nil];
     _twxtUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"User Name" attributes:@{NSForegroundColorAttributeName: color}];
     _txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
     
     //--------  callin initial methods ----------//
     
     isCostumeChanged = NO;
     isHairChanged = NO;
     menuViewOpenClose = YES;
     shareViewOpenClose = YES;
     NSString *htmlString = @"<font color=\"white\"><span style=\"font-family: Arial; font-size: 16\">Log into your paid Voki account <br> to get full access.</font></span><font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\">    <u>Learn More</u></span></font><font color=\"white\"><span style=\"font-family: Arial; font-size: 16\">.</font></span>";
     NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
     
     self.LoginheaderLabel.attributedText = attributedString;
     self.LoginheaderLabel.tintColor = [UIColor orangeColor];
     NSString *whyloginstrg;
     
     
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          
          CGRect oldfr=   __googleBtn.frame;
          oldfr.origin.y = __mailBtn.frame.origin.y;
          __googleBtn.frame =  oldfr;
          
          CGRect twitterfr=   __twitterBtn.frame;
          twitterfr.origin.y = __facebookBtn.frame.origin.y;
          __twitterBtn.frame =  twitterfr;
          
          CGRect whatsframe=   _Whatsbtn.frame;
          whatsframe.origin.y = _video_downloadbtn.frame.origin.y+5;
          _Whatsbtn.frame =  whatsframe;
          
          
     }
     
     
     whyloginstrg  = @"<font color=\"white\"><span style=\"font-family: Arial; font-size: 16\">Paying Voki subscribers & their students may access & share all Voki characters, including all premium characters.<br>If you are a level 1 or higher subscriber or a student (given access by your teacher) please log in to unlock all characters.<br> The app will remember your login so you will not need to repeat it.<b> Please note that login is limited to two devices.<br><br>               </b></span></font>";
     
     
     
     NSMutableAttributedString *TermsattributedString = [[NSMutableAttributedString alloc]initWithString:__termsServicesTextView.text];
     NSString  * termlabel = @"Service";
     NSString *privacylabel = @"Privacy Policy";
     termsrange = [[__termsServicesTextView text] rangeOfString:termlabel];
     Privacyrange = [[__termsServicesTextView text] rangeOfString:privacylabel];
     [TermsattributedString addAttribute:NSLinkAttributeName value:url range:termsrange];
     self._termsServicesTextView.delegate = self;
     UITapGestureRecognizer *termsTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsTapGesture:)];
     termsTapGesture.numberOfTapsRequired = 1;
     termsTapGesture.delegate =  self;
     [__termsServicesTextView addGestureRecognizer:termsTapGesture];
     __termsServicesTextView.tintColor = [UIColor orangeColor];
     NSAttributedString *whyloginstring = [[NSAttributedString alloc] initWithData:[whyloginstrg dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
     _whylogintextview.attributedText = whyloginstring;
     
     NSString *PurchaseHtmlstring = @"<font color=\"white\"><span style=\"font-family: Arial ; font-size: 18\">Unlock hundreds of <br>                                                                                                Voki avatars.</font></span><font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\">    <u>Learn More</u></span></font></font>";
     NSAttributedString *PurchaseattributedString = [[NSAttributedString alloc] initWithData:[PurchaseHtmlstring dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
     _purchaseheaderLabel.attributedText = PurchaseattributedString;
     
     NSString *restorehtmlstring = @"<span style=\"font-family: Arial; font-size: 18\"><font color=\"#fc7d00\"><u>Restore Purchases</u></span><span style=\"font-family: Arial; font-size: 18\"></font>";
     NSAttributedString *restorestring = [[NSAttributedString alloc] initWithData:[restorehtmlstring dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
     _restoretext.attributedText = restorestring;
     /** End  code of Attributed String  – */
     
     
     [self checkLogin];
     [self performSelector:@selector(showreviewAlert) withObject:nil afterDelay:600.0];
     [self LoadLanguageArray];
     
 
     /**  set Initial BOOL Value   – */
     saveCharAudio = false;
     changebutton = false;
     isCostumeChanged = NO;
     isHairChanged = NO;
     menuViewOpenClose = YES;
     shareViewOpenClose = YES;
     __vokiView.hidden=true;
     __clothingView.hidden=true;
     __thirdView.hidden=true;
     _CloeseThirdStripBtn.hidden=true;
     __ImageView.hidden=true;
     _CloseSecondStripBtn.hidden=true;
     __colorPickerView.hidden=true;
     __closePickerView.hidden=true;
     __recordVoiceView.hidden=true;
     __voiceTextView.hidden=true;
     __shareView.hidden=true;
     gesturePanBool = true;
     
     
     /**   Initialize  Array  – */
     fxTypes = [[NSMutableArray alloc]init];
     fxLevels = [[NSMutableArray alloc]init];
     temparr =[[NSMutableArray alloc]init];
     saveCharacter = [NSMutableArray new];
     
     
     /**- Initial Webservice     – */
     [self getSessionApi];
     [self audioEffectApi];
     [self startEventApi];
     
     partofBody = [[NSString alloc]initWithFormat:@"skin"];
     LibPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",@"/assets/img/char/Anime/Aiko_217"] ofType:@"png" inDirectory:@""];
     
     //--------  getting data from xml file ----------//
     int parseObj = [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_XML_PARSED"];
     if(parseObj == 0)
     {
          _XMLDataArray = [[NSMutableArray alloc]init];
          _XMLDataArray = [[ParseObject sharedMethod] ParseFile];
          [[ConnectionToCoreData sharedMethod] SaveIntoCoreData:_XMLDataArray];
     }
     
     CoreDataArray = [[ConnectionToCoreData sharedMethod] FetchDataFromLocalDB];
     //------------------------------------------------------//
     appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     //--------  initializing variables ----------//
     
     //--------  callin initial methods ----------//
     
     
     
     /**- Initialize Product-Id      – */
     
     NSMutableString *xml_Path = [NSString stringWithFormat:@"assets/img/char/Anime/Aiko_217"];
     RUNNING_CHARACTER = xml_Path;
     freecharacetr = [[NSMutableArray alloc]initWithObjects:xml_Path,
                      @"assets/img/char/Animals/Apollo_316",
                      @"assets/img/char/Cats/Abysinian_2587",
                      @"assets/img/char/Classic/Abdul_3370",
                      @"assets/img/char/Digimon/Falcomon_2534",
                      @"assets/img/char/Dogs/Airedale Terrier_2443",
                      @"assets/img/char/Edgy/Cheryl_1196",
                      @"assets/img/char/Fantasy/Evil Queen_27096",
                      @"assets/img/char/Folk/African Man_31238",
                      @"assets/img/char/Historical Figures/Abe Lincoln_431",
                      @"assets/img/char/Holiday/Alvar_2448",
                      @"assets/img/char/Monsters/Alien Prime_90",
                      @"assets/img/char/Oddballs/Toon Bam_1310",
                      @"assets/img/char/POLITICS/Angela Merkel_13791",
                      @"assets/img/char/Randoms/Alien_481",
                      @"assets/img/char/Smilies/Bashful_273",
                      @"assets/img/char/Symbols/Division Symbol_37203",
                      @"assets/img/char/Toons/Cherie_2617",
                      @"assets/img/char/VIP/Ali G_2666",
                      @"assets/img/char/Beach/Beach Dude_283",
                      @"assets/img/char/3D/Amber_1380",
                      nil];
     
     Allproductid = [[NSMutableDictionary alloc]init];
     Allproductid = @{@"Animals": @[@"Purchased",
                                    @"com.Gil.VokiEd.Animals.Bat_790",
                                    @"com.Gil.VokiEd.Animals.Bumble_892",
                                    @"com.Gil.VokiEd.Animals.CapuchinMonkey_1067",
                                    @"com.Gil.VokiEd.Animals.Chester_505",
                                    @"com.Gil.VokiEd.Animals.Dash_406",
                                    @"com.Gil.VokiEd.Animals.EarlBear_672",
                                    @"com.Gil.VokiEd.Animals.ElectricSheep_680",
                                    @"com.Gil.VokiEd.Animals.Fran_404",
                                    @"com.Gil.VokiEd.Animals.Harry_228",
                                    @"com.Gil.VokiEd.Animals.Jaws_92",
                                    @"com.Gil.VokiEd.Animals.JohnsonSnail_678",
                                    @"com.Gil.VokiEd.Animals.KoalaKiku_934",
                                    @"com.Gil.VokiEd.Animals.MaddiePanda_732",
                                    @"com.Gil.VokiEd.Animals.Squeaks_412",
                                    @"com.Gil.VokiEd.Animals.Turkey_110",
                                    @"com.Gil.VokiEd.Animals.Wilbur_405"],
                      
                      @"Anime": @[@"Purchased",
                                  @"com.Gil.VokiEd.Anime.Akira_1083",
                                  @"com.Gil.VokiEd.Anime.Anna_473",
                                  @"com.Gil.VokiEd.Anime.Dai_475",
                                  @"com.Gil.VokiEd.Anime.Hiro_214",
                                  @"com.Gil.VokiEd.Anime.Kai_472",
                                  @"com.Gil.VokiEd.Anime.Kasumi_215",
                                  @"com.Gil.VokiEd.Anime.Kentaro_1064",
                                  @"com.Gil.VokiEd.Anime.Mika_216",
                                  @"com.Gil.VokiEd.Anime.Nari_208",
                                  @"com.Gil.VokiEd.Anime.Sakura_486"],
                      
                      @"Beach": @[@"Purchased",
                                  @"com.Gil.VokiEd.Beach.BeachKid_284",
                                  @"com.Gil.VokiEd.Beach.LifeguardVince_286",
                                  @"com.Gil.VokiEd.Beach.ScubaStephanie_285"],
                      
                      @"Cats": @[ @"Purchased",
                                  @"com.Gil.VokiEd.Cats.Birman_2586",
                                  @"com.Gil.VokiEd.Cats.KitCat_225",
                                  @"com.Gil.VokiEd.Cats.Mainecoon_2588",
                                  @"com.Gil.VokiEd.Cats.Persian_2582",
                                  @"com.Gil.VokiEd.Cats.Pixie_411",
                                  @"com.Gil.VokiEd.Cats.Ragdoll_2650",
                                  @"com.Gil.VokiEd.Cats.Scottishfold_2644",
                                  @"com.Gil.VokiEd.Cats.Siamese_2645"
                                  ],
                      
                      @"Classic": @[@"Purchased",
                                    @"com.Gil.VokiEd.Classic.Abigail_3281",
                                    @"com.Gil.VokiEd.Classic.Aneesa_2469",
                                    @"com.Gil.VokiEd.Classic.Ashley_1105",
                                    @"com.Gil.VokiEd.Classic.Beth_2081",
                                    @"com.Gil.VokiEd.Classic.Brenda_108",
                                    @"com.Gil.VokiEd.Classic.Bryan_103",
                                    @"com.Gil.VokiEd.Classic.Bryson_2931",
                                    @"com.Gil.VokiEd.Classic.Carson_104",
                                    @"com.Gil.VokiEd.Classic.Chloe_107",
                                    @"com.Gil.VokiEd.Classic.Dana_156",
                                    @"com.Gil.VokiEd.Classic.Diane_55",
                                    @"com.Gil.VokiEd.Classic.Douglas__58",
                                    @"com.Gil.VokiEd.Classic.Dylan_157",
                                    @"com.Gil.VokiEd.Classic.Eddie_3026",
                                    @"com.Gil.VokiEd.Classic.Edward_70",
                                    @"com.Gil.VokiEd.Classic.Eva_2766",
                                    @"com.Gil.VokiEd.Classic.Gena_2300",
                                    @"com.Gil.VokiEd.Classic.Grandma_420",
                                    @"com.Gil.VokiEd.Classic.Grandpa_418",
                                    @"com.Gil.VokiEd.Classic.Hector_3377",
                                    @"com.Gil.VokiEd.Classic.Huny_715",
                                    @"com.Gil.VokiEd.Classic.Jack_69",
                                    @"com.Gil.VokiEd.Classic.Jada_2015",
                                    @"com.Gil.VokiEd.Classic.Jane_1321",
                                    @"com.Gil.VokiEd.Classic.Jenny_606",
                                    @"com.Gil.VokiEd.Classic.Jesse__56",
                                    @"com.Gil.VokiEd.Classic.Jiho_634",
                                    @"com.Gil.VokiEd.Classic.Jill_68",
                                    @"com.Gil.VokiEd.Classic.Jiny_636",
                                    @"com.Gil.VokiEd.Classic.Joan__54",
                                    @"com.Gil.VokiEd.Classic.Kayla_3314",
                                    @"com.Gil.VokiEd.Classic.Kenton_3081",
                                    @"com.Gil.VokiEd.Classic.Knox_3075",
                                    @"com.Gil.VokiEd.Classic.Kyle__71",
                                    @"com.Gil.VokiEd.Classic.Leandro_3092",
                                    @"com.Gil.VokiEd.Classic.Meagan_3372",
                                    @"com.Gil.VokiEd.Classic.Mina_616",
                                    @"com.Gil.VokiEd.Classic.Misaki_194",
                                    @"com.Gil.VokiEd.Classic.Miso_641",
                                    @"com.Gil.VokiEd.Classic.Nick_57",
                                    @"com.Gil.VokiEd.Classic.Nicole_53",
                                    @"com.Gil.VokiEd.Classic.Pete_59",
                                    @"com.Gil.VokiEd.Classic.Rachel_62",
                                    @"com.Gil.VokiEd.Classic.Robin_3174",
                                    @"com.Gil.VokiEd.Classic.Roger_72",
                                    @"com.Gil.VokiEd.Classic.Rosana_61",
                                    @"com.Gil.VokiEd.Classic.Sharon_1296",
                                    @"com.Gil.VokiEd.Classic.Skyler_586",
                                    @"com.Gil.VokiEd.Classic.Stephie_656",
                                    @"com.Gil.VokiEd.Classic.Suki_1484",
                                    @"com.Gil.VokiEd.Classic.Takumi_188",
                                    @"com.Gil.VokiEd.Classic.Tammy__63",
                                    @"com.Gil.VokiEd.Classic.Vanessa_1287",
                                    @"com.Gil.VokiEd.Classic.Yuni_629",
                                    ],
                      
                      @"Digimon": @[@"Purchased",
                                    @"com.Gil.VokiEd.Digimon.Gaomon_2530",
                                    @"com.Gil.VokiEd.Digimon.Kamemon_2505",
                                    @"com.Gil.VokiEd.Digimon.Keenan_2544",
                                    @"com.Gil.VokiEd.Digimon.Kudamon_2538",
                                    @"com.Gil.VokiEd.Digimon.Lalamon_2498",
                                    @"com.Gil.VokiEd.Digimon.Masaru_2418",
                                    @"com.Gil.VokiEd.Digimon.Sampson_2535",
                                    @"com.Gil.VokiEd.Digimon.Thomas_2540",
                                    @"com.Gil.VokiEd.Digimon.Yoshi_2539",
                                    ],
                      
                      @"Dogs": @[@"Purchased",
                                 @"com.Gil.VokiEd.Dogs.AkitaInu_2455",
                                 @"com.Gil.VokiEd.Dogs.AlaskanMalamute_2453",
                                 @"com.Gil.VokiEd.Dogs.BassetHound_2454",
                                 @"com.Gil.VokiEd.Dogs.Beagle_2436",
                                 @"com.Gil.VokiEd.Dogs.BostonTerrier_2730",
                                 @"com.Gil.VokiEd.Dogs.Boxer_2467",
                                 @"com.Gil.VokiEd.Dogs.CockerSpaniel_2427",
                                 @"com.Gil.VokiEd.Dogs.Dalmatian_2518",
                                 @"com.Gil.VokiEd.Dogs.DobermanPinscher_2513",
                                 @"com.Gil.VokiEd.Dogs.Dusty_403",
                                 @"com.Gil.VokiEd.Dogs.GermanShepherd_2205",
                                 @"com.Gil.VokiEd.Dogs.GreatDane_2711",
                                 @"com.Gil.VokiEd.Dogs.LittleBritain_2386",
                                 @"com.Gil.VokiEd.Dogs.MiniatureSchnauzer_2689",
                                 @"com.Gil.VokiEd.Dogs.MokeDog_1045",
                                 @"com.Gil.VokiEd.Dogs.Pomeranian_2383",
                                 @"com.Gil.VokiEd.Dogs.Poodle_2554",
                                 @"com.Gil.VokiEd.Dogs.Rottweiler_2517",
                                 @"com.Gil.VokiEd.Dogs.SaintBernard_2512",
                                 @"com.Gil.VokiEd.Dogs.Schipperke_2536",
                                 @"com.Gil.VokiEd.Dogs.Sparky_218",
                                 @"com.Gil.VokiEd.Dogs.Winston_1051",
                                 @"com.Gil.VokiEd.Dogs.YorkshireTerrier_2623"
                                 ],
                      
                      @"Edgy": @[@"Purchased",
                                 @"com.Gil.VokiEd.Edgy.Elise_771",
                                 @"com.Gil.VokiEd.Edgy.Layne_763",
                                 @"com.Gil.VokiEd.Edgy.Nitro_835",
                                 @"com.Gil.VokiEd.Edgy.Pink_891",
                                 @"com.Gil.VokiEd.Edgy.Todd_1227"
                                 ],
                      
                      @"Fantasy": @[@"Purchased",
                                    @"com.Gil.VokiEd.Fantasy.Fairy_26969",
                                    @"com.Gil.VokiEd.Fantasy.Genie_27304",
                                    @"com.Gil.VokiEd.Fantasy.Gnome_27189",
                                    @"com.Gil.VokiEd.Fantasy.Jester_27061",
                                    @"com.Gil.VokiEd.Fantasy.Knight_26877",
                                    @"com.Gil.VokiEd.Fantasy.Mermaid_27370",
                                    @"com.Gil.VokiEd.Fantasy.Prince_26346",
                                    @"com.Gil.VokiEd.Fantasy.Princess_25515",
                                    @"com.Gil.VokiEd.Fantasy.Unicorn_26949"
                                    ],
                      
                      @"Folk": @[@"Purchased",
                                 @"com.Gil.VokiEd.Folk.AfricanWoman_31258",
                                 @"com.Gil.VokiEd.Folk.BedouinMan_33248",
                                 @"com.Gil.VokiEd.Folk.BedouinWoman_33246",
                                 @"com.Gil.VokiEd.Folk.ChineseMan_31288",
                                 @"com.Gil.VokiEd.Folk.ChineseWoman_31323",
                                 @"com.Gil.VokiEd.Folk.GermanMan_31365",
                                 @"com.Gil.VokiEd.Folk.GermanWoman_31576",
                                 @"com.Gil.VokiEd.Folk.IndianMan_31593",
                                 @"com.Gil.VokiEd.Folk.IndianWoman_28332",
                                 @"com.Gil.VokiEd.Folk.InuitMan_32995",
                                 @"com.Gil.VokiEd.Folk.InuitWoman_28548",
                                 @"com.Gil.VokiEd.Folk.JapaneseMan_32917",
                                 @"com.Gil.VokiEd.Folk.JapaneseWoman_32920",
                                 @"com.Gil.VokiEd.Folk.KoreanMan_33815",
                                 @"com.Gil.VokiEd.Folk.KoreanWoman_33719",
                                 @"com.Gil.VokiEd.Folk.MexicanMan_33901",
                                 @"com.Gil.VokiEd.Folk.MexicanWoman_33885",
                                 @"com.Gil.VokiEd.Folk.NativeAmerican Man_34002",
                                 @"com.Gil.VokiEd.Folk.NativeAmerican Woman_34271",
                                 @"com.Gil.VokiEd.Folk.RussianMan_31748",
                                 @"com.Gil.VokiEd.Folk.RussianWoman_31769",
                                 @"com.Gil.VokiEd.Folk.SpanishMan_33199",
                                 @"com.Gil.VokiEd.Folk.SpanishWoman_33212",
                                 ],
                      
                      @"Historical Figures": @[@"Purchased",
                                               @"com.Gil.VokiEd.HistoricalFigures.BenFranklin_260",
                                               @"com.Gil.VokiEd.HistoricalFigures.Buddha_436",
                                               @"com.Gil.VokiEd.HistoricalFigures.Einstein_150",
                                               @"com.Gil.VokiEd.HistoricalFigures.GeorgeWashington_800",
                                               @"com.Gil.VokiEd.HistoricalFigures.Jesus_434",
                                               @"com.Gil.VokiEd.HistoricalFigures.MahatmaGandhi_433",
                                               @"com.Gil.VokiEd.HistoricalFigures.MarieAntoinette_128",
                                               @"com.Gil.VokiEd.HistoricalFigures.Moses_435",
                                               @"com.Gil.VokiEd.HistoricalFigures.UncleSam_679"
                                               ],
                      
                      @"Holiday": @[@"Purchased",
                                    @"com.Gil.VokiEd.Holiday.Alvina_2444",
                                    @"com.Gil.VokiEd.Holiday.DrummerBoy_408",
                                    @"com.Gil.VokiEd.Holiday.EasterBunny_183",
                                    @"com.Gil.VokiEd.Holiday.EasterEgg_184",
                                    @"com.Gil.VokiEd.Holiday.Elf_386",
                                    @"com.Gil.VokiEd.Holiday.GingerMan_409",
                                    @"com.Gil.VokiEd.Holiday.HappyHeart_154",
                                    @"com.Gil.VokiEd.Holiday.Kaya_2402",
                                    @"com.Gil.VokiEd.Holiday.Leprechaun_172",
                                    @"com.Gil.VokiEd.Holiday.Mrs.Claus_950",
                                    @"com.Gil.VokiEd.Holiday.PilgrimMan_941",
                                    @"com.Gil.VokiEd.Holiday.PilgrimWoman_942",
                                    @"com.Gil.VokiEd.Holiday.Pumpkin_100",
                                    @"com.Gil.VokiEd.Holiday.Rudolph_956",
                                    @"com.Gil.VokiEd.Holiday.SantaToo_382",
                                    @"com.Gil.VokiEd.Holiday.Santa_109",
                                    @"com.Gil.VokiEd.Holiday.Scrooge_396",
                                    @"com.Gil.VokiEd.Holiday.Snowman_102",
                                    @"com.Gil.VokiEd.Holiday.StPattyDana_171",
                                    @"com.Gil.VokiEd.Holiday.StPattyDylan_170",
                                    @"com.Gil.VokiEd.Holiday.Turkey_110",
                                    @"com.Gil.VokiEd.Holiday.ValentineDana_106",
                                    @"com.Gil.VokiEd.Holiday.ValentineDylan_105",
                                    @"com.Gil.VokiEd.Holiday.XmasTree_946"
                                    ],
                      
                      @"Monsters": @[@"Purchased",
                                     @"com.Gil.VokiEd.Animals.Bat_790",
                                     @"com.Gil.VokiEd.Monsters.Bones_97",
                                     @"com.Gil.VokiEd.Monsters.Dracula_93",
                                     @"com.Gil.VokiEd.Monsters.Dragon_94",
                                     @"com.Gil.VokiEd.Monsters.Frankenstein_95",
                                     @"com.Gil.VokiEd.Monsters.FriendlyGhost_477",
                                     @"com.Gil.VokiEd.Monsters.Ghost_793",
                                     @"com.Gil.VokiEd.Animals.Jaws_92",
                                     @"com.Gil.VokiEd.Monsters.Pirate_96",
                                     @"com.Gil.VokiEd.Monsters.Werewolf_98",
                                     @"com.Gil.VokiEd.Monsters.Witch_799",
                                     @"com.Gil.VokiEd.Monsters.Wizard_798",
                                     @"com.Gil.VokiEd.Monsters.Zombie_99"
                                     ],
                      
                      @"Oddballs": @[@"Purchased",
                                     @"com.Gil.VokiEd.Oddballs.ToonCam_1304",
                                     @"com.Gil.VokiEd.Oddballs.ToonPam_1309",
                                     @"com.Gil.VokiEd.Oddballs.ToonSam_1214",
                                     @"com.Gil.VokiEd.Oddballs.ToonTam_1283"
                                     ],
                      
                      @"POLITICS": @[@"Purchased",
                                     @"com.Gil.VokiEd.POLITICS.BarackObama_2904",
                                     @"com.Gil.VokiEd.POLITICS.BillClinton_773",
                                     @"com.Gil.VokiEd.POLITICS.DavidCameron_13753",
                                     @"com.Gil.VokiEd.POLITICS.DonaldTrump_35899",
                                     @"com.Gil.VokiEd.POLITICS.FrancoisHollande_24636",
                                     @"com.Gil.VokiEd.POLITICS.GWBush_167",
                                     @"com.Gil.VokiEd.POLITICS.HillaryClinton_646",
                                     @"com.Gil.VokiEd.POLITICS.JoeBiden_1469",
                                     @"com.Gil.VokiEd.POLITICS.JohnKerry_596",
                                     @"com.Gil.VokiEd.POLITICS.JohnMcCain_640",
                                     @"com.Gil.VokiEd.POLITICS.JuliaGillard_24393",
                                     @"com.Gil.VokiEd.POLITICS.JunichiroKoizumi_301",
                                     @"com.Gil.VokiEd.POLITICS.MittRomney_788",
                                     @"com.Gil.VokiEd.POLITICS.NicolasSarkozy_13788",
                                     @"com.Gil.VokiEd.POLITICS.RonaldReagan_760",
                                     @"com.Gil.VokiEd.POLITICS.SarahPalin_3461",
                                     @"com.Gil.VokiEd.POLITICS.SilvioBerlusconi_13824",
                                     @"com.Gil.VokiEd.POLITICS.StephenHarper_1043",
                                     @"com.Gil.VokiEd.POLITICS.TonyBlair_296",
                                     @"com.Gil.VokiEd.POLITICS.VladimirPutin_806"
                                     ],
                      
                      @"Randoms": @[@"Purchased",
                                    @"com.Gil.VokiEd.Randoms.AstroGeorge_690",
                                    @"com.Gil.VokiEd.Animals.Bumble_892",
                                    @"com.Gil.VokiEd.Randoms.ChuckSockpuppet_731",
                                    @"com.Gil.VokiEd.Randoms.Daisy_682",
                                    @"com.Gil.VokiEd.Randoms.Densha_1356",
                                    @"com.Gil.VokiEd.Randoms.FarmerJoe_34710",
                                    @"com.Gil.VokiEd.Randoms.Flower_504",
                                    @"com.Gil.VokiEd.Randoms.FriendlyGhost_477",
                                    @"com.Gil.VokiEd.Monsters.Ghost_793",
                                    @"com.Gil.VokiEd.Randoms.HollyDae_2209",
                                    @"com.Gil.VokiEd.Randoms.Hotdog_652",
                                    @"com.Gil.VokiEd.Randoms.Jasper_675",
                                    @"com.Gil.VokiEd.Randoms.JoeyBabydoll_491",
                                    @"com.Gil.VokiEd.Randoms.JohnnyCorn_890",
                                    @"com.Gil.VokiEd.Randoms.Karuda_1351",
                                    @"com.Gil.VokiEd.Holiday.Kaya_2402",
                                    @"com.Gil.VokiEd.Randoms.LlilyBabydoll_492",
                                    @"com.Gil.VokiEd.Randoms.LooneyLarry_691",
                                    @"com.Gil.VokiEd.Randoms.MotoKai_1384",
                                    @"com.Gil.VokiEd.Randoms.Muggie_2284",
                                    @"com.Gil.VokiEd.Randoms.Nova_1295",
                                    @"com.Gil.VokiEd.Randoms.Obake_1446",
                                    @"com.Gil.VokiEd.Randoms.Robot_482",
                                    @"com.Gil.VokiEd.Randoms.Skully_2166",
                                    @"com.Gil.VokiEd.Randoms.SumoWrestler_270",
                                    @"com.Gil.VokiEd.Randoms.Switch_232",
                                    @"com.Gil.VokiEd.Randoms.ThingersQuincy_2025",
                                    @"com.Gil.VokiEd.Monsters.Witch_799",
                                    @"com.Gil.VokiEd.Monsters.Wizard_798"
                                    ],
                      
                      @"Smilies": @[@"Purchased",
                                    @"com.Gil.VokiEd.Smilies.Cheerful_271",
                                    @"com.Gil.VokiEd.Smilies.Crazy_272",
                                    @"com.Gil.VokiEd.Smilies.Doc_274",
                                    @"com.Gil.VokiEd.Smilies.HappyFace_237",
                                    @"com.Gil.VokiEd.Smilies.Sweety_275"
                                    ],
                      
                      @"Symbols": @[@"Purchased",
                                    @"com.Gil.VokiEd.Symbols.DollarSymbol_37205",
                                    @"com.Gil.VokiEd.Symbols.ExclaimationSymbol_37258",
                                    @"com.Gil.VokiEd.Symbols.HashtagSymbol_37260",
                                    @"com.Gil.VokiEd.Symbols.PeaceSymbol_37275",
                                    @"com.Gil.VokiEd.Symbols.PISymbol_37259",
                                    @"com.Gil.VokiEd.Symbols.PlusSymbol_37133",
                                    @"com.Gil.VokiEd.Symbols.QuestionmarkSymbol_37209",
                                    @"com.Gil.VokiEd.Symbols.SubtractionSymbol_37368",
                                    @"com.Gil.VokiEd.Symbols.TrebleclefSymbol_37276",
                                    ],
                      
                      @"Toons": @[@"Purchased",
                                  @"com.Gil.VokiEd.Toons.Melinda_689",
                                  @"com.Gil.VokiEd.Toons.Merlin_684",
                                  @"com.Gil.VokiEd.Toons.TeenDanielle_749",
                                  @"com.Gil.VokiEd.Toons.TeenJake_742",
                                  @"com.Gil.VokiEd.Toons.TeenKevin_736",
                                  @"com.Gil.VokiEd.Smilies.TeenRyan_626",
                                  @"com.Gil.VokiEd.Smilies.TeenStacey_743",
                                  @"com.Gil.VokiEd.Smilies.TeenZack_625"
                                  ],
                      
                      @"VIP": @[@"Purchased",
                                @"com.Gil.VokiEd.VIP.Borat_2251",
                                @"com.Gil.VokiEd.VIP.BritneySpears_1115",
                                @"com.Gil.VokiEd.VIP.Bruno_2269"
                                ],
                      
                      @"3D": @[@"Purchased",
                               @"com.Gil.VokiEd.3D.Angela_754",
                               @"com.Gil.VokiEd.3D.Erik_2261",
                               @"com.Gil.VokiEd.3D.Ginger_2014",
                               @"com.Gil.VokiEd.3D.Juliette_76",
                               @"com.Gil.VokiEd.3D.Lilly_1302",
                               @"com.Gil.VokiEd.3D.Tom_77"
                               ],
                      
                      
                      };
     
     
}


-(void)deviceOrientation:(NSNotification *)note
{
     UIDevice * device = note.object;
     switch(device.orientation)
     {
               
          case UIDeviceOrientationLandscapeLeft:
               oritentationLeft = true;
               break;
               
          case UIDeviceOrientationLandscapeRight:
               oritentationLeft = false;
               break;
               
          default:
               break;
     };
}

-(void)viewWillDisappear:(BOOL)animated
{
     
     [self removeTimer];
     self.backgroundTask = nil;
     
     if (_backgroundTask.state == NSURLSessionTaskStateRunning) {
          [_backgroundTask cancel];
     }
     
     for (SKPaymentTransaction *aTransaction in [[SKPaymentQueue defaultQueue] transactions])
     {
          [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
          
          [[SKPaymentQueue defaultQueue] finishTransaction:aTransaction];
     }
}


-(void)viewWillAppear:(BOOL)animated
{
     if ([[NSUserDefaults standardUserDefaults] boolForKey:@"VideoPlayed"] == true)
     {
          [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"VideoPlayed"];
          
          return;
     }
     
     playCharacterAud1.alpha = 0;
     [super viewWillAppear:YES];
     if (sharewithemail == true || sharewithfacebook == true)
     {
          playCharacterAud1.alpha = 1;
          playCharacterAud1.hidden = false;
          recording = false;
          
     }
     else
     {
          
          NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"galleryImage"];
          [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"backgalleryImage"];
          
          if (imageData)
          {
               [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"galleryImage"];
               backgroundID = nil;
               for(UIView *subview in __ImageContentScroller.subviews)
               {
                    if ([subview isKindOfClass:[UIButton class]])
                    {
                         UIButton* myButton1 = (UIButton*)subview;
                         myButton1.layer.borderWidth = 0;
                         myButton1.layer.borderColor = [[UIColor clearColor]CGColor];
                    }
               }
               _PlayCharAud1dup.frame = playCharacterAud1.frame;
               playCharacterAud1.hidden = false;
               playCharacterAud1.alpha = 1;
               UIImage *im = [UIImage imageWithData:imageData];
               CGRect rect = CGRectMake(0,0,_canvasWidth,_canvasHeight);
               UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
               [im drawInRect:CGRectMake(0, 0, _canvasWidth, _canvasHeight)];
               UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
               UIGraphicsEndImageContext();
               NSData *imageData1 = UIImagePNGRepresentation(newImage);
               NSString *str = [NSString stringWithFormat:@"%@.png",[NSDate date]];
               str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
               NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
               NSString *documentsDirectory = [paths objectAtIndex:0];
               NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:str];
               [imageData1 writeToFile:localFilePath atomically:YES];
               int  widthInPixels = im.size.width * im.scale;
               int heightInPixels = im.size.height * im.scale;
               NSString *jsString = [NSString stringWithFormat:@"loadBackgroundImage('%@','%d','%d')",localFilePath,widthInPixels,heightInPixels];
               [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
               
          }
          else
          {
               playCharacterAud1.hidden = false;
               playCharacterAud1.alpha = 1;
          }


     }
}

-(void)InitializeVariables
{
     isright = false;
     _RootViewHeight = self.view.frame.size.height;
     _stripWidth = _RootViewHeight/6;
     _isRecording = NO;
     _isAccessoriesSelected = NO;
     vokilogo_imgagess.hidden = true;
     _isRightVokiOpened = NO;
     _isMainButtonCalled = NO;
     isTextLangTableVisible = NO;
     isVoiceTableVisible = NO;
     isFxTableVisible = NO;
     MainBundlePath = [[NSBundle mainBundle] resourcePath];
     _canvasWidth = [[UIScreen mainScreen] bounds].size.width;
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          
     }
     else if (_canvasWidth == 667 )
     {
          self.screencapture .frame = CGRectMake(0, 0, self.screencapture .frame.size.width+5 ,  self.screencapture .frame.size.height);
     }
     else if (_canvasWidth == 736 )
     {
     }
     else if (_canvasWidth == 568 )
     {
          self.screencapture .frame = CGRectMake(0, 0, self.screencapture .frame.size.width+8 ,  self.screencapture .frame.size.height);
     }
     
     _canvasHeight = [[UIScreen mainScreen] bounds].size.height;
     [self ChangeAccessorieBoolValue];
}


#pragma mark - Load character part on screen
-(void)LoadWebView
{
     @autoreleasepool {
          
          
          //--------- Load HTML file using .js and css on UIWebView ----------//
          NSString *htmlPath = [[NSBundle mainBundle] pathForResource:@"index_mem" ofType:@"html" inDirectory:@""];
          NSString *html = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
          NSArray *arr = [html componentsSeparatedByString:@"\n"];
          NSString *HtmlStr = [arr objectAtIndex:0];
          NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
          
          for(int i = 1; i < arr.count ; i++)
          {
               NSString *CurrentStr = [arr objectAtIndex:i];
               if ([CurrentStr rangeOfString:@"/assets/"].location != NSNotFound)
               {
                    CurrentStr = [CurrentStr stringByReplacingOccurrencesOfString:@"/assets/" withString:[NSString stringWithFormat:@"%@/assets/",BundlePath]];
               }
               HtmlStr = [NSString stringWithFormat:@"%@\n%@",HtmlStr,CurrentStr];
          }
          
          HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"background-color:#00;height:800px;width:1300px;float:left;position:relative;" withString:@"background-color:#00;height:H_Valuepx;width:W_Valuepx;float:left;position:relative;"];
          HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"id=\"hostCanvas\" width=\"1300\" height=\"800\"" withString:@"id=\"hostCanvas\" width=\"W_Value\" height=\"H_Value\" style=\"background : \""];
          HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"width:1300px;height:800px; display:none;" withString:@"width:LoaderCanWpx;height:LoaderCanHpx; display:none;"];
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               _whyloginbackview.frame = CGRectMake(70,80, 500 , 480);
               _purchaseloginbackview.frame = CGRectMake(100, 150, 408 , 270);
               
               
               CGRect logind=   _forgotpassword.frame;
               logind.origin.y = _forgotpassword.frame.origin.y-10;
               _forgotpassword.frame =  logind;
               
               CGRect redframe =   _rederrorimg.frame;
               redframe.origin.x = redframe.origin.x + 12 ;
               _rederrorimg.frame =  redframe;
               
               CGRect buyallframe =   _purchasebuyall.frame;
               buyallframe.origin.x = buyallframe.origin.x - 20 ;
               _purchasebuyall.frame =  buyallframe;
               
               
               CGRect purchaselearnframe =   _Purchaselearnmorebtn.frame;
               purchaselearnframe.origin.x = purchaselearnframe.origin.x - 160 ;
               _Purchaselearnmorebtn.frame =  purchaselearnframe;
               
               
               CGRect acclearn =   _acclearnmore.frame;
               acclearn.origin.x = acclearn.origin.x - 80 ;
               acclearn.origin.y = acclearn.origin.y +5 ;
               _acclearnmore.frame =  acclearn;
               
               CGRect oldframe =   _AccountLabel.frame;
               oldframe.origin.y = _AccountLabel.frame.origin.y+20 ;
               _AccountLabel.frame =  oldframe;
               
               CGRect old =   _AccountLabel.frame;
               old.origin.x = _AccountLabel.frame.origin.x+20 ;
               _AccountLabel.frame =  old;
               
               CGRect oldf=   _MainLoginview.frame;
               oldf.origin.y = _MainLoginview.frame.origin.y+70 ;
               
               _MainLoginview.frame =  oldf;
               
               
               CGRect loginframe=   _loginbtn.frame;
               loginframe.origin.y = _loginbtn.frame.origin.y+15 ;
               _loginbtn.frame =  loginframe;
               
               
               CGRect loginfra=   _forgotpassword.frame;
               loginfra.origin.y = _forgotpassword.frame.origin.y+20 ;
               loginfra.size.width = _forgotpassword.frame.size.width - 30;
               loginfra.origin.x = _forgotpassword.frame.origin.x+25 ;
               _forgotpassword.frame =  loginfra;
               
               
               
               CGRect link =   _multiplelogforgetLink.frame;
               link.origin.x = _multiplelogforgetLink.frame.origin.x+70 ;
               _multiplelogforgetLink.frame =  link;
               
          }
          
          else if (_canvasWidth == 667 )
          {
               _purchaseloginbackview.frame = CGRectMake(20, _purchaseloginbackview.frame.origin.y, 408 , 320);
               
               _whyloginbackview.frame = CGRectMake(45, self.whyloginbackview.frame.origin.y, 330 , 330);
          }
          
          else if (_canvasWidth == 736 )
          {
               _whyloginbackview.frame = CGRectMake(50, self.whyloginbackview.frame.origin.y, 340 , 340);
               _purchaseloginbackview.frame = CGRectMake(50, _purchaseloginbackview.frame.origin.y, 408 , 270);
               
          }
          else
          {
               
               CGRect multipleloginframe =   _MultipleLogins.frame;
               multipleloginframe.origin.y = multipleloginframe.origin.y + 15 ;
               _MultipleLogins.frame =  multipleloginframe;
               
               _whyloginbackview.frame = CGRectMake(40, self.whyloginbackview.frame.origin.y, 290 , 275);
               _purchaseloginbackview.frame = CGRectMake(10, _purchaseloginbackview.frame.origin.y, 300 , 290);
               
               _Purchasebuyallbtn.frame = CGRectMake(_Purchasebuyallbtn.frame.origin.x, _Purchasebuyallbtn.frame.origin.y+5, _Purchasebuyallbtn.frame.size.width, _Purchasebuyallbtn.frame.size.height);
               
               CGRect imgframe=   PurchaseWhyALlimg.frame;
               imgframe.origin.x = PurchaseWhyALlimg.frame.origin.x-5 ;
               PurchaseWhyALlimg.frame =  imgframe;
               
               _twxtUserName.frame = CGRectMake(_twxtUserName.frame.origin.x, _twxtUserName.frame.origin.y, _twxtUserName.frame.size.width+50, _twxtUserName.frame.size.height);
               _txtPassword.frame = CGRectMake(_txtPassword.frame.origin.x, _txtPassword.frame.origin.y, _txtPassword.frame.size.width+50, _txtPassword.frame.size.height);
               
               CGRect loginframe=   _loginbtn.frame;
               loginframe.origin.x = _loginbtn.frame.origin.x+16 ;
               loginframe.origin.y = _loginbtn.frame.origin.y+5 ;
               _loginbtn.frame =  loginframe;
               
               
               
               CGRect mainframe = self.MainLoginview.frame;
               mainframe.origin.y = self.MainLoginview.frame.origin.y-10 ;
               self.MainLoginview.frame =  mainframe;
               
               CGRect loginfra=   _forgotpassword.frame;
               loginfra.origin.x = _forgotpassword.frame.origin.x+12 ;
               _forgotpassword.frame =  loginfra;
               
               CGRect login=   GotoFootertext.frame;
               login.origin.x = GotoFootertext.frame.origin.x-38 ;
               GotoFootertext.frame =  login;
               
               CGRect xvalue=   _purchasedetailLabel.frame;
               xvalue.origin.x = _purchasedetailLabel.frame.origin.x-20 ;
               _purchasedetailLabel.frame =  xvalue;
               
               
               CGRect dollarconst=   purchasedollar.frame;
               dollarconst.origin.x = purchasedollar.frame.origin.x-20 ;
               purchasedollar.frame =  dollarconst;
               
               
               CGRect Premiumconst=   PremiumLabel.frame;
               Premiumconst.origin.x = PremiumLabel.frame.origin.x-20 ;
               PremiumLabel.frame =  Premiumconst;
               
               CGRect PurchaseWhyALlimgconst=   PurchaseWhyALlimg.frame;
               PurchaseWhyALlimgconst.origin.x = PurchaseWhyALlimg.frame.origin.x+6 ;
               PurchaseWhyALlimg.frame =  PurchaseWhyALlimgconst;
               
               
               
          }
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"H_Value" withString:[NSString stringWithFormat:@"%.0f",_canvasHeight*2]];
               HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"W_Value" withString:[NSString stringWithFormat:@"%.0f",_canvasWidth*2]];
               HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"LoaderCanH" withString:[NSString stringWithFormat:@"%.0f",_canvasHeight*2]];
               HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"LoaderCanW" withString:[NSString stringWithFormat:@"%.0f",_canvasWidth*2]];
          }
          else
          {
               if (_canvasWidth == 480)
               {
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"H_Value" withString:[NSString stringWithFormat:@"%.0f",_canvasHeight*3]];
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"W_Value" withString:[NSString stringWithFormat:@"%.0f",_canvasWidth*3]];
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"LoaderCanH" withString:[NSString stringWithFormat:@"%.0f",_canvasHeight*3]];
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"LoaderCanW" withString:[NSString stringWithFormat:@"%.0f",_canvasWidth*3]];
               }
               else
               {
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"H_Value" withString:[NSString stringWithFormat:@"%.0f",_canvasHeight*3]];
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"W_Value" withString:[NSString stringWithFormat:@"%.0f",_canvasWidth*3]];
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"LoaderCanH" withString:[NSString stringWithFormat:@"%.0f",_canvasHeight*3]];
                    HtmlStr = [HtmlStr stringByReplacingOccurrencesOfString:@"LoaderCanW" withString:[NSString stringWithFormat:@"%.0f",_canvasWidth*3]];
               }
          }
          
          [___CharacterWebView loadHTMLString:HtmlStr baseURL:[NSURL fileURLWithPath: [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] bundlePath]]]];
          
          
          ___CharacterWebView.scrollView.scrollEnabled = NO;
          ___CharacterWebView.scrollView.delegate = self;
          ___CharacterWebView.mediaPlaybackRequiresUserAction = NO;
          UIScrollView *scrollView = [___CharacterWebView.subviews objectAtIndex:0];
          scrollView.delegate = self;//self must be UIScrollViewDelegate
          //--------------------------------------------------------------------//
          
     }
}
#pragma mark - UIScrollView Delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
     return nil;
}


#pragma mark - UIWebView Delegate
- (BOOL)webView:(UIWebView *)webView2 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
     @autoreleasepool {
          
          
          // Intercept custom location change, URL begins with "js-call:"
          if ([[[request URL] absoluteString] hasPrefix:@"js-call:"])
          {
               // Extract the selector name from the URL
               NSArray *components = [[[request URL] absoluteString] componentsSeparatedByString:@":"];
               //Get the function name
               NSString *function = [components objectAtIndex:1];
               //Get the parameter. Here name is NSString declared Globely
               // Call the given selector
               [self  performSelector:NSSelectorFromString(function)
                           withObject:nil
                           afterDelay:0];
               
               return NO;
          }
          
          return YES;
     }
}

- (NSString *)getSubstring:(NSString *)value betweenString:(NSString *)separator
{
     NSRange firstInstance = [value rangeOfString:separator];
     NSRange secondInstance = [[value substringFromIndex:firstInstance.location + firstInstance.length] rangeOfString:separator];
     NSRange finalRange = NSMakeRange(firstInstance.location + separator.length, secondInstance.location);
     
     return [value substringWithRange:finalRange];
}

#pragma mark - Function called from JavaScript file

-(NSString*)GetBundlePath
{
     NSString* BundlePath = [[NSBundle mainBundle] resourcePath];
     return BundlePath;
}

#pragma mark - Function to calculate size of views and controls

-(void)CalculateViewSize
{
     @autoreleasepool {
          
          __progressImage.frame = CGRectMake(__progressImage.frame.origin.x, __progressImage.frame.origin.y,0 , __progressImage.frame.size.height);
          [[__aboutBtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
          [[__termsBtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
          [[__faqBtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
          [[purchasebtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
          [[__syncBtn imageView]setContentMode: UIViewContentModeScaleAspectFit];
          [[__aboutMainBtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
          [[__termsMainBtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
          [[__faqMainBtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
          [[__syncBtn imageView]setContentMode: UIViewContentModeScaleAspectFit];
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
          {
               
               __descriptionTextView.font = [UIFont systemFontOfSize:22.0f];
               __LanguageLbl.font = [UIFont systemFontOfSize:22.0f];
               __nameLbl.font = [UIFont systemFontOfSize:22.0f];
               __fileTypeLbl.font = [UIFont systemFontOfSize:22.0f];
               __voiceTypeLBL.font = [UIFont systemFontOfSize:22.0f];
               __recordYourVoiceLbl.font = [UIFont systemFontOfSize:22.0f];
               __uptoTimeLbl.font = [UIFont systemFontOfSize:22.0f];
               
               __vokiButton.frame = CGRectMake(__vokiButton.frame.origin.x, __vokiButton.frame.origin.y, __vokiButton.frame.size.width,  __vokiButton.frame.size.height-20);
               __accessoriesButton.frame = CGRectMake(__accessoriesButton.frame.origin.x, __accessoriesButton.frame.origin.y, __accessoriesButton.frame.size.width,  __accessoriesButton.frame.size.height-20);
               __imageButton.frame = CGRectMake(__imageButton.frame.origin.x, __imageButton.frame.origin.y, __imageButton.frame.size.width,  __imageButton.frame.size.height-20);
               __voiceButton.frame = CGRectMake(__voiceButton.frame.origin.x, __voiceButton.frame.origin.y, __voiceButton.frame.size.width,  __voiceButton.frame.size.height-20);
               __colorButton.frame = CGRectMake(__colorButton.frame.origin.x, __colorButton.frame.origin.y, __colorButton.frame.size.width,  __colorButton.frame.size.height-20);
               
               _stripWidth = _stripWidth-23.0;
               
               __clothButton.frame = CGRectMake(__clothButton.frame.origin.x, __clothButton.frame.origin.y, __clothButton.frame.size.width,  __clothButton.frame.size.height-20);
               __hair_Button.frame = CGRectMake(__hair_Button.frame.origin.x, __hair_Button.frame.origin.y, __hair_Button.frame.size.width,  __hair_Button.frame.size.height-20);
               __capButton.frame = CGRectMake(__capButton.frame.origin.x, __capButton.frame.origin.y, __capButton.frame.size.width,__capButton.frame.size.height-20);
               __glassButton.frame = CGRectMake(__glassButton.frame.origin.x, __glassButton.frame.origin.y, __glassButton.frame.size.width,__glassButton.frame.size.height-20);
               __mouth_Button.frame = CGRectMake(__mouth_Button.frame.origin.x, __mouth_Button.frame.origin.y, __mouth_Button.frame.size.width,__mouth_Button.frame.size.height-20);
               __blingButton.frame = CGRectMake(__blingButton.frame.origin.x, __blingButton.frame.origin.y, __blingButton.frame.size.width,__blingButton.frame.size.height-20);
               __fHairbutton.frame = CGRectMake(__fHairbutton.frame.origin.x, __fHairbutton.frame.origin.y, __fHairbutton.frame.size.width,__fHairbutton.frame.size.height-20);
               __propButton.frame = CGRectMake(__propButton.frame.origin.x, __propButton.frame.origin.y, __propButton.frame.size.width,__propButton.frame.size.height-20);
               
               __shareButton.frame = CGRectMake(__shareButton.frame.origin.x, __shareButton.frame.origin.y, __shareButton.frame.size.width,__shareButton.frame.size.height-20);
               _getImageBtn.frame = CGRectMake(_getImageBtn.frame.origin.x, _getImageBtn.frame.origin.y, _getImageBtn.frame.size.width,_getImageBtn.frame.size.height-20);
               __saveToLibBtn.frame = CGRectMake(__saveToLibBtn.frame.origin.x, __saveToLibBtn.frame.origin.y, __saveToLibBtn.frame.size.width,__saveToLibBtn.frame.size.height-20);
               
               __mouthButton.frame = CGRectMake(__mouthButton.frame.origin.x, __mouthButton.frame.origin.y, __mouthButton.frame.size.width,__mouthButton.frame.size.height-20);
               __eyeButton.frame = CGRectMake(__eyeButton.frame.origin.x, __eyeButton.frame.origin.y, __eyeButton.frame.size.width,__eyeButton.frame.size.height-20);
               __hairButton.frame = CGRectMake(__hairButton.frame.origin.x, __hairButton.frame.origin.y, __hairButton.frame.size.width,__hairButton.frame.size.height-20);
               __handButton.frame = CGRectMake(__handButton.frame.origin.x, __handButton.frame.origin.y, __handButton.frame.size.width,__handButton.frame.size.height-20);
               
               __typeButton.frame = CGRectMake(__typeButton.frame.origin.x, __typeButton.frame.origin.y, __typeButton.frame.size.width,__typeButton.frame.size.height-20);
               _recordTypeBtn.frame = CGRectMake(_recordTypeBtn.frame.origin.x, _recordTypeBtn.frame.origin.y, _recordTypeBtn.frame.size.width,_recordTypeBtn.frame.size.height-20);
               __miceButton.frame = CGRectMake(__miceButton.frame.origin.x, __miceButton.frame.origin.y, __miceButton.frame.size.width,__miceButton.frame.size.height-20);
               _ttsMiceBtn.frame = CGRectMake(_ttsMiceBtn.frame.origin.x, _ttsMiceBtn.frame.origin.y, _ttsMiceBtn.frame.size.width,_ttsMiceBtn.frame.size.height-20);
               __recordButton.frame = CGRectMake(__recordButton.frame.origin.x, __recordButton.frame.origin.y, __recordButton.frame.size.width,__recordButton.frame.size.height-35);
               
               _deleteGalleryBtn.frame = CGRectMake(_CloeseThirdStripBtn.frame.origin.x , self.view.frame.size.height - _deleteGalleryBtn.frame.size.width-30, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height-50);
               
               __facebookBtn.frame = CGRectMake(__facebookBtn.frame.origin.x, __facebookBtn.frame.origin.y+10, __facebookBtn.frame.size.width,__facebookBtn.frame.size.height-30);
               __twitterBtn.frame = CGRectMake(__twitterBtn.frame.origin.x, __twitterBtn.frame.origin.y+7, __twitterBtn.frame.size.width,__twitterBtn.frame.size.height-30);
               __googleBtn.frame = CGRectMake(__googleBtn.frame.origin.x, __googleBtn.frame.origin.y-25, __googleBtn.frame.size.width,__googleBtn.frame.size.height-30);
               __mailBtn.frame = CGRectMake(__mailBtn.frame.origin.x, __mailBtn.frame.origin.y-10, __mailBtn.frame.size.width,__mailBtn.frame.size.height-30);
               
               _Whatsbtn.frame = CGRectMake(_Whatsbtn.frame.origin.x, _Whatsbtn.frame.origin.y-10, _Whatsbtn.frame.size.width,_Whatsbtn.frame.size.height-30);
               _video_downloadbtn.frame = CGRectMake(_video_downloadbtn.frame.origin.x, _video_downloadbtn.frame.origin.y-25, _video_downloadbtn.frame.size.width,_video_downloadbtn.frame.size.height-30);
               
               __zeroLbl.font = [UIFont systemFontOfSize:22.0f];
               __halfTimeLbl.font = [UIFont systemFontOfSize:22.0f];
               __timeOverLbl.font = [UIFont systemFontOfSize:22.0f];
               
               [[__aboutBtn imageView] setContentMode: UIViewContentModeScaleToFill];
               [[__termsBtn imageView] setContentMode: UIViewContentModeScaleToFill];
               [[__faqBtn imageView] setContentMode: UIViewContentModeScaleToFill];
               [[purchasebtn imageView]setContentMode:UIViewContentModeScaleToFill];
               [[__aboutMainBtn imageView] setContentMode: UIViewContentModeScaleToFill];
               [[__termsMainBtn imageView] setContentMode: UIViewContentModeScaleToFill];
               [[__faqMainBtn imageView] setContentMode: UIViewContentModeScaleToFill];
               [[__syncBtn imageView]setContentMode: UIViewContentModeScaleToFill];
               [[__closerightMenuNewBtn imageView]setContentMode:UIViewContentModeScaleAspectFit];
               [[__closeRightViewBtn imageView]setContentMode:UIViewContentModeScaleAspectFit];
               
               __closerightMenuNewBtn.frame = CGRectMake(__closerightMenuNewBtn.frame.origin.x, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width+20,__closerightMenuNewBtn.frame.size.height);
               __closeRightViewBtn.frame = CGRectMake(__closeRightViewBtn.frame.origin.x, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width+20,__closeRightViewBtn.frame.size.height);
               
          }
          
          [self ChangeViewRadius];
     }
     
}

#pragma mark - Function to change corner radius of views and controls

-(void)ChangeViewRadius
{
     @autoreleasepool {
          
          
          __ActivityView.layer.cornerRadius = 5;
          __ActivityView.clipsToBounds = YES;
          __ActivityView.layer.borderColor = [UIColor whiteColor].CGColor;
          __ActivityView.layer.borderWidth = 2;
          
          __textVoiceSubmitBtn.layer.cornerRadius = 5;
          __textVoiceSubmitBtn.clipsToBounds = YES;
          __textVoiceSubmitBtn.layer.borderColor = [UIColor whiteColor].CGColor;
          __textVoiceSubmitBtn.layer.borderWidth = 2;
          
          __text_VoiceIDView.layer.cornerRadius = 5;
          __text_VoiceIDView.clipsToBounds = YES;
          __text_VoiceIDView.layer.borderColor = [UIColor whiteColor].CGColor;
          __text_VoiceIDView.layer.borderWidth = 2;
          
          __text_Languageview.layer.cornerRadius = 5;
          __text_Languageview.clipsToBounds = YES;
          __text_Languageview.layer.borderColor = [UIColor whiteColor].CGColor;
          __text_Languageview.layer.borderWidth = 2;
          
          _fxMenuView.layer.cornerRadius = 5;
          _fxMenuView.clipsToBounds = YES;
          _fxMenuView.layer.borderColor = [UIColor whiteColor].CGColor;
          _fxMenuView.layer.borderWidth = 2;
          
          _cancelBtnTable.layer.cornerRadius = 5;
          _cancelBtnTable.clipsToBounds = YES;
          _cancelBtnTable.layer.borderColor = [UIColor whiteColor].CGColor;
          _cancelBtnTable.layer.borderWidth = 2;
          
          __ProgressBGImage.layer.cornerRadius = 5;
          __ProgressBGImage.clipsToBounds = YES;
          __ProgressBGImage.layer.borderColor = [UIColor whiteColor].CGColor;
          __ProgressBGImage.layer.borderWidth = 2;
          
          __progressImage.layer.cornerRadius = 5;
          __progressImage.clipsToBounds = YES;
          
          __voiceDropDownImg.layer.cornerRadius = 5;
          __voiceDropDownImg.clipsToBounds = YES;
          __voiceDropDownImg.layer.borderColor = [UIColor whiteColor].CGColor;
          __voiceDropDownImg.layer.borderWidth = 2;
          
          __text_fileTypeBGImg.layer.cornerRadius = 5;
          __text_fileTypeBGImg.clipsToBounds = YES;
          __text_fileTypeBGImg.layer.borderColor = [UIColor whiteColor].CGColor;
          __text_fileTypeBGImg.layer.borderWidth = 2;
          
          __text_nameBGImg.layer.cornerRadius = 5;
          __text_nameBGImg.clipsToBounds = YES;
          __text_nameBGImg.layer.borderColor = [UIColor whiteColor].CGColor;
          __text_nameBGImg.layer.borderWidth = 2;
          
          __text_languageBGImg.layer.cornerRadius = 5;
          __text_languageBGImg.clipsToBounds = YES;
          __text_languageBGImg.layer.borderColor = [UIColor whiteColor].CGColor;
          __text_languageBGImg.layer.borderWidth = 2;
          
          __text_textViewBgImg.layer.cornerRadius = 5;
          __text_textViewBgImg.clipsToBounds = YES;
          __text_textViewBgImg.layer.borderColor = [UIColor whiteColor].CGColor;
          __text_textViewBgImg.layer.borderWidth = 2;
          
     }
}

#pragma mark - Function to set selected image of UIButtons

-(void)SetSelectedImageOfButtons
{
     @autoreleasepool {
          
          
          [_getImageBtn setBackgroundImage:[UIImage imageNamed:@"addImageOrange.png"] forState:UIControlStateSelected];
          [__mouth_Button setBackgroundImage:[UIImage imageNamed:@"btn_mouth_Sel.png"] forState:UIControlStateSelected];
          [__fHairbutton setBackgroundImage:[UIImage imageNamed:@"btn_facial_hairOrange.png"] forState:UIControlStateSelected];
          [__hair_Button setBackgroundImage:[UIImage imageNamed:@"btn_hair_Sel.png"] forState:UIControlStateSelected];
          [__vokiButton setBackgroundImage:[UIImage imageNamed:@"btn_voki_Sel.png"] forState:UIControlStateSelected];
          [__accessoriesButton setBackgroundImage:[UIImage imageNamed:@"btn_accessories_Sel.png"] forState:UIControlStateSelected];
          [__imageButton setBackgroundImage:[UIImage imageNamed:@"btn_image_Sel.png"] forState:UIControlStateSelected];
          [__colorButton setBackgroundImage:[UIImage imageNamed:@"btn_color_Sel.png"] forState:UIControlStateSelected];
          [__voiceButton setBackgroundImage:[UIImage imageNamed:@"btn_voice_Sel.png"] forState:UIControlStateSelected];
          [__shareButton setBackgroundImage:[UIImage imageNamed:@"btn_share_Sel.png"] forState:UIControlStateSelected];
          [__clothButton setBackgroundImage:[UIImage imageNamed:@"btn_clothing_Sel.png"] forState:UIControlStateSelected];
          [__capButton setBackgroundImage:[UIImage imageNamed:@"btn_hat_Sel.png"] forState:UIControlStateSelected];
          [__glassButton setBackgroundImage:[UIImage imageNamed:@"btn_glasses_Sel.png"] forState:UIControlStateSelected];
          [__blingButton setBackgroundImage:[UIImage imageNamed:@"btn_bling_Sel.png"] forState:UIControlStateSelected];
          [__propButton setBackgroundImage:[UIImage imageNamed:@"btn_props_Sel.png"] forState:UIControlStateSelected];
          [__vokiUpArrow setBackgroundImage:[UIImage imageNamed:@"btn_arrow_up_Sel.png"] forState:UIControlStateSelected];
          [__vokiDownArrow setBackgroundImage:[UIImage imageNamed:@"btn_arrow_down_Sel.png"] forState:UIControlStateSelected];
          [__MouthViewUpBtn setBackgroundImage:[UIImage imageNamed:@"btn_arrow_up_Sel.png"] forState:UIControlStateSelected];
          [__MouthViewdownBtn setBackgroundImage:[UIImage imageNamed:@"btn_arrow_down_Sel.png"] forState:UIControlStateSelected];
          [__vokiDiceBtn setBackgroundImage:[UIImage imageNamed:@"btn_randomize_Sel.png"] forState:UIControlStateSelected];
          [__thirdUpArrow setBackgroundImage:[UIImage imageNamed:@"btn_arrow_up_Sel.png"] forState:UIControlStateSelected];
          [Groupup setBackgroundImage:[UIImage imageNamed:@"btn_arrow_up_Sel.png"] forState:UIControlStateSelected];
          [Groupdown setBackgroundImage:[UIImage imageNamed:@"btn_arrow_down_Sel.png"] forState:UIControlStateSelected];
          [__thirdDownArrow setBackgroundImage:[UIImage imageNamed:@"btn_arrow_down_Sel.png"] forState:UIControlStateSelected];
          [__ImageUpArrow setBackgroundImage:[UIImage imageNamed:@"btn_arrow_up_Sel.png"] forState:UIControlStateSelected];
          [__ImageDownArrow setBackgroundImage:[UIImage imageNamed:@"btn_arrow_down_Sel.png"] forState:UIControlStateSelected];
          [__shareUpArrowBtn setBackgroundImage:[UIImage imageNamed:@"btn_arrow_up_Sel.png"] forState:UIControlStateSelected];
          [__shareDownArrowBtn setBackgroundImage:[UIImage imageNamed:@"btn_arrow_down_Sel.png"] forState:UIControlStateSelected];
          [__mouthButton setBackgroundImage:[UIImage imageNamed:@"btn_mouth_Sel.png"] forState:UIControlStateSelected];
          [__eyeButton setBackgroundImage:[UIImage imageNamed:@"btn_eye_Sel.png"] forState:UIControlStateSelected];
          [__hairButton setBackgroundImage:[UIImage imageNamed:@"btn_hair_Sel.png"] forState:UIControlStateSelected];
          [__typeButton setBackgroundImage:[UIImage imageNamed:@"btn_voive_TTS_Sel.png"] forState:UIControlStateSelected];
          [__miceButton setBackgroundImage:[UIImage imageNamed:@"btn_voive_mic_Sel.png"] forState:UIControlStateSelected];
          [__aboutBtn setImage:[UIImage imageNamed:@"about_Sel.png"] forState:UIControlStateSelected];
          [__syncBtn setBackgroundImage:[UIImage imageNamed:@"Accountunsel.png"] forState:UIControlStateSelected];
          [__termsBtn setImage:[UIImage imageNamed:@"terms_Sel.png"] forState:UIControlStateSelected];
          [__faqBtn setImage:[UIImage imageNamed:@"faq_unSel.png"] forState:UIControlStateSelected];
          [purchasebtn setImage:[UIImage imageNamed:@"Purchase_sel.png"] forState:UIControlStateSelected];
          [__aboutMainBtn setImage:[UIImage imageNamed:@"about_Sel.png"] forState:UIControlStateSelected];
          [__syncMainBtn setImage:[UIImage imageNamed:@"Account.png"] forState:UIControlStateSelected];
          [__termsMainBtn setImage:[UIImage imageNamed:@"terms_Sel.png"] forState:UIControlStateSelected];
          [__faqMainBtn setImage:[UIImage imageNamed:@"faq_Sel.png"] forState:UIControlStateSelected];
          [__facebookBtn setBackgroundImage:[UIImage imageNamed:@"fb_Sel.png"] forState:UIControlStateSelected];
          [self.Whatsbtn setBackgroundImage:[UIImage imageNamed:@"whatsapp_active.png"] forState:UIControlStateSelected];
          [ _video_downloadbtn setBackgroundImage:[UIImage imageNamed:@"d_active.png"] forState:UIControlStateSelected];
          
          [__twitterBtn setBackgroundImage:[UIImage imageNamed:@"tw_Sel.png"] forState:UIControlStateSelected];
          [__googleBtn setBackgroundImage:[UIImage imageNamed:@"google_Sel.png"] forState:UIControlStateSelected];
          [__wBtn setBackgroundImage:[UIImage imageNamed:@"w_sel.png"] forState:UIControlStateSelected];
          [__blogBtn setBackgroundImage:[UIImage imageNamed:@"blogger_Sel.png"] forState:UIControlStateSelected];
          [__mailBtn setBackgroundImage:[UIImage imageNamed:@"email_Sel.png"] forState:UIControlStateSelected];
          [__saveToLibBtn setBackgroundImage:[UIImage imageNamed:@"book.png"] forState:UIControlStateSelected];
     }
}

-(void)viewDidLayoutSubviews
{
     
}



- (void)didReceiveMemoryWarning
{
     
     [super didReceiveMemoryWarning];
     
     memorywarningcount = [[NSUserDefaults standardUserDefaults] integerForKey:@"memorywarningcount"];
     for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
          [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
          
     }
     memorywarningcount ++;
     
     
}
- (NSString*) javascriptOnLoadCompletionHandler
{
     return nil;
}

- (BOOL) javascriptPageLoaded:(UIWebView*)browser {
     
     NSString* page_status;
     return [page_status isEqualToString:@"loaded"];
}




- (void)webViewDidFinishLoad:(UIWebView *)webView
{
     
     [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
}

#pragma mark - Hiding all the views from main screen

-(void)HideAllViews
{
     
     isvoicepanopen = false;
     issecopen = false;
     [_pangestureMovetoChar setEnabled:YES];
     __vokiButton.userInteractionEnabled = YES;
     __accessoriesButton.userInteractionEnabled = YES;
     __shareButton.userInteractionEnabled = YES;
     __imageButton.userInteractionEnabled = YES;
     __colorButton.userInteractionEnabled = YES;
     __voiceButton.userInteractionEnabled = YES;
     [UIView animateWithDuration:0.5 animations:^
      {
           
           
           allcatagoryview.hidden = true;
           //           playCharacterAud1.hidden = false;
           //            playCharacterAud1.alpha = 1;
           _CloseSecondStripBtn.hidden = false;
           [Appbtn setSelected:NO];
           __vokiDiceBtn.alpha = 0;
           _VokiDiceImage.alpha = 0;
           __ImageView.alpha = 0;
           __colorPickerView.alpha = 0;
           
           __voiceTextView.alpha = 0;
           __recordVoiceView.alpha = 0;
           __closePickerView.alpha = 0;
           __clothingView.alpha = 0;
           __vokiView.alpha = 0;
           _CloseSecondStripBtn.alpha = 0;
           __vokiView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y ,  __vokiView.frame.size.width,  __vokiView.frame.size.height);
           __clothingView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y ,  __clothingView.frame.size.width,  __clothingView.frame.size.height);
           
           __ImageView.frame = CGRectMake(self.view.frame.origin.x,__ImageView.frame.origin.y ,__ImageView.frame.size.width,  __ImageView.frame.size.height);
           __colorPickerView.frame = CGRectMake(self.view.frame.origin.x,__colorPickerView.frame.origin.y ,__colorPickerView.frame.size.width,  __colorPickerView.frame.size.height);
           
           
           __voiceTextView.frame = CGRectMake(self.view.frame.origin.x,__voiceTextView.frame.origin.y ,__voiceTextView.frame.size.width,  __voiceTextView.frame.size.height);
           
           __recordVoiceView.frame = CGRectMake(self.view.frame.origin.x,__recordVoiceView.frame.origin.y ,__recordVoiceView.frame.size.width,  __recordVoiceView.frame.size.height);
           
           
      }];
     self.Purchaseview.hidden=true;
     _purchasedallview.hidden = true;
     _CloseSecondStripBtn.hidden=true;
     __closePickerView.hidden=true;
     __closePickerView.frame = CGRectMake(self.view.frame.origin.x,__closePickerView.frame.origin.y ,__closePickerView.frame.size.width,  __closePickerView.frame.size.height);
     
     _CloseSecondStripBtn.frame = CGRectMake( __mainFirstStrip.frame.origin.x +  __mainFirstStrip.frame.size.width,_CloseSecondStripBtn.frame.origin.y ,_CloseSecondStripBtn.frame.size.width,  _CloseSecondStripBtn.frame.size.height);
     __vokiDiceBtn.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 0);
     thirdView = true;
     __aboutView.hidden=true;
     __syncView.hidden=true;
     __termsView.hidden=true;
     __faqView.hidden=true;
     self.Accountview.hidden = true;
     __menuDescriptionView.hidden=true;
     __rightShareBtn.hidden = false;
     _RightVokiLogoView.hidden=true;
     _playBtn.hidden=true;
     _stopBtn.hidden=true;
     [self CharacterMove];
     [_playBtn setBackgroundImage:[UIImage imageNamed:@"playUnsel.png"] forState:UIControlStateNormal];
     [_stopBtn setBackgroundImage:[UIImage imageNamed:@"playSel.png"] forState:UIControlStateNormal];
     
     _isAccessoriesSelected = NO;
     __shareView.hidden = true;
     _deleteGalleryBtn.hidden = true;
     __vokiDiceBtn.hidden = true;
     _VokiDiceImage.hidden = true;
     [__rightShareBtn setEnabled:YES];
     [__rightVokiBtn setEnabled:YES];
     hideallview = true;
     [self.view endEditing:YES];
     [self animateThirdView];
}



-(void)hiderightview
{
     isvoicepanopen = false;
     issecopen = false;
     [_pangestureMovetoChar setEnabled:YES];
     __vokiButton.userInteractionEnabled = YES;
     __accessoriesButton.userInteractionEnabled = YES;
     __shareButton.userInteractionEnabled = YES;
     __imageButton.userInteractionEnabled = YES;
     __colorButton.userInteractionEnabled = YES;
     __voiceButton.userInteractionEnabled = YES;
     [UIView animateWithDuration:0.5 animations:^
      {
           allcatagoryview.hidden = true;
           playCharacterAud1.hidden = false;
           playCharacterAud1.alpha = 1;
           _CloseSecondStripBtn.hidden = false;
           [Appbtn setSelected:NO];
           __vokiDiceBtn.alpha = 0;
           _VokiDiceImage.alpha = 0;
           __ImageView.alpha = 0;
           __colorPickerView.alpha = 0;
           
           __voiceTextView.alpha = 0;
           __recordVoiceView.alpha = 0;
           __closePickerView.alpha = 0;
           __clothingView.alpha = 0;
           __vokiView.alpha = 0;
           _CloseSecondStripBtn.alpha = 0;
           __vokiView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y ,  __vokiView.frame.size.width,  __vokiView.frame.size.height);
           __clothingView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y ,  __clothingView.frame.size.width,  __clothingView.frame.size.height);
           
           __ImageView.frame = CGRectMake(self.view.frame.origin.x,__ImageView.frame.origin.y ,__ImageView.frame.size.width,  __ImageView.frame.size.height);
           __colorPickerView.frame = CGRectMake(self.view.frame.origin.x,__colorPickerView.frame.origin.y ,__colorPickerView.frame.size.width,  __colorPickerView.frame.size.height);
           
           
           __voiceTextView.frame = CGRectMake(self.view.frame.origin.x,__voiceTextView.frame.origin.y ,__voiceTextView.frame.size.width,  __voiceTextView.frame.size.height);
           
           __recordVoiceView.frame = CGRectMake(self.view.frame.origin.x,__recordVoiceView.frame.origin.y ,__recordVoiceView.frame.size.width,  __recordVoiceView.frame.size.height);
           
           
      }];
     self.Purchaseview.hidden=true;
     _purchasedallview.hidden = true;
     _CloseSecondStripBtn.hidden=true;
     __closePickerView.hidden=true;
     __closePickerView.frame = CGRectMake(self.view.frame.origin.x,__closePickerView.frame.origin.y ,__closePickerView.frame.size.width,  __closePickerView.frame.size.height);
     
     _CloseSecondStripBtn.frame = CGRectMake( __mainFirstStrip.frame.origin.x +  __mainFirstStrip.frame.size.width,_CloseSecondStripBtn.frame.origin.y ,_CloseSecondStripBtn.frame.size.width,  _CloseSecondStripBtn.frame.size.height);
     __vokiDiceBtn.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 0);
     thirdView = true;
     __aboutView.hidden=true;
     __syncView.hidden=true;
     __termsView.hidden=true;
     __faqView.hidden=true;
     self.Accountview.hidden = true;
     __menuDescriptionView.hidden=true;
     __rightShareBtn.hidden = false;
     _RightVokiLogoView.hidden=true;
     _playBtn.hidden=true;
     _stopBtn.hidden=true;
     [_playBtn setBackgroundImage:[UIImage imageNamed:@"playUnsel.png"] forState:UIControlStateNormal];
     [_stopBtn setBackgroundImage:[UIImage imageNamed:@"playSel.png"] forState:UIControlStateNormal];
     _isAccessoriesSelected = NO;
     __shareView.hidden = true;
     _deleteGalleryBtn.hidden = true;
     __vokiDiceBtn.hidden = true;
     _VokiDiceImage.hidden = true;
     [__rightShareBtn setEnabled:YES];
     [__rightVokiBtn setEnabled:YES];
     hideallview = true;
     [self.view endEditing:YES];
     [self animateThirdView];
}





#pragma mark - Disabling button selections
-(void)DisableButtonSelection
{
     if(!_isAccessoriesSelected)
     {
          [__clothButton setSelected:NO];
          [__capButton setSelected:NO];
          [__glassButton setSelected:NO];
          [__blingButton setSelected:NO];
          [__propButton setSelected:NO];
          [__hair_Button setSelected:NO];
          [__fHairbutton setSelected:NO];
          [__mouth_Button setSelected:NO];
          [__MouthViewUpBtn setSelected:NO];
          [__MouthViewdownBtn setSelected:NO];
          
     }
     
     [__vokiUpArrow setSelected:NO];
     [__vokiDownArrow setSelected:NO];
     [__shareButton setSelected:NO];
     [__thirdUpArrow setSelected:NO];
     [__thirdDownArrow setSelected:NO];
     [__ImageUpArrow setSelected:NO];
     [__ImageDownArrow setSelected:NO];
     [__typeButton setSelected:NO];
     [__miceButton setSelected:NO];
     [__shareUpArrowBtn setSelected:NO];
     [__shareDownArrowBtn setSelected:NO];
     [__facebookBtn setSelected:NO];
     [self.Whatsbtn setSelected:NO];
     [__twitterBtn setSelected:NO];
     [self.video_downloadbtn setSelected:NO];
     [__googleBtn setSelected:NO];
     [__wBtn setSelected:NO];
     [__blogBtn setSelected:NO];
     [__mailBtn setSelected:NO];
     [__aboutBtn setSelected:NO];
     [__syncBtn setSelected:NO];
     [__termsBtn setSelected:NO];
     [__faqBtn setSelected:NO];
     [purchasebtn setSelected: NO];
     [__aboutMainBtn setSelected:NO];
     [__syncMainBtn setSelected:NO];
     [__termsMainBtn setSelected:NO];
     [__faqMainBtn setSelected:NO];
     [__saveToLibBtn setSelected:NO];
     [_getImageBtn setSelected:NO];
     
}

-(void)DisableMainButtons
{
     [__vokiButton setSelected:NO];
     [__accessoriesButton setSelected:NO];
     [__imageButton setSelected:NO];
     [__colorButton setSelected:NO];
     [__voiceButton setSelected:NO];
     [__shareButton setSelected:NO];
}

#pragma mark - Hiding views from main screen


- (IBAction)MainBgAction:(id)sender
{
     [self HideAllViews];
     [self DisableButtonSelection];
     [self DisableMainButtons];
     
     if(_isMainButtonCalled)
     {
          _isMainButtonCalled = NO;
          
     }
     else
     {
          _isMainButtonCalled = YES;
          
     }
}
- (IBAction)FirstStripCloseAction:(id)sender
{
     
}

- (IBAction)PlayCharacterAudio:(id)sender
{
     SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",[[NSUserDefaults standardUserDefaults]valueForKey:@"SOUND_LINK"]];
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
}

- (void)PlayCharacterAudio1dup:(UIButton*)sender{
     
     @autoreleasepool {
         

          
          if (isrecording == false)
          {
               [self loaderposition];
               UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
               UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
               [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
               [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3()"];
               
               if (sender.selected == YES)
               {
                    [sender setSelected:NO];
                    NSString *isenter = [[NSUserDefaults standardUserDefaults]
                                         stringForKey:@"backgroundenter"];
                    if ([isenter  isEqual: @"yes"])
                    {
                         
                         if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                         {
                              sameaudio = false;
                              Main_videoPath = @"";
                              ishalfrecording = true;
                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(1)"];
                              
                              
                         }
                         else
                         {
                              
                              sameaudio = false;
                              Main_videoPath = @"";
                              ishalfrecording = true;
                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(0)"];
                              
                              
                              
                         }
                    }
                    else
                    {
                         sameaudio = false;
                         Main_videoPath = @"";
                         ishalfrecording = true;
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(0)"];
                         
                    }
               }
               else
               {
                    recording = true;
                    
                    if([__descriptionTextView.text isEqualToString:@"Enter message"]||[__descriptionTextView.text isEqualToString:@""])
                    {
                         
                         NSString *TTS = [[NSUserDefaults standardUserDefaults]valueForKey:@"soundTTS"];
                         if ([TTS containsString:@"http://vhss"])
                         {
                              SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",TTS];
                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
                              if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
                              {
                                   
                                   UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                   [alert show];
                              }
                              
                              
                              
                              
                         }
                         else if (TTS != nil)
                         {
                              if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
                              {
                                   
                                   UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                   [alert show];
                              }
                              
                              
                              SoundLink = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID] :[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :TTS :@"mp3" :@"" :@"" :@"5603124" :@"" :@"evt6bu4n!sDk"];
                              NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink];
                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
                              
                         }
                         else
                         {
                              
                              BOOL str =    [[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunched"];
                              
                              if (str == YES)
                              {
                                   if (_isRightShareOpen != true)
                                   {
                                        saveSceneUrl = nil;
                                        sameaudio = false;
                                        Main_videoPath = @"";
                                        outurltweet = nil;
                                   }
                                   
                                   issavescean = true;
                                   NSMutableArray *femaleAudio = [[NSMutableArray alloc]initWithObjects:@"F-01.mp3",@"F-02.mp3",@"F-03.mp3",@"F-04.mp3",@"F-05.mp3",@"F-06.mp3",@"F-07.mp3",@"F-08.mp3",@"F-09.mp3",@"F-10.mp3",@"F-11.mp3",@"F-12.mp3",@"F-13.mp3",@"F-14.mp3",@"F-15.mp3",@"F-16.mp3",@"F-17.mp3",@"F-18.mp3",@"F-19.mp3",@"F-20.mp3",@"F-21.mp3",@"F-22.mp3",@"F-23.mp3",@"F-24.mp3",@"F-25.mp3",@"F-26.mp3",@"F-27.mp3",@"F-28.mp3",@"F-29.mp3",@"F-30.mp3",@"F-31.mp3", nil];
                                   
                                   NSMutableArray *maleAudio = [[NSMutableArray alloc]initWithObjects:@"M-01.mp3",@"M-02.mp3",@"M-03.mp3",@"M-04.mp3",@"M-05.mp3",@"M-06.mp3",@"M-07.mp3",@"M-08.mp3",@"M-09.mp3",@"M-10.mp3",@"M-11.mp3",@"M-12.mp3",@"M-13.mp3",@"M-14.mp3",@"M-15.mp3",@"M-16.mp3",@"M-17.mp3",@"M-18.mp3",@"M-19.mp3",@"M-20.mp3",@"M-21.mp3",@"M-22.mp3",@"M-23.mp3",@"M-24.mp3",@"M-25.mp3",@"M-26.mp3",@"M-27.mp3",@"M-28.mp3",@"M-29.mp3",@"M-30.mp3",@"M-31.mp3", nil];
                                   
                                   NSMutableArray *OtherAudio = [[NSMutableArray alloc]initWithObjects:@"O-01.mp3",@"O-02.mp3",@"O-03.mp3",@"O-04.mp3",@"O-05.mp3",@"O-06.mp3",@"O-07.mp3",@"O-08.mp3",@"O-09.mp3",@"O-10.mp3",@"O-11.mp3",@"O-12.mp3",@"O-13.mp3",@"O-14.mp3",@"O-15.mp3",@"O-16.mp3",@"O-17.mp3",@"O-18.mp3",@"O-19.mp3",@"O-20.mp3",@"O-21.mp3",@"O-22.mp3",@"O-23.mp3",@"O-24.mp3",@"O-25.mp3",@"O-26.mp3",@"O-27.mp3",@"O-28.mp3",@"O-29.mp3",@"O-30.mp3",@"O-31.mp3", nil];
                                   BOOL str =    [[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunched"];
                                   if (str == YES)
                                   {
                                        if ([genderID isEqualToString:@"1"])
                                        {
                                             
                                             NSInteger l = arc4random_uniform(femaleAudio.count);
                                             NSString *sound = [femaleAudio objectAtIndex:l]; //temp-
                                             [self setsound:sound];
                                             
                                        }
                                        else if ([genderID isEqualToString:@"2"])
                                        {
                                             NSInteger r = arc4random_uniform(maleAudio.count);
                                             NSString *sound = [maleAudio objectAtIndex:r];
                                             [self setsound:sound];
                                             
                                        }
                                        else
                                        {
                                             NSInteger k = arc4random_uniform(OtherAudio.count);
                                             NSString *sound = [OtherAudio objectAtIndex:k];
                                             [self setsound:sound];
                                             
                                        }
                                   }
                              }
                              
                              else
                              {
                                   UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please run app again with working Internet to use This Functionality" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                   [alert show];
                              }
                         }
                         
                    }
                    else
                    {
                         isVoiceTableVisible = NO;
                         isFxTableVisible = NO;
                         __text_VoiceIDView.hidden=true;
                         isTextLangTableVisible = NO;
                         __text_Languageview.hidden=true;
                         _fxMenuView.hidden = true;
                         SoundLink = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID]:[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :__descriptionTextView.text :@"mp3" :fxtype :fxlevel :@"5603124" :@"" :@"evt6bu4n!sDk"];
                         
                         
                         [[NSUserDefaults standardUserDefaults]setValue:__descriptionTextView.text forKey:@"soundTTS"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
                         
                    }
               }
               
          }
          
     }
}


- (void)PlayCharacterAudio1:(UIButton*)sender
{
     @autoreleasepool {
          
          
          [self loaderposition];
          UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
          UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
          [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
          [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3()"];
          
          if (sender.selected == YES)
          {
               [sender setSelected:NO];
               NSString *isenter = [[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"backgroundenter"];
               if ([isenter  isEqual: @"yes"])
               {
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                         sameaudio = false;
                         Main_videoPath = @"";
                         ishalfrecording = true;
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(1)"];
                         
                         
                    }
                    else
                    {
                         sameaudio = false;
                         Main_videoPath = @"";
                         ishalfrecording = true;
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(0)"];
                         
                    }
               }
               else
               {
                    sameaudio = false;
                    Main_videoPath = @"";
                    ishalfrecording = true;
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3(0)"];
                    
               }
          }
          else
          {
               recording = true;
               
               
               if([__descriptionTextView.text isEqualToString:@"Enter message"]||[__descriptionTextView.text isEqualToString:@""])
               {
                    
                    NSString *TTS = [[NSUserDefaults standardUserDefaults]valueForKey:@"soundTTS"];
                    if ([TTS containsString:@"http://vhss"])
                    {
                         SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",TTS];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
                         
                         
                    }
                    else if (TTS != nil)
                    {
                         
                         SoundLink = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID] :[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :TTS :@"mp3" :@"" :@"" :@"5603124" :@"" :@"evt6bu4n!sDk"];
                         NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
                         
                    }
                    else
                    {
                         if (_isRightShareOpen != true)
                         {
                              sameaudio = false;
                              saveSceneUrl = nil;
                              Main_videoPath = @"";
                              outurltweet = nil;
                         }
                         
                         
                         NSMutableArray *femaleAudio = [[NSMutableArray alloc]initWithObjects:@"F-01.mp3",@"F-02.mp3",@"F-03.mp3",@"F-04.mp3",@"F-05.mp3",@"F-06.mp3",@"F-07.mp3",@"F-08.mp3",@"F-09.mp3",@"F-10.mp3",@"F-11.mp3",@"F-12.mp3",@"F-13.mp3",@"F-14.mp3",@"F-15.mp3",@"F-16.mp3",@"F-17.mp3",@"F-18.mp3",@"F-19.mp3",@"F-20.mp3",@"F-21.mp3",@"F-22.mp3",@"F-23.mp3",@"F-24.mp3",@"F-25.mp3",@"F-26.mp3",@"F-27.mp3",@"F-28.mp3",@"F-29.mp3",@"F-30.mp3",@"F-31.mp3", nil];
                         
                         NSMutableArray *maleAudio = [[NSMutableArray alloc]initWithObjects:@"M-01.mp3",@"M-02.mp3",@"M-03.mp3",@"M-04.mp3",@"M-05.mp3",@"M-06.mp3",@"M-07.mp3",@"M-08.mp3",@"M-09.mp3",@"M-10.mp3",@"M-11.mp3",@"M-12.mp3",@"M-13.mp3",@"M-14.mp3",@"M-15.mp3",@"M-16.mp3",@"M-17.mp3",@"M-18.mp3",@"M-19.mp3",@"M-20.mp3",@"M-21.mp3",@"M-22.mp3",@"M-23.mp3",@"M-24.mp3",@"M-25.mp3",@"M-26.mp3",@"M-27.mp3",@"M-28.mp3",@"M-29.mp3",@"M-30.mp3",@"M-31.mp3", nil];
                         
                         NSMutableArray *OtherAudio = [[NSMutableArray alloc]initWithObjects:@"O-01.mp3",@"O-02.mp3",@"O-03.mp3",@"O-04.mp3",@"O-05.mp3",@"O-06.mp3",@"O-07.mp3",@"O-08.mp3",@"O-09.mp3",@"O-10.mp3",@"O-11.mp3",@"O-12.mp3",@"O-13.mp3",@"O-14.mp3",@"O-15.mp3",@"O-16.mp3",@"O-17.mp3",@"O-18.mp3",@"O-19.mp3",@"O-20.mp3",@"O-21.mp3",@"O-22.mp3",@"O-23.mp3",@"O-24.mp3",@"O-25.mp3",@"O-26.mp3",@"O-27.mp3",@"O-28.mp3",@"O-29.mp3",@"O-30.mp3",@"O-31.mp3", nil];
                         
                         if ([genderID isEqualToString:@"1"])
                         {
                              NSInteger l = arc4random_uniform(femaleAudio.count);
                              NSString *sound = [femaleAudio objectAtIndex:l]; //temp-
                              [[NSUserDefaults standardUserDefaults] setObject:sound forKey:@"defaultsound"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              [self setsound:sound];
                              
                         }
                         else if ([genderID isEqualToString:@"2"])
                         {
                              NSInteger r = arc4random_uniform(maleAudio.count);
                              NSString *sound = [maleAudio objectAtIndex:r];
                              [[NSUserDefaults standardUserDefaults] setObject:sound forKey:@"defaultsound"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              [self setsound:sound];
                              
                         }
                         else
                         {
                              NSInteger k = arc4random_uniform(OtherAudio.count);
                              NSString *sound = [OtherAudio objectAtIndex:k];
                              [[NSUserDefaults standardUserDefaults] setObject:sound forKey:@"defaultsound"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              [self setsound:sound];
                              
                         }
                    }
               }
               else
               {
                    isVoiceTableVisible = NO;
                    isFxTableVisible = NO;
                    __text_VoiceIDView.hidden=true;
                    isTextLangTableVisible = NO;
                    __text_Languageview.hidden=true;
                    _fxMenuView.hidden = true;
                    SoundLink = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID]:[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :__descriptionTextView.text :@"mp3" :fxtype :fxlevel :@"5603124" :@"" :@"evt6bu4n!sDk"];
                    [[NSUserDefaults standardUserDefaults]setValue:__descriptionTextView.text forKey:@"soundTTS"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
                    
                    
               }
          }
          
     }
}
-(void)setsound:(NSString *)sound
{
     
     [audioActivity stopAnimating];
     issavescean = true;
     
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          
          __rightShareBtn.enabled = true;
          
          if (sound !=nil)
          {
               
               
               self.__CharacterWebView.mediaPlaybackRequiresUserAction = NO;
               self.__CharacterWebView.allowsInlineMediaPlayback = YES;
               
               SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",sound];
               
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
          }
          
          
          [self.view setUserInteractionEnabled:YES];
          
          
          
          
          
     }
     else
     {
          
          audioActivity= [[UIActivityIndicatorView alloc]
                          initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
          audioActivity.center=self.view.center;
          audioActivity.frame = CGRectMake(playCharacterAud1.frame.origin.x+20,playCharacterAud1.frame.origin.y-30,35,35);
          audioActivity.color = [UIColor blackColor];
          [audioActivity startAnimating];
          [self.view addSubview:audioActivity];
          if (sound !=nil)
          {
               
               
               NSString *result;
               NSScanner *scanner = [NSScanner scannerWithString:sound];
               NSCharacterSet *cs1 = [NSCharacterSet characterSetWithCharactersInString:@".mp3"];
               
               [scanner scanUpToCharactersFromSet:cs1 intoString:&result];
               
               NSLog(@"Finally ==%@",result);
               NSString *path = [[NSBundle mainBundle] pathForResource:result ofType:@".mp3"];
               [[NSUserDefaults standardUserDefaults] setObject:path forKey:@"AudioPath"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               NSLog(@"%@",path);
               if (path!=nil)
               {
                    
                    isfirstecord = false;
                    [self audioApi:path];
               }
               else
               {
                    [audioActivity stopAnimating];
                    [self.view setUserInteractionEnabled:YES];
               }
               
          }
          
     }
     
}

#pragma mark - vokiView methods
- (IBAction)VokiAction:(id)sender
{
     if ([sender isSelected])
     {
          [self HideAllViews];
          [self DisableButtonSelection];
          [self DisableMainButtons];
     }
     else
     {
          [self moveRightMenu];
          [self HideAllViews];
          [self DisableButtonSelection];
          [self DisableMainButtons];
          
          menuViewOpenClose = YES;
          shareViewOpenClose = YES;
          [gestureRecognizerRight setEnabled:NO];
          [self moveCharacterFirstView];
          
          [UIView animateWithDuration:0.5 animations:^
           {
                __vokiDiceBtn.alpha = 1;
                __vokiDiceBtn.hidden = false;
                _VokiDiceImage.alpha = 1;
                _VokiDiceImage.hidden = false;
                __vokiView.alpha = 1;
                _CloseSecondStripBtn.alpha = 1;
                __vokiView.hidden=false;
                _CloseSecondStripBtn.hidden=false;
                
                __vokiView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8, __vokiView.frame.origin.y, __clothingView.frame.size.width, __vokiView.frame.size.height);
                _CloseSecondStripBtn.frame = CGRectMake(__vokiView.frame.origin.x+__vokiView.frame.size.width , _CloseSecondStripBtn.frame.origin.y, _CloseSecondStripBtn.frame.size.width, _CloseSecondStripBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                [gestureRecognizer setEnabled:YES];
                
           }];
          
          [__vokiButton setSelected:YES];
          
          [self LoadVokiScroller];
     }
}

- (IBAction)VokiUpAction:(id)sender
{
     [self DisableButtonSelection];
     [__vokiUpArrow setSelected:YES];
     
     if (__vokiScroller.contentOffset.y > 0)
     {
          if (__vokiScroller.contentOffset.y < _stripWidth)
          {
               [__vokiScroller setContentOffset:CGPointMake(0, 0) animated:YES];
               
          }
          else
          {
               [__vokiScroller setContentOffset:CGPointMake(0, __vokiScroller.contentOffset.y-_stripWidth)animated:YES];
          }
     }
     else
     {
          [__vokiUpArrow setSelected:NO];
     }
}

- (IBAction)VokiDownAction:(id)sender
{
     [self DisableButtonSelection];
     [__vokiDownArrow setSelected:YES];
     
     if(__vokiScroller.contentOffset.y+__vokiScroller.frame.size.height < __vokiScroller.contentSize.height )
     {
          if(__vokiScroller.contentOffset.y+_stripWidth +__vokiScroller.frame.size.height > __vokiScroller.contentSize.height )
          {
               [__vokiScroller setContentOffset:CGPointMake(0, __vokiScroller.contentOffset.y) animated:YES];
               [__vokiDownArrow setSelected:NO];
          }
          else
          {
               [__vokiScroller setContentOffset:CGPointMake(0, __vokiScroller.contentOffset.y+_stripWidth) animated:YES];
          }
     }
}
- (IBAction)VokiDiceAction:(id)sender
{
     NSLog(@"%@",fxtype);
     NSLog(@"%@",fxlevel);
     
     if (israndomcomplete == false)
     {
          saveSceneUrl = nil;
          Main_videoPath = @"";
          sameaudio = false;
          outurltweet = nil;
          
          _VokiDiceImage.animationImages =
          [NSArray arrayWithObjects:
           [UIImage imageNamed:@"btn_randomize_anim01_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim02_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim03_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim04_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim05_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim06_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim07_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim08_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim01_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim02_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim03_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim04_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim05_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim06_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim07_128x128.png"],
           [UIImage imageNamed:@"btn_randomize_anim08_128x128.png"],
           nil];
          _VokiDiceImage.animationRepeatCount = 0; // 0 means repeat without stop.
          _VokiDiceImage.animationDuration = 1.0; // Animation duration
          
          [_VokiDiceImage startAnimating];
          israndomcomplete = true;
          
          
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.10 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                         ^{
                              
                              israndomcomplete = false;
                              
                         });
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
               
               [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
          });
          recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerCalled) userInfo:nil repeats:YES];
          
          rotateDiceCount = 0;
     }
     
}
-(void)LoadVokiScroller
{
     @autoreleasepool
     {
          
          if (allcatagoryview.hidden == false)
          {
               NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
               NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
               NSString *paths = RUNNING_CHARACTER;
               NSString *islogin = [[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"islogin"];
               
               //To wheck wheather sharing should availble or not !
               
               if ([soldcharcters containsObject:paths]|| [freecharacetr containsObject:paths])
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
                    
               }
               
               double delayInSeconds = 0.2;
               dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
               dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                    allcatagoryview.hidden = true;
                    playCharacterAud1.alpha = 1;
                    playCharacterAud1.hidden = false;
                    
               });
               
          }
          else
          {
               allcatagoryview.hidden = true;
               playCharacterAud1.alpha = 1;
               playCharacterAud1.hidden = false;
          }
          issecopen = true;
          
          
          for(UIView *subview in __vokiScroller.subviews)
          {
               [subview removeFromSuperview];
          }
          NSMutableArray* arrayOfArr = [NSMutableArray new];
          [arrayOfArr addObjectsFromArray:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char"]]];
          
          for(int i = 0; i < arrayOfArr.count; i++)
          {
               
               NSArray* Array =[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",[arrayOfArr objectAtIndex:i]]];
               
               if(Array.count == 1)
               {
                    if([[Array objectAtIndex:0] isEqualToString:@"icons"])
                    {
                         [arrayOfArr removeObjectAtIndex:i];
                    }
               }
               
          }
          
          NSArray* Array = (NSArray*)arrayOfArr;
          VokiScrollerArray = [[NSMutableArray alloc]init];
          [VokiScrollerArray addObjectsFromArray:Array];
          int x = 0;
          int y = _stripWidth;
          _previousVokiTag = 0;
          
          [__vokiScroller setContentSize:CGSizeMake(__vokiScroller.frame.size.width, _stripWidth*22)];
          Appbtn = [UIButton buttonWithType:UIButtonTypeCustom];
          [Appbtn addTarget:self action:@selector(openapppurchase) forControlEvents:UIControlEventTouchUpInside];
          [Appbtn setImage:[UIImage imageNamed:@"group_sel.png"] forState:UIControlStateSelected];
          Appbtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
          [Appbtn setImage:[UIImage imageNamed:@"group.png"] forState:UIControlStateNormal];
          Appbtn.frame = CGRectMake(0, 0, __vokiScroller.frame.size.width, _stripWidth);
          [Appbtn setSelected:NO];
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               Appbtn.frame = CGRectMake(0, 0, __vokiScroller.frame.size.width, _stripWidth);
               
          }
          else
          {
               Appbtn.frame = CGRectMake(10, 0, __vokiScroller.frame.size.width-14, _stripWidth-10);
               
          }
          UILongPressGestureRecognizer *name = [[UILongPressGestureRecognizer alloc] init];
          [name addTarget:self action:@selector(userLongPressedVokigroup:)];
          [Appbtn addGestureRecognizer:name];
          [__vokiScroller addSubview:Appbtn];
          
          
          for(int i = 0; i<[VokiScrollerArray count]; i++)
          {
               UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
               [button addTarget:self action:@selector(PickVokiImage:) forControlEvents:UIControlEventTouchUpInside];
               [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_unSel.png",[VokiScrollerArray objectAtIndex:i]]] forState:UIControlStateNormal];
               [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_Sel.png",[VokiScrollerArray objectAtIndex:i]]] forState:UIControlStateSelected];
               UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] init];
               [gr addTarget:self action:@selector(userLongPressedVokiCharacter:)];
               [button addGestureRecognizer:gr];
               
               
               __block NSString *image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"assets/img/char/%@/%@",[VokiScrollerArray objectAtIndex:i],[self FetchImageIcon:@"assets/img/char" :[VokiScrollerArray objectAtIndex:i]]] ofType:@"png" inDirectory:@""];
               
               
               __block NSString *imagePathNormal;
               if(image_Path == nil)
               {
                    [button setBackgroundImage:[UIImage imageNamed:@"btn_img_unSel"] forState:UIControlStateNormal];
               }
               else
               {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                         
                         
                         NSString *str = [NSString stringWithFormat:@"assets/img/char/%@/",[Array objectAtIndex:i]];
                         if (selectedCategory == nil || selectedCategory == [NSNull class])
                         {
                              
                         }
                         else
                         {
                              if ([str containsString:selectedCategory])
                              {
                                   imagePathNormal = image_Path;
                                   image_Path = [image_Path stringByReplacingOccurrencesOfString:@"base" withString:@"over"];
                              }
                         }
                         
                         UIImage *image = [UIImage imageWithContentsOfFile:image_Path];
                         UIImage *imageNormal = [UIImage imageWithContentsOfFile:imagePathNormal];
                         UIImage *image1  = [self resizeimage:image];
                         UIImage *imageNormal1 = [self resizeimage:imageNormal];
                         dispatch_async(dispatch_get_main_queue(), ^{
                              
                              if ([str containsString:selectedCategory])
                              {
                                   [button setBackgroundImage:image1 forState:UIControlStateSelected];
                                   [button setBackgroundImage:imageNormal1 forState:UIControlStateNormal];
                                   [button setSelected:YES];
                                   _previousVokiTag = i+555;
                                   
                                   if (i >= 3)
                                   {
                                        if ( i== 6)
                                        {
                                             [UIView animateWithDuration:0.7 animations:^{
                                                  
                                                  [__vokiScroller setContentOffset:CGPointMake(0, button.frame.origin.y + 5)];
                                                  
                                             }];
                                        }
                                        
                                        else
                                        {
                                             [UIView animateWithDuration:0.7 animations:^{
                                                  [__vokiScroller setContentOffset:CGPointMake(0, button.frame.origin.y)];
                                                  
                                             }];
                                        }
                                        
                                   }
                                   else
                                   {
                                        [UIView animateWithDuration:0.7 animations:^{
                                             [__vokiScroller setContentOffset:CGPointMake(0, 0)];
                                        }];
                                   }
                                   
                              }
                              else
                              {
                                   [button setBackgroundImage:image forState:UIControlStateNormal];
                              }
                              
                              
                         });
                    });
               }
               button.frame = CGRectMake(x, y, __vokiScroller.frame.size.width, _stripWidth);
               button.tag = i + 555;
               buttonHeightSecondScroller = button.frame.size.height;
               [__vokiScroller addSubview:button];
               y = y + _stripWidth;
          }
          
          
     }
     
}


-(UIImage *)resizeimage:(UIImage *)image
{
     CGRect rect = CGRectMake(0,0,__vokiScroller.frame.size.width,_stripWidth);
     CGFloat scale = [[UIScreen mainScreen]scale];
     UIGraphicsBeginImageContextWithOptions(rect.size, NO, scale);
     [image drawInRect:CGRectMake(0,0,rect.size.width,rect.size.height)];
     UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return  newImage;
     
}



-(IBAction)PickVokiImage:(id)sender
{
     
     @autoreleasepool {
          
          NSString *paths = RUNNING_CHARACTER;
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
          NSString *islogin = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"islogin"];
          
          //To wheck wheather sharing should availble or not !
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcters containsObject:paths]|| [freecharacetr containsObject:paths])
                    {
                         
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         
                    }
                    else
                    {
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         _sidedollar.enabled = true;
                         _Purchasedollerimg.hidden = false;
                         _sidedollar.hidden = false;
                         
                    }
                    
               }
               else
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
               }
               
          }
          
          else
          {
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
               
          }
          
          isgroupopen = false;
          [UIView animateWithDuration:0.1 animations:^{
               
               allcatagoryview.hidden = true;
               playCharacterAud1.hidden = false;
               playCharacterAud1.alpha = 1;
               _CloseSecondStripBtn.hidden = false;
               
          }];
          
          [Appbtn setSelected:NO];
          button1 = (UIButton *)[__vokiScroller viewWithTag:[sender tag]];
          
          [__shareButton setSelected:NO];
          if(_previousVokiTag != 0)
          {
               UIButton *button = (UIButton *)[__vokiScroller viewWithTag:_previousVokiTag];
               [button setSelected:NO];
          }
          
          [button1 setSelected:YES];
          NSString *image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"assets/img/char/%@/%@",[VokiScrollerArray objectAtIndex:(int)[sender tag]-555],[self FetchImageIcon:@"assets/img/char" :[VokiScrollerArray objectAtIndex:(int)[sender tag]-555]]] ofType:@"png" inDirectory:@""];
          
          NSString *replaceOver = [image_Path stringByReplacingOccurrencesOfString:@"base" withString:@"over"];
          if(image_Path == nil)
          {
               [button1 setBackgroundImage:[UIImage imageNamed:@"btn_img_unSel"] forState:UIControlStateNormal];
          }
          else
          {
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    UIImage *image = [UIImage imageWithContentsOfFile:replaceOver];
                    UIImage *image1 = [self resizeimage:image1];
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [button1 setBackgroundImage:image forState:UIControlStateSelected];
                    });
               });
          }
          _previousVokiTag = (int)[sender tag];
          [UIView animateWithDuration:0.5 animations:^
           {
                _deleteGalleryBtn.alpha =0;
                __shareView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                __vokiDiceBtn.alpha = 1;
                __vokiDiceBtn.hidden = false;
                _VokiDiceImage.alpha = 1;
                _VokiDiceImage.hidden = false;
                __thirdView.hidden=false;
                __vokiDiceBtn.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 0);
                __thirdView.frame = CGRectMake(__vokiView.frame.origin.x+__vokiView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__vokiView.frame.origin.x, __shareView.frame.origin.y, __shareView.frame.size.width, __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                
           }];
          thirdView = false;
          [self CharacterMove];
          [self CallThirdStrip];
          NSArray* Array = [self RemoveIconImage:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",[VokiScrollerArray objectAtIndex:(int)[sender tag]-555]]]];
          [[NSUserDefaults standardUserDefaults] setValue:Array forKey:@"SaveCharacterPath"];
          [[NSUserDefaults standardUserDefaults]synchronize];
          [self LoadVokiThirdScroller:[NSString stringWithFormat:@"assets/img/char/%@",[VokiScrollerArray objectAtIndex:(int)[sender tag]-555]] :Array];
          
     }
}

-(void)openapppurchase
{
     [self closethirdstrip];
     thirdView = false;
     isgroupopen = true;
     _Purchasedollerimg.alpha = 0;
     _sidedollar.alpha = 0;
     [UIView animateWithDuration:0.1 animations:^{
          allcatagoryview.hidden = false;
          playCharacterAud1.hidden = true;
          playCharacterAud1.alpha = 0;
          
     }];
     NSString *paths = RUNNING_CHARACTER;
     selecteimagpath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",paths] ofType:@"png" inDirectory:@""];
     [button1 setSelected:NO];
     [Appbtn setSelected:YES];
     allcatagoryview.frame = CGRectMake(__clothingView.frame.origin.x+2*__clothingView.frame.size.width+16, __clothingView.frame.origin.y, self.view.frame.size.width/2+40,self.view.frame.size.height);
     float X_Co = (allcatagoryview.frame.size.width - Groupdown.frame.size.width)/2;
     Groupdown.frame = CGRectMake(X_Co, Groupdown.frame.origin.y, Groupdown.frame.size.width, Groupdown.frame.size.height);
     Groupup.frame = CGRectMake(X_Co, Groupup.frame.origin.y, Groupup.frame.size.width, Groupup.frame.size.height);
     currentIndexPath = 20;
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          //          currentIndexPath = 30;                   // code commented - 21 Dec
          
     }
     [allcatacollectionview reloadData];
}

-(void)LoadVokiThirdScroller:(NSString*)path :(NSArray*)arrayOfString
{
     @autoreleasepool {
          issecopen = true;
          for(UIView *subview in __thirdContentScroller.subviews)
          {
               [subview removeFromSuperview];
          }
          int x = 0;
          int y = 0;
          _previousVokiScrollerTag = 0;
          int count = 0;
          if(arrayOfString.count == 0)
          {
               count = 1;
          }
          else
          {
               __thirdUpArrow.hidden = false;
               __thirdDownArrow.hidden = false;
               count = [arrayOfString count];
               randomArrayCount = count;
               VokiContentArray = [[NSMutableArray alloc]init];
               [VokiContentArray addObjectsFromArray:arrayOfString];
               if (count <= 5)
               {
                    __thirdUpArrow.hidden = true;
                    __thirdDownArrow.hidden = true;
               }
          }
          float buttonHeight = 0.0;
          __block int SelectCharIndex = 0;
          
          //  NSString *output_path = [NSString stringWithFormat:@"%@/VokiContent",output];
          NSString *output_path = [NSString stringWithFormat:@"%@",output];
          
          for(int i = 0; i<count; i++)
          {
               
               UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
               [button addTarget:self action:@selector(PickVokiContent:) forControlEvents:UIControlEventTouchUpInside];
               
               dollar =[[UIImageView alloc]init];
               dollar.image = [UIImage imageNamed:@"dollar.png"];
               
               if (i==0)
               {
                    dollar.hidden = YES;
               }
               
               NSString *image_Path = @"";
               if(arrayOfString.count == 0)
               {
                    
                    image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/icons/base/btn_128x128",path] ofType:@"png" inDirectory:@""];
               }
               else
               {
                    
                    NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                    
                    image_Path = [myBundle pathForResource:[NSString stringWithFormat:@"%@/%@/thumb_128x128",path,[arrayOfString objectAtIndex:i]] ofType:@"png" inDirectory:@""];
                    
                    
                    //                    image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/%@/thumb_128x128",path,[arrayOfString objectAtIndex:i]] ofType:@"png" inDirectory:@""];
                    
                    
               }
               
               if(image_Path == nil)
               {
                    [button setBackgroundImage:[UIImage imageNamed:@"btn_img_unSel"] forState:UIControlStateNormal];
               }
               
               else
               {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                         UIImage *image = [UIImage imageWithContentsOfFile:image_Path];
                         UIImage *image1 = [self resizeimage:image];
                         dispatch_async(dispatch_get_main_queue(), ^{
                              [button setBackgroundImage:image1 forState:UIControlStateNormal];
                              [button setBackgroundColor:[UIColor whiteColor]];
                              button.layer.cornerRadius = 5;
                              button.clipsToBounds= YES;
                              if ([spriteSheetSelectedChar containsString:[arrayOfString objectAtIndex:i]]) {
                                   button.layer.borderWidth = 3;
                                   button.layer.borderColor = [[UIColor orangeColor]CGColor];
                                   SelectCharIndex = i;
                              }
                              if ([spriteSheetSelectedChar containsString:[arrayOfString objectAtIndex:SelectCharIndex]])
                              {
                                   UIButton *button1 = (UIButton *)[__thirdContentScroller viewWithTag:SelectCharIndex+777];
                                   button1.layer.borderWidth = 3;
                                   button1.layer.borderColor = [[UIColor orangeColor]CGColor];
                                   
                                   if (SelectCharIndex > 4)
                                   {
                                        [UIView animateWithDuration:0.7 animations:^{
                                             [__thirdContentScroller setContentOffset:CGPointMake(0, button1.frame.origin.y-(self.view.frame.size.height/2))];
                                        }];
                                        
                                   }
                                   else
                                   {
                                        [UIView animateWithDuration:0.7 animations:^{
                                             [__thirdContentScroller setContentOffset:CGPointMake(0, 0)];
                                             
                                        }];
                                   }
                              }
                              else
                              {
                                   [__thirdContentScroller setContentOffset:CGPointMake(0, 0)];
                              }
                         });
                    });
               }
               button.frame = CGRectMake(x+8, y, __thirdContentScroller.frame.size.width-16, _stripWidth-14);
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    button.frame = CGRectMake(x+8, y, __thirdContentScroller.frame.size.width-16, _stripWidth-23);
                    dollar.frame = CGRectMake(60, y+70, 30, 30);
               }
               else
               {
                    if (_canvasWidth == 667)
                    {
                         dollar.frame = CGRectMake(35, y+35, 20, 20);
                    }
                    else if (_canvasWidth == 736)
                    {
                         dollar.frame = CGRectMake(40, y+40, 22, 22);
                         
                    }
                    else
                    {
                         dollar.frame = CGRectMake(32, y+31, 18, 18);
                    }
               }
               
               button.tag = i + 777;
               dollar.tag = button.tag;
               NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
               NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
               NSString *charasset = [NSString stringWithFormat:@"%@/%@",path,[arrayOfString objectAtIndex:i]];
               NSString *islogin = [[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"islogin"];
               
               if (![islogin  isEqual: @"yes"] )
               {
                    if (isbuyall == NO)
                    {
                         if ([soldcharcters containsObject:charasset] || [freecharacetr containsObject:charasset] )
                         {
                              
                              dollar.hidden = true;
                              
                              
                         }
                         else
                         {
                              dollar.hidden = false;
                              
                         }
                    }
                    else
                    {
                         dollar.hidden = true;
                    }
                    
               }
               
               else
               {
                    dollar.hidden = true;
               }
               
               buttonHeight = button.frame.size.height;
               UILongPressGestureRecognizer *CharacterName = [[UILongPressGestureRecognizer alloc] init];
               [CharacterName addTarget:self action:@selector(userLongPressedCharacterName:)];
               [button addGestureRecognizer:CharacterName];
               [__thirdContentScroller addSubview:button];
               [__thirdContentScroller addSubview:dollar];
               y = y + _stripWidth;
          }
          
          [__thirdContentScroller setContentSize:CGSizeMake(0, (buttonHeight+18)*count)];
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               [__thirdContentScroller setContentSize:CGSizeMake(0, (buttonHeight+10)*count)];
          }
     }
     
}

- (void)userLongPressedCharacterName:(UILongPressGestureRecognizer*)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
           }];
          UILongPressGestureRecognizer* gesture=(UILongPressGestureRecognizer*)sender;
          NSInteger i = gesture.view.tag - 777;
          NSString *Without_ = [VokiContentArray objectAtIndex:i];
          
          if ([Without_ containsString:@"Symbol"])
          {
               NSArray *Arr = [Without_ componentsSeparatedByString:@"Symbol"];
               Without_ = [Arr objectAtIndex:0];
               
          }
          else
          {
               NSArray *Arr = [Without_ componentsSeparatedByString:@"_"];
               Without_ = [Arr objectAtIndex:0];
          }
          
          
          if ([Without_ isEqualToString:@"Questionmark Symbol"])
          {
               Without_ = @"Question Mark Symbol";
          }
          else if ([Without_ isEqualToString:@"Subtraction Symbol"])
          {
               Without_ = @"Minus Symbol";
          }
          _itemName.text = Without_;
     }
}


#pragma mark - Load Desc.xml for voki
#pragma mark - call this function if using images for characters on voki tab

-(IBAction)PickVokiContent:(id)sender
{
     
     @autoreleasepool {
          for(UIView *subview in __thirdContentScroller.subviews)
          {
               if ([subview isKindOfClass:[UIButton class]])
               {
                    UIButton* myButton1 = (UIButton*)subview;
                    myButton1.layer.borderWidth = 0;
                    myButton1.layer.borderColor = [[UIColor clearColor]CGColor];
               }
          }
          UIButton* myButton = (UIButton*)sender;
          myButton.layer.borderWidth = 3;
          myButton.layer.borderColor = [[UIColor orangeColor]CGColor];
          
          dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
               
               
               
               [audioActivity stopAnimating];
               selectedcharid = [sender tag];
               saveCharAudio = false;
               __vokiDiceBtn.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 0);
               NSString* xml_Path = @"";
               
               if(_previousVokiTag == 0)
               {
                    xml_Path = [NSString stringWithFormat:@"assets/img/char/Randoms/%@",[VokiContentArray objectAtIndex:(int)[sender tag]-777]];
               }
               else
               {
                    xml_Path = [NSString stringWithFormat:@"assets/img/char/%@/%@",[VokiScrollerArray objectAtIndex:_previousVokiTag-555],[VokiContentArray objectAtIndex:(int)[sender tag]-777]];
               }
               
               RUNNING_CHARACTER = xml_Path;
               
               saveSceneUrl = nil;
               Main_videoPath = @"";
               sameaudio = false;
               outurltweet = nil;
               NSString *key = [VokiScrollerArray objectAtIndex:_previousVokiTag-555];
               NSArray * characters = [ Allproductid objectForKey:key];
               Productforbuy = [characters objectAtIndex:(int)[sender tag]-777];
               
               if (![Productforbuy  isEqual: @"Purchased"])
               {
                    [self fetchAvailableProducts];
               }
               
               
               selectedCategory = [NSString stringWithFormat:@"assets/img/char/%@/",[VokiScrollerArray objectAtIndex:_previousVokiTag-555]];
               [[NSUserDefaults standardUserDefaults]setValue:selectedCategory forKey:@"selectedCategory"];
               [[NSUserDefaults standardUserDefaults]synchronize];
               [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
               
               dispatch_async(dispatch_get_main_queue(), ^(void){
                    //Run UI Updates
                    
                    
                    if(xml_Path != nil)
                    {
                         
                         NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                         NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
                         NSString *islogin = [[NSUserDefaults standardUserDefaults]
                                              stringForKey:@"islogin"];
                         
                         if (![islogin  isEqual: @"yes"] )
                         {
                              if (isbuyall == NO)
                              {
                                   if ([soldcharcters containsObject:xml_Path]|| [freecharacetr containsObject:xml_Path])
                                   {
                                        _Purchasedollerimg.alpha = 0;
                                        _sidedollar.alpha = 0;
                                        
                                   }
                                   else
                                   {
                                        _Purchasedollerimg.alpha = 1;
                                        _sidedollar.alpha = 1;
                                        _Purchasedollerimg.alpha = 1;
                                        _sidedollar.alpha = 1;
                                        _sidedollar.enabled = true;
                                        _Purchasedollerimg.hidden = false;
                                        _sidedollar.hidden = false;
                                   }
                              }
                              else
                              {
                                   _Purchasedollerimg.alpha = 0;
                                   _sidedollar.alpha = 0;
                              }
                              
                         }
                         
                         else
                         {
                              _Purchasedollerimg.alpha = 0;
                              _sidedollar.alpha = 0;
                         }
                         
                         
                         
                         
                         NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
                         NSMutableArray *arr = [[ParseObject sharedMethod] CallDescXML:1 :xml_Path];
                         if(arr.count != 0)
                         {
                              spriteSheetSelectedChar = [[arr objectAtIndex:0] valueForKey:@"spriteSheet"];
                              [[NSUserDefaults standardUserDefaults]setValue:spriteSheetSelectedChar forKey:@"spriteSheetSelectedChar"];
                              [[NSUserDefaults standardUserDefaults]synchronize];
                              
                              NSString *Costume,*fhair,*Glasses,*Hair,*Hat,*mouth,*Necklace,*Props;
                              
                              for(int i = 0; i < [[[arr objectAtIndex:0] valueForKey:@"Type"] count]; i ++)
                              {
                                   for(NSString *key in [[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i])
                                   {
                                        if([key isEqualToString:@"Costume"])
                                        {
                                             Costume = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                        }
                                        else if([key isEqualToString:@"fhair"])
                                        {
                                             fhair = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                        }
                                        else if([key isEqualToString:@"Hair"])
                                        {
                                             Hair = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                        }
                                        else if([key isEqualToString:@"Hat"])
                                        {
                                             Hat = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                        }
                                        else if([key isEqualToString:@"mouth"])
                                        {
                                             mouth = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             
                                        }
                                        else if([key isEqualToString:@"Necklace"])
                                        {
                                             Necklace = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                        }
                                        else if([key isEqualToString:@"Glasses"])
                                        {
                                             Glasses = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                        }
                                        else if([key isEqualToString:@"Props"])
                                        {
                                             Props = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                        }
                                   }
                              }
                              //NSString *output_path = [NSString stringWithFormat:@"%@/VokiContent",output];
                              NSString *output_path = [NSString stringWithFormat:@"%@",output];
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              
                              LibPath = [myBundle pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",xml_Path] ofType:@"png" inDirectory:@""];
                              
                              NSString *jsString = [NSString stringWithFormat:@"RunXMLfile('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",BundlePath,[[arr objectAtIndex:0] valueForKey:@"spriteSheet"],[[arr objectAtIndex:0] valueForKey:@"colorString"],Costume ?: @"0",fhair?: @"0",Glasses?: @"0",Hair?: @"0",Hat?: @"0",mouth?: @"0",Necklace?: @"969",Props?: @"0"];
                              
                              jsString = [jsString stringByReplacingOccurrencesOfString:@"World" withString:@"Historical Figures"];
                              
                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                              
                              //                              [[NSURLCache sharedURLCache] removeAllCachedResponses];
                              //
                              //                              // set an empty cache
                              //                              NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
                              //                              [NSURLCache setSharedURLCache:sharedCache];
                              
                              
                              xpos = [[arr objectAtIndex:0] valueForKey:@"XPOS"];
                              yposvalue = [[arr objectAtIndex:0] valueForKey:@"YPOS"];
                              scalevalue = [[arr objectAtIndex:0] valueForKey:@"SCALE"];
                              
                              NSString *pos = [NSString stringWithFormat:@"positionChar('%@','%@','%@')",@"0.14",yposvalue,scalevalue];
                              
                              
                              
                              
                              
                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:pos];
                              
                              genderID = [[arr objectAtIndex:0] valueForKey:@"genderID"];
                              
                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"enableScreen()"];
                              
                              //                              if ([__LanguageLbl.text isEqualToString: @"English"])
                              //                              {
                              //                                   engineID = 3;
                              //                                   if ([genderID isEqualToString:@"1"])
                              //                                   {
                              //                                        _textVoiceID=3;
                              //
                              //                                   }
                              //                                   if ([genderID isEqualToString:@"2"])
                              //                                   {
                              //                                        _textVoiceID=2;
                              //
                              //                                   }
                              //                                   if ([genderID isEqualToString:@"4"])
                              //                                   {
                              //                                        _textVoiceID=3;
                              //
                              //                                   }
                              //                                   if ([genderID isEqualToString:@"3"])
                              //                                   {
                              //                                        _textVoiceID=3;
                              //
                              //                                   }
                              //                              }
                              
                              [self loaderposition];
                              
                         }
                         else
                         {
                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"Work under progress" delegate: nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                              [alert show];
                         }
                    }
                    else
                    {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate: nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                         [alert show];
                    }
                    
               });
               
          });
          
     }
}

-(void) webViewDidStartLoad:(UIWebView *)webView
{
     [UIApplication sharedApplication].networkActivityIndicatorVisible = true;
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
     [[NSUserDefaults standardUserDefaults] synchronize];
     
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"did fail load error" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alert show];
     
     
}

-(void)AddWebView
{
     dispatch_async(dispatch_get_main_queue(), ^{
          @autoreleasepool
          {
               ___CharacterWebView = [[UIWebView alloc]init];
               ___CharacterWebView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, _canvasWidth, _canvasHeight+2);
               ___CharacterWebView.backgroundColor = [UIColor clearColor];
               [___CharacterWebView setScalesPageToFit:YES];
               ___CharacterWebView.delegate=self;
               
               self.__CharacterWebView.mediaPlaybackRequiresUserAction = NO;
               self.__CharacterWebView.allowsInlineMediaPlayback = YES;
               
               
               
               gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
               [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
               
               [self.captureview addGestureRecognizer:gestureRecognizer];
               gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
               [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
               LoaderView =  [[UIView alloc] initWithFrame:CGRectMake(100,100,60,60)];
               
               _pangestureMovetoChar.delegate = self;
               TapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
               TapGesture.numberOfTapsRequired = 1;
               TapGesture.delegate =  self;
               [_BgViewForTable addGestureRecognizer:TapGesture];
               CloseImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ClosetapGesture:)];
               CloseImageTapGesture.numberOfTapsRequired = 1;
               CloseImageTapGesture.delegate =  self;
               //[_imgClose addGestureRecognizer:CloseImageTapGesture];
               _itemName = [[UILabel alloc]init];
               _itemName.frame = CGRectMake(100, 100, 150, 40);
               _itemName.alpha = 0;
               _itemName.textColor = [UIColor whiteColor];
               _itemName.textColor = [UIColor whiteColor];
               _itemName.text = @"Item Name";
               _itemName.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
               _itemName.layer.cornerRadius = 15;
               _itemName.clipsToBounds =YES;
               _itemName.textAlignment = NSTextAlignmentCenter;
               [_itemName setAdjustsFontSizeToFitWidth:YES];
               
               [_itemName setCenter:___CharacterWebView.center];
               [___CharacterWebView addSubview:_itemName];
               
               playCharacterAud1 = [UIButton buttonWithType:UIButtonTypeCustom];
               [playCharacterAud1 addTarget:self action:@selector(PlayCharacterAudio1dup:) forControlEvents:UIControlEventTouchUpInside];
               [_PlayCharAud1dup addTarget:self action:@selector(PlayCharacterAudio1:) forControlEvents:UIControlEventTouchUpInside];
               [playCharacterAud1 setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"playRoundBtn"]] forState:UIControlStateNormal];
               playCharacterAud1.frame = CGRectMake(200, 100, 65, 65);
               _PlayCharAud1dup.frame = playCharacterAud1.frame;
               
               transparentIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
               
               [transparentIndicator setFrame:CGRectMake((LoaderView.frame.size.width/2)-(transparentIndicator.frame.size.width/2), (LoaderView.frame.size.height/2)-(transparentIndicator.frame.size.height/2), transparentIndicator.frame.size.width, transparentIndicator.frame.size.height)];
               transparentIndicator.color = [UIColor blackColor];
               [self.screencapture addGestureRecognizer:gestureRecognizerRight];
               [self.screencapture addSubview:___CharacterWebView];
               [self.screencapture sendSubviewToBack:___CharacterWebView];
               [self.screencapture addSubview:vokilogo_img];
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                     }];
                    playCharacterAud1.frame = CGRectMake((self.view.frame.size.width+2  - __mainFirstStrip.frame.size.width)/2 - 45 + playCharacterAud1.frame.size.width/1.5 , self.view.frame.size.height - 105, 95, 95);
                    _PlayCharAud1dup.frame = playCharacterAud1.frame;
                    __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8+__thirdView.frame.size.width,10,100, 100);
                    _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8+__thirdView.frame.size.width,10,100, 100);
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         playCharacterAud1.alpha = 1;
                         
                    });
               }
               else
               {
                    if (_canvasWidth == 480)
                    {
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                          }];
                         
                         playCharacterAud1.frame = CGRectMake((self.view.frame.size.width+2 - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                         _PlayCharAud1dup.frame = playCharacterAud1.frame;
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                          }];
                         
                         playCharacterAud1.frame = CGRectMake((self.view.frame.size.width+2 - __mainFirstStrip.frame.size.width)/2 -25 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                         _PlayCharAud1dup.frame = playCharacterAud1.frame;
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                         
                    }
                    else if (_canvasWidth == 667)
                    {
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                          }];
                         playCharacterAud1.frame = CGRectMake((self.view.frame.size.width-15- __mainFirstStrip.frame.size.width)/2 - 27 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                         _PlayCharAud1dup.frame = playCharacterAud1.frame;
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                         
                         
                    }
                    
                    else
                    {
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                          }];
                         playCharacterAud1.frame = CGRectMake((self.view.frame.size.width-2  - __mainFirstStrip.frame.size.width)/2 - 20 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                         _PlayCharAud1dup.frame = playCharacterAud1.frame;
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                    _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                    
               }
               [LoaderView setCenter:___CharacterWebView.center];
               LoaderView.frame = CGRectMake(playCharacterAud1.frame.origin.x+30, LoaderView.frame.origin.y + 20, 60, 60);
               
               
               [___CharacterWebView addSubview:playCharacterAud1];
               [___CharacterWebView addSubview:LoaderView];
               
               [LoaderView addSubview:transparentIndicator];
               LoaderView.hidden=YES;
               [self LoadWebView];
               _deleteGalleryBtn.alpha=0;
               _deleteGalleryBtn.frame = CGRectMake(_CloeseThirdStripBtn.frame.origin.x, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
               __vokiDiceBtn.alpha=0;
               _VokiDiceImage.alpha=0;
               
               __text_Languageview.frame = CGRectMake((self.view.frame.size.width/2)-(__text_Languageview.frame.size.width/2), __text_Languageview.frame.origin.y, __text_Languageview.frame.size.width, __text_Languageview.frame.size.height);
               __text_VoiceIDView.frame = CGRectMake((self.view.frame.size.width/2)-(__text_VoiceIDView.frame.size.width/2), __text_VoiceIDView.frame.origin.y, __text_VoiceIDView.frame.size.width, __text_VoiceIDView.frame.size.height);
               
               
               _fxMenuView.frame = CGRectMake((self.view.frame.size.width/2)-(_fxMenuView.frame.size.width/2), _fxMenuView.frame.origin.y, _fxMenuView.frame.size.width, _fxMenuView.frame.size.height);
               _cancelBtnTable.frame = CGRectMake((self.view.frame.size.width/2)-(__text_VoiceIDView.frame.size.width/2), _cancelBtnTable.frame.origin.y, __text_Languageview.frame.size.width, _cancelBtnTable.frame.size.height);
               
               __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y, __rightShareView.frame.size.width, __rightShareView.frame.size.height);
               
               __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y ,__rightMenuView.frame.size.width,__rightMenuView.frame.size.height);
          }
     });
     
}
-(void)DeleteWebView
{
     dispatch_async(dispatch_get_main_queue(), ^{
          ___CharacterWebView.delegate=nil;
          [___CharacterWebView loadHTMLString:@"" baseURL:nil];
          [___CharacterWebView stopLoading];
          [___CharacterWebView removeFromSuperview];
          ___CharacterWebView = nil;
     });
     [self clearSavedData];
}

-(void)CallEvent
{
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:[[NSUserDefaults standardUserDefaults]valueForKey:@"JAVA_STRING"]];
}

-(void)clearSavedData
{
     
     
     [self clearCookies];
     [self ClearCacheDirectory];
}
-(void)clearCookies
{
     NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
     for (NSHTTPCookie *cookie in [cookieStorage cookies]) {
          [cookieStorage deleteCookie:cookie];
     }
}
-(void)ClearCacheDirectory
{
     
}



-(void)ResumeCharacter
{
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"resumeChar()"]];
}

#pragma mark - checking for icon image

-(NSString*)FetchImageIcon:(NSString*)path :(NSString*)ValueString
{
     
     
     NSArray* Array = [[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"%@/%@",path,ValueString]];
     NSString *imagePath = @"";
     
     if(Array.count != 0)
     {
          imagePath = [self FetchIconFolder:Array :ValueString];
          
          if(imagePath.length == 0)
          {
               imagePath = [NSString stringWithFormat:@"%@/thumb_128x128",[Array objectAtIndex:0]];
          }
     }
     
     return imagePath;
}

-(NSString*)FetchIconFolder:(NSArray*)Arr :(NSString*)name
{
     NSString *path = @"";
     
     for(int i = 0; i < Arr.count; i++)
     {
          if([[Arr objectAtIndex:i] isEqualToString:@"icons"])
          {
               name = [name lowercaseString];
               path = @"icons/base/btn_128x128";
               path =[path stringByReplacingOccurrencesOfString:@"btn_128x128" withString:@"btn_"];
               path = [NSString stringWithFormat:@"%@%@_%@",path,name,@"128x128"];
               path = [path stringByReplacingOccurrencesOfString:@"celebs" withString:@"vip"];
               path = [path stringByReplacingOccurrencesOfString:@"folk" withString:@"world"];
               path = [path stringByReplacingOccurrencesOfString:@"historical figures" withString:@"historical"];
               path = [path stringByReplacingOccurrencesOfString:@"randoms" withString:@"random"];
               path = [path stringByReplacingOccurrencesOfString:@"3d" withString:@"3D"];
               break;
          }
     }
     return path;
}

-(NSArray*)RemoveIconImage:(NSArray*)Arr
{
     NSMutableArray* arrayOfArr = [NSMutableArray new];
     [arrayOfArr addObjectsFromArray:Arr];
     
     for(int i = 0; i < arrayOfArr.count; i++)
     {
          if([[arrayOfArr objectAtIndex:i] isEqualToString:@"icons"])
          {
               [arrayOfArr removeObjectAtIndex:i];
          }
     }
     return (NSArray*)arrayOfArr;
}

#pragma mark - AccessoriesView method

- (IBAction)AccessoriesAction:(id)sender
{
     
     
     if ([sender isSelected])
     {
          [self HideAllViews];
          [self DisableButtonSelection];
     }
     
     else
     {
          [_MouthViewScroller setContentSize:CGSizeMake(__vokiScroller.frame.size.width, _stripWidth*10)];
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               [_MouthViewScroller setContentSize:CGSizeMake(__vokiScroller.frame.size.width, _stripWidth*11)];
          }
          accSelectedCount = -12;
          [gestureRecognizer setEnabled:YES];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"LoadXmlFile()"];
          
          [self moveRightMenu];
          [self HideAllViews];
          [self DisableButtonSelection];
          [self DisableMainButtons];
          [self moveCharacterFirstView];
          _isAccessoriesSelected = YES;
          menuViewOpenClose = YES;
          shareViewOpenClose = YES;
          [__accessoriesButton setSelected:YES];
          [gestureRecognizerRight setEnabled:NO];
          [__clothButton setEnabled:YES];
          [__capButton setEnabled:YES];
          [__glassButton setEnabled:YES];
          [__blingButton setEnabled:YES];
          [__propButton setEnabled:YES];
          [__hair_Button setEnabled:YES];
          [__fHairbutton setEnabled:YES];
          [__mouth_Button setEnabled:YES];
          [UIView animateWithDuration:0.5 animations:^
           {
                
                __clothingView.alpha = 1;
                _CloseSecondStripBtn.alpha = 1;
                _CloseSecondStripBtn.hidden=false;
                __clothingView.hidden=false;
                
                __clothingView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8, __clothingView.frame.origin.y, __clothingView.frame.size.width, __clothingView.frame.size.height);
                _CloseSecondStripBtn.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width , _CloseSecondStripBtn.frame.origin.y, _CloseSecondStripBtn.frame.size.width, _CloseSecondStripBtn.frame.size.height);
           }];
          
          NSMutableArray *arr = [[ParseObject sharedMethod] CallDescXML:1 :RUNNING_CHARACTER];
          if (arr.count== 0)
          {
               [__clothButton setEnabled:NO];
               [__capButton setEnabled:NO];
               [__glassButton setEnabled:NO];
               [__blingButton setEnabled:NO];
               [__propButton setEnabled:NO];
               [__hair_Button setEnabled:NO];
               [__fHairbutton setEnabled:NO];
               [__mouth_Button setEnabled:NO];
          }
          else
          {
               [temparr removeAllObjects];
               
               NSArray *key = [[arr objectAtIndex:0] valueForKey:@"Types"];
               {
                    for (int i = 0; i<[key count]; i++)
                    {
                         NSArray *k = [[key objectAtIndex:i]allKeys];
                         [temparr addObject:[k objectAtIndex:0]];
                    }
                    
                    
                    if (![temparr containsObject:@"Costume"])
                    {
                         [__clothButton setEnabled:NO];
                    }
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"Costume"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"Costume"];
                         
                         if ([Value containsString:@","])
                         {
                              [__clothButton setEnabled:YES];
                         }
                         else
                         {
                              [__clothButton setEnabled:NO];
                         }
                         
                         
                    }
                    
                    if (![temparr containsObject:@"Hat"])
                    {
                         [__capButton setEnabled:NO];
                    }
                    
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"Hat"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"Hat"];
                         
                         if ([Value containsString:@","])
                         {
                              [__capButton setEnabled:YES];
                         }
                         else
                         {
                              [__capButton setEnabled:NO];
                         }
                         
                         
                    }
                    
                    
                    if (![temparr containsObject:@"Glasses"])
                    {
                         [__glassButton setEnabled:NO];
                    }
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"Glasses"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"Glasses"];
                         
                         if ([Value containsString:@","])
                         {
                              [__glassButton setEnabled:YES];
                         }
                         else
                         {
                              [__glassButton setEnabled:NO];
                         }
                    }
                    
                    
                    if (![temparr containsObject:@"Necklace"])
                    {
                         [__blingButton setEnabled:NO];
                    }
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"Necklace"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"Necklace"];
                         
                         if ([Value containsString:@","])
                         {
                              [__blingButton setEnabled:YES];
                         }
                         else
                         {
                              [__blingButton setEnabled:NO];
                         }
                    }
                    
                    
                    if (![temparr containsObject:@"Props"])
                    {
                         [__propButton setEnabled:NO];
                    }
                    
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"Props"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"Props"];
                         
                         if ([Value containsString:@","])
                         {
                              [__propButton setEnabled:YES];
                         }
                         else
                         {
                              [__propButton setEnabled:NO];
                         }
                    }
                    
                    
                    if (![temparr containsObject:@"Hair"])
                    {
                         [__hair_Button setEnabled:NO];
                    }
                    
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"Hair"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"Hair"];
                         
                         if ([Value containsString:@","])
                         {
                              [__hair_Button setEnabled:YES];
                         }
                         else
                         {
                              [__hair_Button setEnabled:NO];
                         }
                    }
                    
                    
                    if (![temparr containsObject:@"fhair"])
                    {
                         [__fHairbutton setEnabled:NO];
                    }
                    
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"fhair"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"fhair"];
                         
                         if ([Value containsString:@","])
                         {
                              [__fHairbutton setEnabled:YES];
                         }
                         else
                         {
                              [__fHairbutton setEnabled:NO];
                         }
                    }
                    
                    
                    if (![temparr containsObject:@"mouth"])
                    {
                         [__mouth_Button setEnabled:NO];
                    }
                    else
                         
                    {
                         NSInteger i = [temparr indexOfObject:@"mouth"];
                         
                         NSString *Value = [[key objectAtIndex:i]valueForKey:@"mouth"];
                         
                         if ([Value containsString:@","])
                         {
                              [__mouth_Button setEnabled:YES];
                         }
                         else
                         {
                              [__mouth_Button setEnabled:NO];
                         }
                    }
               }
          }
     }
     
}
- (IBAction)ClothAction:(id)sender
{
     [UIView animateWithDuration:0.1 animations:^{
          
          allcatagoryview.hidden = true;
          playCharacterAud1.alpha = 1;
          playCharacterAud1.hidden = false;
          
     }];
     
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__clothButton setSelected:NO];
          
     }
     else
     {
          [self ChangeAccessorieBoolValue];
          isCloathSelected = YES;
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__clothButton setSelected:YES];
          
          
          [UIView animateWithDuration:0.5 animations:^
           {
                
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                
           }];
          
          
          
          [self CharacterMove];
          
          [self LoadAccessoriesThirdScroller:@"Costume"];
          
          [self CallThirdStrip];
     }
}

- (IBAction)CapAction:(id)sender
{
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__capButton setSelected:NO];
          
     }
     else
     {
          
          
          [self ChangeAccessorieBoolValue];
          isCapSelected = YES;
          
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__capButton setSelected:YES];
          
          [self LoadAccessoriesThirdScroller:@"Hat"];
          
          
          [UIView animateWithDuration:0.5 animations:^
           {
                
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                
           }];
          [self CharacterMove];
          [self CallThirdStrip];
     }
}

- (IBAction)GlassAction:(id)sender
{
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__glassButton setSelected:NO];
          
     }
     else
     {
          
          
          [self ChangeAccessorieBoolValue];
          isGlassesSelected = YES;
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__glassButton setSelected:YES];
          
          [self LoadAccessoriesThirdScroller:@"Glasses"];
          
          [UIView animateWithDuration:0.5 animations:^
           {
                
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                
           }];
          
          
          [self CharacterMove];
          [self CallThirdStrip];
     }
}

- (IBAction)BlingAction:(id)sender
{
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__blingButton setSelected:NO];
          
     }
     else
     {
          
          [self ChangeAccessorieBoolValue];
          isBlingSelected = YES;
          
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__blingButton setSelected:YES];
          
          [self LoadAccessoriesThirdScroller:@"Necklace"];
          
          
          [UIView animateWithDuration:0.5 animations:^
           {
                
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                
           }];
          
          
          [self CharacterMove];
          [self CallThirdStrip];
     }
}

- (IBAction)PropsAction:(id)sender
{
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__propButton setSelected:NO];
          
     }
     else
     {
          
          
          [self ChangeAccessorieBoolValue];
          isPropsSelected = YES;
          
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__propButton setSelected:YES];
          
          [self LoadAccessoriesThirdScroller:@"Props"];
          
          
          [UIView animateWithDuration:0.5 animations:^
           {
                
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                
           }];
          
          
          [self CharacterMove];
          [self CallThirdStrip];
     }
}

- (IBAction)Hair_Action:(id)sender
{
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__hair_Button setSelected:NO];
          
     }
     else
     {
          
          
          [self ChangeAccessorieBoolValue];
          isHairSelected = YES;
          
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__hair_Button setSelected:YES];
          [self LoadAccessoriesThirdScroller:@"Hair"];
          
          [UIView animateWithDuration:0.5 animations:^
           {
                
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                
           }];
          
          
          [self CharacterMove];
          [self CallThirdStrip];
     }
}

- (IBAction)fHairAction:(id)sender
{
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__fHairbutton setSelected:NO];
          
     }
     else
     {
          
          [self ChangeAccessorieBoolValue];
          isfHairSelected = YES;
          
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__fHairbutton setSelected:YES];
          [self LoadAccessoriesThirdScroller:@"fhair"];
          
          [UIView animateWithDuration:0.5 animations:^
           {
                
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                
           }];
          
          
          [self CharacterMove];
          [self CallThirdStrip];
     }
}

- (IBAction)Mouth_Action:(id)sender
{
     if ([sender isSelected])
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           }];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
          [__mouth_Button setSelected:NO];
          
     }
     else
     {
          
          [self ChangeAccessorieBoolValue];
          isMouthSelected = YES;
          
          _isAccessoriesSelected = NO;
          [self DisableButtonSelection];
          [__mouth_Button setSelected:YES];
          [self LoadAccessoriesThirdScroller:@"mouth"];
          
          
          [UIView animateWithDuration:0.5 animations:^
           {
                _CloeseThirdStripBtn.alpha = 1;
                __thirdView.alpha = 1;
                
                __thirdView.hidden=false;
                thirdView = false;
                __thirdView.frame = CGRectMake(__clothingView.frame.origin.x+__clothingView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
           }];
          [self CharacterMove];
          [self CallThirdStrip];
     }
}

#pragma mark - call this function if using images on accessories tab

-(void)LoadAccessoriesThirdScroller:(NSString*)Type
{
     @autoreleasepool {
          
          //NSString *output_path = [NSString stringWithFormat:@"%@/VokiContent",output];
          NSString *output_path = [NSString stringWithFormat:@"%@",output];
          
          for(UIView *subview in __thirdContentScroller.subviews)
          {
               [subview removeFromSuperview];
          }
          NSString *t = [NSString stringWithFormat:@"getAcess('%@')",Type];
          NSString *AccesorySelected = [___CharacterWebView stringByEvaluatingJavaScriptFromString:t];
          accSelectedCount = -12;
          if ([AccesorySelected isEqualToString:@"0"]||[AccesorySelected isEqualToString:@"-1"])
          {
               accSelectedCount = -12;
          }
          
          int x = 0;
          int y = 0;
          _previousTag = 0;
          _previousAccessoryTag = 0;
          
          
          NSMutableArray *arr = [[ParseObject sharedMethod] CallDescXML:1 :RUNNING_CHARACTER];
          AccessoryDataarray = [[NSMutableArray alloc]init];
          [AccessoryDataarray addObjectsFromArray:arr];
          
          accessoryListArray = [[NSMutableArray alloc]init];
          NSArray *HairArray;
          if (arr.count!=0)
          {
               
               for(int i = 0; i < [[[arr objectAtIndex:0] valueForKey:@"Types"] count]; i ++)
               {
                    for(NSString *key in [[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i])
                    {
                         if([key isEqualToString:Type])
                         {
                              HairArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              break;
                         }
                    }
               }
               
               if(HairArray.count == 1 && [[HairArray objectAtIndex:0] intValue] == 0)
               {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"No item found for the selected accessory." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         _isAccessoriesSelected = NO;
                         __thirdView.hidden=true;
                         thirdView = true;
                         [self CharacterMove];
                         _CloeseThirdStripBtn.hidden=true;
                         _CloseSecondStripBtn.hidden=false;
                    });
               }
               else
               {
                    if ([HairArray containsObject:AccesorySelected])
                    {
                         accSelectedCount = [HairArray indexOfObject:AccesorySelected];
                         
                    }
                    
                    [accessoryListArray addObjectsFromArray:HairArray];
                    float buttonHeight = 0.0;
                    __thirdUpArrow.hidden = false;
                    __thirdDownArrow.hidden = false;
                    
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                    {
                         
                         if (HairArray.count <= 6)
                         {
                              __thirdUpArrow.hidden = true;
                              __thirdDownArrow.hidden = true;
                         }
                         
                    }
                    else
                    {
                         if (HairArray.count <= 5)
                         {
                              __thirdUpArrow.hidden = true;
                              __thirdDownArrow.hidden = true;
                         }
                    }
                    
                    for(int i = 0; i < [HairArray  count]; i++)
                    {
                         UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                         [button addTarget:self action:@selector(PickAccessory:) forControlEvents:UIControlEventTouchUpInside];
                         [[button imageView] setContentMode: UIViewContentModeScaleToFill];
                         
                         button.frame = CGRectMake(x+8, y, __thirdContentScroller.frame.size.width-16, _stripWidth-14);
                         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                         {
                              button.frame = CGRectMake(x+8, y, __thirdContentScroller.frame.size.width-16, _stripWidth-23);
                              
                         }
                         
                         NSString *imageName;
                         
                         NSString *image_Path;
                         if([Type isEqualToString:@"Costume"])
                         {
                              imageName = @"thumb_128x128";
                              image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"assets/img/acc/id/%@/%@/%@",Type,[HairArray objectAtIndex:i],imageName] ofType:@"png" inDirectory:@""];
                         }
                         else if([Type isEqualToString:@"Hat"])
                         {
                              imageName = @"Left.ohv2";
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              
                              image_Path = [myBundle pathForResource:[NSString stringWithFormat:@"%@/%@/%@_%@",RUNNING_CHARACTER,@"acc",[HairArray objectAtIndex:i],@"thumb_128x128"] ofType:@"png" inDirectory:@""];
                              
                         }
                         else if([Type isEqualToString:@"Glasses"])
                         {
                              
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              
                              image_Path = [myBundle pathForResource:[NSString stringWithFormat:@"%@/%@/%@_%@",RUNNING_CHARACTER,@"acc",[HairArray objectAtIndex:i],@"thumb_128x128"] ofType:@"png" inDirectory:@""];
                              
                              
                         }
                         else if([Type isEqualToString:@"Necklace"])
                         {
                              [[button imageView] setContentMode: UIViewContentModeScaleAspectFit];
                              imageName = @"Front.ohv2";
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"assets/img/acc/id/%@/%@/%@",Type,[HairArray objectAtIndex:i],imageName] ofType:@"png" inDirectory:@""];
                         }
                         else if([Type isEqualToString:@"Props"])
                         {
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              
                              imageName = @"thumb_128x128";
                              image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"assets/img/acc/id/%@/%@/%@",Type,[HairArray objectAtIndex:i],imageName] ofType:@"png" inDirectory:@""];
                         }
                         else if([Type isEqualToString:@"Hair"])
                         {
                              imageName = @"Left.ohv2";
                              
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              
                              image_Path = [myBundle pathForResource:[NSString stringWithFormat:@"%@/%@/%@_%@",RUNNING_CHARACTER,@"acc",[HairArray objectAtIndex:i],@"thumb_128x128"] ofType:@"png" inDirectory:@""];
                              
                         }
                         else if([Type isEqualToString:@"fhair"])
                         {
                              imageName = @"Left.ohv2";
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              
                              image_Path = [myBundle pathForResource:[NSString stringWithFormat:@"%@/%@/%@_%@",RUNNING_CHARACTER,@"acc",[HairArray objectAtIndex:i],@"thumb_128x128"] ofType:@"png" inDirectory:@""];
                         }
                         else if([Type isEqualToString:@"mouth"])
                         {
                              
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              image_Path = [myBundle pathForResource:[NSString stringWithFormat:@"%@/%@/%@_%@",RUNNING_CHARACTER,@"acc",[HairArray objectAtIndex:i],@"thumb_128x128"] ofType:@"png" inDirectory:@""];
                              
                              
                              
                         }
                         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                              UIImage *image = [UIImage imageWithContentsOfFile:image_Path];
                              UIImage *image1 = [ self resizeimage:image];
                              dispatch_async(dispatch_get_main_queue(), ^{
                                   [button setImage:image1 forState:UIControlStateNormal];
                                   [button setBackgroundColor:[UIColor whiteColor]];
                                   button.layer.cornerRadius = 5;
                                   button.clipsToBounds= YES;
                              });
                         });
                         buttonHeight = button.frame.size.height;
                         button.tag = [[HairArray objectAtIndex:i] intValue]+ 777;
                         
                         
                         if (i==accSelectedCount)
                         {
                              button.layer.borderWidth = 3;
                              button.layer.borderColor = [[UIColor orangeColor]CGColor];
                              
                         }
                         
                         
                         [__thirdContentScroller addSubview:button];
                         y = y + _stripWidth;
                    }
                    
                    [__thirdContentScroller setContentSize:CGSizeMake(0, (buttonHeight+16)*[HairArray count])];
                    
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                    {
                         [__thirdContentScroller setContentSize:CGSizeMake(0, (buttonHeight+23)*[HairArray count])];
                         
                    }
                    
                    if (accSelectedCount != -12)
                    {
                         UIButton *button1 = (UIButton *)[__thirdContentScroller viewWithTag:[[HairArray objectAtIndex:accSelectedCount] intValue]+777];
                         button1.layer.borderWidth = 3;
                         button1.layer.borderColor = [[UIColor orangeColor]CGColor];
                         
                         if (accSelectedCount > 4)
                         {
                              [UIView animateWithDuration:0.7 animations:^{
                                   [__thirdContentScroller setContentOffset:CGPointMake(0, button1.frame.origin.y-(self.view.frame.size.height/2))];
                                   
                              }];
                              
                         }
                         else
                         {
                              [UIView animateWithDuration:0.7 animations:^{
                                   [__thirdContentScroller setContentOffset:CGPointMake(0, 0)];
                                   
                              }];
                              
                              
                         }
                         
                    }
                    else
                    {
                         [__thirdContentScroller setContentOffset:CGPointMake(0, 0)];
                    }
                    
                    
               }
          }
     }
}

-(void)ChangeAccessorieBoolValue
{
     isCloathSelected = NO;
     isCapSelected = NO;
     isGlassesSelected = NO;
     isBlingSelected = NO;
     isPropsSelected = NO;
     isHairSelected = NO;
     isfHairSelected = NO;
     isMouthSelected = NO;
}

-(void)LoadHairStyles:(int)Index
{
     NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
     NSString *javascriptString;
     javascriptString = [NSString stringWithFormat:@"changeHairsStyle_%d('%@')",Index, BundlePath];
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:javascriptString];
}

-(void)LoadHairAccessory:(NSString*)path :(NSString*)AccID
{
     NSString *javascriptString = [NSString stringWithFormat:@"changeHairsStyle('%@/%@','%@')", MainBundlePath,path,AccID];
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:javascriptString];
}

-(void)LoadCostumeAccessory:(NSString*)path :(NSString*)AccID
{
     NSString *javascriptString = [NSString stringWithFormat:@"changeCostume('%@/%@','%@')", MainBundlePath,path,AccID];
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:javascriptString];
}

-(void)LoadThirdScroller
{
     for(UIView *subview in __thirdContentScroller.subviews)
     {
          [subview removeFromSuperview];
     }
     
     NSMutableArray *ImageScrollerArray = [[NSMutableArray alloc]initWithObjects:@"btn_sports",@"btn_toons",@"btn_nightlife",@"btn_city",@"btn_myBGs",@"btn_travel",@"btn_holiday",@"btn_skin", nil];
     int x = 0;
     int y = 0;
     _previousTag = 0;
     [__thirdContentScroller setContentSize:CGSizeMake(0, _stripWidth*[ImageScrollerArray count])];
     for(int i = 0; i<[ImageScrollerArray count]; i++)
     {
          UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
          [button addTarget:self action:@selector(PickContent:) forControlEvents:UIControlEventTouchUpInside];
          [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"btn_image_unSel.png"]] forState:UIControlStateNormal];
          [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"btn_image_Sel.png"]] forState:UIControlStateSelected];
          button.frame = CGRectMake(x, y, _stripWidth, _stripWidth);
          button.tag = i + 777;
          [__thirdContentScroller addSubview:button];
          y = y + _stripWidth;
     }
     [__thirdContentScroller setContentOffset:CGPointMake(0, 0)];
}
-(IBAction)PickContent:(id)sender
{
     if(_previousTag != 0)
     {
          UIButton *button = (UIButton *)[__thirdContentScroller viewWithTag:_previousTag];
          [button setSelected:NO];
     }
     
     UIButton *pickbutton = (UIButton *)[__thirdContentScroller viewWithTag:[sender tag]];
     [pickbutton setSelected:YES];
     
     _previousTag = (int)[sender tag];
}

-(NSString*)CheckSelectedAccessory
{
     NSString *Category = [NSString new];
     
     if(isCloathSelected)
     {
          Category = @"costume";
     }
     else if(isCapSelected)
     {
          Category = @"hat";
     }
     else if(isGlassesSelected)
     {
          Category = @"glasses";
     }
     else if(isBlingSelected)
     {
          Category = @"necklace";
     }
     else if(isPropsSelected)
     {
          Category = @"props";
     }
     else if(isHairSelected)
     {
          Category = @"Hair";
     }
     else if(isfHairSelected)
     {
          Category = @"fhair";
     }
     else if(isMouthSelected)
     {
          Category = @"mouth";
     }
     return Category;
}



-(IBAction)PickAccessory:(id)sender
{
     
     [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
     [[NSUserDefaults standardUserDefaults] synchronize];
     saveSceneUrl = nil;
     Main_videoPath = @"";
     sameaudio = false;
     outurltweet = nil;
     for(UIView *subview in __thirdContentScroller.subviews)
     {
          if ([subview isKindOfClass:[UIButton class]])
          {
               UIButton* myButton1 = (UIButton*)subview;
               myButton1.layer.borderWidth = 0;
               myButton1.layer.borderColor = [[UIColor clearColor]CGColor];
          }
     }
     
     
     UIButton* myButton = (UIButton*)sender;
     myButton.layer.borderWidth = 3;
     myButton.layer.borderColor = [[UIColor orangeColor]CGColor];
     
     if(_previousAccessoryTag != 0)
     {
          UIButton *button = (UIButton *)[__thirdContentScroller viewWithTag:_previousAccessoryTag];
          [button setSelected:NO];
     }
     
     UIButton *button1 = (UIButton *)[__thirdContentScroller viewWithTag:[sender tag]];
     [button1 setSelected:YES];
     
     _previousAccessoryTag = (int)[sender tag];
     
     NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
     
     if(isCloathSelected)
     {
          
          NSString *jsString = [NSString stringWithFormat:@"LoadCostumeAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     else if(isCapSelected)
     {
          NSString *jsString = [NSString stringWithFormat:@"LoadHatAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     else if(isGlassesSelected)
     {
          NSString *jsString = [NSString stringWithFormat:@"LoadGlassesAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     else if(isBlingSelected)
     {
          NSString *jsString = [NSString stringWithFormat:@"LoadNecklaceAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     else if(isPropsSelected)
     {
          NSString *jsString = [NSString stringWithFormat:@"LoadPropsAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     else if(isHairSelected)
     {
          NSString *jsString = [NSString stringWithFormat:@"LoadHairAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     else if(isfHairSelected)
     {
          NSString *jsString = [NSString stringWithFormat:@"LoadfhairAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     else if(isMouthSelected)
     {
          NSString *jsString = [NSString stringWithFormat:@"LoadmouthAccessory('%@','%@','%@','%d')",BundlePath,[[AccessoryDataarray objectAtIndex:0] valueForKey:@"spriteSheet"],[[AccessoryDataarray objectAtIndex:0] valueForKey:@"colorString"],(int)[sender tag]-777];
          
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
     }
     
     
     
}

- (IBAction)MouthViewUpAction:(id)sender
{
     _isAccessoriesSelected = YES;
     
     [self DisableButtonSelection];
     
     [__MouthViewUpBtn setSelected:YES];
     
     if (_MouthViewScroller.contentOffset.y > 0)
     {
          if (_MouthViewScroller.contentOffset.y < _stripWidth)
          {
               [_MouthViewScroller setContentOffset:CGPointMake(0, 0) animated:YES];
          }
          else
          {
               [_MouthViewScroller setContentOffset:CGPointMake(0, _MouthViewScroller.contentOffset.y-_stripWidth) animated:YES];
          }
     }
     else
     {
          [__MouthViewUpBtn setSelected:NO];
     }
}

- (IBAction)MouthViewDownAction:(id)sender
{
     _isAccessoriesSelected = YES;
     
     [self DisableButtonSelection];
     
     [__MouthViewdownBtn setSelected:YES];
     
     if(_MouthViewScroller.contentOffset.y+_MouthViewScroller.frame.size.height < _MouthViewScroller.contentSize.height )
     {
          if(_MouthViewScroller.contentOffset.y+_stripWidth +_MouthViewScroller.frame.size.height> _MouthViewScroller.contentSize.height )
          {
               [_MouthViewScroller setContentOffset:CGPointMake(0, _MouthViewScroller.contentOffset.y) animated:YES];
               [__MouthViewdownBtn setSelected:NO];
          }
          else
          {
               [_MouthViewScroller setContentOffset:CGPointMake(0, _MouthViewScroller.contentOffset.y+_stripWidth) animated:YES];
          }
     }
}

#pragma mark - JS Hair Accessories

-(void)CallJSAccesories:(NSString*)AccessoryType :(NSString*)path :(NSString*)AccID
{
     
     if(isCapSelected)
     {
          [self LoadHairAccessory:path :AccID];
     }
     else if(isCloathSelected)
     {
          [self LoadCostumeAccessory:path :AccID];
     }
}


#pragma mark - Imageview method

- (IBAction)ImageAction:(id)sender
{
     
     if ([sender isSelected])
     {
          [self HideAllViews];
          [self DisableButtonSelection];
          [self DisableMainButtons];
     }
     else
     {
          
          BOOL str =    [[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunched"];
          
          if (str == YES)
          {
               [gestureRecognizer setEnabled:YES];
               [self HideAllViews];
               [self DisableButtonSelection];
               [self DisableMainButtons];
               [self moveCharacterFirstView];
               menuViewOpenClose = YES;
               shareViewOpenClose = YES;
               [gestureRecognizerRight setEnabled:NO];
               [UIView animateWithDuration:0.5 animations:^
                {
                     __ImageView.alpha = 1;
                     _CloseSecondStripBtn.alpha = 1;
                     __ImageView.hidden=false;
                     _CloseSecondStripBtn.hidden=false;
                     
                     __ImageView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8, __ImageView.frame.origin.y, __ImageView.frame.size.width, __ImageView.frame.size.height);
                     _CloseSecondStripBtn.frame = CGRectMake(__ImageView.frame.origin.x+__ImageView.frame.size.width , _CloseSecondStripBtn.frame.origin.y, _CloseSecondStripBtn.frame.size.width, _CloseSecondStripBtn.frame.size.height);
                }];
               [__imageButton setSelected:YES];
               [self moveRightMenu];
               [self LoadImageScroller];
          }
          else
          {
               UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please run app again with working Internet to use This Functionality" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [alert show];
               
          }
     }
}

- (IBAction)ImageUpAction:(id)sender
{
     [self DisableButtonSelection];
     [__ImageUpArrow setSelected:YES];
     
     if (__ImageContentScroller.contentOffset.y > 0)
     {
          if (__ImageContentScroller.contentOffset.y < _stripWidth)
          {
               [__ImageContentScroller setContentOffset:CGPointMake(0, 0) animated:YES];
          }
          else
          {
               [__ImageContentScroller setContentOffset:CGPointMake(0, __ImageContentScroller.contentOffset.y-_stripWidth) animated:YES];
          }
     }
     else
     {
          [__ImageUpArrow setSelected:NO];
     }
}

- (IBAction)ImageDownAction:(id)sender
{
     [self DisableButtonSelection];
     [__ImageDownArrow setSelected:YES];
     
     if(__ImageContentScroller.contentOffset.y+__ImageContentScroller.frame.size.height < __ImageContentScroller.contentSize.height )
     {
          if(__ImageContentScroller.contentOffset.y+_stripWidth +__ImageContentScroller.frame.size.height> __ImageContentScroller.contentSize.height )
          {
               [__ImageContentScroller setContentOffset:CGPointMake(0, __ImageContentScroller.contentOffset.y) animated:YES];
               [__ImageDownArrow setSelected:NO];
          }
          else
          {
               [__ImageContentScroller setContentOffset:CGPointMake(0, __ImageContentScroller.contentOffset.y+_stripWidth) animated:YES];
          }
     }
}
-(void)LoadImageScroller
{
     for(UIView *subview in __ImageContentScroller.subviews)
     {
          [subview removeFromSuperview];
     }
     
     NSMutableArray *ImageScrollerArray = [[NSMutableArray alloc]initWithObjects:@"bedroom",@"bluewave",@"cafe",@"castle",@"classroom",@"disco",@"fairytale",@"lab", @"landscape",@"ski_slopes",@"soccer_stadium",@"space",@"splatter_paint",@"spotlight",@"spring",@"stage",@"supermarket",@"underthesea",@"wildwest",@"woods",nil];
     
     int x = 0;
     int y = 0;
     //     _previousImageTag = 0;
     
     [__ImageContentScroller setContentSize:CGSizeMake(__ImageContentScroller.frame.size.width, _stripWidth*[ImageScrollerArray count])];
     
     for(int i = 0; i<[ImageScrollerArray count]; i++)
     {
          UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
          [button addTarget:self action:@selector(PickImage:) forControlEvents:UIControlEventTouchUpInside];
          
          
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
               dispatch_async(dispatch_get_main_queue(), ^{
                    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[ImageScrollerArray objectAtIndex:i]]] forState:UIControlStateNormal];
                    
                    if (_previousImageTag == i+999)
                    {
                         button.layer.borderWidth = 3;
                         button.layer.cornerRadius = 3;
                         button.layer.borderColor = [[UIColor orangeColor]CGColor];
                         
                         
                         if (i > 4)
                         {
                              [UIView animateWithDuration:0.7 animations:^{
                                   [__ImageContentScroller setContentOffset:CGPointMake(0, button.frame.origin.y-(self.view.frame.size.height/2))];
                                   
                              }];
                              
                         }
                         else
                         {
                              [UIView animateWithDuration:0.7 animations:^{
                                   [__ImageContentScroller setContentOffset:CGPointMake(0, 0)];
                              }];
                         }
                    }
                    
               });
          });
          button.frame = CGRectMake(x+8, y, __ImageContentScroller.frame.size.width-16, _stripWidth-14);
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               button.frame = CGRectMake(x+8, y, __ImageContentScroller.frame.size.width-16, _stripWidth-23);
          }
          button.tag = i + 999;
          UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] init];
          [gr addTarget:self action:@selector(userLongPressed:)];
          [button addGestureRecognizer:gr];
          [__ImageContentScroller addSubview:button];
          y = y + _stripWidth;
     }
}
- (void)userLongPressed:(UILongPressGestureRecognizer*)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          NSMutableArray *ImageScrollerArray = [[NSMutableArray alloc]initWithObjects:@"Bedroom",@"Blue Wave",@"Cafe",@"Castle",@"Class Room",@"Disco",@"Fairytale",@"Lab", @"Landscape",@"Ski Slopes",@"Soccer Stadium",@"Space",@"Splatter paint",@"Spotlight",@"Spring",@"Stage",@"Supermarket",@"Under the sea",@"Wildwest",@"Woods",nil];
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
           }];
          UILongPressGestureRecognizer* gesture=(UILongPressGestureRecognizer*)sender;
          NSInteger i = gesture.view.tag - 999;
          _itemName.text = [ImageScrollerArray objectAtIndex:i];
     }
}


-(IBAction)PickImage:(id)sender
{
     saveSceneUrl = nil;
     Main_videoPath = @"";
     sameaudio = false;
     outurltweet = nil;
     
     for(UIView *subview in __ImageContentScroller.subviews)
     {
          if ([subview isKindOfClass:[UIButton class]])
          {
               UIButton* myButton1 = (UIButton*)subview;
               myButton1.layer.borderWidth = 0;
               myButton1.layer.borderColor = [[UIColor clearColor]CGColor];
          }
     }
     UIButton* myButton = (UIButton*)sender;
     myButton.layer.borderWidth = 3;
     myButton.layer.cornerRadius = 3;
     myButton.layer.borderColor = [[UIColor orangeColor]CGColor];
     
     NSMutableArray* arrayOfArr = [NSMutableArray new];
     [arrayOfArr addObjectsFromArray:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"full_BG"]]];
     
     NSString* xml_Path = @"";
     xml_Path = [NSString stringWithFormat:@"full_BG/%@",[arrayOfArr objectAtIndex:[sender tag]-999]];
     
     _previousImageTag = (int)[sender tag];
     if(xml_Path != nil)
     {
          NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
          NSMutableArray *arr = [[ParseObject sharedMethod] CallDescXML:1 :xml_Path];
          
          
          if(arr.count != 0)
          {
               NSString *Background;
               for(int i = 0; i < [[[arr objectAtIndex:0] valueForKey:@"Type"] count]; i ++)
               {
                    for(NSString *key in [[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i])
                    {
                         if([key isEqualToString:@"Background"])
                         {
                              Background = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                         }
                         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                         {
                              
                              if([key isEqualToString:@"IBackground"])
                              {
                                   Background = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                              }
                         }
                    }
               }
               backgroundID = Background;
               
               if ( [backgroundID isEqualToString:@"1464258"])
               {
                    backgroundID = @"1464247";
               }
               
               NSLog(@"your bundle id is %@",backgroundID);
               NSString *imagePth = [NSString stringWithFormat:@"%@/%@/%@",BundlePath,xml_Path,Background];
               
               NSString *jsString = [NSString stringWithFormat:@"defaultloadBackgroundImage('%@.jpg','%@','%@')",imagePth,@"auto",@"100%"];
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    
                    NSString *widthX = [NSString stringWithFormat:@"%0.0f",_canvasWidth/4.85];
                    widthX = [widthX stringByAppendingString:@"%"];
                    jsString = [NSString stringWithFormat:@"defaultloadBackgroundImage('%@.jpg','%@','%@')",imagePth,widthX,@"100%"];
                    
               }
               
               else
               {
                    if (_canvasWidth == 480)
                    {
                         jsString = [NSString stringWithFormat:@"defaultloadBackgroundImage('%@.jpg','%@','%@')",imagePth,@"auto",@"100%"];
                    }
               }
               
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
               [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               // remove all cached responses
               
          }
     }
}

#pragma mark - Scrollview Delegate Method

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//
//}

#pragma mark - third strip methods for voki, images and accessories

- (IBAction)ThirdUpAction:(id)sender
{
     _isAccessoriesSelected = YES;
     
     [self DisableButtonSelection];
     
     [__thirdUpArrow setSelected:YES];
     
     if (__thirdContentScroller.contentOffset.y > 0)
     {
          if (__thirdContentScroller.contentOffset.y < _stripWidth)
          {
               [__thirdContentScroller setContentOffset:CGPointMake(0, 0) animated:YES];
          }
          else
          {
               [__thirdContentScroller setContentOffset:CGPointMake(0, __thirdContentScroller.contentOffset.y-_stripWidth) animated:YES];
          }
     }
     else
     {
          [__thirdUpArrow setSelected:NO];
     }
}

- (IBAction)ThirdDownAction:(id)sender
{
     _isAccessoriesSelected = YES;
     
     [self DisableButtonSelection];
     
     [__thirdDownArrow setSelected:YES];
     
     if(__thirdContentScroller.contentOffset.y+__thirdContentScroller.frame.size.height < __thirdContentScroller.contentSize.height )
     {
          if(__thirdContentScroller.contentOffset.y+_stripWidth +__thirdContentScroller.frame.size.height> __thirdContentScroller.contentSize.height )
          {
               [__thirdContentScroller setContentOffset:CGPointMake(0, __thirdContentScroller.contentOffset.y) animated:YES];
               [__thirdDownArrow setSelected:NO];
          }
          else
          {
               [__thirdContentScroller setContentOffset:CGPointMake(0, __thirdContentScroller.contentOffset.y+_stripWidth) animated:YES];
          }
     }
}
#pragma mark - Close third strip
- (IBAction)CloseThirdStripAction:(id)sender
{
     [self closethirdstrip];
}

-(void)closethirdstrip
{
     _isAccessoriesSelected = NO;
     
     [UIView animateWithDuration:0.5 animations:^
      {
           _deleteGalleryBtn.alpha = 0;
           __thirdView.alpha = 0;
           _CloeseThirdStripBtn.alpha = 0;
           __shareView.alpha = 0;
           thirdView = false;
           __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
           _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
           __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
           _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
           __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           [self secondScrollerDeselectedButton];
      }];
     
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          playCharacterAud1.alpha = 0;
          
          
          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
          NSString *Xvalue =  [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"230",ypos];
          
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 1;
                }];
          });
          
     }
     else
     {
          if (_canvasWidth == 480)
          {
               playCharacterAud1.alpha = 0;
               
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue =  [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"180",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          
          else if (_canvasWidth == 736)
          {
               playCharacterAud1.alpha = 0;
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"220",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    playCharacterAud1.alpha = 1;
                    _PlayCharAud1dup.frame = playCharacterAud1.frame;
                    
               });
          }
          else if(_canvasWidth == 667)
          {
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"220",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width +20 - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          else
          {
               playCharacterAud1.alpha = 0;
               [UIView animateWithDuration:0.5 animations:^
                {
                     NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                     NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"190",ypos];
                     [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width + 25 - __mainFirstStrip.frame.size.width +__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
     }
     
     _CloseSecondStripBtn.hidden=false;
     
     
}

- (IBAction)CloseSecondStripAction:(id)sender
{
     [self HideAllViews];
     [self DisableMainButtons];
}

-(void)CallThirdStrip
{
     _CloeseThirdStripBtn.hidden=false;
     _CloseSecondStripBtn.hidden=true;
}


#pragma mark - Color methods

- (IBAction)ColorAction:(id)sender
{
     isright = false;
     issecopen = false;
     
     if ([sender isSelected])
     {
          [self HideAllViews];
          [self DisableButtonSelection];
          [self DisableMainButtons];
          
     }
     else
     {
          [gestureRecognizer setEnabled:YES];
          menuViewOpenClose = YES;
          shareViewOpenClose = YES;
          [gestureRecognizerRight setEnabled:NO];
          [self HideAllViews];
          [self DisableMainButtons];
          
          [self AddColorPickerView];
          [self moveRightMenu];
          [__colorButton setSelected:YES];
          
          
     }
}
-(void)AddColorPickerView
{
     
     NSArray *bodyTypes = [[NSArray alloc]initWithObjects:@"eyes",@"skin",@"mouth",@"hair",@"makeup",nil];
     
     NSString *isAlpha= [NSString stringWithFormat:@"isAlpha('%@')",@"blush"];
     
     NSString *isAlphaReturn = [___CharacterWebView stringByEvaluatingJavaScriptFromString:isAlpha];
     
     
     if ([isAlphaReturn isEqualToString:@"true"])
     {
          [_colorblush setEnabled:YES];
     }
     else
          
     {
          [_colorblush setEnabled:NO];
     }
     
     for (int i=0; i<bodyTypes.count; i++)
     {
          NSString * bodypart = [bodyTypes objectAtIndex:i];
          
          NSString *iscolorjs = [NSString stringWithFormat:@"iscolorBool('%@')",bodypart];
          
          NSString *iscolorRetutn = [___CharacterWebView stringByEvaluatingJavaScriptFromString:iscolorjs];
          
          if (i == 0)
          {
               if ([iscolorRetutn isEqualToString:@"true"])
               {
                    [__eyeButton setEnabled:YES];
               }
               else
                    
               {
                    [__eyeButton setEnabled:NO];
               }
               
          }
          
          else if (i ==1)
          {
               if ([iscolorRetutn isEqualToString:@"true"])
               {
                    [__handButton setEnabled:YES];
               }
               else
                    
               {
                    [__handButton setEnabled:NO];
               }
               
          }
          else if (i ==2)
          {
               if ([iscolorRetutn isEqualToString:@"true"])
               {
                    [__mouthButton setEnabled:YES];
               }
               else
                    
               {
                    [__mouthButton setEnabled:NO];
               }
               
          }
          
          else if (i==3)
               
          {
               if ([iscolorRetutn isEqualToString:@"true"])
               {
                    [__hairButton setEnabled:YES];
               }
               else
                    
               {
                    [__hairButton setEnabled:NO];
               }
               
          }
          else
               
          {
               if ([iscolorRetutn isEqualToString:@"true"])
               {
                    [_colorMakeUp setEnabled:YES];
               }
               else
               {
                    [_colorMakeUp setEnabled:NO];
               }
          }
     }
     
     
     [[NSUserDefaults standardUserDefaults]setFloat:_brightNessPicker.frame.size.width forKey:@"brightWidth"];
     [[NSUserDefaults standardUserDefaults]synchronize];
     __closePickerView.frame = CGRectMake(__colorPickerView.frame.origin.x+__colorPickerView.frame.size.width , __closePickerView.frame.origin.y, __closePickerView.frame.size.width, __closePickerView.frame.size.height);
     [UIView animateWithDuration:0.5 animations:^
      {
           __colorPickerView.alpha = 1;
           __closePickerView.alpha = 1;
           __closePickerView.hidden=false;
           __colorPickerView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8, __colorPickerView.frame.origin.y, __colorPickerView.frame.size.width, __colorPickerView.frame.size.height);
           __closePickerView.frame = CGRectMake(__colorPickerView.frame.origin.x+__colorPickerView.frame.size.width , __closePickerView.frame.origin.y, __closePickerView.frame.size.width, __closePickerView.frame.size.height);
           
           NSString *part = [NSString stringWithFormat:@"getBodyPartColor('%@')",partofBody];
           NSString *colorjs = [___CharacterWebView stringByEvaluatingJavaScriptFromString:part];
           NSString *hex = [NSString stringWithFormat:@"#%lX",
                            (unsigned long)[colorjs integerValue]];
           UIColor *color1 = [self getUIColorObjectFromHexString:hex alpha:1];
           _huePickerColor.color = color1;
           _brightNessPicker.color = color1;
      }];
     
     __colorPickerView.hidden=false;
     [self movecharact];
     
     
     if (isalphable == true)
     {
          
     }
     else
     {
     }
     
     
     
}
-(void)colorPicked:(UIColor *)newColor forPicker:(ILSaturationBrightnessPickerView *)picker
{
     [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
     [[NSUserDefaults standardUserDefaults] synchronize];
     
     saveSceneUrl = nil;
     Main_videoPath = @"";
     sameaudio = false;
     outurltweet = nil;
     const  CGFloat *components = CGColorGetComponents(newColor.CGColor);
     
     CGFloat r = components[0];
     CGFloat g = components[1];
     CGFloat b = components[2];
     
     NSString *str =  [NSString stringWithFormat:@"#%02lX%02lX%02lX",
                       lroundf(r * 255),
                       lroundf(g * 255),
                       lroundf(b * 255)];
     
     NSString *hexColor = [str stringByReplacingOccurrencesOfString:@"#" withString:@"0x"];
     dispatch_async(dispatch_get_main_queue(),^{
          NSString *jsString = [NSString stringWithFormat:@"changeEyeColorToGreen('%@','%@')",partofBody,hexColor];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
          [gestureRecognizer setEnabled:NO];
          [_pangestureMovetoChar setEnabled:NO];
     });
     
     
}
- (IBAction)ClosePickerAction:(id)sender
{
     [self DisableMainButtons];
     [self HideAllViews];
}
- (IBAction)HandAction:(id)sender
{
     
     [__mouthButton setSelected:NO];
     [__eyeButton setSelected:NO];
     [__handButton setSelected:NO];
     [__hairButton setSelected:NO];
     
     partofBody = @"skin";
     [self DisableButtonSelection];
     [__handButton setSelected:YES];
     
     NSString *colorjs = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('skin')"];
     NSString *hex = [NSString stringWithFormat:@"#%lX",
                      (unsigned long)[colorjs integerValue]];
     UIColor *color1 = [self getUIColorObjectFromHexString:hex alpha:1];
     
     
     _huePickerColor.color = color1;
     _brightNessPicker.color = color1;
     
     
     
}

- (IBAction)MouthAction:(id)sender
{
     
     [__mouthButton setSelected:NO];
     [__eyeButton setSelected:NO];
     [__handButton setSelected:NO];
     [__hairButton setSelected:NO];
     
     partofBody = @"mouth";
     [self DisableButtonSelection];
     [__mouthButton setSelected:YES];
     NSString *colorjs = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('mouth')"];
     
     NSString *hex = [NSString stringWithFormat:@"#%lX",
                      (unsigned long)[colorjs integerValue]];
     
     UIColor *color1 = [self getUIColorObjectFromHexString:hex alpha:1];
     _huePickerColor.color = color1;
     _brightNessPicker.color = color1;
}

- (IBAction)EyeAction:(id)sender
{
     
     [__mouthButton setSelected:NO];
     [__eyeButton setSelected:NO];
     [__handButton setSelected:NO];
     [__hairButton setSelected:NO];
     
     partofBody = @"eyes";
     [self DisableButtonSelection];
     [__eyeButton setSelected:YES];
     
     NSString *colorjs = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('eyes')"];
     NSString *hex = [NSString stringWithFormat:@"#%lX",
                      (unsigned long)[colorjs integerValue]];
     UIColor *color1 = [self getUIColorObjectFromHexString:hex alpha:1];
     _huePickerColor.color = color1;
     _brightNessPicker.color = color1;
}
- (IBAction)HairAction:(id)sender
{
     [__mouthButton setSelected:NO];
     [__eyeButton setSelected:NO];
     [__handButton setSelected:NO];
     [__hairButton setSelected:NO];
     
     partofBody = @"hair";
     
     [self DisableButtonSelection];
     [__hairButton setSelected:YES];
     NSString *colorjs = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('hair')"];
     NSString *hex = [NSString stringWithFormat:@"#%lX",
                      (unsigned long)[colorjs integerValue]];
     UIColor *color1 = [self getUIColorObjectFromHexString:hex alpha:1];
     _huePickerColor.color = color1;
     _brightNessPicker.color = color1;
}
- (IBAction)makeUp:(id)sender
{
     partofBody = @"makeup";
     
     [self DisableButtonSelection];
     
     NSString *colorjs = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('makeup')"];
     NSString *hex = [NSString stringWithFormat:@"0x%lX",
                      (unsigned long)[colorjs integerValue]];
     UIColor *color1 = [self getUIColorObjectFromHexString:hex alpha:1];
     [NKcolorPickerView  setColor:color1];
}
- (IBAction)blush:(id)sender
{
     isalphable = true;
     NSString *colorjs = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getAlpha('blush')"];
     
}

#pragma mark - Voice methods

- (IBAction)VoiceAction:(id)sender
{
     
     [UIView animateWithDuration:0.1 animations:^{
          
          allcatagoryview.hidden = true;
          playCharacterAud1.hidden = false;
          playCharacterAud1.alpha = 1;
          
     }];
     issavescean = false;
     isright = false;
     issecopen = false;
     if ([sender isSelected])
     {
          [self HideAllViews];
          
          [self DisableButtonSelection];
          [self DisableMainButtons];
          
     }
     else
     {
          
          [gestureRecognizer setEnabled:YES];
          menuViewOpenClose = YES;
          shareViewOpenClose = YES;
          [gestureRecognizerRight setEnabled:NO];
          if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
          {
               alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
               blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
               alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
               alertVc.deleteView.hidden = true;
               alertVc.ratealert.hidden = true;
               alertVc.blackview1.hidden = true;
               alertVc.unblockvookichar.hidden = true;
               alertVc.whiteborderView.hidden = false;
               [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
               [self.view addSubview:blurViewCalledbuy];
          }
          else
          {
               [self HideAllViews];
               [self DisableButtonSelection];
               [self DisableMainButtons];
               [__voiceButton setSelected:YES];
               [__miceButton setSelected:YES];
               [self moveRightMenu];
               __closePickerView.frame = CGRectMake(__recordVoiceView.frame.origin.x+__recordVoiceView.frame.size.width , __closePickerView.frame.origin.y, __closePickerView.frame.size.width, __closePickerView.frame.size.height);
               [UIView animateWithDuration:0.5 animations:^
                {
                     __voiceTextView.alpha = 1;
                     __recordVoiceView.alpha = 1;
                     __closePickerView.alpha = 1;
                     __closePickerView.hidden=false;
                     __closePickerView.hidden=false;
                     __colorPickerView.hidden = false;
                     
                     __voiceTextView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8, __voiceTextView.frame.origin.y, __voiceTextView.frame.size.width, __voiceTextView.frame.size.height);
                     
                     __recordVoiceView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8, __recordVoiceView.frame.origin.y, __recordVoiceView.frame.size.width, __recordVoiceView.frame.size.height);
                     __closePickerView.frame = CGRectMake(__recordVoiceView.frame.origin.x+__recordVoiceView.frame.size.width , __closePickerView.frame.origin.y, __closePickerView.frame.size.width, __closePickerView.frame.size.height);
                }];
               __recordVoiceView.hidden=false;
               __closePickerView.hidden=false;
               
               if (__voiceTextView.hidden==false)
               {
                    __recordVoiceView.hidden=true;
                    [__typeButton setSelected:YES];
               }
               [self movecharactVoice];
               
               
          }
     }
}
- (IBAction)TypeAction:(id)sender
{
     [self DisableButtonSelection];
     [__typeButton setSelected:YES];
     
     __voiceTextView.hidden=false;
     __recordVoiceView.hidden=true;
     
     __closePickerView.hidden=false;
     __closePickerView.frame = CGRectMake(__voiceTextView.frame.origin.x+__voiceTextView.frame.size.width , __closePickerView.frame.origin.y, __closePickerView.frame.size.width, __closePickerView.frame.size.height);
     
}

- (IBAction)MiceAction:(id)sender
{
     [self.view endEditing:YES];
     
     [self DisableButtonSelection];
     [__miceButton setSelected:YES];
     
     __voiceTextView.hidden=true;
     __recordVoiceView.hidden=false;
}

- (IBAction)RecordAction:(id)sender
{
     
     [UIView animateWithDuration:0.1 animations:^{
          
          allcatagoryview.hidden = true;
          playCharacterAud1.hidden = false;
          playCharacterAud1.alpha = 1;
          
     }];
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.whiteborderView.hidden = false;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.blackview1.hidden = true;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
     }
     else
     {
          
          saveSceneUrl = nil;
          Main_videoPath = @"";
          sameaudio = false;
          outurltweet = nil;
          recording = true;
          [audioActivity stopAnimating];
          
          [_playBtn setSelected:NO];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"stop_mp3()"];
          [_playBtn setBackgroundImage:[UIImage imageNamed:@"playsel.png"] forState:UIControlStateNormal];
          [_stopBtn setBackgroundImage:[UIImage imageNamed:@"playUnsel.png"] forState:UIControlStateNormal];
          UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
          UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
          [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
          [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
          
          if(audioPlayer.playing)
          {
               [audioPlayer stop];
               audioPlayer=nil;
          }
          
          issavescean = false;
          if(!_isRecording)
          {
               
               
               switch ([[AVAudioSession sharedInstance] recordPermission]) {
                    case AVAudioSessionRecordPermissionGranted:
                    {
                         _isRecording = YES;
                         recordCounting = 0;
                         [self ProgressIncrementValue];
                         
                         [__recordButton setBackgroundImage:[UIImage imageNamed:@"StopBtn.png"] forState:UIControlStateNormal];
                         
                         
                         _progressTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ShowProgress) userInfo:nil repeats:YES];
                         
                         [self startRecording];
                         
                         
                         
                         
                    }
                         break;
                    case AVAudioSessionRecordPermissionDenied:
                    {
                         NSLog(@"Permission denied");
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"Allow Voki to Use Microphone from Settings" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alert show];
                    }
                         break;
                    case AVAudioSessionRecordPermissionUndetermined:
                    {
                         [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                              if (granted)
                              {
                                   
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        _isRecording = YES;
                                        recordCounting = 0;
                                        [self ProgressIncrementValue];
                                        
                                        [__recordButton setBackgroundImage:[UIImage imageNamed:@"StopBtn.png"] forState:UIControlStateNormal];
                                        
                                        
                                        _progressTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ShowProgress) userInfo:nil repeats:YES];
                                        
                                        [self startRecording];
                                   });
                                   
                                   
                                   
                              }
                              else {
                                   NSLog(@"Permission denied");
                              }
                         }];
                         
                    }
                         break;
                    default:
                         break;
               }
               
               
               
          }
          else
          {
               [self StopProgress];
               [self stopRecording];
               
               __uptoTimeLbl.text = @"upto 60 sec long";
          }
     }
}

#pragma mark - Load Voice text view drop down tables listing

-(void)LoadLanguageArray
{
     __text_voiceIDTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
     
     _textLanguageID = 1;
     _textVoiceID=3;
     engineID = 3;
     
     __LanguageLbl.text = @"English";
     __nameLbl.text = @"Julie";
     
     _textLanguageArray = [[NSMutableArray alloc]initWithObjects:@"Arabic:27",@"Catalan:5",@"Chinese:10",@"Danish:19",@"Dutch:11",@"English:1",@"Esperanto:31",@"Finnish:23",@"French:4",@"Galician:15",@"German:3",@"Greek:8",@"Italian:7",@"Japanese:12",@"Korean:13",@"Norwegian:20",@"Polish:14",@"Portuguese:6",@"Romanian:30",@"Russian:21",@"Spanish:2",@"Swedish:9",@"Turkish:16", nil];
     
     _textMainVoiceIDArray = [[NSMutableArray alloc]initWithObjects:@"1:Alan (Australian):9:2",@"1:Allison (US):7:2",@"1:Catherine (UK):6:2",@"1:Dave (US):2:2",@"1:Elizabeth (UK):4:2",@"1:Grace (Australian):10:2",@"1:Simon (UK):5:2",@"1:Steven (US):8:2",@"1:Susan (US):1:2",@"1:Veena (Indian):11:2",@"2:Carmen (Castilian):1:2",@"2:Juan (Castilian):2:2",@"2:Francisca (Chilean):3:2",@"2:Diego (Argentine):4:2",@"2:Esperanza (Mexican):5:2",@"2:Jorge (Castilian):6:2",@"2:Carlos (American):7:2",@"2:Soledad (American):8:2",@"2:Leonor (Castilian):9:2",@"2:Ximena:10:2",@"3:Stefan:2:2",@"3:Katrin:3:2",@"4:Bernard:2:2",@"4:Jolie:3:2",@"4:Florence:4:2",@"4:Charlotte:5:2",@"4:Olivier:6:2",@"5:Montserrat:1:2",@"5:Jordi:2:2",@"5:Empar (Valencian):3:2",@"6:Gabriela (Brazilian):1:2",@"6:Amalia (European):2:2",@"6:Eusebio (European):3:2",@"6:Fernanda (Brazilian):4:2",@"6:Felipe (Brazilian):5:2",@"7:Paola:1:2",@"7:Silvana:2:2",@"7:Valentina:3:2",@"7:Luca:5:2",@"7:Marcello:6:2",@"7:Roberto:7:2",@"7:Matteo:8:2",@"7:Giulia:9:2",@"7:Federica:10:2",@"8:Afroditi:1:2",@"8:Nikos:3:2",@"9:Annika:1:2",@"9:Sven:2:2",@"10:Linlin (Mandarin):1:2",@"10:Lisheng (Mandarin):2:2",@"11:Willem:1:2",@"11:Saskia:2:2",@"14:Zosia:1:2",@"14:Krzysztof:2:2",@"15:Carmela:1:2",@"16:Kerem:1:2",@"16:Zeynep:2:2",@"16:Selin:3:2",@"19:Frida:1:2",@"19:Magnus:2:2",@"20:Vilde:1:2",@"20:Henrik:2:2",@"21:Olga:1:2",@"21:Dmitri:2:2",@"23:Milla:1:2",@"23:Marko:2:2",@"27:Tarik:1:2",@"27:Laila:2:2",@"30:Ioana:1:2",@"31:Ludoviko:1:2",@"1:Kate (US):1:3",@"1:Paul (US):2:3",@"1:Julie (US):3:3",@"1:Bridget (UK):4:3",@"1:Hugh (UK):5:3",@"1:Ashley (US):6:3",@"1:James (US):7:3",@"2:Violeta:1:3",@"2:Francisco (Mexican):2:3",@"4:Chloe (Canadian):1:3",@"10:Lily (Mandarin):1:3",@"10:Hui (Mandarin):3:3",@"10:Liang (Mandarin):4:3",@"12:Show:2:3",@"12:Misaki:3:3",@"13:Yumi:1:3",@"13:Junwoo:2:3", nil];
     
     
     
     [__textLanguageTable reloadData];
     
     dispatch_time_t deferTime = 4.0f;
     dispatch_after(deferTime, dispatch_get_main_queue(), ^{
          [self ReloadVoiceIDTable];
          
     });
     
}


-(void)ReloadVoiceIDTable
{
     if(_textVoiceArray.count == 0)
     {
          _textVoiceArray = [[NSMutableArray alloc]init];
     }
     else
     {
          [_textVoiceArray removeAllObjects];
     }
     
     for(int x=0; x<_textMainVoiceIDArray.count; x++)
     {
          NSArray* arr = [[_textMainVoiceIDArray objectAtIndex:x] componentsSeparatedByString:@":"];
          if(_textLanguageID == [[arr objectAtIndex:0] intValue])
          {
               [_textVoiceArray addObject:[_textMainVoiceIDArray objectAtIndex:x]];
          }
     }
     __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, self.view.frame.origin.y + 56, __text_VoiceIDView.frame.size.width, _cancelBtnTable.frame.origin.y-66);
     
     
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          if (_textVoiceArray.count ==1)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-20, __text_VoiceIDView.frame.size.width, 60);
          }
          
          if (_textVoiceArray.count ==2)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-80, __text_VoiceIDView.frame.size.width, 120);
          }
          if (_textVoiceArray.count ==3)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-140, __text_VoiceIDView.frame.size.width, 180);
          }
          if (_textVoiceArray.count ==5)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-260, __text_VoiceIDView.frame.size.width, 300);
          }
          if (_textVoiceArray.count ==6)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-320, __text_VoiceIDView.frame.size.width, 360);
          }
          
     }
     
     else
     {
          if (_textVoiceArray.count ==1)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-50, __text_VoiceIDView.frame.size.width, 70);
          }
          
          if (_textVoiceArray.count ==2)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-60, __text_VoiceIDView.frame.size.width, 80);
          }
          if (_textVoiceArray.count ==3)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-100, __text_VoiceIDView.frame.size.width, 120);
          }
          if (_textVoiceArray.count ==5)
          {
               __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-180, __text_VoiceIDView.frame.size.width, 200);
          }
          
     }
     
     [__text_voiceIDTable reloadData];
}

#pragma mark - wexewtView Delegate Method



- (BOOL)textView:(UITextView*) textView shouldChangeTextInRange: (NSRange) range replacementText: (NSString*) text
{
     //     if (textfeil)
     //          if ([textField.text length] > kMaxTextFieldStringLength)
     //               return NO;
     //          else
     //               return YES;
     //     }
     if (textView == txtShareText) {
          
          NSString* newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
          
          if (int(140 - newText.length) >= 0)
          {
               lblCharaterCount.text = [NSString stringWithFormat:@"%lu", (140 - newText.length)];
               
          }
          
          return textView.text.length + (text.length - range.length) <= 140;
     }
     
     if (textView  ==  __descriptionTextView )
     {
          
          
          if ([textView isFirstResponder])
          {
               if ([[[textView textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textView textInputMode] primaryLanguage])
               {
                    return NO;
               }
          }
          if ([text isEqualToString:@"\n"])
          {
               [textView resignFirstResponder];
               return YES;
          }
          
          return  [[textView text] length] - range.length + text.length <= 600;
          
     }
     else
     {
          
          if ([text isEqualToString:@"\n"])
          {
               [textView resignFirstResponder];
               return NO;
          }
          return YES;
     }
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
     if(textView==txtShareText && textView.textColor == [UIColor lightGrayColor]){
          
          txtShareText.text = @"";
          txtShareText.textColor = [UIColor blackColor];
     }
     
     return YES;
     
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
     if(textView==txtShareText){
          
          return;
     }
     
     if([textView.text isEqualToString:@"Enter message"])
     {
          textView.text = nil;
     }
}

-(void) textViewDidChange:(UITextView *)textView
{
     if(txtShareText.text.length == 0 && textView==txtShareText){
          
          lblCharaterCount.text = @"140";
          txtShareText.textColor = [UIColor lightGrayColor];
          txtShareText.text = @"Write your message here..";
          [txtShareText resignFirstResponder];
     }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
     
     if(textView==txtShareText){
          
          return;
     }
     
     
     if(textView.text.length == 0)
     {
          textView.text = @"Enter message";
     }
     
     else
     {
          if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
          {
               alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
               blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
               alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
               alertVc.deleteView.hidden = true;
               alertVc.ratealert.hidden = true;
               alertVc.blackview1.hidden = true;
               alertVc.unblockLearnmore.hidden = true;
               alertVc.unblockvookichar.hidden = true;
               alertVc.whiteborderView.hidden = false;
               [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
               [self.view addSubview:blurViewCalledbuy];
               
               
          }
          else
          {
               recording = true;
               saveSceneUrl = nil;
               Main_videoPath = @"";
               sameaudio = false;
               outurltweet = nil;
               isTextLangTableVisible = NO;
               __text_Languageview.hidden=true;
               
               //          if ([__LanguageLbl.text isEqualToString:@"English"])
               //          {
               //               engineID = 3;
               //               if ([genderID isEqualToString:@"1"])
               //               {
               //                    __nameLbl.text = @"Julie";
               //                    _textVoiceID = 3;
               //               }
               //               if ([genderID isEqualToString:@"2"])
               //               {
               //                    __nameLbl.text = @"Paul";
               //                    _textVoiceID = 2;
               //
               //               }
               //               if ([genderID isEqualToString:@"4"])
               //               {
               //                    __nameLbl.text = @"Julie";
               //                    _textVoiceID = 3;
               //
               //               }
               //               if ([genderID isEqualToString:@"3"])
               //               {
               //                    __nameLbl.text = @"Julie";
               //                    _textVoiceID = 3;
               //               }
               //          }
               
               
               if(![__descriptionTextView.text isEqualToString:@"Enter message"]||![__descriptionTextView.text isEqualToString:@""])
               {
                    saveCharAudio = false;
                    finaltextvoiceID = _textVoiceID;
                    NSString* SoundLink1 = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID]:[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :__descriptionTextView.text :@"mp3" :fxtype :fxlevel :@"5603124" :@"" :@"evt6bu4n!sDk"];
                    
                    
                    //               NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"[,?\\.`\"]"
                    //                                                                                           options:0
                    //                                                                                             error:NULL];
                    //
                    //               NSString *cleanedString = [expression stringByReplacingMatchesInString:SoundLink1
                    //                                                                              options:0
                    //                                                                                range:NSMakeRange(0, SoundLink1.length)
                    //                                                                         withTemplate:@""];
                    
                    
                    
                    NSString *newString = [SoundLink1 stringByReplacingOccurrencesOfString:@"[!?., ]+" withString:@"" options: NSRegularExpressionSearch range:NSMakeRange(0, SoundLink1.length)];
                    NSLog(@"\"%@\" was converted to \"%@\"", SoundLink1, newString);
                    
                    NSURL *URL = [NSURL URLWithString:SoundLink1];
                    NSData *data = [NSData dataWithContentsOfURL:URL];
                    
                    if (data == nil) {
                         
                         
                    }
                    NSString *appDocDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                    NSString *storePath = [appDocDir stringByAppendingPathComponent:@"Mp3File.mp3"];
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:storePath])
                    {
                         [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
                    }
                    
                    BOOL success = [data writeToFile:storePath atomically:YES];
                    
                    if (success) {
                         [[NSUserDefaults standardUserDefaults] setObject:storePath forKey:@"AudioPath"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                    } else {
                         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AudioPath"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    
                    [[NSUserDefaults standardUserDefaults]setValue:SoundLink1 forKey:@"saveTTSLink"];
                    
                    NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink1];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
                    
                    [self loaderposition];
                    
                    UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
                    UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
                    [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
                    [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
                    
               }
          }
     }
}

- (IBAction)DropDownAction:(id)sender
{
     ttsAudioRecord = true;
     if(isFxTableVisible)
     {
          isFxTableVisible = NO;
          _fxMenuView.hidden=true;
     }
     else
     {
          isFxTableVisible = YES;
          _fxMenuView.hidden = false;
          __text_VoiceIDView.hidden=true;
          __text_Languageview.hidden=true;
          _BgViewForTable.hidden = false;
          _cancelBtnTable.hidden = false;
     }
     
}



- (IBAction)Text_LanguageAction:(id)sender
{
     if(isTextLangTableVisible)
     {
          isTextLangTableVisible = NO;
          __text_Languageview.hidden=true;
     }
     else
     {
          isTextLangTableVisible = YES;
          __text_Languageview.hidden=false;
          __text_VoiceIDView.hidden=true;
          _fxMenuView.hidden = true;
          _BgViewForTable.hidden = false;
          _cancelBtnTable.hidden = false;
     }
}


- (void)tappedTextView:(UITapGestureRecognizer *)tapGesture {
     if (tapGesture.state != UIGestureRecognizerStateEnded) {
          return;
     }
     
     UITextView *textView = (UITextView *)tapGesture.view;
     CGPoint tapLocation = [tapGesture locationInView:textView];
     UITextPosition *textPosition = [textView closestPositionToPoint:tapLocation];
     NSDictionary *attributes = [textView textStylingAtPosition:textPosition inDirection:UITextStorageDirectionForward];
     
     NSURL *url = attributes[NSLinkAttributeName];
     
     if (url) {
          [[UIApplication sharedApplication] openURL:url];
     }
}

#pragma mark - UITableView Delegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     if(tableView == __textLanguageTable)
     {
          return [_textLanguageArray count];
     }
     else if(tableView==_fxTableView)
     {
          return [fxmenuItems count];
     }
     else if(tableView == tableViewAccounts) //prabhjot
     {
          return TwitterAccountArray.count;
     }
     return [_textVoiceArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          return 60;
     }
     else
     {
          return 40;
     }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if(tableView == __textLanguageTable)
     {
          static NSString *CellIdentifier = @"textTableCell";
          textTableCell *Cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
          if (Cell == nil)
          {
               Cell = [[textTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
          }
          NSArray *textArray = [[_textLanguageArray objectAtIndex:indexPath.row] componentsSeparatedByString:@":"];
          Cell.backgroundColor = [UIColor clearColor];
          Cell.selectionStyle = UITableViewCellSelectionStyleNone;
          Cell._textTable_lbl.text = [NSString stringWithFormat:@"%@",[textArray objectAtIndex:0]];
          Cell._textTable_lbl.textAlignment = NSTextAlignmentCenter;
          Cell.layoutMargins = UIEdgeInsetsZero;
          Cell.preservesSuperviewLayoutMargins = false;
          tableView.separatorInset = UIEdgeInsetsZero;
          return Cell;
     }
     else if (tableView == _fxTableView)
     {
          static NSString *CellIdentifier = @"fxmenuxell";
          FXmenucell *Cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
          if (Cell == nil)
          {
               Cell = [[FXmenucell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
          }
          Cell.backgroundColor = [UIColor clearColor];
          Cell.selectionStyle = UITableViewCellSelectionStyleNone;
          Cell.fxmenu.text = [fxmenuItems[indexPath.row]valueForKey:@"NAME"];
          Cell.fxmenu.textAlignment = NSTextAlignmentCenter;
          Cell.layoutMargins = UIEdgeInsetsZero;
          Cell.preservesSuperviewLayoutMargins = false;
          tableView.separatorInset = UIEdgeInsetsZero;
          
          return Cell;
     }
     
     //prabhjot
     else if (tableView == tableViewAccounts){
          
          UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
          UILabel *lbltext = [cell viewWithTag:10];
          lbltext.text = [TwitterAccountArray objectAtIndex:indexPath.row];
          return cell;
          
     }
     
     else
     {
          static NSString *CellIdentifier = @"voiceIDCell";
          voiceIDCell *Cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
          if (Cell == nil)
          {
               Cell = [[voiceIDCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
          }
          NSArray *sortedStrings =
          [_textVoiceArray sortedArrayUsingSelector:@selector(compare:)];
          NSArray *textArray = [[sortedStrings objectAtIndex:indexPath.row] componentsSeparatedByString:@":"];
          
          Cell.backgroundColor = [UIColor clearColor];
          Cell.selectionStyle = UITableViewCellSelectionStyleNone;
          Cell._voiceID_lbl.text = [NSString stringWithFormat:@"%@",[textArray objectAtIndex:1]];
          Cell._voiceID_lbl.textAlignment = NSTextAlignmentCenter;
          
          //  NSArray *CurrentArray = [[sortedStrings objectAtIndex:0] componentsSeparatedByString:@":"];
          
          //_textVoiceID =  [[NSString stringWithFormat:@"%@",[CurrentArray objectAtIndex:2]]intValue];
          
          //          if ([__LanguageLbl.text isEqualToString:@"English"])
          //          {
          //               engineID = 3;
          //               if ([genderID isEqualToString:@"1"])
          //               {
          //                    __nameLbl.text = @"Julie";
          //                    _textVoiceID = 3;
          //               }
          //               if ([genderID isEqualToString:@"2"])
          //               {
          //                    __nameLbl.text = @"Paul";
          //                    _textVoiceID = 2;
          //
          //               }
          //               if ([genderID isEqualToString:@"4"])
          //               {
          //                    __nameLbl.text = @"Julie";
          //                    _textVoiceID = 3;
          //
          //               }
          //               if ([genderID isEqualToString:@"3"])
          //               {
          //                    __nameLbl.text = @"Julie";
          //                    _textVoiceID = 3;
          //               }
          //          }
          Cell.layoutMargins = UIEdgeInsetsZero;
          Cell.preservesSuperviewLayoutMargins = false;
          tableView.separatorInset = UIEdgeInsetsZero;
          
          return Cell;
     }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
     //prabhjot
     
     isTextLangTableVisible = NO;
     __text_Languageview.hidden=true;
     isFxTableVisible = NO;
     _fxMenuView.hidden = true;
     isVoiceTableVisible = NO;
     __text_VoiceIDView.hidden=true;
     
     recording = true;
     [audioActivity removeFromSuperview];
     saveSceneUrl = nil;
     Main_videoPath = @"";
     sameaudio = false;
     outurltweet = nil;
     
     if(tableView == __textLanguageTable)
     {
          isTextLangTableVisible = NO;
          __text_Languageview.hidden=true;
          
          NSArray *textArray = [[_textLanguageArray objectAtIndex:indexPath.row] componentsSeparatedByString:@":"];
          __LanguageLbl.text = [NSString stringWithFormat:@"%@",[textArray objectAtIndex:0]];
          _textLanguageID = [[textArray objectAtIndex:1] intValue];
          _BgViewForTable.hidden = true;
          _cancelBtnTable.hidden = true;
          [self ReloadVoiceIDTable];
          NSArray *sortedStrings =
          [_textVoiceArray sortedArrayUsingSelector:@selector(compare:)];
          
          if ([__LanguageLbl.text isEqualToString:@"English"])
          {
               engineID = 3;
               if ([genderID isEqualToString:@"1"])
               {
                    __nameLbl.text = @"Julie";
                    _textVoiceID = 3;
               }
               if ([genderID isEqualToString:@"2"])
               {
                    __nameLbl.text = @"Paul";
                    _textVoiceID = 2;
                    
               }
               if ([genderID isEqualToString:@"4"])
               {
                    __nameLbl.text = @"Julie";
                    _textVoiceID = 3;
                    
               }
               if ([genderID isEqualToString:@"3"])
               {
                    __nameLbl.text = @"Julie";
                    _textVoiceID = 3;
               }
          }
          else
          {
               
               NSArray *textArray1 = [[sortedStrings objectAtIndex:0] componentsSeparatedByString:@":"];
               _textVoiceID = [[textArray1 objectAtIndex:2] intValue];
               engineID = [[textArray1 objectAtIndex:3] intValue];
               __nameLbl.text =  [NSString stringWithFormat:@"%@",[textArray1 objectAtIndex:1]];
               
          }
          if(![__descriptionTextView.text isEqualToString:@"Enter message"]&&![__descriptionTextView.text isEqualToString:@""])
          {                    [Activityindicator startAnimating];
               
               NSLog(@"%@",__descriptionTextView.text);
               saveCharAudio = false;
               finaltextvoiceID = _textVoiceID;
               SoundLink = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID]:[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :__descriptionTextView.text :@"mp3" :fxtype :fxlevel :@"5603124" :@"" :@"evt6bu4n!sDk"];
               
               playTime++;
               [audioActivity stopAnimating];
               NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink];
               
               
               NSString *str =  [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
               NSLog(@"string is %lu",(unsigned long)str.length);
               [self loaderposition];
               [[NSUserDefaults standardUserDefaults]setValue:SoundLink forKey:@"saveTTSLink"];
               
               
               
               //               NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"[,?\\.`\"]"
               //                                                                                           options:0
               //                                                                                             error:NULL];
               //
               //               NSString *cleanedString = [expression stringByReplacingMatchesInString:SoundLink1
               //                                                                              options:0
               //                                                                                range:NSMakeRange(0, SoundLink1.length)
               //                                                                         withTemplate:@""];
               
               
               
               NSString *newString = [SoundLink stringByReplacingOccurrencesOfString:@"[!?., ]+" withString:@"" options: NSRegularExpressionSearch range:NSMakeRange(0, SoundLink.length)];
               NSLog(@"\"%@\" was converted to \"%@\"", SoundLink, newString);
               
               NSURL *URL = [NSURL URLWithString:SoundLink];
               NSData *data = [NSData dataWithContentsOfURL:URL];
               
               if (data == nil) {
                    
                    
               }
               NSString *appDocDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
               NSString *storePath = [appDocDir stringByAppendingPathComponent:@"Mp3File.mp3"];
               
               if ([[NSFileManager defaultManager] fileExistsAtPath:storePath])
               {
                    [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
               }
               
               BOOL success = [data writeToFile:storePath atomically:YES];
               
               if (success) {
                    [[NSUserDefaults standardUserDefaults] setObject:storePath forKey:@"AudioPath"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
               } else {
                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AudioPath"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
               }
               
               UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
               UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
               [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
               [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
          }
          
          
          
     }
     else if (tableView == _fxTableView)
     {
          
          isFxTableVisible = NO;
          _fxMenuView.hidden = true;
          
          if ( ttsAudioRecord == true)
          {
               
               NSString *name    = [fxmenuItems[indexPath.row]valueForKey:@"NAME"];
               if ([name isEqualToString:@"none"])
               {
                    __voiceTypeLBL.text = @"FX";
               }
               else
               {
                    __voiceTypeLBL.text = name;
                    
               }
               recordFXtypeLevel =  [fxmenuItems[indexPath.row]valueForKey:@"PARAM"];
               
               if (recordWithFXPath)
               {
                    [[NSUserDefaults standardUserDefaults] setObject:recordWithFXPath forKey:@"AudioPath"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    WithFxvalue = true;
                    [self audioApi:recordWithFXPath];
                    
               }
               
          }
          else
          {
               
               NSArray *sortedStrings =
               [_textVoiceArray sortedArrayUsingSelector:@selector(compare:)];
               for (int c =0 ;c<[sortedStrings count]; c++)
               {
                    NSArray *text = [[sortedStrings objectAtIndex:c] componentsSeparatedByString:@":"];
                    NSString *voicename = [text objectAtIndex:1];
                    _BgViewForTable.hidden = true;
                    _cancelBtnTable.hidden = true;
                    
                    
                    if ([voicename isEqualToString:__nameLbl.text])
                    {
                         NSArray *textArray = [[sortedStrings objectAtIndex:c] componentsSeparatedByString:@":"];
                         
                         _textVoiceID = [[textArray objectAtIndex:2] intValue];
                         engineID = [[textArray objectAtIndex:3] intValue];
                         
                    }
                    else if ( [voicename isEqualToString:@"Julie (US)"])
                    {
                         NSArray *textArray = [[sortedStrings objectAtIndex:c] componentsSeparatedByString:@":"];
                         _textVoiceID = [[textArray objectAtIndex:2] intValue];
                         engineID = [[textArray objectAtIndex:3] intValue];
                    }
                    
               }
               
               //   __fileTypeLbl.text = [fxmenuItems[indexPath.row]valueForKey:@"NAME"];
               
               
               NSString *name    = [fxmenuItems[indexPath.row]valueForKey:@"NAME"];
               if ([name isEqualToString:@"none"])
               {
                    __fileTypeLbl.text = @"FX";
               }
               else
               {
                    __fileTypeLbl.text = name;
                    
               }
               fxtype = [fxTypes objectAtIndex:indexPath.row];
               
               fxlevel = [fxLevels objectAtIndex:indexPath.row];
               _BgViewForTable.hidden = true;
               _cancelBtnTable.hidden = true;
               
               if(![__descriptionTextView.text isEqualToString:@"Enter message"]&&![__descriptionTextView.text isEqualToString:@""])
               {
                    [Activityindicator startAnimating];
                    saveCharAudio = false;
                    finaltextvoiceID = _textVoiceID;
                    
                    NSString* SoundLink1 = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID]:[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :__descriptionTextView.text :@"mp3" :fxtype :fxlevel :@"5603124" :@"" :@"evt6bu4n!sDk"];
                    
                    NSString *encodedString = SoundLink1;
                    SoundLink1 = [NSString stringWithUTF8String:[encodedString cStringUsingEncoding:[NSString defaultCStringEncoding]]];
                    
                    
                    
                    
                    playTime++;
                    
                    [[NSUserDefaults standardUserDefaults]setValue:SoundLink1 forKey:@"saveTTSLink"];
                    [audioActivity stopAnimating];
                    NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink1];
                    NSLog(@"Play Time ... %d %@",playTime, audio);
                    
                    NSString *str =    [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
                    NSLog(@"string is %lu",(unsigned long)str.length);
                    [self loaderposition];
                    
                    NSString *newString = [SoundLink1 stringByReplacingOccurrencesOfString:@"[!?., ]+" withString:@"" options: NSRegularExpressionSearch range:NSMakeRange(0, SoundLink1.length)];
                    NSLog(@"\"%@\" was converted to \"%@\"", SoundLink1, newString);
                    
                    NSURL *URL = [NSURL URLWithString:SoundLink1];
                    NSData *data = [NSData dataWithContentsOfURL:URL];
                    
                    if (data == nil) {
                         
                         
                    }
                    NSString *appDocDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                    NSString *storePath = [appDocDir stringByAppendingPathComponent:@"Mp3File.mp3"];
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:storePath])
                    {
                         [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
                    }
                    
                    BOOL success = [data writeToFile:storePath atomically:YES];
                    
                    if (success) {
                         [[NSUserDefaults standardUserDefaults] setObject:storePath forKey:@"AudioPath"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                    } else {
                         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AudioPath"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    
                    [Activityindicator stopAnimating];
                    
                    UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
                    UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
                    [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
                    [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
               }
               
          }
          _BgViewForTable.hidden = true;
          _cancelBtnTable.hidden = true;
     }
     else
     {
          isVoiceTableVisible = NO;
          __text_VoiceIDView.hidden=true;
          NSArray *sortedStrings =
          [_textVoiceArray sortedArrayUsingSelector:@selector(compare:)];
          NSArray *textArray = [[sortedStrings objectAtIndex:indexPath.row] componentsSeparatedByString:@":"];
          
          if (sortedStrings.count>1)
          {
               
               _BgViewForTable.hidden = true;
               _cancelBtnTable.hidden = true;
               __nameLbl.text = [NSString stringWithFormat:@"%@",[textArray objectAtIndex:1]];
          }
          else
          {
               _BgViewForTable.hidden = true;
               _cancelBtnTable.hidden = true;
               __nameLbl.text = [NSString stringWithFormat:@"%@",[textArray objectAtIndex:0]];
          }
          _textVoiceID = [[textArray objectAtIndex:2] intValue];
          engineID = [[textArray objectAtIndex:3] intValue];
     
          if(![__descriptionTextView.text isEqualToString:@"Enter message"]&&![__descriptionTextView.text isEqualToString:@""])
          {
               [audioActivity startAnimating];
               saveCharAudio = false;
               finaltextvoiceID = _textVoiceID;
               NSString* SoundLink1 = [[ServerConnection sharedMethod]SendDataToServer:[NSString stringWithFormat:@"%d",engineID]:[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :__descriptionTextView.text :@"mp3" :fxtype :fxlevel :@"5603124" :@"" :@"evt6bu4n!sDk"];
               
               playTime++;
               
               [[NSUserDefaults standardUserDefaults]setValue:SoundLink1 forKey:@"saveTTSLink"];
               [audioActivity stopAnimating];
               
               NSString *audio = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink1];
               
               NSLog(@"Play Time ... %d %@",playTime, audio);
               
               NSString *str =   [___CharacterWebView stringByEvaluatingJavaScriptFromString:audio];
               NSLog(@"string is %lu",(unsigned long)str.length);
               [self loaderposition];
               NSString *newString = [SoundLink1 stringByReplacingOccurrencesOfString:@"[!?., ]+" withString:@"" options: NSRegularExpressionSearch range:NSMakeRange(0, SoundLink1.length)];
               NSLog(@"\"%@\" was converted to \"%@\"", SoundLink1, newString);
               
               NSURL *URL = [NSURL URLWithString:SoundLink1];
               NSData *data = [NSData dataWithContentsOfURL:URL];
               
               if (data == nil) {
                    
                    
               }
               NSString *appDocDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
               NSString *storePath = [appDocDir stringByAppendingPathComponent:@"Mp3File.mp3"];
               
               if ([[NSFileManager defaultManager] fileExistsAtPath:storePath])
               {
                    [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
               }
               
               BOOL success = [data writeToFile:storePath atomically:YES];
               
               if (success) {
                    [[NSUserDefaults standardUserDefaults] setObject:storePath forKey:@"AudioPath"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
               } else {
                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AudioPath"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
               }
               
               UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
               UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
               [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
               [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
          }
     }
     
}

- (IBAction)Text_nameAction:(id)sender
{
     if(isVoiceTableVisible)
     {
          isVoiceTableVisible = NO;
          __text_VoiceIDView.hidden=true;
     }
     else
     {
          isVoiceTableVisible = YES;
          __text_VoiceIDView.hidden=false;
          __text_Languageview.hidden=true;
          _fxMenuView.hidden = true;
          _BgViewForTable.hidden = false;
          _cancelBtnTable.hidden = false;
          
          __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, self.view.frame.origin.y + 56, __text_VoiceIDView.frame.size.width, _cancelBtnTable.frame.origin.y-66);
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               if (_textVoiceArray.count ==1)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-20, __text_VoiceIDView.frame.size.width, 60);
               }
               if (_textVoiceArray.count ==2)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-80, __text_VoiceIDView.frame.size.width, 120);
               }
               if (_textVoiceArray.count ==3)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-140, __text_VoiceIDView.frame.size.width, 180);
               }
               if (_textVoiceArray.count ==5)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-260, __text_VoiceIDView.frame.size.width, 300);
               }
               if (_textVoiceArray.count ==6)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-320, __text_VoiceIDView.frame.size.width, 360);
               }
          }
          else
          {
               if (_textVoiceArray.count ==1)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-50, __text_VoiceIDView.frame.size.width, 70);
               }
               
               if (_textVoiceArray.count ==2)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-60, __text_VoiceIDView.frame.size.width, 80);
               }
               if (_textVoiceArray.count ==3)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-100, __text_VoiceIDView.frame.size.width, 120);
               }
               if (_textVoiceArray.count ==5)
               {
                    __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, _cancelBtnTable.frame.origin.y-_cancelBtnTable.frame.size.height-180, __text_VoiceIDView.frame.size.width, 200);
               }
          }
     }
}

- (IBAction)Text_fileTypeAction:(id)sender
{
     ttsAudioRecord = false;
     if(isFxTableVisible)
     {
          isFxTableVisible = NO;
          _fxMenuView.hidden=true;
     }
     else
     {
          isFxTableVisible = YES;
          _fxMenuView.hidden = false;
          __text_VoiceIDView.hidden=true;
          __text_Languageview.hidden=true;
          _BgViewForTable.hidden = false;
          _cancelBtnTable.hidden = false;
     }
     
}
- (IBAction)submitTextAction:(id)sender
{
     if([__descriptionTextView.text isEqualToString:@"Enter message"])
     {
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"Please enter text" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
     }
     else
     {
          isVoiceTableVisible = NO;
          __text_VoiceIDView.hidden=true;
          isTextLangTableVisible = NO;
          __text_Languageview.hidden=true;
          [self ShowLoader];
          SoundLink = [[ServerConnection sharedMethod]SendDataToServer:@"3" :[NSString stringWithFormat:@"%d",_textLanguageID] :[NSString stringWithFormat:@"%d",_textVoiceID] :__descriptionTextView.text :@"mp3" :@"" :@"" :@"5603124" :@"" :@"evt6bu4n!sDk"];
          
          [self StopLoader];
          
          [[NSUserDefaults standardUserDefaults]setValue:SoundLink forKey:@"SOUND_LINK"];
          [[NSUserDefaults standardUserDefaults]synchronize];
          
          if([SoundLink rangeOfString:@"http://cache.oddcast.com"].location == NSNotFound)
          {
               UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:SoundLink delegate: nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
               [alert show];
               
          }
          else
          {
               NSString *javascriptString = [NSString stringWithFormat:@"play_mp3_2('%@')",SoundLink];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:javascriptString];
          }
     }
}
#pragma mark - Record voice progress view
-(void)hideplayCharacterAud1
{
     self.playCharacterAud.frame = playCharacterAud1.frame;
     [self.captureview addSubview:playCharacterAud1];
     playCharacterAud1.alpha = 1;
     playCharacterAud1 .hidden = false;
     
}
-(void)ShowLoader
{
     playCharacterAud1.alpha = 0;
     playCharacterAud1 .hidden = true;
     __loadeMainView.hidden = NO;
     [__ActivityIndicator startAnimating];
}

-(void)checkFirstTimeOldData{
   
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          [self removeTimer];
          Sliding_view.hidden = true;
          [self  hideplayCharacterAud1];
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@" Download Assets need Internet connection & Your connection seems to be unavailable. Please run app again with Working Internet To use All Functionality";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
     }
     
     else
     {
          NSFileManager *fileManager = [NSFileManager defaultManager];
          NSArray *URLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
          NSURL *documentsDirectory = URLs[0];
          NSURL *destinationPath = [documentsDirectory URLByAppendingPathComponent:@"ios-data-v1.zip"];
          BOOL fileExists = [fileManager fileExistsAtPath:[destinationPath path]];
          
          imageCount = 0;
          imageViewForDownloadingImages.image = [UIImage imageNamed:[splashArray objectAtIndex:imageCount]];
          vokilogo_img.hidden = true;
          
          if (fileExists == true)
          {
               _downloadview.hidden = false;
               Sliding_view.hidden = false;
               
               NSFileManager *fileManager = [NSFileManager defaultManager];
               NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
               
               NSString *filePath = [documentsPath stringByAppendingPathComponent:@"ios-data-v1.zip"];
               NSError *error;
               BOOL success = [fileManager removeItemAtPath:filePath error:&error];
               if (success) {
                    NSLog(@"remove ios-data-v1.zip success");
               }else{
                    
               }
              NSString *filePath2 = [documentsPath stringByAppendingPathComponent:@"unZipDirName"];
               BOOL success1 = [fileManager removeItemAtPath:filePath2 error:&error];
               if (success1) {
                    NSLog(@"remove unZipDirName success");
               }else{
                    
               }
               
               sliding = [NSTimer scheduledTimerWithTimeInterval:9.3f target:self selector:@selector(slidingcalled) userInfo:nil repeats:NO];
               [[NSRunLoop mainRunLoop] addTimer:sliding forMode:NSRunLoopCommonModes];
               [self saveMp3Files];
               
          }
          else
          {
               _downloadview.hidden = false;
               Sliding_view.hidden = false;
               
               sliding = [NSTimer scheduledTimerWithTimeInterval:9.3f target:self selector:@selector(slidingcalled) userInfo:nil repeats:NO];
               [[NSRunLoop mainRunLoop] addTimer:sliding forMode:NSRunLoopCommonModes];
               [self saveMp3Files];
          }
     }
     
}

-(void)StopLoader
{
     __loadeMainView.hidden = YES;
     
     [__ActivityIndicator stopAnimating];
     
     //check the old data in the application
     BOOL isOldData =    [[NSUserDefaults standardUserDefaults]boolForKey:@"isOldData"];
     
     if (isOldData == NO)
     {
          [self  checkFirstTimeOldData];
          
     }else{
     
     BOOL str =    [[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunched"];
     if (str == NO)
     {
          
          if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
          {
               [self removeTimer];
               Sliding_view.hidden = true;
               [self  hideplayCharacterAud1];
               alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
               blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
               alertVc.lblText.text=@" Download Assets need Internet connection & Your connection seems to be unavailable. Please run app again with Working Internet To use All Functionality";
               alertVc.deleteView.hidden = true;
               alertVc.ratealert.hidden = true;
               alertVc.blackview1.hidden = true;
               alertVc.unblockvookichar.hidden = true;
               alertVc.unblockLearnmore.hidden = true;
               alertVc.whiteborderView.hidden = false;
               [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
               [self.view addSubview:blurViewCalledbuy];
          }
          
          else
          {
               
               NSFileManager *fileManager = [NSFileManager defaultManager];
               NSArray *URLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
               NSURL *documentsDirectory = URLs[0];
               NSURL *destinationPath = [documentsDirectory URLByAppendingPathComponent:@"ios-data-v1.zip"];
               BOOL fileExists = [fileManager fileExistsAtPath:[destinationPath path]];
               
               imageCount = 0;
               imageViewForDownloadingImages.image = [UIImage imageNamed:[splashArray objectAtIndex:imageCount]];
               vokilogo_img.hidden = true;
               
               if (fileExists == true)
               {
                    dispatch_async(dispatch_get_main_queue(), ^{
                         
                         self.unzipview.hidden = false;
                         self.backgroundTask  = nil;
                         if (_backgroundTask.state == NSURLSessionTaskStateRunning)
                         {
                              [_backgroundTask cancel];
                         }
                         Sliding_view.hidden = false;
                         _downloadview.hidden = false;
                         download_installimg.image = [UIImage imageNamed:@"installing"];
                         
                         sliding = [NSTimer scheduledTimerWithTimeInterval:9.3f target:self selector:@selector(slidingcalled) userInfo:nil repeats:NO];
                         
                         [[NSRunLoop mainRunLoop] addTimer:sliding forMode:NSRunLoopCommonModes];
                         [self unzipFile:documentsDirectory];
                    });
               }
               else
               {
                    _downloadview.hidden = false;
                    Sliding_view.hidden = false;
                    
                    
                    sliding = [NSTimer scheduledTimerWithTimeInterval:9.3f target:self selector:@selector(slidingcalled) userInfo:nil repeats:NO];
                    [[NSRunLoop mainRunLoop] addTimer:sliding forMode:NSRunLoopCommonModes];
                    [self saveMp3Files];
               }
          }
     }
     else
     {
         [[UIApplication sharedApplication] setIdleTimerDisabled: NO]; // Keep alive false

          vokilogo_img.hidden = false;
          _downloadview.hidden = true;
          // But when you read the file you have to use absolute path + relative path:
          Sliding_view.hidden = true;
          NSFileManager *fileManager = [NSFileManager defaultManager];
          NSString *fullCachePath = ((NSURL*)[[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] ).path;
          output = [fullCachePath stringByAppendingPathComponent:@"unZipDirName"];
          [self savecharactersimage];
          [self removeTimer];
          [self performSelector:@selector(hideplayCharacterAud1) withObject:nil afterDelay:0.50];
     }
   }
     
}
- (void)slidingcalled
{
     NSLog(@"%d",imageCount);
     imageCount++;
     
     if (imageCount == splashArray.count)
     {
          imageCount = 0;
          imageViewForDownloadingImages.image = [UIImage imageNamed:[splashArray objectAtIndex:splashArray.count-1]];
          imageView2ForDownloadingImages.image = [UIImage imageNamed:[splashArray objectAtIndex:imageCount]];
     }
     else
     {
          imageViewForDownloadingImages.image = [UIImage imageNamed:[splashArray objectAtIndex:imageCount-1]];
          imageView2ForDownloadingImages.image = [UIImage imageNamed:[splashArray objectAtIndex:imageCount]];
          
     }
     
     
     imageViewForDownloadingImages.alpha = 1;
     imageView3ForDownloadingImages.alpha = 1;
     imageView2ForDownloadingImages.alpha = 0;
     
     
     [UIView animateWithDuration:4.0
                           delay:0.0
                         options:UIViewAnimationOptionCurveEaseIn
                      animations:^{
                           
                           imageViewForDownloadingImages.alpha = 0;
                           
                           
                      }
                      completion:^(BOOL finished){
                           
                      }
      
      ];
     
     [UIView animateWithDuration:2.0
                           delay:0.0
                         options:UIViewAnimationOptionCurveEaseIn
                      animations:^{
                           
                           imageView2ForDownloadingImages.alpha = 1;
                      }
                      completion:^(BOOL finished){
                           
                           [self performSelector:@selector(slidingcalled) withObject:nil afterDelay:9.0];
                           
                      }
      ];
     
     
     //     // 1.back to the middle of sections
     //     NSIndexPath *currentIndexPathReset = [self resetIndexPath];
     //     // 2.next position
     //     NSInteger nextItem = currentIndexPathReset.item + 1;
     //     if (nextItem == [splashArray count]) {
     //          nextItem = 0;
     //
     //     }
     //     NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:0];
     //
     //     [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
     //
     //          [sliding_collectionview scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
     //
     //
     //     }completion:^(BOOL finished){
     //
     //     }];
     // 3.scroll to next position
     
}


- (NSIndexPath *)resetIndexPath
{
     
     
     // NSInteger currentIndex = sliding_collectionview .contentOffset.x / sliding_collectionview.frame.size.width;
     
     // currentIndexPath
     NSIndexPath *currentIndexPath = [[sliding_collectionview indexPathsForVisibleItems] lastObject];
     // back to the middle of sections
     NSIndexPath *currentIndexPathReset = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:0];
     // [sliding_collectionview scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
     return currentIndexPathReset;
}
- (void)removeTimer
{
     Sliding_view.hidden = true;
     
     // stop NSTimer
     [sliding invalidate];
     // clear NSTimer
     sliding = nil;
}

-(void)savecharactersimage
{
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
          
          NSMutableArray* arrayOfArr = [NSMutableArray new];
          [arrayOfArr addObjectsFromArray:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char"]]];
          NSArray* Array = (NSArray*)arrayOfArr;
          VokiScrollerArray = [[NSMutableArray alloc]init];
          [VokiScrollerArray addObjectsFromArray:Array];
          Allcaracters =[[NSMutableArray alloc]init];
          NSMutableArray *arr = [[NSMutableArray alloc]init];
          
          NSString *output_path = [NSString stringWithFormat:@"%@",output];
          
          for(int i = 0; i < arrayOfArr.count; i++)
          {
               
               NSArray* Array =[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",[arrayOfArr objectAtIndex:i]]];
               
               if(Array.count == 1)
               {
                    if([[Array objectAtIndex:0] isEqualToString:@"icons"])
                    {
                         [arrayOfArr removeObjectAtIndex:i];
                    }
               }
               
          }
          
          for (int i =0; i<[VokiScrollerArray count];i++)
          {
               NSString *path =   [NSString stringWithFormat:@"assets/img/char/%@",[VokiScrollerArray objectAtIndex:i]];
               NSArray* Array = [self RemoveIconImage:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",[VokiScrollerArray objectAtIndex:i]]]];
               
               for (int j=0;j< [Array count];j++)
               {
                    NSString *str = [Array objectAtIndex:j];
                    
                    if ([arr containsObject:str])
                    {
                         
                         
                    }
                    else
                    {
                         [arr addObject:[Array objectAtIndex:j]];
                         NSString *image_Path = @"";
                         if(Array.count == 0)
                         {
                              image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/icons/base/btn_128x128",path] ofType:@"png" inDirectory:@""];
                         }
                         else
                         {
                              NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                              
                              image_Path = [myBundle  pathForResource:[NSString stringWithFormat:@"%@/%@/thumb_128x128",path,[Array objectAtIndex:j]] ofType:@"png" inDirectory:@""];
                         }
                         if(image_Path == nil)
                         {
                              
                         }
                         else
                         {
                              [ Allcaracters addObject: image_Path];
                         }
                    }
                    
               }
               
          }
          dispatch_async(dispatch_get_main_queue(), ^{
               
          });
     });
     
}
-(void)StopProgress
{
     [__recordButton setBackgroundImage:[UIImage imageNamed:@"RecBtn.png"] forState:UIControlStateNormal];
     _isRecording = NO;
     [_progressTimer invalidate];
     _progressTimer = nil;
     
     _progressIncrement = 0;
     __progressImage.frame = CGRectMake(__progressImage.frame.origin.x, __progressImage.frame.origin.y,0 , __progressImage.frame.size.height);
     
     __recordYourVoiceLbl.text = @"record your voice";
}
-(void)ProgressIncrementValue
{
     _progressIncrement = __ProgressBGImage.frame.size.width / 60;
}
- (void)ShowProgress
{
     if(__progressImage.frame.size.width >= (__ProgressBGImage.frame.size.width - 14))
     {
          [_progressTimer invalidate];
          _progressTimer = nil;
          __uptoTimeLbl.text = @"upto 60 sec long";
          [self stopRecording];
          [self StopProgress];
     }
     else
     {
          NSString *imageWidth = [NSString stringWithFormat:@"%f",__progressImage.frame.size.width + _progressIncrement];
          __progressImage.frame = CGRectMake(__progressImage.frame.origin.x, __progressImage.frame.origin.y,imageWidth.floatValue-0.2, __progressImage.frame.size.height);
          
          recordCounting = recordCounting + 1;
          
          if (recordCounting >60)
          {
               [_progressTimer invalidate];
               _progressTimer = nil;
               __uptoTimeLbl.text = @"upto 60 sec long";
               [self stopRecording];
               [self StopProgress];
               
          }
          __uptoTimeLbl.text = [NSString stringWithFormat:@"%i sec...",recordCounting];
     }
}


#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

- (void) startRecording
{
     WithFxvalue = false;
     
     [self stopPlaying:@""];
     
     [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
     
     AVAudioSession *audioSession = [AVAudioSession sharedInstance];
     NSError *err = nil;
     [audioSession setCategory :AVAudioSessionCategoryRecord error:&err];
     
     if(err)
     {
          return;
     }
     
     [audioSession setActive:YES error:&err];
     
     err = nil;
     
     if(err)
     {
          return;
     }
     
     NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSNumber numberWithFloat: 44100.0],                  AVSampleRateKey,
                               [NSNumber numberWithInt:  kAudioFormatLinearPCM],                   AVFormatIDKey,
                               [NSNumber numberWithInt: 2],                              AVNumberOfChannelsKey,
                               [NSNumber numberWithInt: AVAudioQualityHigh],                       AVEncoderAudioQualityKey,
                               nil];
     
     
     
     recorderFilePath = [NSString stringWithFormat:@"%@/track", DOCUMENTS_FOLDER];
     
     //NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
     NSURL *url= [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"RecordedFile"]];
     err = nil;
     recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:settings error:&err];
     
     if(!recorder)
     {
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Warning" message: [err localizedDescription] delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
          return;
     }
     
     //prepare to record
     [recorder setDelegate:self];
     [recorder prepareToRecord];
     recorder.meteringEnabled = YES;
     
     // start recording
     [recorder recordForDuration:(NSTimeInterval) 90];
     __recordYourVoiceLbl.text = @"recording...";
}
- (void) stopRecording
{
     [self loaderposition];
     //[audioActivity stopAnimating];
     [self.view setUserInteractionEnabled:NO];
     //[audioActivity removeFromSuperview];
     
     // add prabhjot
     [audioActivity stopAnimating];
     audioActivity= [[UIActivityIndicatorView alloc]
                     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
     audioActivity.center=self.view.center;
     audioActivity.frame = CGRectMake(playCharacterAud1.frame.origin.x+20,playCharacterAud1.frame.origin.y-30,35,35);
     audioActivity.color = [UIColor blackColor];
     [audioActivity startAnimating];
     [self.view addSubview:audioActivity];

     
     
     [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
     AVAudioSession *audioSession = [AVAudioSession sharedInstance];
     NSError *err = nil;
     [audioSession setCategory :AVAudioSessionCategoryPlayback error:&err];
     [recorder stop];
     
     [NSThread detachNewThreadSelector:@selector(toMp3) toTarget:self withObject:nil];
     
     if ([[NSFileManager defaultManager] isWritableFileAtPath:recorderFilePath])
     {
          NSLog(@"Writable");
     }
     else
     {
          NSLog(@"Not Writable");
     }
     __recordYourVoiceLbl.text = @"record your voice";
}


#pragma mark - Audio Player

- (IBAction)startPlaying:(id)sender
{
     if(!_isRecording)
     {
          if(recorderFilePath.length != 0)
          {
               if(soundID)
               {
                    AudioServicesDisposeSystemSoundID(soundID);
               }
               //Get a URL for the sound file
               NSURL *filePath = [NSURL fileURLWithPath:recorderFilePath isDirectory:NO];
               //Use audio sevices to create the sound
               AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
               //Use audio services to play the sound
               AudioServicesPlaySystemSound(soundID);
               
               NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@", recorderFilePath]];
               NSError *error;
               
               audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
               [audioPlayer prepareToPlay];
               audioPlayer.numberOfLoops = 0;
               audioPlayer.delegate = self;
               [audioPlayer play];
               
               [_playBtn setBackgroundImage:[UIImage imageNamed:@"playSel.png"] forState:UIControlStateNormal];
               [_stopBtn setBackgroundImage:[UIImage imageNamed:@"playUnsel.png"] forState:UIControlStateNormal];
               
          }
          else
          {
               [[[UIAlertView alloc] initWithTitle:@"No recording found!!!" message:@"Please record the voice." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
          }
     }
}

- (IBAction)stopPlaying:(id)sender
{
     [_playBtn setBackgroundImage:[UIImage imageNamed:@"playUnsel.png"] forState:UIControlStateNormal];
     [_stopBtn setBackgroundImage:[UIImage imageNamed:@"playSel.png"] forState:UIControlStateNormal];
     
     if(audioPlayer.playing)
     {
          [audioPlayer stop];
          audioPlayer=nil;
     }
}
#pragma mark - Left Share methods

- (IBAction)ShareAction:(id)sender
{
     
     [UIView animateWithDuration:0.1 animations:^{
          
          allcatagoryview.hidden = true;
          playCharacterAud1.alpha = 1;
          playCharacterAud1.hidden = false;
          [Appbtn setSelected:false];
     }];
     
     [self DisableButtonSelection];
     deleteImageIndex = 0;
     [self LoadShareScroller];
     [UIView animateWithDuration:0.5 animations:^
      {
           __vokiDiceBtn.alpha = 1;
           __vokiDiceBtn.hidden = false;
           _VokiDiceImage.alpha = 1;
           _VokiDiceImage.hidden = false;
           //_deleteGalleryBtn.alpha = 1;
           __thirdView.alpha = 0;
           __shareView.alpha = 1;
           _CloeseThirdStripBtn.alpha = 1;
           __shareView.hidden=false;
           _CloeseThirdStripBtn.hidden=false;
           thirdView = false;
           __shareView.frame = CGRectMake(__vokiView.frame.origin.x + __vokiView.frame.size.width + 8, __shareView.frame.origin.y, __shareView.frame.size.width, __shareView.frame.size.height);
           _CloeseThirdStripBtn.frame = CGRectMake(__shareView.frame.origin.x+__shareView.frame.size.width , _CloseSecondStripBtn.frame.origin.y, _CloseSecondStripBtn.frame.size.width, _CloseSecondStripBtn.frame.size.height);
           
           _deleteGalleryBtn.frame = CGRectMake(_CloeseThirdStripBtn.frame.origin.x , self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
           if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
           {
                _deleteGalleryBtn.frame = CGRectMake(_CloeseThirdStripBtn.frame.origin.x , self.view.frame.size.height - _deleteGalleryBtn.frame.size.width-30, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
           }
           __thirdView.frame = CGRectMake(__vokiView.frame.origin.x , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
           __vokiDiceBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           _VokiDiceImage.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           _deleteGalleryBtn.hidden = false;
      }];
     [self CallThirdStrip];
     [self CharacterMove];
     [__shareButton setSelected:YES];
     
     if (VokiContentArray.count == 0)
     {
          __vokiDiceBtn.alpha = 0;
          _VokiDiceImage.alpha = 0;
     }
     else
     {
          __vokiDiceBtn.alpha = 1;
          _VokiDiceImage.alpha = 1;
     }
}


- (IBAction)deleteGalleryImageBtn:(id)sender
{
     alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
     blurViewCalled = [[[alertVc showPopUp] subviews] objectAtIndex:0];
     alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
     alertVc.deleteView.hidden = false;
     alertVc.ratealert.hidden = true;
     alertVc.unblockvookichar.hidden = true;
     alertVc.whiteborderView.hidden = true;
     alertVc.blackview1.hidden= true;
     [alertVc.deleteCloseBtn addTarget:self action:@selector(dismissView:) forControlEvents:UIControlEventTouchUpInside];
     [alertVc.nobtn addTarget:self action:@selector(dismissView:) forControlEvents:UIControlEventTouchUpInside];
     [alertVc.yesBtn addTarget:self action:@selector(dismissImage:) forControlEvents:UIControlEventTouchUpInside];
     [blurViewCalled1 removeFromSuperview];
     [blurViewCalled removeFromSuperview];
     [self.view addSubview:blurViewCalled];
}


-(void)LoadShareScroller
{
     
     
     @autoreleasepool {
          
          
          for(UIView *subview in __shareScroller.subviews)
          {
               [subview removeFromSuperview];
          }
          
          NSArray *ScrollerArray = [[NSUserDefaults standardUserDefaults]valueForKey:@"saveToLib"];
          int x = 0;
          int y = 0;
          _previousShareTag = 0;
          
          [__shareScroller setPagingEnabled:YES];
          [__shareScroller setContentSize:CGSizeMake(__shareScroller.frame.size.width, _stripWidth*[ScrollerArray count]+100)];
          
          for(int i = 0; i<[ScrollerArray count]; i++)
          {
               _deleteGalleryBtn.alpha = 1;
               
               UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
               [button addTarget:self action:@selector(PickShareImage:) forControlEvents:UIControlEventTouchUpInside];
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    NSString *base64=[[ScrollerArray valueForKey:@"image64"] objectAtIndex:i];
                    NSURL *url =[NSURL URLWithString:base64];
                    NSData *imageData = [NSData dataWithContentsOfURL:url];
                    UIImage *image1 = [UIImage imageWithData:imageData];
                    UIImage *ret = [self resizeimage:image1];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [button setImage:ret forState:UIControlStateNormal];
                         [button setBackgroundColor:[UIColor whiteColor]];
                         button.layer.cornerRadius = 5;
                         button.clipsToBounds= YES;
                    });
               });
               
               _deleteGalleryBtn.hidden = false;
               
               button.frame = CGRectMake(x+8, y, __shareScroller.frame.size.width-16, _stripWidth-14);
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    button.frame = CGRectMake(x+8, y, __shareScroller.frame.size.width-16, _stripWidth-23);
                    
               }
               
               button.tag = i + 444;
               [__shareScroller addSubview:button];
               y = y + _stripWidth;
          }
          
          [self.view setUserInteractionEnabled:YES];
          [__saveToLibBtn setEnabled:YES];
     }
}
-(IBAction)PickShareImage:(id)sender
{
     [self.view setUserInteractionEnabled:false];
     saveSceneUrl = nil;
     Main_videoPath = @"";
     sameaudio = false;
     saveCharAudio = true;
     for(UIView *subview in __shareScroller.subviews)
     {
          
          if ([subview isKindOfClass:[UIButton class]])
          {
               UIButton* myButton1 = (UIButton*)subview;
               myButton1.layer.borderWidth = 0;
               myButton1.layer.borderColor = [[UIColor clearColor]CGColor];
               [(UIButton *)subview setSelected:NO];
               
          }
     }
     
     UIButton* myButton = (UIButton*)sender;
     myButton.layer.borderWidth = 3;
     myButton.layer.borderColor = [[UIColor orangeColor]CGColor];
     if(_previousShareTag != 0)
     {
          UIButton *button = (UIButton *)[__shareScroller viewWithTag:_previousShareTag];
          [button setSelected:NO];
     }
     
     UIButton *button1 = (UIButton *)[__shareScroller viewWithTag:[sender tag]];
     [button1 setSelected:YES];
     deleteImageIndex = (int)[sender tag]-444;
     NSMutableArray *shareScrollerArray = [[NSUserDefaults standardUserDefaults]valueForKey:@"saveToLib"];
     NSMutableString *xml_Path = [NSMutableString alloc];
     NSMutableString *xml_Path1 = [NSMutableString alloc];
     LibPath = [[shareScrollerArray valueForKey:@"lib"] objectAtIndex:[sender tag]-444];
     xml_Path = [[shareScrollerArray valueForKey:@"lib"] objectAtIndex:[sender tag]-444];
     xml_Path1 = [[shareScrollerArray valueForKey:@"arr1"] objectAtIndex:[sender tag]-444];
     selectedCategory = [[shareScrollerArray valueForKey:@"arr2"] objectAtIndex:[sender tag]-444];
     
     if ( selectedCategory == [NSNull null] || selectedCategory == nil || selectedCategory == NULL)
     {
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"something wrong" delegate: nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
          [alert show];
     }
     else
     {
          [[NSUserDefaults standardUserDefaults]setValue:selectedCategory forKey:@"selectedCategory"];
          
          
          spriteSheetSelectedChar = [[shareScrollerArray valueForKey:@"arr3"] objectAtIndex:[sender tag]-444];
          
          RUNNING_CHARACTER = xml_Path1;
          NSRange range = [xml_Path rangeOfString:@"unZipDirName/"];
          
          if (range.location == NSNotFound)
          {
          }
          else
          {
               NSMutableString *subStr = [[NSMutableString alloc]init];
               subStr = [xml_Path substringWithRange:NSMakeRange(range.location + range.length, xml_Path.length -(range.location + range.length))];
               
               subStr = [subStr stringByReplacingOccurrencesOfString:@"/thumb_128x128.png" withString:@""];
               
               
               NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
               NSMutableArray *arr = [[ParseObject sharedMethod] CallDescXML:1 :subStr];
               
               if(arr.count != 0)
               {
                    NSString *Costume,*fhair,*Glasses,*Hair,*Hat,*mouth,*Necklace,*Props;
                    NSString *stracc = [[shareScrollerArray valueForKey:@"accessory"] objectAtIndex:[sender tag]-444];
                    NSArray* foo = [stracc componentsSeparatedByString: @"/oh."];
                    NSString *str = [foo objectAtIndex:0];
                    NSArray *ar = [str componentsSeparatedByString: @"/"];
                    
                    for (int i =0; i<ar.count; i++)
                    {
                         if (i==2)
                         {
                              Costume = [ar objectAtIndex:i];
                         }
                         if (i==3)
                         {
                              mouth = [ar objectAtIndex:i];
                         }
                         if (i==4)
                         {
                              Hair = [ar objectAtIndex:i];
                              
                              if ([Hair isEqualToString:@"0"])
                              {
                                   Hair = @"858";
                              }
                         }
                         if (i==5)
                         {
                              fhair = [ar objectAtIndex:i];
                         }
                         if (i==6)
                         {
                              Hat = [ar objectAtIndex:i];
                         }
                         if (i==7)
                         {
                              Necklace = [ar objectAtIndex:i];
                         }
                         if (i==8)
                         {
                              Glasses = [ar objectAtIndex:i];
                         }
                         if (i==11)
                         {
                              Props = [ar objectAtIndex:i];
                         }
                    }
                    
                    NSString *eye=[[shareScrollerArray valueForKey:@"eye"] objectAtIndex:[sender tag]-444];
                    NSString *hex = [NSString stringWithFormat:@"0x%lX",
                                     (unsigned long)[eye integerValue]];
                    
                    NSString *mouthC=[[shareScrollerArray valueForKey:@"mouth"] objectAtIndex:[sender tag]-444];
                    NSString *hex1 = [NSString stringWithFormat:@"0x%lX",
                                      (unsigned long)[mouthC integerValue]];
                    
                    
                    NSString *skinC=[[shareScrollerArray valueForKey:@"skin"] objectAtIndex:[sender tag]-444];
                    NSString *hex3 = [NSString stringWithFormat:@"0x%lX",
                                      (unsigned long)[skinC integerValue]];
                    
                    
                    NSString *hairC=[[shareScrollerArray valueForKey:@"hair"] objectAtIndex:[sender tag]-444];
                    NSString *hex2 = [NSString stringWithFormat:@"0x%lX",
                                      (unsigned long)[hairC integerValue]];
                    
                    NSString *jsString = [NSString stringWithFormat:@"RunXMLfileWithColor('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",BundlePath,[[arr objectAtIndex:0] valueForKey:@"spriteSheet"],[[arr objectAtIndex:0] valueForKey:@"colorString"],Costume ?: @"0",fhair?: @"0",Glasses?: @"0",Hair?: @"0",Hat?: @"0",mouth?: @"0",Necklace?: @"969",Props?: @"0",@"eyes",hex,@"skin",hex3,@"hair",hex2,@"mouth",hex1];
                    
                    jsString = [jsString stringByReplacingOccurrencesOfString:@"World" withString:@"Historical Figures"];
                    
                    
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                    
                    // remove all cached responses
                    //               [[NSURLCache sharedURLCache] removeAllCachedResponses];
                    //
                    //               // set an empty cache
                    //               NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
                    //               [NSURLCache setSharedURLCache:sharedCache];
                    
                    xpos = [[arr objectAtIndex:0] valueForKey:@"XPOS"];
                    yposvalue = [[arr objectAtIndex:0] valueForKey:@"YPOS"];
                    scalevalue = [[arr objectAtIndex:0] valueForKey:@"SCALE"];
                    
                    NSString *pos = [NSString stringWithFormat:@"positionChar('%@','%@','%@')",@"0.14",yposvalue,scalevalue];
                    
                    
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:pos];
                    genderID = [[arr objectAtIndex:0] valueForKey:@"genderID"];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"enableScreen()"];
                    
                    if ([__LanguageLbl.text isEqualToString: @"English"])
                    {
                         
                         
                         //                         if ([genderID isEqualToString:@"1"])
                         //                         {
                         //                              _textVoiceID=3;
                         //                              engineID = 3;
                         //
                         //                         }
                         //                         if ([genderID isEqualToString:@"2"])
                         //                         {
                         //                              _textVoiceID=2;
                         //                              engineID = 3;
                         //
                         //                         }
                         //                         if ([genderID isEqualToString:@"4"])
                         //                         {
                         //                              _textVoiceID=3;
                         //                              engineID = 3;
                         //                         }
                         //                         if ([genderID isEqualToString:@"3"])
                         //                         {
                         //                              _textVoiceID=3;
                         //                              engineID = 3;
                         //                         }
                    }
                    [self loaderposition];
                    
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
                    NSString *islogin = [[NSUserDefaults standardUserDefaults]
                                         stringForKey:@"islogin"];
                    
                    //To wheck wheather sharing should availble or not !
                    if (![islogin  isEqual: @"yes"] )
                    {
                         if (isbuyall == NO)
                         {
                              if ([soldcharcters containsObject:xml_Path1]|| [freecharacetr containsObject:xml_Path1])
                              {
                                   
                                   _Purchasedollerimg.alpha = 0;
                                   _sidedollar.alpha = 0;
                                   
                              }
                              else
                              {
                                   _Purchasedollerimg.alpha = 1;
                                   _sidedollar.alpha = 1;
                                   _sidedollar.enabled = true;
                                   _Purchasedollerimg.hidden = false;
                                   _sidedollar.hidden = false;
                                   
                              }
                              
                         }
                         else
                         {
                              _Purchasedollerimg.alpha = 0;
                              _sidedollar.alpha = 0;
                         }
                         
                    }
                    
                    else
                    {
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         
                    }
                    [self performSelector:@selector(LoadVokiScroller) withObject:self afterDelay:0.100];
                    
               }
               
               else
               {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"Work under progress" delegate: nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                    [alert show];
                    
               }
          }
     }
     [self.view setUserInteractionEnabled:true];
     
     
}

- (IBAction)SaherUpAction:(id)sender
{
     [self DisableButtonSelection];
     [__shareUpArrowBtn setSelected:YES];
     
     if (__shareScroller.contentOffset.y > 0)
     {
          if (__shareScroller.contentOffset.y < _stripWidth)
          {
               [__shareScroller setContentOffset:CGPointMake(0, 0) animated:YES];
          }
          else
          {
               [__shareScroller setContentOffset:CGPointMake(0, __shareScroller.contentOffset.y-_stripWidth) animated:YES];
          }
     }
     else
     {
          [__shareUpArrowBtn setSelected:NO];
     }
}

- (IBAction)ShareDownAction:(id)sender
{
     [self DisableButtonSelection];
     [__shareDownArrowBtn setSelected:YES];
     
     if(__shareScroller.contentOffset.y+__shareScroller.frame.size.height < __shareScroller.contentSize.height )
     {
          if(__shareScroller.contentOffset.y+_stripWidth +__shareScroller.frame.size.height> __shareScroller.contentSize.height )
          {
               [__shareScroller setContentOffset:CGPointMake(0, __shareScroller.contentOffset.y) animated:YES];
               [__shareDownArrowBtn setSelected:NO];
          }
          else
          {
               [__shareScroller setContentOffset:CGPointMake(0, __shareScroller.contentOffset.y+_stripWidth) animated:YES];
          }
     }
}
- (IBAction)SaveToLibAction:(id)sender
{
     
     
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          if (memorywarningcount!=0)
          {
               if (memorywarningcount >= 2 && memorywarningcount <=5)
               {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"You would receive memory warning please delete some of your characters in save Libraury" delegate: nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                    [alert show];
               }
               else
               {
                    if (memorywarningcount ==1)
                    {
                         
                    }
                    else
                    {
                         isenable = true;
                         [__saveToLibBtn setEnabled:false];
                    }
               }
          }
     }
     
     if (_canvasWidth <= 570 ||  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          [self.view setUserInteractionEnabled:YES];
          [self performSelector:@selector(savetolibmethod) withObject:self afterDelay:0.300];
          
     }
     else
     {
          [self savetolibmethod];
     }
     
     
     
     
}
-(void)savetolibmethod
{
     
     
     NSString *CreateThumbnail = [[NSMutableString alloc]init];
     [self DisableButtonSelection];
     [__saveToLibBtn setSelected:YES];
     
     NSString *widthThumb = [NSString stringWithFormat:@"%.0f",self.view.frame.size.height*2.2];
     NSString *xThumb = [NSString stringWithFormat:@"-%.0f",self.view.frame.size.height*1.6];
     NSString *yThumb = @"-50";
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          xThumb = [NSString stringWithFormat:@"-%.0f",self.view.frame.size.height-300];
          widthThumb = [NSString stringWithFormat:@"%.0f",self.view.frame.size.height*1.5];
          yThumb = @"-30";
     }
     
     if (_canvasWidth == 480)
     {
          xThumb = [NSString stringWithFormat:@"-%.0f",self.view.frame.size.height*1.1];
          
     }
     
     CreateThumbnail = [NSString stringWithFormat:@"CreateThumbnail('%@','%@','%@','%@')",widthThumb,widthThumb,xThumb,yThumb];
     NSString *Base64 = [___CharacterWebView stringByEvaluatingJavaScriptFromString:CreateThumbnail];
     NSString *acces = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"fetchAPILink()"];
     NSString *eyes = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('eyes')"];
     NSString *mouth = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('mouth')"];
     NSString *hair = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('hair')"];
     NSString *skin = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getBodyPartColor('skin')"];
     NSString *audioSave = [[NSUserDefaults standardUserDefaults]valueForKey:@"saveTTSLink"];
     
     if (audioSave == nil)
     {
          audioSave = @"";
     }
     
     NSMutableString *arr = RUNNING_CHARACTER;
     NSMutableString *arr1 =  [[NSUserDefaults standardUserDefaults]valueForKey:@"selectedCategory"];
     NSMutableString *arr2 = [[NSUserDefaults standardUserDefaults]valueForKey:@"spriteSheetSelectedChar"];
     NSMutableDictionary *dict = [NSMutableDictionary dictionary];
     
     dict =  [NSMutableDictionary dictionaryWithObjectsAndKeys:acces,@"accessory",
              eyes,@"eye",
              mouth,@"mouth",
              hair,@"hair",
              skin,@"skin",
              Base64,@"image64",
              [LibPath mutableCopy],@"lib",
              arr,@"arr1",
              arr1,@"arr2",
              arr2,@"arr3",
              
              nil];
     
     
     NSLog(@"%@",dict);
     
     
     NSMutableArray *resaved = [[NSMutableArray alloc]init];
     
     if ([[NSUserDefaults standardUserDefaults]valueForKey:@"saveToLib"])
     {
          resaved = [[[NSUserDefaults standardUserDefaults]valueForKey:@"saveToLib"]mutableCopy];
     }
     
     [resaved addObject:dict];
     [[NSUserDefaults standardUserDefaults]setValue:resaved forKey:@"saveToLib"];
     
     [self LoadShareScroller];
     
}

#pragma mark - Delete Alert Action

-(void)dismissView:(id)sender
{
     //     _Purchasedollerimg.alpha = 1;
     //     _sidedollar.alpha = 1;
     [blurViewCalled removeFromSuperview];
     [blurViewCalled1 removeFromSuperview];
     [blurViewCalled2 removeFromSuperview];
     [alertVc.ratealert removeFromSuperview];
}

-(void)dismissinternet:(id)sender
{
     
     [self.sidedollar setEnabled:true];
     [blurViewCalledbuy removeFromSuperview];
}

-(void)dismissImage:(id)sender
{
     
     if (isenable == true )
     {
          memorywarningcount--;
     }
     if (memorywarningcount ==2 || [shareScrollerArray count]<=18)
     {
          memorywarningcount = 0;
          NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
          [defaults setInteger:memorywarningcount forKey:@"memorywarningcount"];
          [defaults synchronize];
          isenable = false;
          [__saveToLibBtn setEnabled:YES];
     }
     [blurViewCalled removeFromSuperview];
     NSMutableArray *shareScrollerArray = [[[NSUserDefaults standardUserDefaults]valueForKey:@"saveToLib"]mutableCopy];
     if (shareScrollerArray.count <= 0)
     {
          _deleteGalleryBtn.hidden = true;
     }
     else
     {
          _deleteGalleryBtn.hidden = false;
          [shareScrollerArray removeObjectAtIndex:deleteImageIndex];
          [[NSUserDefaults standardUserDefaults]setValue:shareScrollerArray forKey:@"saveToLib"];
          deleteImageIndex = 0;
          [self LoadShareScroller];
     }
}


#pragma mark - Right Voki logo action
- (IBAction)RightVokiAction:(id)sender
{
     _Whyloginview.hidden = true;
     _purchasedallview.hidden = true;
     _purchaselearnmore.hidden = true;
     _purchaselearnmore.hidden= true;
     playCharacterAud1.hidden = true;
     playCharacterAud1.alpha = 0;
     UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
     [__aboutTextView addGestureRecognizer:tapRecognizer];
     [blurViewCalled1 removeFromSuperview];
     NSString *paths = RUNNING_CHARACTER;
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
     NSString *islogin = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"islogin"];
     
     if (![islogin  isEqual: @"yes"])
     {
          if (isbuyall == NO)
          {
               if ([soldcharcters containsObject:paths]|| [freecharacetr containsObject:paths])
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
                    
               }
               else
               {
                    _Purchasedollerimg.alpha = 1;
                    _sidedollar.alpha = 1;
                    _Purchasedollerimg.alpha = 1;
                    _sidedollar.alpha = 1;
                    _Purchasedollerimg.alpha = 1;
                    _sidedollar.alpha = 1;
                    _sidedollar.enabled = true;
                    _Purchasedollerimg.hidden = false;
                    _sidedollar.hidden = false;
               }
          }
          else
          {
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
               _sidedollar.enabled = false;
          }
          
     }
     
     else
     {
          _Purchasedollerimg.alpha = 0;
          _sidedollar.alpha = 0;
          
     }
     
     
     
     
     [UIView animateWithDuration:0.1 animations:^{
          
          
          allcatagoryview.hidden = true;
          //          playCharacterAud1.hidden = false;
          //           playCharacterAud1.alpha = 1;
          [Appbtn setSelected:false];
     }];
     
     [self HideAllViews];
     [self DisableMainButtons];
     [self DisableButtonSelection];
     
     if(shareViewOpenClose == NO)
     {
          
          [UIView animateWithDuration:0.8 animations:^
           {
                playCharacterAud1.alpha = 0;
                playCharacterAud1.hidden = true;
                
                __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y, __rightMenuView.frame.size.width, __rightMenuView.frame.size.height);
                __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
                
                __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
                __closerightMenuNewBtn.frame = CGRectMake(self.view.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
                [self characterLeftMoveShare];
                shareViewOpenClose = YES;
                [__rightVokiBtn setEnabled:YES];
                [self HideAllViews];
           }];
          
          _isRightShareOpen = NO;
          vokilogo_imgagess.hidden = false;
          _isRightVokiOpened = YES;
          
          if (menuViewOpenClose == NO)
          {
               _isRightShareOpen = NO;
               _isRightMenuOpen = YES;
               rightmenu = true;
               [self characterLeftMoveMenu];
          }
          else
          {
               _isRightShareOpen = NO;
               _isRightMenuOpen = NO;
               rightmenu = false;
               _sidedollar.alpha = 0;
               _Purchasedollerimg.alpha = 0;
               
          }
          
          
          [__aboutBtn setSelected:YES];
          [__aboutMainBtn setSelected:YES];
          __aboutView.hidden=false;
          __menuDescriptionView.hidden=false;
          
          
          
          __syncView.hidden=true;
          __termsView.hidden=true;
          __faqView.hidden=true;
     }
     
     else
     {
          
          if(_isRightVokiOpened)
          {
               playCharacterAud1.alpha = 1;
               playCharacterAud1.hidden = false;
               
               //               _sidedollar.enabled = true;
               //               _sidedollar.hidden = false;
               //               _sidedollar.alpha = 1;
               
               vokilogo_imgagess.hidden = true;
               _isRightVokiOpened = NO;
               
               if (menuViewOpenClose == NO)
               {
                    _isRightShareOpen = NO;
                    _isRightMenuOpen = YES;
                    rightmenu = true;
                    
                    [self characterLeftMoveMenu];
               }
               else
               {
                    
                    _isRightShareOpen = NO;
                    _isRightMenuOpen = NO;
                    rightmenu = false;
                    
                    
               }
               
               
               __aboutView.hidden=true;
               __menuDescriptionView.hidden=true;
               __rightShareBtn.hidden = false;
               [__rightShareBtn setEnabled:YES];
               [__rightVokiBtn setEnabled:YES];
          }
          else
          {
               playCharacterAud1.alpha = 0;
               playCharacterAud1.hidden = true;
               vokilogo_imgagess.hidden = false;
               _isRightVokiOpened = YES;
               
               if (menuViewOpenClose == NO)
               {
                    _isRightShareOpen = NO;
                    _isRightMenuOpen = YES;
                    rightmenu = true;
                    [self characterLeftMoveMenu];
               }
               else
               {
                    _isRightShareOpen = NO;
                    _isRightMenuOpen = NO;
                    rightmenu = false;
                    _sidedollar.alpha = 0;
                    _Purchasedollerimg.alpha = 0;
                    
               }
               
               
               [__aboutBtn setSelected:YES];
               [__aboutMainBtn setSelected:YES];
               __aboutView.hidden=false;
               __menuDescriptionView.hidden=false;
               
               
          }
          __syncView.hidden=true;
          __termsView.hidden=true;
          __faqView.hidden=true;
     }
     
}

#pragma mark - Right menu methods
- (IBAction)closeMainStripBtn:(id)sender
{
     [UIView animateWithDuration:0.5 animations:^
      {
           __mainFirstStrip.frame = CGRectMake(self.view.frame.origin.x, __mainFirstStrip.frame.origin.y ,__mainFirstStrip.frame.size.width,__mainFirstStrip.frame.size.height);
           __mainFirstStrip.alpha = 1;
           _closemainStripBtn.alpha = 0;
           
      }];
     
     if (__rightMenuView.frame.origin.x != self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width || __rightShareView.frame.origin.x != self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width)
     {
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.2 , self.view.frame.size.height - 105, 95, 95);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
               
          }
          else
          {
               if (_canvasWidth == 480)
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
               }
               
               else if (_canvasWidth == 736)
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
               }
               else
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/3 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
               }
          }
          
          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"",ypos];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
     }
     else
     {
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - 5 - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 105, 95, 95);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
          }
          else
          {
               if (_canvasWidth == 480)
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - 5 - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
               }
               
               else if (_canvasWidth == 736)
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - 5 - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
               }
               else if (_canvasWidth == 667)
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width -10 - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
               }
               else
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - 5 - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
               }
          }
          
          
          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"50",ypos];
          
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
     }
     
}


- (IBAction)AboutAction:(id)sender
{
     
     UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
     [__aboutTextView addGestureRecognizer:tapRecognizer];
     _Whyloginview.hidden = true;
     _purchasedallview.hidden = true;
     _purchaselearnmore.hidden = true;
     self.Purchaseview.hidden=true;
     [self DisableButtonSelection];
     [__syncMainBtn setSelected:NO];
     [__syncMainBtn setSelected:NO];
     _Whyloginview.hidden = YES;
     self.Accountview.hidden = YES;
     [__aboutBtn setSelected:YES];
     [__aboutMainBtn setSelected:YES];
     __aboutView.hidden=false;
     __syncView.hidden=true;
     __termsView.hidden=true;
     __faqView.hidden=true;
     self.Accountview.hidden = true;
     __menuDescriptionView.hidden=false;
     [gestureRecognizer setEnabled:NO];
     [gestureRecognizerRight setEnabled:NO];
     
}


- (IBAction)Purchaseaction:(id)sender
{
     
     UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
     
     [_purchaselogintextview addGestureRecognizer:tapRecognizer];
     _purchaselearnmore.hidden = true;
     
     NSString *islogin = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"islogin"];
     if (![islogin  isEqual: @"yes"])
     {
          if (isbuyall == YES)
          {
               [self DisableButtonSelection];
               _purchasedallview.hidden = false;
               self.Purchaseview.hidden=true;
               [__syncMainBtn setSelected:NO];
               [__syncBtn setSelected:NO];
               _Whyloginview.hidden = YES;
               self.Accountview.hidden = YES;
               [__termsBtn setSelected:YES];
               [purchasebtn setSelected:YES];
               [__termsMainBtn setSelected:NO];
               self.Accountview.hidden = true;
               __aboutView.hidden=true;
               __syncView.hidden=true;
               __termsView.hidden=true;
               __faqView.hidden=true;
               __menuDescriptionView.hidden=false;
          }
          else
          {
               [self DisableButtonSelection];
               self.Purchaseview.hidden=false;
               _purchasedallview.hidden = true;
               
               [__syncMainBtn setSelected:NO];
               [__syncBtn setSelected:NO];
               _Whyloginview.hidden = YES;
               self.Accountview.hidden = YES;
               [__termsBtn setSelected:YES];
               [purchasebtn setSelected:YES];
               [__termsMainBtn setSelected:NO];
               self.Accountview.hidden = true;
               __aboutView.hidden=true;
               __syncView.hidden=true;
               __termsView.hidden=true;
               __faqView.hidden=true;
               __menuDescriptionView.hidden=false;
          }
          
          
          
     }
     
     else
     {
          [self DisableButtonSelection];
          _purchasedallview.hidden = false;
          self.Purchaseview.hidden=true;
          [__syncMainBtn setSelected:NO];
          [__syncBtn setSelected:NO];
          _Whyloginview.hidden = YES;
          self.Accountview.hidden = YES;
          [__termsBtn setSelected:YES];
          [purchasebtn setSelected:YES];
          [__termsMainBtn setSelected:NO];
          self.Accountview.hidden = true;
          __aboutView.hidden=true;
          __syncView.hidden=true;
          __termsView.hidden=true;
          __faqView.hidden=true;
          __menuDescriptionView.hidden=false;
     }
     
}

- (IBAction)closepurchaselearnmore:(id)sender
{
     
     _purchaselearnmore.hidden = true;
     [self DisableButtonSelection];
     self.Purchaseview.hidden=false;
     _purchasedallview.hidden = true;
     
     [__syncMainBtn setSelected:NO];
     [__syncBtn setSelected:NO];
     _Whyloginview.hidden = YES;
     self.Accountview.hidden = YES;
     [__termsBtn setSelected:YES];
     [purchasebtn setSelected:YES];
     [__termsMainBtn setSelected:NO];
     self.Accountview.hidden = true;
     __aboutView.hidden=true;
     __syncView.hidden=true;
     __termsView.hidden=true;
     __faqView.hidden=true;
     __menuDescriptionView.hidden=false;
}

- (IBAction)closeMultipleloginaction:(id)sender

{
     
     [UIView animateWithDuration:0.1 animations:^{
          
          self.txtPassword.text = @"";
          self.twxtUserName.text = @"";
          self.MultipleLogins.hidden = true;
          self.MainLoginview.hidden = false;
          self.Accountview.hidden = false;
          
     }];
     
     
}



- (IBAction)LoginAction:(id)sender
{
     
     
     [_txtPassword resignFirstResponder];
     [_twxtUserName resignFirstResponder];
     self.Accountview.frame =  myframe;
     
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
     }
     else
     {
          
          if (([self.twxtUserName.text  isEqual: @""]) || ([self.txtPassword.text isEqual: @""]))
          {
               
               _LoginErroMsg.hidden = false;
               _LoginheaderLabel.hidden = true;
               _LoginErroMsg.text = @"Username or password not recognized. \n      Please try again.";
               _rederrorimg.hidden = false;
               
          }
          
          else
          {
               
               UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
               
               activityView.center=self.Accountview.center;
               [activityView startAnimating];
               
               //Update UI
               [self.Accountview addSubview:activityView];
               
               
               NSString *currentDeviceId = [[NSUserDefaults standardUserDefaults]
                                            stringForKey:@"currentDeviceId"];
               NSString *strURL = @"https://vhss.oddcast.com/mng/mobile/mngMobileLogin.php";
               
               
               NSDictionary *params = @{@"User": self.twxtUserName.text,
                                        @"Pswd": self.txtPassword.text,
                                        @"DeviceID":currentDeviceId,
                                        @"clearDevices":@"0"};
               AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
               
               
               
               
               
               [manager POST:strURL parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                    
                    
                    
                    NSDictionary *jsonDict =(NSDictionary *)responseObject ;
                    
                    NSError *error;
                    
                    int status = [[jsonDict valueForKey:@"Status"]intValue];
                    NSString *errormesage = [jsonDict valueForKey:@"Error"];
                    
                    if (status == 0)
                    {
                         
                         
                         [UIView animateWithDuration:0.2 animations:^{
                              
                              
                              _Loginusrdetailview.hidden = false;
                              _LoginuserName.text = self.twxtUserName.text;
                              self.MultipleLogins.hidden = true;
                              self.Accountview.hidden = false;
                              self.MainLoginview.hidden = true;
                              self.Whyloginview.hidden  = true;
                         }];
                         [activityView stopAnimating];
                         
                         
                         NSString *PHPSESSID = [jsonDict valueForKey:@"PHPSESSID"];
                         [[NSUserDefaults standardUserDefaults] setObject:PHPSESSID forKey:@"PHPSESSID"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"ismultiplelogin"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"islogin"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         [[NSUserDefaults standardUserDefaults] setObject:self.twxtUserName.text forKey:@"username"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                    }
                    
                    else
                    {
                         
                         if (status == 73)
                         {
                              
                              [activityView stopAnimating];
                              _LoginErroMsg.hidden = false;
                              NSString *  learnmore = @"<font color=\"white\"><span style=\"font-family: Arial; font-size: 14\"> Your <b>Basic</b> Voki account is not <br> eligible.You must be a paid Voki.com <br>subscriber to log in to the app. <br/></span></font>";
                              NSAttributedString *attributed = [[NSAttributedString alloc] initWithData:[learnmore dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                              [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"islogin"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              _LoginErroMsg.attributedText = attributed;
                              _LoginheaderLabel.hidden = true;
                              
                              _rederrorimg.hidden = false;
                              
                         }
                         
                         else  if (status == 72)
                         {
                              
                              [activityView stopAnimating];
                              
                              [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"islogin"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"ismultiplelogin"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              
                              
                              [UIView animateWithDuration:0.2 animations:^{
                                   
                                   self.MultipleLogins.hidden = false;
                                   self.MainLoginview.hidden = true;
                                   self.Accountview.hidden = false;
                                   
                              }];
                              [activityView stopAnimating];
                              
                         }
                         else if  (status == 71)
                         {
                              
                              
                              
                              [activityView stopAnimating];
                              
                              [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"islogin"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              _LoginErroMsg.hidden = false;
                              _LoginheaderLabel.hidden = true;
                              _LoginErroMsg.text = @"Username or password not recognized. \n      Please try again.";
                              _rederrorimg.hidden = false;
                              
                              [activityView stopAnimating];
                              
                         }
                         
                         else if (status == 1)
                         {
                              
                              
                              [activityView stopAnimating];
                              [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"islogin"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              
                              _LoginErroMsg.hidden = false;
                              _LoginheaderLabel.hidden = true;
                              _LoginErroMsg.text = @"Username or password not recognized. \n      Please try again.";
                              _rederrorimg.hidden = false;
                              
                              [activityView stopAnimating];
                              
                         }
                    }
                    
               }
                
                     failure:^(NSURLSessionDataTask *task, NSError *error) {
                          
                     }];
               
          }
          
     }
     
}

-(void)getindicator
{
     Activityindicator = [[UIActivityIndicatorView alloc]
                          initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
     Activityindicator.center=self.Accountview.center;
     [Activityindicator startAnimating];
     [self.Accountview addSubview:Activityindicator];
}

-(void)stopindicator
{
     [Activityindicator stopAnimating];
}

- (IBAction)purchaselearnAction:(id)sender
{
     [self DisableButtonSelection];
     self.Purchaseview.hidden=true;
     _purchasedallview.hidden = true;
     [__syncMainBtn setSelected:NO];
     [__syncBtn setSelected:NO];
     _Whyloginview.hidden = YES;
     self.Accountview.hidden = YES;
     [__termsBtn setSelected:YES];
     [purchasebtn setSelected:YES];
     [__termsMainBtn setSelected:NO];
     self.Accountview.hidden = true;
     __aboutView.hidden=true;
     __syncView.hidden=true;
     __termsView.hidden=true;
     __faqView.hidden=true;
     __menuDescriptionView.hidden=false;
     _purchaselearnmore.hidden = false;
}



- (IBAction)sidedollar:(id)sender

{
     [self.sidedollar setEnabled:false];
     purchaseopen = false;
     [self HideAllViews];
     [self DisableMainButtons];
     buyall = false;
     
     dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
     
     dispatch_async(q, ^{
          [self performSelectorOnMainThread:@selector(showAlert) withObject:nil waitUntilDone:YES];
          
     });
     
     
     [gestureRecognizer setEnabled:NO];
     [gestureRecognizerRight setEnabled:YES];
     
     
}

-(void)showAlert
{
     
     if (alertshown == false)
     {
          alertshown = true;
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalled1 = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.unblockvookichar.hidden = false;
          // do work here
          alertVc.unblockvookichar.alpha = 1;
          [self.view addSubview:blurViewCalled1];
          alertVc.whiteborderView.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          [alertVc.unblocklearnmore addTarget:self action:@selector(displaylearnmoreview) forControlEvents:UIControlEventTouchUpInside];
          [alertVc.unlockbuyallbtn addTarget:self action:@selector(unlockbuyallbtnAction) forControlEvents:UIControlEventTouchUpInside];
          [alertVc.BuyProduct addTarget:self action:@selector(inapppurchaseAction:) forControlEvents:UIControlEventTouchUpInside];
          [alertVc.unblockrestoreAction addTarget:self action:@selector(restorecharacters) forControlEvents:UIControlEventTouchUpInside];
          [alertVc.vokiblockclose addTarget:self action:@selector(dismissblockcontact1) forControlEvents:UIControlEventTouchUpInside];
          [alertVc.vokilogin addTarget:self action:@selector(unblocklogin) forControlEvents:UIControlEventTouchUpInside];
     }
}


-(void)unblocklogin
{
     alertshown = false;
     [blurViewCalled2 removeFromSuperview];
     [blurViewCalled1 removeFromSuperview];
     vokilogo_imgagess.hidden = false;
     _isRightVokiOpened = YES;
     _isRightShareOpen = NO;
     _isRightMenuOpen = NO;
     rightmenu = false;
     _sidedollar.alpha = 0;
     _Purchasedollerimg.alpha = 0;
     [__aboutBtn setSelected:YES];
     [__aboutMainBtn setSelected:YES];
     __aboutView.hidden=false;
     __menuDescriptionView.hidden=false;
     __syncView.hidden=true;
     __termsView.hidden=true;
     __faqView.hidden=true;
     [self AccountAction];
     
     
}




- (IBAction)signout:(id)sender
{
     
     [self getindicator];
     NSString *ID = [[NSUserDefaults standardUserDefaults]
                     stringForKey:@"PHPSESSID"];
     
     if (ID == nil)
     {
          [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"islogin"];
          [[NSUserDefaults standardUserDefaults] synchronize];
          
          [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"ismultiplelogin"];
          [[NSUserDefaults standardUserDefaults] synchronize];
          _txtPassword.text = @"";
          _twxtUserName.text = @"";
          _LoginErroMsg.hidden = true;
          _LoginheaderLabel.hidden = false;
          _Loginusrdetailview.hidden = true;
          self.Accountview.hidden = false;
          self.MainLoginview.hidden = false;
          self.MultipleLogins.hidden = true;
          _Whyloginview.hidden = true;
          __aboutView.hidden=true;
          __termsView.hidden=true;
          __faqView.hidden=true;
          __aboutView.hidden=true;
          __menuDescriptionView.hidden=false;
          [self stopindicator];
          [self stopindicator];
          
          return;
     }
     
     AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     NSDictionary *parameters = @{@"PHPSESSID": ID};
     [manager POST:@"https://vhss.oddcast.com/mng/mobile/mngMobileLogout.php" parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
          
          NSDictionary *jsonDict = (NSMutableDictionary *)responseObject;
          
          if (jsonDict != nil)
          {
               int status = [[jsonDict valueForKey:@"Status"]intValue];
               if (status == 0)
               {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"islogin"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"ismultiplelogin"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    _txtPassword.text = @"";
                    _twxtUserName.text = @"";
                    _LoginErroMsg.hidden = true;
                    _LoginheaderLabel.hidden = false;
                    _Loginusrdetailview.hidden = true;
                    self.Accountview.hidden = false;
                    self.MainLoginview.hidden = false;
                    self.MultipleLogins.hidden = true;
                    _Whyloginview.hidden = true;
                    __aboutView.hidden=true;
                    __termsView.hidden=true;
                    __faqView.hidden=true;
                    __aboutView.hidden=true;
                    __menuDescriptionView.hidden=false;
                    [self stopindicator];
                    [self stopindicator];
               }
          }
          
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
          
          [self stopindicator];
     }];
     
     
}



- (IBAction)ForgotPassword:(id)sender
{
     
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalled = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalled];
     }
     
     else
     {
          NSString *trimmedString = [self.twxtUserName.text stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
          if([trimmedString isEqualToString:@""])
          {
               UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!" message:@"Please Enter a valid UserName" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
               [alert show];
          }
          else
          {
               
               
               UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
               
               activityView.center=self.Accountview.center;
               [activityView startAnimating];
               
               //Update UI
               [self.Accountview addSubview:activityView];
               
               
               NSString *currentDeviceId = [[NSUserDefaults standardUserDefaults]
                                            stringForKey:@"currentDeviceId"];
               NSString *strURL = @"https://vhss.oddcast.com/mng/mobile/mngMobileForgotPassword.php";
               
               NSDictionary *params = @{@"User": self.twxtUserName.text};
               AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
               [manager POST:strURL parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                    
                    
                    
                    NSDictionary *jsonDict =(NSDictionary *)responseObject ;
                    
                    
                    int status = [[jsonDict valueForKey:@"Status"]intValue];
                    
                    if (status == 0)
                    {
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Password!" message:@"Password has been sent to your registered Email-Address" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                         [alert show];
                         [activityView stopAnimating];
                         
                         
                    }
                    
                    
                    else if(status == 1)
                    {
                         
                         [activityView stopAnimating];
                         
                    }
                    
               }
                
                
                     failure:^(NSURLSessionDataTask *task, NSError *error) {
                          
                     }];
               
               
               
          }
          
          
          
     }
     
     
}






- (IBAction)Logoutotherdevice:(id)sender

{
     
     [self MultipleLogout];
     
}


-(void)MultipleLogout
{
     
     
     NSString *currentDeviceId = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"currentDeviceId"];
     UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                              initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
     
     activityView.center=self.Accountview.center;
     [activityView startAnimating];
     [self.Accountview addSubview:activityView];
     NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
     NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
     
     NSURL *url = [NSURL URLWithString:@"https://vhss.oddcast.com/mng/mobile/mngMobileLogin.php"];
     NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
     NSString * params = [NSString stringWithFormat:@"User=%@&Pswd=%@&DeviceID=%@&clearDevices=1",self.twxtUserName.text,self.txtPassword.text,currentDeviceId];
     [urlRequest setHTTPMethod:@"POST"];
     [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
     
     NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                            
                                            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                            
                                            NSString *errormesage = [jsonDict valueForKey:@"Error"];
                                            if (([errormesage  isEqual: @""]) || ([errormesage isKindOfClass:[NSNull class]]))
                                                 
                                                 
                                            {
                                                 int status = [[jsonDict valueForKey:@"Status"]intValue];
                                                 if (status == 0)
                                                 {
                                                      [UIView animateWithDuration:0.2 animations:^{
                                                           
                                                           [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"islogin"];
                                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                                           
                                                           [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"ismultiplelogin"];
                                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                                           [[NSUserDefaults standardUserDefaults] setObject:self.twxtUserName.text forKey:@"username"];
                                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                                           _Loginusrdetailview.hidden = false;
                                                           _LoginuserName.text = self.twxtUserName.text;
                                                           self.MultipleLogins.hidden = true;
                                                           self.Accountview.hidden = false;
                                                           self.MainLoginview.hidden = true;
                                                           self.Whyloginview.hidden  = true;
                                                      }];
                                                      [activityView stopAnimating];
                                                      
                                                 }
                                            }
                                            else
                                            {
                                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                                                 [alert show];
                                                 [activityView stopAnimating];
                                            }
                                            
                                            
                                       }];
     [dataTask resume];
     
}



- (IBAction)closewhyloginview:(id)sender
{
     
     
     _Accountview.alpha=1;
     _Whyloginview.hidden = true;
     _Accountview.hidden = false;
}


- (void)tappedloginclose:(UITapGestureRecognizer *)tapGesture

{
     _Whyloginview.hidden = true;
     _Accountview.hidden = false;
}


- (IBAction)LearnMore:(id)sender
{
     [UIView animateWithDuration:0.25 animations:^{
          
          [ _twxtUserName resignFirstResponder];
          [_txtPassword resignFirstResponder];
          [gestureRecognizer setEnabled:NO];
          [gestureRecognizerRight setEnabled:NO];
          [_Whyloginview setUserInteractionEnabled:true];
          __aboutView.hidden=true;
          __syncView.hidden=true;
          __termsView.hidden=true;
          __faqView.hidden=true;
          self._rightMenuView.hidden= true;
          self.Accountview.hidden = YES;
          self.Accountview.alpha = 0;
          _Whyloginview.hidden = false;
          self.fxMenuView.hidden = true;
          self.Accountview.hidden = true;
          _Accountview.hidden = true;
          [_whyloginclosepng setUserInteractionEnabled:YES];
          [_closewhyloginbtn removeGestureRecognizer:TapGesture];
          [_closewhyloginbtn removeGestureRecognizer:_pangestureMovetoChar];
          
     }];
     
}

- (IBAction)SyncAction:(id)sender
{
     
     [self AccountAction];
}

-(void)AccountAction
{
     self.Accountview.alpha = 1;
     UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
     
     [GotoFootertext addGestureRecognizer:tapRecognizer];
     _purchaselearnmore.hidden = true;
     
     NSString *islogin = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"islogin"];
     
     _purchasedallview.hidden = true;
     
     self.Purchaseview.hidden=true;
     _rederrorimg.hidden = true;
     [self DisableButtonSelection];
     _LoginErroMsg.hidden = true;
     _Loginusrdetailview.hidden = true;
     self.MultipleLogins.hidden = true;
     _Whyloginview.hidden = true;
     [__syncBtn setSelected:YES];
     [__syncMainBtn setSelected:YES];
     __aboutView.hidden=true;
     __termsView.hidden=true;
     __faqView.hidden=true;
     __aboutView.hidden=true;
     
     if ([islogin  isEqual: @"yes"])
          
     {
          NSString *ismultiplelogin = [[NSUserDefaults standardUserDefaults]
                                       stringForKey:@"ismultiplelogin"];
          
          if ( [ismultiplelogin isEqual: @"yes"] )
          {
               [UIView animateWithDuration:0.2 animations:^{
                    
                    self.MultipleLogins.hidden = false;
                    self.MainLoginview.hidden = true;
                    self.Accountview.hidden = false;
               }];
          }
          
          
          else
          {
               
               _Loginusrdetailview.hidden = false;
               _LoginuserName.text = [[NSUserDefaults standardUserDefaults]
                                      stringForKey:@"username"];
               
               self.MultipleLogins.hidden = true;
               self.Accountview.hidden = false;
               self.MainLoginview.hidden = true;
          }
     }
     else
     {
          _txtPassword.text = @"";
          _twxtUserName.text = @"";
          _LoginheaderLabel.hidden = false;
          self.Accountview.hidden = false;
          self.MainLoginview.hidden = false;
          [__syncBtn setSelected:YES];
          [__syncMainBtn setSelected:YES];
          
     }
     
}

- (IBAction)TermsAction:(id)sender
{
     _Termsprivacyview.hidden = true;
     _Privacyview.hidden = true;
     
     UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
     
     [__termsServicesTextView addGestureRecognizer:tapRecognizer];
     self._termsServicesTextView.hidden = false;
     
     [self DisableButtonSelection];
     _purchasedallview.hidden = true;
     self.Purchaseview.hidden=true;
     [__syncMainBtn setSelected:NO];
     [__syncBtn setSelected:NO];
     _Whyloginview.hidden = YES;
     self.Accountview.hidden = YES;
     _purchaselearnmore.hidden = true;
     [__termsBtn setSelected:YES];
     [purchasebtn setSelected:NO];
     [__termsMainBtn setSelected:YES];
     self.Accountview.hidden = true;
     __aboutView.hidden=true;
     __syncView.hidden=true;
     __termsView.hidden=false;
     __faqView.hidden=true;
     __menuDescriptionView.hidden=false;
}

- (IBAction)FaqAction:(id)sender
{
     _purchaselearnmore.hidden = true;
     
     self.Purchaseview.hidden=true;
     _purchasedallview.hidden = true;
     [self DisableButtonSelection];
     [__syncMainBtn setSelected:NO];
     [__syncBtn setSelected:NO];
     [__faqBtn setSelected:YES];
     
     _Whyloginview.hidden = YES;
     self.Accountview.hidden = YES;
     [__faqMainBtn setSelected:YES];
     __aboutView.hidden=true;
     __syncView.hidden=true;
     self.Accountview.hidden = true;
     
     __termsView.hidden=true;
     __faqView.hidden=false;
     __menuDescriptionView.hidden=false;
}
-(void)ClosetapGesture:(UIGestureRecognizer*)sender
{
     _Whyloginview.hidden = true;
     _Accountview.hidden = false;
}
-(void)tapGesture:(UIGestureRecognizer*)sender
{
     
     _BgViewForTable.hidden = true;
     __text_Languageview.hidden = true;
     __text_VoiceIDView.hidden = true;
     _fxMenuView.hidden = true;
     isTextLangTableVisible = NO;
     isVoiceTableVisible = NO;
     isFxTableVisible = NO;
     _cancelBtnTable.hidden = true;
     __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, self.view.frame.origin.y + 56, __text_VoiceIDView.frame.size.width, _cancelBtnTable.frame.origin.y-66);
}

- (IBAction)cancelTableBtn:(id)sender
{
     _BgViewForTable.hidden = true;
     __text_Languageview.hidden = true;
     __text_VoiceIDView.hidden = true;
     _fxMenuView.hidden = true;
     isTextLangTableVisible = NO;
     isVoiceTableVisible = NO;
     isFxTableVisible = NO;
     _cancelBtnTable.hidden = true;
     __text_VoiceIDView.frame =CGRectMake(__text_VoiceIDView.frame.origin.x, self.view.frame.origin.y + 56, __text_VoiceIDView.frame.size.width, _cancelBtnTable.frame.origin.y-66);
}
- (IBAction)leftMenumoveBtn:(id)sender
{
     [self leftmenu];
     
}

-(void)leftmenu
{
     if (isgroupopen == true)
     {
          NSString *paths  = RUNNING_CHARACTER;
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
          NSString *islogin = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"islogin"];
          
          //To wheck wheather sharing should availble or not !
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcters containsObject:paths]|| [freecharacetr containsObject:paths])
                    {
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         
                    }
                    else
                    {
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         _sidedollar.enabled = true;
                    }
               }
               else
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
               }
               
          }
          
          else
          {
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
               
          }
          [UIView animateWithDuration:0.1 animations:^{
               thirdView = true;
               isgroupopen = false;
               [gestureRecognizer setEnabled:YES];
               
               allcatagoryview.hidden = true;
               playCharacterAud1.hidden = false;
               playCharacterAud1.alpha = 1;
          }];
     }
     else
     {
          
          for(UIView *subview in __shareScroller.subviews)
          {
               
               if ([subview isKindOfClass:[UIButton class]])
               {
                    [subview removeFromSuperview];
                    [(UIButton *)subview setSelected:NO];
                    
               }
          }
          if (thirdView == false)
          {
               _isAccessoriesSelected = NO;
               [self CharacterMove];
               [self secondScrollerDeselectedButton];
               [UIView animateWithDuration:0.5 animations:^
                {
                     __shareView.alpha = 0;
                     _deleteGalleryBtn.alpha = 0;
                     __thirdView.alpha = 0;
                     _CloeseThirdStripBtn.alpha = 0;
                     thirdView = true;
                     __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                     _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                     __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                     _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                     __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                     _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                     
                     
                }];
               
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"190",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    _CloeseThirdStripBtn.hidden=true;
                    _CloseSecondStripBtn.hidden=false;
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width -22 - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 105, 95, 95);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
               else
               {
                    if (_canvasWidth == 480)
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"190",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         _CloeseThirdStripBtn.hidden=true;
                         _CloseSecondStripBtn.hidden=false;
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"190",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         _CloeseThirdStripBtn.hidden=true;
                         _CloseSecondStripBtn.hidden=false;
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.2 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else if(_canvasWidth == 667)
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"190",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         _CloeseThirdStripBtn.hidden=true;
                         _CloseSecondStripBtn.hidden=false;
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"190",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         _CloeseThirdStripBtn.hidden=true;
                         _CloseSecondStripBtn.hidden=false;
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width + 7 - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
               }
               
               
          }
          
          else if ((__vokiView.alpha == 0)&&(__clothingView.alpha ==0)&&(__ImageView.alpha == 0)&&(__shareView.alpha == 0)&(__recordVoiceView.alpha == 0)&&(__colorPickerView.alpha == 0)&&(__mainFirstStrip.alpha == 1))
          {
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"25",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.5 animations:^
                {
                     __mainFirstStrip.frame = CGRectMake(self.view.frame.origin.x - __mainFirstStrip.frame.size.width, __mainFirstStrip.frame.origin.y ,__mainFirstStrip.frame.size.width,__mainFirstStrip.frame.size.height);
                     __mainFirstStrip.alpha = 0;
                     _closemainStripBtn.alpha = 1;
                }];
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width)/2+8 - playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 105, 95, 95);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else
               {
                    
                    
                    
                    [UIView animateWithDuration:0.7 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width)/2 - 7 - playCharacterAud1.frame.size.width/4, self.view.frame.size.height - 75, playCharacterAud1.frame.size.width, playCharacterAud1.frame.size.height);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
          }
          
          else if (__mainFirstStrip.alpha == 0)
          {
               
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"60",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               
               
               [UIView animateWithDuration:0.5 animations:^
                {
                     __mainFirstStrip.frame = CGRectMake(self.view.frame.origin.x, __mainFirstStrip.frame.origin.y ,__mainFirstStrip.frame.size.width,__mainFirstStrip.frame.size.height);
                     __mainFirstStrip.alpha = 1;
                     _closemainStripBtn.alpha = 0;
                }];
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                          
                     }];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width-25- __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 105, 95, 95);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else
               {
                    if (_canvasWidth == 480)
                    {
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else if (_canvasWidth == 667)
                    {
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width -15 - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else
                    {
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
               }
               
          }
          else
          {
               [self HideAllViews];
               [self DisableMainButtons];
          }
          issecopen = false;
          isright = false;
          isvoicepanopen = false;
          menuViewOpenClose = YES;
          shareViewOpenClose = YES;
          [gestureRecognizer setEnabled:YES];
          __rightVokiBtn.enabled = YES;
          [UIView animateWithDuration:0.8 animations:^
           {
                
                
                __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
                __closerightMenuNewBtn.frame = CGRectMake(self.view.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
                
                __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y ,__rightMenuView.frame.size.width,__rightMenuView.frame.size.height);
                __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
           }];
          
          _isRightShareOpen = NO;
          _isRightMenuOpen = NO;
          rightmenu = false;
          
          
          [__rightShareBtn setEnabled:YES];
     }
     
}
#pragma mark - close all right views methods

- (IBAction)CloseRightViews:(id)sender
{
     isright = false;
     _isRightShareOpen = NO;
     _isRightMenuOpen = NO;
     rightmenu = false;
     [self DisableButtonSelection];
     [self HideAllViews];
     //_leftmovemenubtn.hidden = false;
     [UIView animateWithDuration:0.8 animations:^
      {
           
           __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
           __closerightMenuNewBtn.frame = CGRectMake(self.view.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
           __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y ,__rightMenuView.frame.size.width,__rightMenuView.frame.size.height);
           __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
      }];
     menuViewOpenClose = YES;
     shareViewOpenClose = YES;
     
     [gestureRecognizer setEnabled:YES];
     [__rightShareBtn setEnabled:YES];
     [__rightVokiBtn setEnabled:YES];
}

-(void)displaylearnmoreview
{
     
     
     alertVc1 = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
     blurViewCalled2 = [[[alertVc1 showPopUp] subviews] objectAtIndex:0];
     alertVc.unblockvookichar.alpha = 0;
     alertVc1.unblocklearnmore.hidden = false;
     alertVc1.whiteborderView.hidden = true;
     alertVc1.deleteView.hidden = true;
     alertVc1.ratealert.hidden = true;
     [alertVc1.closeunblocklearnmore addTarget:self action:@selector(dismissblockcontact) forControlEvents:UIControlEventTouchUpInside];
     [alertVc1.Learnmorebuyallbtn addTarget:self action:@selector(LearnmorebuyallbtnAction) forControlEvents:UIControlEventTouchUpInside];
     [alertVc1.learnmoreloginbtn addTarget:self action:@selector(unblocklogin) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:blurViewCalled2];
     
     
}



-(void)unlockbuyallbtnAction
{
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          
          if(_isRightVokiOpened)
               
          {
               
               
          }
          else
          {
               self.Purchasedollerimg.alpha = 1;
               self.sidedollar.alpha =1;
               self.sidedollar.enabled = true;
          }
          
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
          alertVc.unlockbuyallbtn.enabled = true;
          alertVc.vokiblockclose.enabled = true;
          alertVc.unblockrestoreAction.enabled = true;
          alertVc.Learnmorebuyallbtn.enabled = true;
          alertVc.closeunblocklearnmore.enabled = true;
          _restorebtn.enabled = true;
          _purchasebuyall.enabled = true;
          _Purchasebuyallbtn.enabled = true;
          
          
     }
     else
     {
          
          alertVc1.Learnmorebuyallbtn.enabled = false;
          alertVc1.closeunblocklearnmore.enabled = false;
          alertVc.unblockrestoreAction.enabled = false;
          alertVc.vokiblockclose.enabled = false;
          alertVc.BuyProduct.enabled = false;
          alertVc.unblocklearnmore.enabled = false;
          alertVc.unlockbuyallbtn.enabled = false;
          inappprogressview = [[UIActivityIndicatorView alloc]
                               initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
          inappprogressview.center=self.view.center;
          [inappprogressview setBackgroundColor:[UIColor blackColor]];
          inappprogressview.layer.cornerRadius = 10;
          [inappprogressview startAnimating];
          [self.view addSubview:inappprogressview];
          //Productforbuy = BundleID;
          buyall = true;
          [self fetchAvailableProducts];
          [self performSelector:@selector(callinapp)  withObject:nil afterDelay:2.0];
          
     }
     
}


-(void)LearnmorebuyallbtnAction
{
     
     @autoreleasepool {
          
          UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
          alertVc1.Learnmorebuyallbtn.enabled = false;
          alertVc1.closeunblocklearnmore.enabled = false;
          if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
          {
               alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
               blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
               alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
               alertVc.deleteView.hidden = true;
               alertVc.ratealert.hidden = true;
               [alertVc.unblocklearnmorealert addGestureRecognizer:tapRecognizer];
               alertVc.blackview1.hidden = true;
               alertVc.unblockLearnmore.hidden = true;
               alertVc.unblockvookichar.hidden = true;
               alertVc.whiteborderView.hidden = false;
               [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
               [self.view addSubview:blurViewCalledbuy];
               alertVc1.Learnmorebuyallbtn.enabled = true;
               alertVc1.closeunblocklearnmore.enabled = true;
          }
          else
          {
               inappprogressview = [[UIActivityIndicatorView alloc]
                                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
               inappprogressview.center=self.view.center;
               [inappprogressview setBackgroundColor:[UIColor blackColor]];
               inappprogressview.layer.cornerRadius = 10;
               [inappprogressview startAnimating];
               [self.view addSubview:inappprogressview];
               //  Productforbuy = BundleID;
               buyall = true;
               [self fetchAvailableProducts];
               [self performSelector:@selector(callinapp)  withObject:nil afterDelay:2.0];
               
          }
     }
     
}

-(void)dismissblockcontact
{
     
     [blurViewCalled2 removeFromSuperview];
     alertVc.unblockvookichar.alpha = 1;
}
-(void)dismissblockcontact1
{
     alertshown = false;
     [blurViewCalled removeFromSuperview];
     [blurViewCalled1 removeFromSuperview];
     [blurViewCalled2 removeFromSuperview];
     [alertVc.ratealert removeFromSuperview];
     vokilogo_imgagess.hidden = true;
     _isRightVokiOpened = NO;
     [self.sidedollar setEnabled:true];
}
#pragma mark - Right share methods
-(void)Rightclose
{
     isvoicepanopen = false;
     issecopen = false;
     
     if ( _isRightMenuOpen  == YES)
     {
          [self hiderightview];
          
     }
     _isRightShareOpen = YES;
     _isRightMenuOpen = NO;
     [UIView animateWithDuration:0.8 animations:^
      {
           
           __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y, __rightMenuView.frame.size.width, __rightMenuView.frame.size.height);
           
      }];
     
     
     [UIView animateWithDuration:0.8 animations:^
      {
           isvoicepanopen = false;
           issecopen = false;
           rightmenu = false;
           [__rightVokiBtn setEnabled:YES];
           menuViewOpenClose = YES;
           shareViewOpenClose = YES;
           _isRightShareOpen = NO;
           __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y, __rightMenuView.frame.size.width, __rightMenuView.frame.size.height);
           __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
           __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
           __closerightMenuNewBtn.frame = CGRectMake(self.view.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
           [self characterLeftMoveShare];
           [__rightVokiBtn setEnabled:YES];
           [self HideAllViews];
      }];
}

- (IBAction)RightShareAction:(id)sender
{
     
     // [self fxaudiowithfile];
     
     
     
     
     @autoreleasepool {
          
          [self.view setUserInteractionEnabled:false];
          __rightShareBtn.enabled = false;
          [audioActivity stopAnimating];
          __rightShareBtn.enabled = false;
          NSString *paths = RUNNING_CHARACTER;
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
          NSString *islogin = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"islogin"];
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcters containsObject:paths]|| [freecharacetr containsObject:paths])
                    {
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         SharingAvailble = true;
                         
                    }
                    else
                    {
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         SharingAvailble = false;
                    }
               }
               else
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
                    SharingAvailble = true;
               }
               
          }
          
          else
          {
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
               SharingAvailble = true;
          }
          
          if (SharingAvailble == true)
          {
               isvoicepanopen = false;
               issecopen = false;
               
               if ( _isRightMenuOpen  == YES)
               {
                    [self hiderightview];
                    
               }
               
               _isRightShareOpen = YES;
               _isRightMenuOpen = NO;
               vokilogo_imgagess.hidden = true;
               [UIView animateWithDuration:0.8 animations:^
                {
                     
                     __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y, __rightMenuView.frame.size.width, __rightMenuView.frame.size.height);
                     
                }];
               
               if(shareViewOpenClose == YES)
               {
                    [self.view setUserInteractionEnabled:true];
                    
                    if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
                    {
                         [self.view setUserInteractionEnabled:true];
                         alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
                         blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
                         alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
                         alertVc.deleteView.hidden = true;
                         alertVc.ratealert.hidden = true;
                         alertVc.unblockLearnmore.hidden = true;
                         alertVc.unblockvookichar.hidden = true;
                         alertVc.blackview1.hidden = true;
                         alertVc.whiteborderView.hidden = false;
                         [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
                         [self.view addSubview:blurViewCalledbuy];
                         __rightShareBtn.enabled = true;
                         
                    }
                    
                    else
                    {
                         Videotempsave = true;
                         
                         
                         
                         __closerightMenuNewBtn.frame = CGRectMake(__rightShareView.frame.origin.x-__closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
                         __rightShareBtn.enabled = true;
                         
                         
                         [self Movecharonrightaction];
                         
                         
                         
                    }
                    
                    
               }
               else
               {
                    [UIView animateWithDuration:0.8 animations:^
                     {
                          isvoicepanopen = false;
                          issecopen = false;
                          rightmenu = false;
                          [__rightVokiBtn setEnabled:YES];
                          menuViewOpenClose = YES;
                          shareViewOpenClose = YES;
                          _isRightShareOpen = NO;
                          __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y, __rightMenuView.frame.size.width, __rightMenuView.frame.size.height);
                          __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
                          __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
                          __closerightMenuNewBtn.frame = CGRectMake(self.view.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
                          [self characterLeftMoveShare];
                          [__rightVokiBtn setEnabled:YES];
                          [self HideAllViews];
                          __rightShareBtn.enabled = true;
                          [self.view setUserInteractionEnabled: true];
                          
                          
                     }];
               }
               [__rightShareBtn setEnabled:YES];
               
          }
          else
          {
               
               alertshown = true;
               shareViewOpenClose = YES;
               menuViewOpenClose = YES;
               _isRightShareOpen = NO;
               _Purchasedollerimg.alpha = 1;
               _sidedollar.alpha = 1;
               __closerightMenuNewBtn.frame = CGRectMake(__rightShareView.frame.origin.x-__closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
               purchaseopen = false;
               alertVc = nil;
               alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
               blurViewCalled1 = [[[alertVc showPopUp] subviews] objectAtIndex:0];
               alertVc.unblockvookichar.hidden = false;
               alertVc.unblockvookichar.alpha = 1;
               alertVc.whiteborderView.hidden = true;
               alertVc.unblockLearnmore.hidden = true;
               alertVc.deleteView.hidden = true;
               alertVc.ratealert.hidden = true;
               [alertVc.unblocklearnmore addTarget:self action:@selector(displaylearnmoreview) forControlEvents:UIControlEventTouchUpInside];
               [alertVc.unlockbuyallbtn addTarget:self action:@selector(unlockbuyallbtnAction) forControlEvents:UIControlEventTouchUpInside];
               [alertVc.BuyProduct addTarget:self action:@selector(inapppurchaseAction:) forControlEvents:UIControlEventTouchUpInside];
               [alertVc.unblockrestoreAction addTarget:self action:@selector(restorecharacters) forControlEvents:UIControlEventTouchUpInside];
               [alertVc.vokiblockclose addTarget:self action:@selector(dismissblockcontact1) forControlEvents:UIControlEventTouchUpInside];
               [alertVc.vokilogin addTarget:self action:@selector(unblocklogin) forControlEvents:UIControlEventTouchUpInside];
               [self.view addSubview:blurViewCalled1];
               alertVc.unblockLearnmore.hidden = true;
               __rightShareBtn.enabled = true;
               [self HideAllViews];
               [self DisableMainButtons];
               [self.view setUserInteractionEnabled:true];
          }
     }
     
}



-(void)Movecharonrightaction
{
     if (recordprocess == false )
     {
          [audioActivity stopAnimating];
          _video_downloadbtn.enabled = true;
          _Whatsbtn.enabled = true;
          __facebookBtn.enabled = true;
          self._googleBtn.enabled = true;
          __googleBtn.enabled = true;
          __facebookBtn.enabled = true;
          __mailBtn.enabled = true;
          __twitterBtn.enabled = true;
          self._twitterBtn.enabled = true;
          [self.video_downloadbtn setSelected:NO];
          _video_downloadbtn.enabled = true;
     }
     else
     {
          [audioActivity stopAnimating];
          _video_downloadbtn.enabled = false;
          _Whatsbtn.enabled = false;
          __facebookBtn.enabled = false;
          self._googleBtn.enabled = false;
          __googleBtn.enabled = false;
          __facebookBtn.enabled = false;
          __mailBtn.enabled = false;
          __twitterBtn.enabled = false;
          self._twitterBtn.enabled = false;
          [self.video_downloadbtn setSelected:NO];
          _video_downloadbtn.enabled = false;
     }
     
     @autoreleasepool
     {
          
          [self.view setUserInteractionEnabled:true];
          playCharacterAud1.alpha = 1;
          playCharacterAud1.hidden = false;
          _Purchasedollerimg.alpha = 1;
          _sidedollar.alpha = 1;
          NSString *paths = RUNNING_CHARACTER;
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
          NSString *islogin = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"islogin"];
          [self HideAllViews];
          [self DisableMainButtons];
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcters containsObject:paths]|| [freecharacetr containsObject:paths])
                    {
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         
                    }
                    else
                    {
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                    }
               }
               else
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
               }
               
          }
          
          else
          {
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
          }
          [UIView animateWithDuration:0.1 animations:^{
               allcatagoryview.hidden = true;
               playCharacterAud1.hidden = false;
               playCharacterAud1.alpha = 1;
               [Appbtn setSelected:false];
          }];
          
          [self characterLeftMoveShare];
          [UIView animateWithDuration:0.8 animations:^
           {
                
                __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y, __rightMenuView.frame.size.width, __rightMenuView.frame.size.height);
                __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
                __rightShareView.frame = CGRectMake(self.view.frame.size.width - __rightShareView.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
                __closerightMenuNewBtn.frame = CGRectMake(__rightShareView.frame.origin.x-__closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
                menuViewOpenClose = YES;
                shareViewOpenClose = NO;
                [__rightVokiBtn setEnabled:YES];
           }];
          
          
          rightmenu = false;
          [gestureRecognizer setEnabled:NO];
          [gestureRecognizerRight setEnabled:YES];
          
          __closerightMenuNewBtn.frame = CGRectMake(__rightShareView.frame.origin.x-__closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
          
          _isRightShareOpen = YES;
          
     }
     
}

- (IBAction)FacebookAction:(id)sender
{
     
     
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
     }
     
     else
     {
          
          [self startTransparentIndicator];
          [self DisableButtonSelection];
          [self.video_downloadbtn setSelected:NO];
          [__facebookBtn setSelected:YES];
          
          videotwittershare = false;
          sharewithfacebook = true;
          appDelegate.isFacebookCalled = YES;
          appDelegate.isTwitterCalled = NO;
          
          
          if ( outurltweet == nil)
          {
               
               NSString *Audiopath;
               
               
               Audiopath = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"AudioPath"];
               
               
               
               NSString *sourcePath = [[NSUserDefaults standardUserDefaults]
                                       stringForKey:@"VideoPath"];
               
               if (sourcePath == nil || Audiopath == nil || [sourcePath isEqualToString:@""] || [sourcePath isEqual: [NSNull null]] || [Audiopath isEqualToString:@""] || [Audiopath isEqual: [NSNull null]] )
               {
                    [self stopTransparentIndicator];
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please play the audio to enable video generation, then share on facebook!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
               }
               else
               {
                    
                    Videotempsave = false;
                    videofbshare = true;
                    videotwittershare = false;
                    [self videorecording];
               }
               
               
          }
          else
          {
               [self ShareContentOnFBUsingSLComposer:outurltweet];
          }
          
          // [self ShareContentOnFBUsingSLComposer];
          [self.view setUserInteractionEnabled:true];
          
          
     }
}

-(void)ShareOnFB
{
     FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
     content.contentURL = [NSURL URLWithString:@"http://developers.facebook.com"];
     [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
}


#pragma mark - FBSDKSharing Methods

-(void)ShareOnFBUsingSDK
{
     
     _photos = [[self class] demoPhotos:@"http://www.brihaspatitech.com/" :@"testing" :@"bg1"];
     SIPhoto *photo =  [self _currentPhoto];
     FBSDKShareDialog *facebookShareDialog = [self getShareDialogWithContentURL:[self _currentPhoto].objectURL];
     
     if ([facebookShareDialog canShow])
     {
          [self _shareFacebookWithPhoto:photo];
     }
     
}

+ (NSArray *)demoPhotos:(NSString*)URL :(NSString*)Title :(NSString*)Image
{
     NSData *imageData = UIImagePNGRepresentation([UIImage imageNamed:Image]);
     return @[[SIPhoto photoWithObjectURL:[NSURL URLWithString:URL] title:Title rating:5 image:[UIImage imageWithData:imageData]],];
}

- (SIPhoto *)_currentPhoto
{
     
     return _photos[0];
}

- (void)_shareFacebookWithPhoto:(SIPhoto *)photo
{
     [FBSDKShareDialog showFromViewController:self.parentViewController withContent:[self getShareLinkContentWithContentURL:photo.objectURL] delegate:nil];
}

- (FBSDKShareLinkContent *)getShareLinkContentWithContentURL:(NSURL *)objectURL
{
     FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
     content.contentURL = objectURL;
     return content;
}

- (FBSDKShareDialog *)getShareDialogWithContentURL:(NSURL *)objectURL
{
     FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
     shareDialog.shareContent = [self getShareLinkContentWithContentURL:objectURL];
     return shareDialog;
}

#pragma mark - FBSDKSharing Delegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
     
     if (results.count != 0)
     {
          [self stopTransparentIndicator];
          playCharacterAud1.hidden = false;
          playCharacterAud1.alpha = 1;
          sharewithfacebook = false;
          sharewithemail = false;
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Facebook" message:@"Your Voki has been shared" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
          [alert show];
          alert.tag = 22;
          sharecount++;
          [self saveCharApiTrack];
          __facebookBtn.enabled = true;
          __twitterBtn.enabled = true;
          __googleBtn.enabled = true;
          __mailBtn.enabled = true;
          _Whatsbtn.enabled = true;
          _video_downloadbtn.enabled = true;
          
     }
     else
     {
          [self stopTransparentIndicator];
          playCharacterAud1.hidden = false;
          playCharacterAud1.alpha = 1;
          sharewithfacebook = false;
          sharewithemail = false;
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Facebook" message:@"Your video was successfully shared" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
          [alert show];
          alert.tag = 22;
          sharecount++;
          [self saveCharApiTrack];
          __facebookBtn.enabled = true;
          __twitterBtn.enabled = true;
          __googleBtn.enabled = true;
          __mailBtn.enabled = true;
          _Whatsbtn.enabled = true;
          _video_downloadbtn.enabled = true;
     }
     
     
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
     [self stopTransparentIndicator];
     
     __facebookBtn.enabled = true;
     __twitterBtn.enabled = true;
     __googleBtn.enabled = true;
     __mailBtn.enabled = true;
     _Whatsbtn.enabled = true;
     _video_downloadbtn.enabled = true;
     NSString *message = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?:
     @"There was a problem sharing, please try again later.";
     NSString *title = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops!";
     
     [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
     [self stopTransparentIndicator];
     __facebookBtn.enabled = true;
     __twitterBtn.enabled = true;
     __googleBtn.enabled = true;
     __mailBtn.enabled = true;
     _Whatsbtn.enabled = true;
     _video_downloadbtn.enabled = true;
}

#pragma mark - FB Sharing with SLComposeViewController
-(void)ShareContentOnFBUsingSLComposer :(NSURL *)sourcePathString
{
     [self startTransparentIndicator];
     dispatch_async(dispatch_get_main_queue(), ^ {
          
          //code to be executed on the main queue after delay
          
          
          //     if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]]){
          
          [self stopTransparentIndicator];
          __facebookBtn.enabled = true;
          __twitterBtn.enabled = true;
          __googleBtn.enabled = true;
          __mailBtn.enabled = true;
          _Whatsbtn.enabled = true;
          _video_downloadbtn.enabled = true;
          
          if (![[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"])
          {
               [login1 logOut];
               [login1 logInWithPublishPermissions:@[@"publish_actions"]  fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
                {
                     NSLog(@"PERRRRRR %@",result.grantedPermissions);
                     NSLog(@"error facebook :%@",error);
                     if (result.isCancelled == true) {
                          [self stopTransparentIndicator];
                          
                          __facebookBtn.enabled = true;
                          __twitterBtn.enabled = true;
                          __googleBtn.enabled = true;
                          __mailBtn.enabled = true;
                          _Whatsbtn.enabled = true;
                          _video_downloadbtn.enabled = true;
                     }
                     
                     else if (!error)
                     {
                          txtShareText.text = @"Write your message here..";
                          txtShareText.textColor = [UIColor lightGrayColor];
                          lblCharaterCount.text = @"140";
                          
                          thumbnailImage =  [self loadThumbNail:sourcePathString];
                          imgVideoThumbnail.image = thumbnailImage;
                          [self stopTransparentIndicator];
                          lblSharingHeading.text = @"Facebook";
                          [self.view addSubview:shareDialogView];
                          
                     }
                     else
                     {
                          
                          login1 = [[FBSDKLoginManager alloc] init];
                          [login1 logOut];
                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Facebook" message:@"You don't have permission to share!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                          [alert show];
                          
                          [self stopTransparentIndicator];
                          
                          __facebookBtn.enabled = true;
                          __twitterBtn.enabled = true;
                          __googleBtn.enabled = true;
                          __mailBtn.enabled = true;
                          _Whatsbtn.enabled = true;
                          _video_downloadbtn.enabled = true;
                     }
                }];
          }
          else
          {
               lblCharaterCount.text = @"140";
               txtShareText.text = @"Write your message here..";
               txtShareText.textColor = [UIColor lightGrayColor];
               thumbnailImage =  [self loadThumbNail:sourcePathString];
               imgVideoThumbnail.image = thumbnailImage;
               [self stopTransparentIndicator];
               lblSharingHeading.text = @"Facebook";
               [self.view addSubview:shareDialogView];
          }
          
          
          //prabhjot
          
          
          //          FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc]init];
          //          FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
          //          video.videoURL = sourcePathString;
          //          FBSDKShareVideoContent *content = [[  FBSDKShareVideoContent alloc] init];
          //          content.video = video;
          //          shareDialog.shareContent = content;
          //          shareDialog.fromViewController = self;
          //          [shareDialog setMode:FBSDKShareDialogModeShareSheet];
          //            [shareDialog show];
          //          if ([shareDialog canShow])
          //          {
          //               if (content.video == nil)
          //               {
          //                    [shareDialog show];
          //               }
          //               else
          //               {
          //
          //                    [shareDialog resig]
          //
          //
          //               }
          //          }
          
          //     }
          //     else
          //     {
          //          [self sharecontentonfb:Main_videoPath];
          //     }
          
     });
     
}

-(void)sharecontentonfb : (NSString *)path
{
     [self startTransparentIndicator];
     [self uploadvideo:Main_videoPath];
}



-(void)uploadvideo :(NSString *)sourcePathString
{
     
     //prabhjot
     
     //     thumbnailImage =  [self loadThumbNail:sourcePathString];
     //     imgVideoThumbnail.image = thumbnailImage;
     //     [self stopTransparentIndicator];
     //     [self.view addSubview:shareDialogView];
     //     txtShareText.text = @"Voki";
     
     
     
     
     
     
     
     NSString *filePath = sourcePathString;
     NSData *videoData = [NSData dataWithContentsOfFile:filePath];
     NSString *text = @"";
     if (txtShareText.textColor != [UIColor lightGrayColor]) {
          text = txtShareText.text;
     }
     
     
     NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    videoData, @"video.mov",
                                    @"Voki", @"title",
                                    text, @"description",
                                    nil];
     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/videos"
                                        parameters:params
                                        HTTPMethod:@"POST"]
      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
           if ([error.userInfo[FBSDKGraphRequestErrorGraphErrorCode] isEqual:@200]) {
                
                __facebookBtn.enabled = true;
                __twitterBtn.enabled = true;
                __googleBtn.enabled = true;
                __mailBtn.enabled = true;
                _Whatsbtn.enabled = true;
                _video_downloadbtn.enabled = true;
                [self stopTransparentIndicator];
                
           }
           
           else
           {
                if (!error)
                {
                     
                     __facebookBtn.enabled = true;
                     __twitterBtn.enabled = true;
                     __googleBtn.enabled = true;
                     __mailBtn.enabled = true;
                     _Whatsbtn.enabled = true;
                     _video_downloadbtn.enabled = true;
                     playCharacterAud1.hidden = false;
                     playCharacterAud1.alpha = 1;
                     sharewithfacebook = false;
                     sharewithemail = false;
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Facebook" message:@"Your Voki has been shared" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                     [alert show];
                     alert.tag = 22;
                     sharecount++;
                     [self stopTransparentIndicator];
                     [self saveCharApiTrack];
                     [self stopTransparentIndicator];
                     
                }
                else
                {
                     NSLog(@"error.localizedDescription error.localizedDescription error.localizedDescription error.localizedDescription error.localizedDescription error.localizedDescription error.localizedDescription error.localizedDescription error.localizedDescription%@",error.localizedDescription);
                     
                     __facebookBtn.enabled = true;
                     __twitterBtn.enabled = true;
                     __googleBtn.enabled = true;
                     __mailBtn.enabled = true;
                     _Whatsbtn.enabled = true;
                     _video_downloadbtn.enabled = true;
                     playCharacterAud1.hidden = false;
                     playCharacterAud1.alpha = 1;
                     [self stopTransparentIndicator];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Facebook" message:@"There is some Permision Error!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                     [alert show];
                     __facebookBtn.enabled = true;
                     __twitterBtn.enabled = true;
                     __googleBtn.enabled = true;
                     __mailBtn.enabled = true;
                     _Whatsbtn.enabled = true;
                     _video_downloadbtn.enabled = true;
                     [self stopTransparentIndicator];
                     
                }
           }
      }];
}


- (void)shareDialog:(FBSDKShareDialog *)dialog
{
     
     NSError *error;
     if (![dialog validateWithError:&error]) {
          _alertController = [AlertControllerUtility alertControllerWithTitle:@"Invalid share content" message:@"Error validating share content"];
          [self presentViewController:_alertController animated:YES completion:nil];
          __facebookBtn.enabled = true;
          __twitterBtn.enabled = true;
          __googleBtn.enabled = true;
          __mailBtn.enabled = true;
          _Whatsbtn.enabled = true;
          _video_downloadbtn.enabled = true;
          return;
     }
     if (![dialog show]) {
          _alertController = [AlertControllerUtility alertControllerWithTitle:@"Invalid share content" message:@"Error opening dialog"];
          [self presentViewController:_alertController animated:YES completion:nil];
          __facebookBtn.enabled = true;
          __twitterBtn.enabled = true;
          __googleBtn.enabled = true;
          __mailBtn.enabled = true;
          _Whatsbtn.enabled = true;
          _video_downloadbtn.enabled = true;
     }
}






#pragma mark - FB Sharing with ACAccountStore

-(void)ShareOnFBUsingAccounts
{
     ACAccountStore* accountStore = [[ACAccountStore alloc]init];
     ACAccountType *FBaccountType= [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
     
     
     NSDictionary *fbInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"1653551541589302", ACFacebookAppIdKey, @[@"email"], ACFacebookPermissionsKey, ACFacebookAudienceOnlyMe, ACFacebookAudienceKey, nil];
     
     [accountStore requestAccessToAccountsWithType:FBaccountType options:fbInfo completion:
      ^(BOOL granted, NSError *e) {
           if (granted)
           {
                ACAccount* facebookAccount;
                NSArray *accounts = [accountStore accountsWithAccountType:FBaccountType];
                //it will always be the last object with single sign on
                facebookAccount = [accounts lastObject];
                
                
                ACAccountCredential *facebookCredential = [facebookAccount credential];
                NSString *accessToken = [facebookCredential oauthToken];
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"testing", @"message", nil];
                
                // Set up the Facebook Graph API URL for posting to a user's timeline
                NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];
                
                // Set up the request, passing all the required parameters
                SLRequest *fbShareRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:requestURL parameters:params];
                
                // Set up the account for this request
                fbShareRequest.account = facebookAccount;
                
                // Perform the request
                [fbShareRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                     // In the completion handler, echo back the response
                     // which should the Facebook post ID
                     
                     NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                }];
                
           } else
           {
                //Fail gracefully...
                
                sleep(10);
                
                
           }
      }];
}

-(void)sccountSelectedForTwitter:(UIAlertAction *)action
{
     NSLog(@"%@",action.title);
     
     lblCharaterCount.text = @"140";
     txtShareText.text = @"Write your message here..";
     txtShareText.textColor = [UIColor lightGrayColor];
     selectedAccount = [TwitterDeviceArray objectAtIndex:[TwitterAccountArray indexOfObject:action.title]];
     lblSharingHeading.text = @"Twitter";
     imgVideoThumbnail.image = thumbnailImage;
     [self stopTransparentIndicator];
     
     [self.view addSubview:shareDialogView];
}

-(void)refreshTwitterBasedOnSignIn:(NSURL *)filename
{
     //prabhjot
     
     
     NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 9 ");
     
     if (IS_IOS11orHIGHER == false) {
     
     
     ACAccountStore *accountStore = [[ACAccountStore alloc]init];
     
     if (accountStore != nil)
     {
          ACAccountType *accountType = [accountStore    accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
          if (accountType != nil)
          {
               [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
                {
                     if (granted)
                     {
                          isTwitterFirstTimeCount = isTwitterFirstTimeCount+2;
                          
                          //Succesful Access
                          [TwitterAccountArray removeAllObjects];
                          TwitterDeviceArray = [accountStore accountsWithAccountType:accountType];
                          
                          thumbnailImage = nil;
                          thumbnailImage =  [self loadThumbNail:filename];
                          imgVideoThumbnail.image = thumbnailImage;
                          
                          videoURL = filename;
                          
                          if (TwitterDeviceArray.count == 1) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    txtShareText.text = @"Write your message here..";
                                    txtShareText.textColor = [UIColor lightGrayColor];
                                    lblCharaterCount.text = @"140";
                                    selectedAccount = [TwitterDeviceArray objectAtIndex:0];
                                    [tableViewAccounts reloadData];
                                    [self stopTransparentIndicator];
                                    [self.view addSubview:shareDialogView];
                                    lblSharingHeading.text = @"Twitter";
                                    return ;
                               });
                               
                          }else if (TwitterDeviceArray.count == 0){
                               
                               //nslog show alert
                               dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [self stopTransparentIndicator];
                                    
                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Voki" message:@"Please add atleast one Twitter account in the settings." preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *SettingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                         
                                         sharewithfacebook = false;
                                         sharewithemail = false;
                                         sharingwithTwitter = false;
                                         [audioActivity stopAnimating];
                                         _video_downloadbtn.enabled = true;
                                         _Whatsbtn.enabled = true;
                                         __facebookBtn.enabled = true;
                                         self._googleBtn.enabled = true;
                                         __googleBtn.enabled = true;
                                         __facebookBtn.enabled = true;
                                         __mailBtn.enabled = true;
                                         __twitterBtn.enabled = true;
                                         self._twitterBtn.enabled = true;
                                         [self.video_downloadbtn setSelected:NO];
                                         _video_downloadbtn.enabled = true;
                                         [__twitterBtn setSelected:false];
                                         [self stopTransparentIndicator];
                                         
                                         NSLog(@"Settings");
                                    }];
                                    
                                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                         NSLog(@"OK");
                                         
                                         sharewithfacebook = false;
                                         sharewithemail = false;
                                         sharingwithTwitter = false;
                                         [audioActivity stopAnimating];
                                         _video_downloadbtn.enabled = true;
                                         _Whatsbtn.enabled = true;
                                         __facebookBtn.enabled = true;
                                         self._googleBtn.enabled = true;
                                         __googleBtn.enabled = true;
                                         __facebookBtn.enabled = true;
                                         __mailBtn.enabled = true;
                                         __twitterBtn.enabled = true;
                                         self._twitterBtn.enabled = true;
                                         [self.video_downloadbtn setSelected:NO];
                                         _video_downloadbtn.enabled = true;
                                         [__twitterBtn setSelected:false];
                                         [self stopTransparentIndicator];
                                         
                                    }];
                                    
                                    
                                    
                                    [alertController addAction:SettingsAction];
                                    [alertController addAction:okAction];
                                    [self presentViewController:alertController animated: YES completion: nil];
                                    
                               });
                               
                               
                          }else  {
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                                    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Please select account" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                                    
                                    for (int i = 0; i < TwitterDeviceArray.count; i++) {
                                         
                                         ACAccount *currentAccount = [TwitterDeviceArray objectAtIndex:i];
                                         [TwitterAccountArray addObject:currentAccount.userFullName];
                                         
                                         [actionSheet addAction:[UIAlertAction actionWithTitle:currentAccount.userFullName style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                              
                                              // Cancel button tappped.
                                              
                                              [self sccountSelectedForTwitter:action];
                                              [self dismissViewControllerAnimated:YES completion:^{
                                                   
                                                   
                                              }];
                                         }]];
                                         
                                    }
                                    
                                    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                         
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              sharewithfacebook = false;
                                              sharewithemail = false;
                                              sharingwithTwitter = false;
                                              [audioActivity stopAnimating];
                                              _video_downloadbtn.enabled = true;
                                              _Whatsbtn.enabled = true;
                                              __facebookBtn.enabled = true;
                                              self._googleBtn.enabled = true;
                                              __googleBtn.enabled = true;
                                              __facebookBtn.enabled = true;
                                              __mailBtn.enabled = true;
                                              __twitterBtn.enabled = true;
                                              self._twitterBtn.enabled = true;
                                              [self.video_downloadbtn setSelected:NO];
                                              _video_downloadbtn.enabled = true;
                                              [__twitterBtn setSelected:false];
                                              [self stopTransparentIndicator];
                                              
                                         });
                                         
                                         
                                         
                                         // OK button tapped.
                                         
                                         [self dismissViewControllerAnimated:YES completion:^{
                                         }];
                                    }]];
                                    
                                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                                    {
                                         actionSheet.popoverPresentationController.sourceView = self.view;
                                         actionSheet.popoverPresentationController.sourceRect = CGRectMake((self.view.bounds.size.width-250) / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
                                    }
                               
                                    
                                    // Present action sheet.
                                    [self presentViewController:actionSheet animated:YES completion:nil];
                                    
                                    //                               dispatch_async(dispatch_get_main_queue(), ^{
                                    //
                                    //                                    [tableViewAccounts reloadData];
                                    //                                    [self stopTransparentIndicator];
                                    //                                    txtShareText.text = @"Voki";
                                    //                                    [self.view addSubview:accountsBGView];
                                    //                                    [self.view bringSubviewToFront:accountsBGView];
                                    //
                                    //                                    return ;
                                    //                               });
                               });
                          }
                     }
                     else
                     {
                          //Access Denied
                          NSLog(@"access denied");
                     }
                }];
          }
     }
          
          
     
      }else{
     
     
//          TWTRComposer *composer = [[TWTRComposer alloc] init];
//          [composer setText:@"Voki"];
//     
//     
//          [composer setURL:filename];
//     
//          if (filename != nil)
//          {
//               NSData *data = [NSData dataWithContentsOfURL:filename];
//              [composer setImage:[UIImage imageWithData:data]];
//     
//          }
//     
//          [self.view endEditing:YES];
//     
//          // Called from a UIViewController
//          [composer showFromViewController:self completion:^(TWTRComposerResult result) {
//               if (result == TWTRComposerResultCancelled) {
//                    sharewithfacebook = false;
//                    sharewithemail = false;
//                    sharingwithTwitter = false;
//                    [audioActivity stopAnimating];
//                    _video_downloadbtn.enabled = true;
//                    _Whatsbtn.enabled = true;
//                    __facebookBtn.enabled = true;
//                    self._googleBtn.enabled = true;
//                    __googleBtn.enabled = true;
//                    __facebookBtn.enabled = true;
//                    __mailBtn.enabled = true;
//                    __twitterBtn.enabled = true;
//                    self._twitterBtn.enabled = true;
//                    [self.video_downloadbtn setSelected:NO];
//                    _video_downloadbtn.enabled = true;
//                    [__twitterBtn setSelected:false];
//                    [self stopTransparentIndicator];
//               }
//               else if (result == TWTRComposerResultDone) {
//                    sharewithfacebook = false;
//                    sharewithemail = false;
//                    sharingwithTwitter = false;
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Twitter" message:@"Your video was successfully shared" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//                    [alert show];
//                    alert.tag = 22;
//                    sharecount++;
//                    [self saveCharApiTrack];
//                    [audioActivity stopAnimating];
//                    _video_downloadbtn.enabled = true;
//                    _Whatsbtn.enabled = true;
//                    __facebookBtn.enabled = true;
//                    self._googleBtn.enabled = true;
//                    __googleBtn.enabled = true;
//                    __facebookBtn.enabled = true;
//                    __mailBtn.enabled = true;
//                    __twitterBtn.enabled = true;
//                    self._twitterBtn.enabled = true;
//                    [self.video_downloadbtn setSelected:NO];
//                    _video_downloadbtn.enabled = true;
//                    [__twitterBtn setSelected:false];
//                    [self stopTransparentIndicator];
//               }
//               else
//               {
//                    sharewithfacebook = false;
//                    sharewithemail = false;
//                    sharingwithTwitter = false;
//                    [audioActivity stopAnimating];
//                    _video_downloadbtn.enabled = true;
//                    _Whatsbtn.enabled = true;
//                    __facebookBtn.enabled = true;
//                    self._googleBtn.enabled = true;
//                    __googleBtn.enabled = true;
//                    __facebookBtn.enabled = true;
//                    __mailBtn.enabled = true;
//                    __twitterBtn.enabled = true;
//                    self._twitterBtn.enabled = true;
//                    [self.video_downloadbtn setSelected:NO];
//                    _video_downloadbtn.enabled = true;
//                    [__twitterBtn setSelected:false];
//                    [self stopTransparentIndicator];
//               }
//          }];
//     
     
    // return;
           
           
          if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
                TWTRComposerViewController *composer = [[TWTRComposerViewController  emptyComposer] initWithInitialText:@"Voki" image:nil videoURL:filename] ;
               composer.delegate = self;
               [self presentViewController:composer animated:YES completion:nil];
          } else {
               [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
                    if (session) {
                        
                         TWTRComposerViewController *composer = [[TWTRComposerViewController  emptyComposer] initWithInitialText:@"Voki" image:nil videoURL:filename ] ;
                         composer.delegate = self;

                         [self presentViewController:composer animated:YES completion:nil];
                    } else {
                         if ([error.localizedDescription isEqualToString:@"User cancelled login from Twitter App"]) {
                              sharewithfacebook = false;
                              sharewithemail = false;
                              sharingwithTwitter = false;
                              [audioActivity stopAnimating];
                              _video_downloadbtn.enabled = true;
                              _Whatsbtn.enabled = true;
                              __facebookBtn.enabled = true;
                              self._googleBtn.enabled = true;
                              __googleBtn.enabled = true;
                              __facebookBtn.enabled = true;
                              __mailBtn.enabled = true;
                              __twitterBtn.enabled = true;
                              self._twitterBtn.enabled = true;
                              [self.video_downloadbtn setSelected:NO];
                              _video_downloadbtn.enabled = true;
                              [__twitterBtn setSelected:false];
                              [self stopTransparentIndicator];
                              return ;
                         }
     
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Twitter Accounts Available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"cancel", nil];
                         alert.tag = 1;
     
                    }
               }];
          }
     }
}

- (void)composerDidCancel:(TWTRComposerViewController *)controller
{
     
     TWTRSessionStore *store = [Twitter sharedInstance].sessionStore;
     if (store.session.userID != nil) {
          [store logOutUserID:store.session.userID];
     }
     
     //let store = Twitter.sharedInstance().sessionStore
//     if let userID = store.session()?.userID {
//          store.logOutUserID(userID)
//     }
     
     sharewithfacebook = false;
     sharewithemail = false;
     sharingwithTwitter = false;
     [audioActivity stopAnimating];
     _video_downloadbtn.enabled = true;
     _Whatsbtn.enabled = true;
     __facebookBtn.enabled = true;
     self._googleBtn.enabled = true;
     __googleBtn.enabled = true;
     __facebookBtn.enabled = true;
     __mailBtn.enabled = true;
     __twitterBtn.enabled = true;
     self._twitterBtn.enabled = true;
     [self.video_downloadbtn setSelected:NO];
     _video_downloadbtn.enabled = true;
     [__twitterBtn setSelected:false];
     [self stopTransparentIndicator];
}

/**
 * Called when the user successfully sends a Tweet. The resulting Tweet object is returned.
 * This method is called after the view controller is dimsissed and the API response is
 * received.
 */

- (void)composerDidSucceed:(TWTRComposerViewController *)controller withTweet:(TWTRTweet *)tweet
{
     TWTRSessionStore *store = [Twitter sharedInstance].sessionStore;
     if (store.session.userID != nil) {
          [store logOutUserID:store.session.userID];
     }
     
     NSLog(@"postt");
     sharewithfacebook = false;
     sharewithemail = false;
     sharingwithTwitter = false;
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Twitter" message:@"Your video was successfully shared" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
     [alert show];
     alert.tag = 22;
     sharecount++;
     [self saveCharApiTrack];
     [audioActivity stopAnimating];
     _video_downloadbtn.enabled = true;
     _Whatsbtn.enabled = true;
     __facebookBtn.enabled = true;
     self._googleBtn.enabled = true;
     __googleBtn.enabled = true;
     __facebookBtn.enabled = true;
     __mailBtn.enabled = true;
     __twitterBtn.enabled = true;
     self._twitterBtn.enabled = true;
     [self.video_downloadbtn setSelected:NO];
     _video_downloadbtn.enabled = true;
     [__twitterBtn setSelected:false];
     [self stopTransparentIndicator];
}

/**
 * This method is called if the composer is not able to send the Tweet.
 * The view controller will not be dismissed automatically if this method is called.
 */
- (void)composerDidFail:(TWTRComposerViewController *)controller withError:(NSError *)error
{
                             sharewithfacebook = false;
                             sharewithemail = false;
                             sharingwithTwitter = false;
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Twitter" message:@"some Error occured. Please Try Again!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                             [alert show];
                             [audioActivity stopAnimating];
                             _video_downloadbtn.enabled = true;
                             _Whatsbtn.enabled = true;
                             __facebookBtn.enabled = true;
                             self._googleBtn.enabled = true;
                             __googleBtn.enabled = true;
                             __facebookBtn.enabled = true;
                             __mailBtn.enabled = true;
                             __twitterBtn.enabled = true;
                             self._twitterBtn.enabled = true;
                             [self.video_downloadbtn setSelected:NO];
                             _video_downloadbtn.enabled = true;
                             [__twitterBtn setSelected:false];
                              [self stopTransparentIndicator];
}

- (IBAction)TwitterAction:(id)sender
{
     NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 1 ");
     
     if (sharingwithTwitter == false)
     {
          sharingwithTwitter = true;
          
          @autoreleasepool {
               
               if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
               {
                    sharingwithTwitter = false;
                    alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
                    blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
                    alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
                    alertVc.deleteView.hidden = true;
                    alertVc.ratealert.hidden = true;
                    alertVc.blackview1.hidden = true;
                    alertVc.unblockLearnmore.hidden = true;
                    alertVc.unblockvookichar.hidden = true;
                    alertVc.whiteborderView.hidden = false;
                    [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:blurViewCalledbuy];
                    return;
               }
               else
               {
                    Acesstotweet = false;
                    [self getAccount];
                    [self startTransparentIndicator];
                    [self DisableButtonSelection];
                    [self.video_downloadbtn setSelected:NO];
                    [__facebookBtn setSelected:NO];
                    [self._twitterBtn setSelected:YES];
                    sharewithfacebook = true;
                    appDelegate.isFacebookCalled = NO;
                    appDelegate.isTwitterCalled = YES;
                    Videotempsave = false;
                    videofbshare = true;
                    videotwittershare = true;
                    
                    
                    if (outurltweet == nil)
                    {
                         NSString *Audiopath;
                         
                         Audiopath = [[NSUserDefaults standardUserDefaults]
                                      stringForKey:@"AudioPath"];
                         
                         NSString *sourcePath = [[NSUserDefaults standardUserDefaults]
                                                 stringForKey:@"VideoPath"];
                         
                         if (sourcePath == nil || Audiopath == nil || [sourcePath isEqualToString:@""] || [sourcePath isEqual: [NSNull null]] || [Audiopath isEqualToString:@""] || [Audiopath isEqual: [NSNull null]] )
                         {
                              sharingwithTwitter = false;
                              UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please play the audio to enable video generation, then share on Twitter!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                              [alert show];
                              [self stopTransparentIndicator];
                              
                              
                         }
                         else
                         {
                              NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 2 ");
                              
                              Videotempsave = false;
                              videofbshare = true;
                              videotwittershare = true;
                              [self startTransparentIndicator];
                              [self videorecording];
                         }
                    }
                    
                    else
                         
                    {
                         
                         NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 2-1 ");
                         
                         
                         [self startTransparentIndicator];
                         Main_videoPath = [[NSUserDefaults standardUserDefaults]
                                           stringForKey:@"shreingvideosocial"];
                         sharingCount = 2;
                         
                         double delayInSeconds = 4.0;
                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                         [__twitterBtn setSelected:YES];
                         
                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                              //code to be executed on the main queue after delay
                              [self ShareContentOnTwitterUsingSLComposer:outurltweet];
                         });
                         
                         
                    }
                    [self.view setUserInteractionEnabled:true];
               }
          }
     }
}

+ (BOOL) userHasAccessToTwitter {
     return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}


-(void)getAccount    {
     
     
     ACAccountStore *account = [[ACAccountStore alloc] init];
     ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
     [account requestAccessToAccountsWithType:accountType options:nil
                                   completion:^(BOOL granted, NSError *error)
      {
           if (granted == YES) {
                
             

                NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
                if ([arrayOfAccounts count] > 0) {
                     ACAccount *twitterAccount = [arrayOfAccounts lastObject];
                     if (twitterAccount == nil) {
                          NSLog(@"ACAccount = nil");
                          //                          TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
                          //                          NSString *userID = store.session.userID;
                          TweeterAcess = true;
                          // [store logOutUserID:userID];
                          Acesstotweet = false;
                          return;
                     }
                     else
                     {
                          TweeterAcess = true;
                          Acesstotweet = true;
                     }
                }
                else
                {
                     //                     TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
                     //                     NSString *userID = store.session.userID;
                     TweeterAcess = true;
                     //[store logOutUserID:userID];
                     Acesstotweet = false;
                }
                
                if (isTwitterFirstTimeCount == 0) {
                     [self ShareContentOnTwitterUsingSLComposer:fileNameFirst];
                }
                
                isTwitterFirstTimeCount = isTwitterFirstTimeCount+2;
                
           }
           else
           {

                //                TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
                //                NSString *userID = store.session.userID;
                TweeterAcess = false;
                // [store logOutUserID:userID];
                Acesstotweet = false;
                
                if (IS_IOS11orHIGHER == true) {
                     if (isTwitterFirstTimeCount == 0) {
                          isTwitterFirstTimeCount = isTwitterFirstTimeCount+2;
                          if (fileNameFirst != nil) {
                                [self ShareContentOnTwitterUsingSLComposer:fileNameFirst];
                          }
                         
                     }
                }
               
                
                isTwitterFirstTimeCount = isTwitterFirstTimeCount+1;

           }
           
      }];
}
//prabhjot
#pragma mark - Sharing Dialog Buttons Action

- (IBAction)btnPostAction:(id)sender {
     
     if (videotwittershare == true )
     {
          NSData *webData = [NSData dataWithContentsOfURL:videoURL];
          
          if (webData == nil) {
               
               return;
          }
          
          [self.view endEditing:YES ];
          [shareDialogView removeFromSuperview];
          [self startTransparentIndicator];
          
          NSString *text = @"";
          if (txtShareText.textColor != [UIColor lightGrayColor]) {
               text = txtShareText.text;
          }
          [SocialVideoHelper uploadTwitterVideo:webData comment:text account:selectedAccount    withCompletion:^(BOOL success, NSString *errorMessage) {
               
               if (success){
                    
                    NSLog(@"postt");
                    sharewithfacebook = false;
                    sharewithemail = false;
                    sharingwithTwitter = false;
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Twitter" message:@"Your video was successfully shared" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                    alert.tag = 22;
                    sharecount++;
                    [self saveCharApiTrack];
                    [audioActivity stopAnimating];
                    _video_downloadbtn.enabled = true;
                    _Whatsbtn.enabled = true;
                    __facebookBtn.enabled = true;
                    self._googleBtn.enabled = true;
                    __googleBtn.enabled = true;
                    __facebookBtn.enabled = true;
                    __mailBtn.enabled = true;
                    __twitterBtn.enabled = true;
                    self._twitterBtn.enabled = true;
                    [self.video_downloadbtn setSelected:NO];
                    _video_downloadbtn.enabled = true;
                    [__twitterBtn setSelected:false];
                    [self stopTransparentIndicator];
               }
               else
               {
                    [self.view endEditing:YES ];
                    [shareDialogView removeFromSuperview];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Twitter" message:@"Your video was not shared. Please try again." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                    
                    sharewithfacebook = false;
                    sharewithemail = false;
                    sharingwithTwitter = false;
                    [audioActivity stopAnimating];
                    _video_downloadbtn.enabled = true;
                    _Whatsbtn.enabled = true;
                    __facebookBtn.enabled = true;
                    self._googleBtn.enabled = true;
                    __googleBtn.enabled = true;
                    __facebookBtn.enabled = true;
                    __mailBtn.enabled = true;
                    __twitterBtn.enabled = true;
                    self._twitterBtn.enabled = true;
                    [self.video_downloadbtn setSelected:NO];
                    _video_downloadbtn.enabled = true;
                    [__twitterBtn setSelected:false];
                    [self stopTransparentIndicator];
               }
          }];
     }
     else
     {
          [self.view endEditing:YES ];
          [shareDialogView removeFromSuperview];
          [self startTransparentIndicator];
          [self sharecontentonfb:Main_videoPath];
     }
     
     
}

- (IBAction)btnCancelAction:(id)sender {
     
     [self.view endEditing:YES ];
     [shareDialogView removeFromSuperview];
     
     sharewithfacebook = false;
     sharewithemail = false;
     sharingwithTwitter = false;
     [audioActivity stopAnimating];
     _video_downloadbtn.enabled = true;
     _Whatsbtn.enabled = true;
     __facebookBtn.enabled = true;
     self._googleBtn.enabled = true;
     __googleBtn.enabled = true;
     __facebookBtn.enabled = true;
     __mailBtn.enabled = true;
     __twitterBtn.enabled = true;
     self._twitterBtn.enabled = true;
     [self.video_downloadbtn setSelected:NO];
     _video_downloadbtn.enabled = true;
     [__twitterBtn setSelected:false];
     [self stopTransparentIndicator];
     
}



- (IBAction)btnCrossAccountAction:(id)sender {
     
     [self.view endEditing:YES ];
     [accountsBGView removeFromSuperview];
     //constraintHeightOfAccountsTable.constant = 0;
     
}
- (IBAction)btnVideoPlayAction:(id)sender {
     [self.view endEditing:YES];
     // create an AVPlayer
     
     [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"VideoPlayed"];
     
     AVPlayer *player ;
     if (videotwittershare == true )
     {
          player = [AVPlayer playerWithURL:videoURL];
          
     }else{
          
          NSString *filePath;
          //prabh_new
          if (![[[outurltweet absoluteString] substringToIndex:1]  isEqual: @"f"]) {
               filePath = [NSString stringWithFormat:@"%@%@",@"file://",outurltweet];
               
          }
          else
          {
               filePath = [outurltweet absoluteString];
          }
          
          
          player = [AVPlayer playerWithURL:[NSURL URLWithString:filePath]];
          
     }
     
     
     
     AVPlayerViewController *playerViewController = [AVPlayerViewController new];
     playerViewController.view.frame = self.view.frame;
     playerViewController.player = player;
     //     playerViewController.delegate = self;
     playerViewController.player = player;
     [self presentViewController:playerViewController animated:YES completion:nil];
     [player play];
     
     
}


#pragma mark - Twitter Sharing with SLComposeViewController
-(void)ShareContentOnTwitterUsingSLComposer:(NSURL *)filename
{
     NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 8 ");
    
     fileNameFirst = filename;
     
     if (IS_IOS11orHIGHER == true) {
          [self startTransparentIndicator];
          [self refreshTwitterBasedOnSignIn:filename];
          return;
     }
     
     
     if (TweeterAcess == true)
     {
          
          //check if Video Duration is Greater than 30 Sec
//          commented by remove the limit
//          NSString *sourcePath = [[NSUserDefaults standardUserDefaults]
//                                  stringForKey:@"VideoPath"];
//          
//          NSURL *VideoURL = [[NSURL alloc] initFileURLWithPath:sourcePath];
//          AVURLAsset* firstAsset = [[AVURLAsset alloc]initWithURL:VideoURL options:nil];
          
          // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
//          CMTime duration = firstAsset.duration;
//          float seconds = CMTimeGetSeconds(duration);
          
          
          
          
//          if (status == false )
//          {
//               sharingwithTwitter = false;
//
//               
//               UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!" message:@"Sharing on twitter is limited to 25-30 seconds or shorter. Please update your Voki audio & try again."   delegate:self cancelButtonTitle:nil otherButtonTitles: @"ok", nil];
//               alert.tag = 3255;
//               [audioActivity stopAnimating];
//               _video_downloadbtn.enabled = true;
//               _Whatsbtn.enabled = true;
//               __facebookBtn.enabled = true;
//               self._googleBtn.enabled = true;
//               __googleBtn.enabled = true;
//               __facebookBtn.enabled = true;
//               __mailBtn.enabled = true;
//               __twitterBtn.enabled = true;
//               self._twitterBtn.enabled = true;
//               [self.video_downloadbtn setSelected:NO];
//               _video_downloadbtn.enabled = true;
//               [alert show];
//               [self stopTransparentIndicator];
//               
//               return;
//               
//               
//          }
//          else
//          if (seconds > 30)
//          {
//               sharingwithTwitter = false;
//
//               UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!" message:@"Sharing on twitter is limited to 25-30 seconds or shorter. Please update your Voki audio & try again."   delegate:self cancelButtonTitle:nil otherButtonTitles: @"ok", nil];
//               alert.tag = 3255;
//               [audioActivity stopAnimating];
//               _video_downloadbtn.enabled = true;
//               _Whatsbtn.enabled = true;
//               __facebookBtn.enabled = true;
//               self._googleBtn.enabled = true;
//               __googleBtn.enabled = true;
//               __facebookBtn.enabled = true;
//               __mailBtn.enabled = true;
//               __twitterBtn.enabled = true;
//               self._twitterBtn.enabled = true;
//               [self.video_downloadbtn setSelected:NO];
//               _video_downloadbtn.enabled = true;
//               [alert show];
//               [self stopTransparentIndicator];
//               return;
//               
//          }
//          else
//          {
               [self startTransparentIndicator];
               
               [self refreshTwitterBasedOnSignIn:filename];
               
//          }
     }
     else
     {
          
          sharingwithTwitter = false;

          _video_downloadbtn.enabled = true;
          _Whatsbtn.enabled = true;
          __facebookBtn.enabled = true;
          self._googleBtn.enabled = true;
          __googleBtn.enabled = true;
          __facebookBtn.enabled = true;
          __mailBtn.enabled = true;
          __twitterBtn.enabled = true;
          self._twitterBtn.enabled = true;
          [self.video_downloadbtn setSelected:NO];
          _video_downloadbtn.enabled = true;
          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Give Access To Twitter" delegate:self cancelButtonTitle:@"Setting" otherButtonTitles:@"cancel", nil];
          alert.tag = 1;
          
          [audioActivity stopAnimating];
          _video_downloadbtn.enabled = true;
          _Whatsbtn.enabled = true;
          __facebookBtn.enabled = true;
          self._googleBtn.enabled = true;
          __googleBtn.enabled = true;
          __facebookBtn.enabled = true;
          __mailBtn.enabled = true;
          __twitterBtn.enabled = true;
          self._twitterBtn.enabled = true;
          [self.video_downloadbtn setSelected:NO];
          _video_downloadbtn.enabled = true;
          [self stopTransparentIndicator];
          if (isTwitterFirstTimeCount <= 1) {
               return;
          }
          [alert show];
     }
}

#pragma mark - Google Plus sharing

- (IBAction)GoogleAction:(id)sender
{
     
     
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
     }
     
     else
     {
          if (saveSceneUrl == nil)
          {
               whatsappshared = NO;
               Googleshared = YES;
               Mailshared = NO;
               [self createsharedLink];
               
               
          }
          else
          {
               [self GooglePlussharing];
               
          }
          
     }
}

-(void)GooglePlussharing
{
     [__googleBtn setSelected:YES];
     [__facebookBtn setSelected:NO];
     [_video_downloadbtn setSelected:NO];
     [__mailBtn setSelected:NO];
     [__twitterBtn setSelected:NO];
     [self.Whatsbtn setSelected:NO];
     [self refreshInterfaceBasedOnSignIn];
}





//creating shared Link tomail and Google ...

-(void)createsharedLink
{
     if (backgroundID == nil)
     {
          
          NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"backgalleryImage"];
          
          if (imageData)
          {
               UIImage *im = [UIImage imageWithData:imageData];
               ownBackround = UIImageJPEGRepresentation(im, 0.1);
               [self ownbackgroundApi:ownBackround];
          }
          
          else  if (saveSceneUrl == nil)
          {
               
               characterCSURL = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"fetchAPILink()"];
               [self saveScene];
          }
          
     }
     else  if (saveSceneUrl == nil)
     {
          
          characterCSURL = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"fetchAPILink()"];
          [self saveScene];
          
          
          
          
          
     }
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
     // NSLog(@"Received error %@ and auth object %@",error, auth);
     if (error) {
          // Do some error handling here.
     } else {
          [self refreshInterfaceBasedOnSignIn];
     }
}


-(void)refreshInterfaceBasedOnSignIn {
     
     appDelegate.isFacebookCalled = NO;
     appDelegate.isTwitterCalled = NO;
     
     if ([[GPPSignIn sharedInstance] authentication])
     {
          
          [self DisableButtonSelection];
          [__googleBtn setSelected:YES];
          [self.view setUserInteractionEnabled:true];
          [audioActivity stopAnimating];
          [self googleaction];
     }
     else
     {
     
          
          GPPSignIn *signIn = [GPPSignIn sharedInstance];
          signIn.shouldFetchGooglePlusUser = YES;
          signIn.clientID = GOOGLE_PLUS_CLIENT_ID;
          sharewithfacebook = true;
          signIn.delegate = self;
          [signIn trySilentAuthentication];
          [signIn authenticate];
     }
}




-(void)googleaction
{
     
     @autoreleasepool {
          
          [self.view setUserInteractionEnabled:true];
          [audioActivity stopAnimating];
          
          NSString *widthThumb = [NSString stringWithFormat:@"%.0f",self.view.frame.size.height*2.2];
          NSString *xThumb = [NSString stringWithFormat:@"-%.0f",self.view.frame.size.height*1.6];
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               xThumb = [NSString stringWithFormat:@"-%.0f",self.view.frame.size.height-80];
               widthThumb = [NSString stringWithFormat:@"%.0f",self.view.frame.size.height*1.0];
          }
          else
          {
               if (_canvasWidth == 480)
               {
                    xThumb = [NSString stringWithFormat:@"-%.0f",self.view.frame.size.height*1.1];
               }
          }
          NSString *CreateThumbnail = [NSString stringWithFormat:@"CreateThumbnail('%@','%@','%@','%@')",widthThumb,widthThumb,xThumb,@"-50"];
          NSString *Base64 = [___CharacterWebView stringByEvaluatingJavaScriptFromString:CreateThumbnail];
          NSMutableArray *resaved = [[NSMutableArray alloc]init];
          
          if (!resaved) {
               resaved=[[NSMutableArray alloc] init];
          }
          
          
          NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       
                                       Base64,@"image64",
                                       [LibPath mutableCopy],@"lib",
                                       nil];
          
          [resaved addObject:dict];
          NSString *base6=[[resaved valueForKey:@"image64"] objectAtIndex:0];
          NSURL *url1 =[NSURL URLWithString:base6];
          NSData *imageData = [NSData dataWithContentsOfURL:url1];
          UIImage *ret2 = [UIImage imageWithData:imageData];
          [self.view setUserInteractionEnabled:true];
          appDelegate.isFacebookCalled = NO;
          appDelegate.isTwitterCalled = NO;
          
          //[self shareButtonClicked:@""];
          [audioActivity stopAnimating];
          id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
          [GPPShare sharedInstance].delegate = self;
          [shareBuilder attachImage:ret2];
          sharewithfacebook = false;
          sharewithemail = false;
          NSString *temp =[NSString stringWithFormat:@"%@%@%@%@%@%@",@"My Voki has something to say - check it out:",@"\n",saveSceneUrl,@"\n",@"\n",@"To create your own Voki go to - www.voki.com"];
          [shareBuilder setPrefillText:temp];
          [shareBuilder open];
          
          
     }
}

- (void)finishedSharingWithError:(NSError *)error {
     NSString *text;
     
     if (!error) {
          text = @"Success";
          
          [self saveCharApiTrack];
          sharecount ++ ;
          sharewithfacebook = false;
          sharewithemail = false;
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Google Plus" message:@"Your video was successfully shared" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
          alert.tag = 22;
          [alert show];
          
     }
     else if (error.code == kGPPErrorShareboxCanceled) {
          text = @"Canceled";
     } else {
          text = [NSString stringWithFormat:@"Error (%@)", [error localizedDescription]];
     }
     
}

#pragma mark VedioDownloadAction
- (IBAction)video_downloadAction:(id)sender
{
     
     if ([Main_videoPath isEqualToString:@""])
     {
          Videotempsave = false;
          videofbshare = false;
          _Whatsbtn.enabled = false;
          __googleBtn.enabled = false;
          __facebookBtn.enabled = false;
          __twitterBtn.enabled = false;
          __mailBtn.enabled = false;
          _video_downloadbtn.enabled = false;
          [self DisableButtonSelection];
          _video_downloadbtn.selected = YES;
          [audioActivity stopAnimating];
          
          NSString *sourcePath = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"VideoPath"];
          
          
          NSString *Audiopath;
          //if (WithFxvalue == true)
          //{
          // Audiopath = [[NSUserDefaults standardUserDefaults]
          //stringForKey:@"AudioPath1"];
          
          
          
          //          }
          //          else
          //          {
          Audiopath = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"AudioPath"];
          // }
          
          
          [self startTransparentIndicator];
          
          if (sourcePath == nil || Audiopath == nil || [sourcePath isEqualToString:@""] || [sourcePath isEqual: [NSNull null]] || [Audiopath isEqualToString:@""] || [Audiopath isEqual: [NSNull null]] )
          {
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.000 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [self videorecording];
                    
               });
               
          }
          else
          {
               [self videorecording];
          }
          
     }
     else
     {
          Videotempsave = false;
          videofbshare = false;
          _Whatsbtn.enabled = false;
          __googleBtn.enabled = false;
          __facebookBtn.enabled = false;
          __twitterBtn.enabled = false;
          __mailBtn.enabled = false;
          _video_downloadbtn.enabled = false;
          [self DisableButtonSelection];
          _video_downloadbtn.selected = YES;
          [audioActivity stopAnimating];
          NSString *sourcePath = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"VideoPath"];
          
          [self savevideoingaller:Main_videoPath];
          
          
     }
     
     
}

-(void)savevideoingaller:(NSString*)exportPath
{
     NSURL *movieURL = [NSURL fileURLWithPath:exportPath];
     
     ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
     [library writeVideoAtPathToSavedPhotosAlbum:movieURL
                                 completionBlock:^(NSURL *assetURL, NSError *error)
      {
           
           
           if (error)
           {
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                     [audioActivity stopAnimating];
                     UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"Fail!" message: @"The user has denied the application access to their media" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert1 show];
                     [self stopTransparentIndicator];
                     
                     [audioActivity stopAnimating];
                     _video_downloadbtn.enabled = true;
                     _Whatsbtn.enabled = true;
                     __facebookBtn.enabled = true;
                     self._googleBtn.enabled = true;
                     __googleBtn.enabled = true;
                     __facebookBtn.enabled = true;
                     __mailBtn.enabled = true;
                     __twitterBtn.enabled = true;
                     self._twitterBtn.enabled = true;
                     [self.video_downloadbtn setSelected:NO];
                     _video_downloadbtn.enabled = true;
                });
                
           }
           else
           {
                
                if ([assetURL isEqual:NULL] || [assetURL isEqual:nil]||[assetURL isEqual:@""])
                {
                     [self stopTransparentIndicator];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                          [audioActivity stopAnimating];
                          UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"Fail!" message:@"Video Fail To save in Galley!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                          [alert1 show];
                          [audioActivity stopAnimating];
                          _video_downloadbtn.enabled = true;
                          _Whatsbtn.enabled = true;
                          __facebookBtn.enabled = true;
                          self._googleBtn.enabled = true;
                          __googleBtn.enabled = true;
                          __facebookBtn.enabled = true;
                          __mailBtn.enabled = true;
                          __twitterBtn.enabled = true;
                          self._twitterBtn.enabled = true;
                          [self.video_downloadbtn setSelected:NO];
                          _video_downloadbtn.enabled = true;
                     });
                }
                else
                {
                     
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                          [audioActivity stopAnimating];
                          UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Video saved to the gallery" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                          [alert1 show];
                          [audioActivity stopAnimating];
                          _video_downloadbtn.enabled = true;
                          _Whatsbtn.enabled = true;
                          __facebookBtn.enabled = true;
                          __googleBtn.enabled = true;
                          __facebookBtn.enabled = true;
                          self._googleBtn.enabled = true;
                          self._twitterBtn.enabled = true;
                          __twitterBtn.enabled = true;
                          __mailBtn.enabled = true;
                          [self.video_downloadbtn setSelected:NO];
                          _video_downloadbtn.enabled = true;
                          
                          
                     });
                }
                
           }
           
           
      }];
     
}




-(void)videorecording
{
     
     if (ishalfrecording == true)
          
     {
          sameaudio = false;
          
          UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"You must playback your entire Voki scene before sharing to video" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
          _Whatsbtn.enabled = true;
          __googleBtn.enabled = true;
          __twitterBtn.enabled = true;
          __facebookBtn.enabled = true;
          __mailBtn.enabled = true;
          [audioActivity stopAnimating];
          _video_downloadbtn.selected = NO;
          _video_downloadbtn.enabled = true;
          sharingwithTwitter = false;
          [self stopTransparentIndicator];
          return;
     }
     
     
     
     [audioActivity stopAnimating];
     NSString *Audiopath;
     //     if (WithFxvalue == true)
     //     {
     //          Audiopath = [[NSUserDefaults standardUserDefaults]
     //                       stringForKey:@"AudioPath1"];
     //
     //
     //
     //     }
     //     else
     //     {
     Audiopath = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"AudioPath"];
     
     NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 3 ");
     
     
     NSString *sourcePath = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"VideoPath"];
     
     if (sourcePath == nil || Audiopath == nil || [sourcePath isEqualToString:@""] || [sourcePath isEqual: [NSNull null]] || [Audiopath isEqualToString:@""] || [Audiopath isEqual: [NSNull null]] )
     {
          [self stopTransparentIndicator];
          _Whatsbtn.enabled = true;
          __googleBtn.enabled = true;
          __twitterBtn.enabled = true;
          __facebookBtn.enabled = true;
          __mailBtn.enabled = true;
          [audioActivity stopAnimating];
          _video_downloadbtn.selected = NO;
          _video_downloadbtn.enabled = true;
          if (Videotempsave == false)
          {
               sharingwithTwitter = false;
               [self stopTransparentIndicator];
               
               UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please play the audio to enable video generation, then click this button again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
               [alert show];
          }
     }
     else
     {
          sharingwithTwitter = false;
          _Whatsbtn.enabled = false;
          __googleBtn.enabled = false;
          __facebookBtn.enabled = false;
          __mailBtn.enabled = false;
          __twitterBtn.enabled = false;
          _video_downloadbtn.enabled = false;
          [self videosaveinlibrarury:sourcePath :Audiopath];
          
     }
     
     
}

-(void)exportDidFinish:(AVAssetExportSession*)session {
     
     NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 7 ");
     
     NSLog(@"FINISH FINISH FINISH FINISH FINISH FINISH FINISH FINISH FINISH %@ %@",Main_videoPath, session.outputURL);
     
     if (session.status == AVAssetExportSessionStatusCompleted) {
          
          NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 7 - 1 ");
          
          NSURL *outputURL = session.outputURL;
          
          [[NSUserDefaults standardUserDefaults] setObject:Main_videoPath forKey:@"shreingvideosocial"];
          [[NSUserDefaults standardUserDefaults] synchronize];
          
          outurltweet = outputURL;
          
          if (videofbshare == true)
          {
               NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 7 - 2 ");
               
               // Main_videoPath = Path;
               if (videotwittershare == true )
               {
                    NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 7 - 3 ");
                    
                    [self stopTransparentIndicator];
                    
                    [self ShareContentOnTwitterUsingSLComposer:outurltweet];
                    return;
                    
                    
               }
               else
               {
                    
                    if (videotwittershare == true )
                    {
                         
                         [self stopTransparentIndicator];
                         
                         [self ShareContentOnTwitterUsingSLComposer:outurltweet];
                         return;
                    }
                    else
                    {
                         
                         [self stopTransparentIndicator];
                         [self ShareContentOnFBUsingSLComposer:outurltweet];
                         return;
                         
                    }
                    
                    
                    
               }
               
          }
          else
          {
               ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
               if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                    [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                         dispatch_async(dispatch_get_main_queue(), ^{
                              if (error)
                              {
                                   
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                                                  delegate:nil cancelButtonTitle:@"OK"
                                                                         otherButtonTitles:nil];
                                   [alert show];
                                   [self stopTransparentIndicator];
                                   
                                   _video_downloadbtn.enabled = true;
                                   _Whatsbtn.enabled = true;
                                   __facebookBtn.enabled = true;
                                   __googleBtn.enabled = true;
                                   __facebookBtn.enabled = true;
                                   self._googleBtn.enabled = true;
                                   self._twitterBtn.enabled = true;
                                   __twitterBtn.enabled = true;
                                   __mailBtn.enabled = true;
                                   [self.video_downloadbtn setSelected:NO];
                                   _video_downloadbtn.enabled = true;
                              } else {
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
                                                                                  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                   [alert show];
                                   
                                   
                                   _video_downloadbtn.enabled = true;
                                   _Whatsbtn.enabled = true;
                                   __facebookBtn.enabled = true;
                                   __googleBtn.enabled = true;
                                   __facebookBtn.enabled = true;
                                   self._googleBtn.enabled = true;
                                   self._twitterBtn.enabled = true;
                                   __twitterBtn.enabled = true;
                                   __mailBtn.enabled = true;
                                   [self.video_downloadbtn setSelected:NO];
                                   _video_downloadbtn.enabled = true;
                              }
                         });
                    }];
               }
          }
          
          
          
          
          dispatch_async(dispatch_get_main_queue(), ^{
               
               
               
               
               if (Videotempsave == true )
               {
                    
                    [self stopTransparentIndicator];
                    
                    
                    
               }
               else
               {
                    if (videofbshare == true)
                    {
                         // Main_videoPath = Path;
                         if (videotwittershare == true )
                         {
                              
                              
                         }
                         else
                         {
                              
                              
                         }
                         
                    }
                    else
                    {
                         
                         
                    }
               }
               
          });
          
          
          
          
     }
     else{
          sharewithfacebook = false;
          sharewithemail = false;
          sharingwithTwitter = false;
          [audioActivity stopAnimating];
          _video_downloadbtn.enabled = true;
          _Whatsbtn.enabled = true;
          __facebookBtn.enabled = true;
          self._googleBtn.enabled = true;
          __googleBtn.enabled = true;
          __facebookBtn.enabled = true;
          __mailBtn.enabled = true;
          __twitterBtn.enabled = true;
          self._twitterBtn.enabled = true;
          [self.video_downloadbtn setSelected:NO];
          _video_downloadbtn.enabled = true;
          [__twitterBtn setSelected:false];
          [self stopTransparentIndicator];
          
          UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"oops!" message: @"Generated video is corrupt.Please Try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert1 show];
     }
}

-(void) video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
     if(error)
          NSLog(@"Finished saving video with error: %@", error);
}


-(void)videosaveinlibrarury:(NSString*)path :(NSString*)AudioPath
{
     NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 4 ");
     
     NSURL *audiooutputURL = [[NSURL alloc] initFileURLWithPath:AudioPath];
     NSURL *VideoURL = [[NSURL alloc] initFileURLWithPath:path];
     
     AVURLAsset* firstAsset = [[AVURLAsset alloc]initWithURL:VideoURL options:nil];
     AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:audiooutputURL options:nil];
     
     
     
     if (firstAsset !=nil && audioAsset!=nil) {
          // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        
        //  AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:VideoURL options:nil];
        //  AVMutableComposition* mixComposition = [AVMutableComposition composition];
          CMTime duration = firstAsset.duration;
          float seconds = CMTimeGetSeconds(duration);
          
          NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG %@ ----- %@ ",audiooutputURL, VideoURL);
          
          
          if (seconds == 0.0)
          {
               NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 5 ");
              
               UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"oops!" message: @"Generated video is corrupt.Please Try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
               [alert1 show];
               [self stopTransparentIndicator];
               [audioActivity stopAnimating];
               _video_downloadbtn.enabled = true;
               _Whatsbtn.enabled = true;
               __facebookBtn.enabled = true;
               self._googleBtn.enabled = true;
               __googleBtn.enabled = true;
               __facebookBtn.enabled = true;
               __mailBtn.enabled = true;
               __twitterBtn.enabled = true;
               self._twitterBtn.enabled = true;
               [self.video_downloadbtn setSelected:NO];
               _video_downloadbtn.enabled = true;
               
          }
          else
          {
               
//               AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
//               AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
//               //If you need audio as well add the Asset Track for audio here
//               
//               [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration) ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
//               
//               [compositionVideoTrack setPreferredTransform:[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] preferredTransform]];
//               
//               CGSize sizeOfVideo=[videoAsset naturalSize];
//               
//               //TextLayer defines the text they want to add in Video
//               //Text of watermark
//               //Image of watermark
//               UIImage *myImage=[UIImage imageNamed:@"Vokiicon.png"];
//               CALayer *layerCa = [CALayer layer];
//               layerCa.contents = (id)myImage.CGImage;
//               layerCa.frame = CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
//               layerCa.opacity = 1.0;
//               
//               
//               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
//               {
//                    
//                    layerCa.frame = CGRectMake(sizeOfVideo.width - 100, 700, 85, 85);
//               }
//               else if (_canvasWidth >= 667)
//               {
//                    layerCa.frame = CGRectMake(sizeOfVideo.width - 100, 290, 85, 85);
//               }
//               else
//               {
//                    layerCa.frame = CGRectMake(sizeOfVideo.width - 100, 220, 85, 85);
//               }
//               
//               CALayer *parentLayer=[CALayer layer];
//               CALayer *videoLayer=[CALayer layer];
//               parentLayer.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
//               videoLayer.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
//               [parentLayer addSublayer:videoLayer];
//               [parentLayer addSublayer:layerCa];
//               
//               
//               AVMutableVideoComposition *videoComposition=[AVMutableVideoComposition videoComposition] ;
//               videoComposition.frameDuration=CMTimeMake(1, 30);
//               videoComposition.renderSize=sizeOfVideo;
//               videoComposition.animationTool=[AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
//               
//               
//               AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
//               instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
//               AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
//               AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
//               instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
//               videoComposition.instructions = [NSArray arrayWithObject: instruction];
//               
//               NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//               NSString *documentsDirectory = [paths objectAtIndex:0];
//               NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
//                                        [NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
//               NSURL *url = [NSURL fileURLWithPath:myPathDocs];
//               
//               AVAssetExportSession *exportSession1 = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
//               exportSession1.videoComposition=videoComposition;
//               exportSession1.outputURL = url;
//               exportSession1.outputFileType = AVFileTypeQuickTimeMovie;
//               [exportSession1 exportAsynchronouslyWithCompletionHandler:^{
//                    switch (exportSession1.status)
//                    {
//                         case AVAssetExportSessionStatusCompleted:
//                         {
               
                              AVURLAsset* firstAsset = [[AVURLAsset alloc]initWithURL:VideoURL options:nil];
                              NSLog(@"%f, %f",(CMTimeGetSeconds(firstAsset.duration)),(CMTimeGetSeconds(audioAsset.duration)));
                              
                              double  time =  (CMTimeGetSeconds(firstAsset.duration));
                              
                              double  time1 =  (CMTimeGetSeconds(audioAsset.duration));
                              
                              double startTimeoFView = (CMTimeGetSeconds(firstAsset.duration))-(CMTimeGetSeconds(audioAsset.duration));
                              
                              //          double startTimeoFView = CMTimeCompare(firstAsset.duration, audioAsset.duration);
                              NSLog(@"End Time  %f", time);
                              NSLog(@"End Time  dounle  %f", time1);
                              
                              NSLog(@"START TIME START TIME START TIME START TIME START TIME v START TIME START TIME START TIME v v v v START TIME START TIME START TIME %f", startTimeoFView);
                              
                              NSString *str = [NSString stringWithFormat:@"%.2f",startTimeoFView];
                              double difference = [str doubleValue];
                              
                         if (difference >= 0.47 && difference <0.99 )
                         {
          
                              startTimeoFView = startTimeoFView + 0.20 ;
                         }
//

               
                              float startMilliseconds = (startTimeoFView  * 1000);
                              float endMilliseconds = ((CMTimeGetSeconds(firstAsset.duration)) * 1000);
                              NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                              NSString *documentsDirectory = [paths objectAtIndex:0];
                              NSFileManager *manager = [NSFileManager defaultManager];
                              NSString *outputURL = [documentsDirectory stringByAppendingPathComponent:@"output"] ;
                              [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
                              outputURL = [outputURL stringByAppendingPathComponent:@"output.mov"];
                              // Remove Existing File
                              [manager removeItemAtPath:outputURL error:nil];
                              AVURLAsset *videoAsset = [AVURLAsset URLAssetWithURL:VideoURL options:nil];
                              AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:videoAsset presetName:AVAssetExportPresetHighestQuality];
                              exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
                              exportSession.outputFileType = AVFileTypeQuickTimeMovie;
                              CMTimeRange timeRange = CMTimeRangeMake(CMTimeMake(startMilliseconds, 1000), CMTimeMake(endMilliseconds - startMilliseconds, 1000));
                              exportSession.timeRange = timeRange;
                              
                              [exportSession exportAsynchronouslyWithCompletionHandler:^{
                                   switch (exportSession.status) {
                                        case AVAssetExportSessionStatusCompleted:
                                             // Custom method to import the Exported Video
                                             
                                             NSLog(@"exportSession.outputURL exportSession.outputURL exportSession.outputURL %@",exportSession.outputURL);
                                        {
                                             NSLog(@"Export OK");
                                             
                                             AVURLAsset* firstAsset = [[AVURLAsset alloc]initWithURL:exportSession.outputURL options:nil];
                                             AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:audiooutputURL options:nil];
                                             
                                             AVMutableComposition*  mixComposition = [[AVMutableComposition alloc] init];
                                             // 2 - Video track
                                             AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                                                                 preferredTrackID:kCMPersistentTrackID_Invalid];
                                             
                                             [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration)
                                                                 ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
                                             // 3 - Audio track
                                             if (audioAsset!=nil){
                                                  AVMutableCompositionTrack *AudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                                                                      preferredTrackID:kCMPersistentTrackID_Invalid];
                                                  [AudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration)
                                                                      ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];//temp-
                                             }
                                             // 4 - Get path
                                             
                                             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                             NSString *documentsDirectory = [paths objectAtIndex:0];
                                             NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                                                                      [NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
                                             Main_videoPath = myPathDocs;
                                             NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                                             // 5 - Create exporter
                                             AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                                                               presetName:AVAssetExportPresetHighestQuality];
                                             exporter.outputURL=url;
                                             exporter.outputFileType = AVFileTypeQuickTimeMovie;
                                             exporter.shouldOptimizeForNetworkUse = YES;
                                             [exporter exportAsynchronouslyWithCompletionHandler:^{
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                       NSLog(@"LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG - 6 ");
                                                       
                                                       [self exportDidFinish:exporter];
                                                  });
                                             }];
                                             
                                             
                                             
                                             
                                        }
                                             break;
                                        case AVAssetExportSessionStatusFailed:
                                        {
                                             sharewithfacebook = false;
                                             sharewithemail = false;
                                             sharingwithTwitter = false;
                                             [audioActivity stopAnimating];
                                             _video_downloadbtn.enabled = true;
                                             _Whatsbtn.enabled = true;
                                             __facebookBtn.enabled = true;
                                             self._googleBtn.enabled = true;
                                             __googleBtn.enabled = true;
                                             __facebookBtn.enabled = true;
                                             __mailBtn.enabled = true;
                                             __twitterBtn.enabled = true;
                                             self._twitterBtn.enabled = true;
                                             [self.video_downloadbtn setSelected:NO];
                                             _video_downloadbtn.enabled = true;
                                             [__twitterBtn setSelected:false];
                                             [self stopTransparentIndicator];
                                             
                                             UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"oops!" message: @"Generated video is corrupt.Please Try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                             [alert1 show];
                                             
                                             NSLog(@"Failed:%@",exportSession.error);
                                             [self stopTransparentIndicator];
                                        }
                                             //
                                             
                                             
                                             
                                             break;
                                        case AVAssetExportSessionStatusCancelled:
                                             //
                                             break;
                                        default:
                                             break;
                                   }
                              }];
                              
                              
                              
                              
                              
//                         }
//                              
//                              
//                              break;
//                         case AVAssetExportSessionStatusFailed:
//                              
//                         {
//                              
//                              sharewithfacebook = false;
//                              sharewithemail = false;
//                              sharingwithTwitter = false;
//                              [audioActivity stopAnimating];
//                              _video_downloadbtn.enabled = true;
//                              _Whatsbtn.enabled = true;
//                              __facebookBtn.enabled = true;
//                              self._googleBtn.enabled = true;
//                              __googleBtn.enabled = true;
//                              __facebookBtn.enabled = true;
//                              __mailBtn.enabled = true;
//                              __twitterBtn.enabled = true;
//                              self._twitterBtn.enabled = true;
//                              [self.video_downloadbtn setSelected:NO];
//                              _video_downloadbtn.enabled = true;
//                              [__twitterBtn setSelected:false];
//                              [self stopTransparentIndicator];
//                              
//                              UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"oops!" message: @"Generated video is corrupt.Please Try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                              [alert1 show];
//                              
//                         }
//                              
//                              NSLog (@"AVAssetExportSessionStatusFailed: %@", exportSession1.error);
//                              [self stopTransparentIndicator];
//                              
//                              break;
//                         case AVAssetExportSessionStatusCancelled:
//                              NSLog(@"Export Cancelled");
//                              break;
//                    }
//               }];
               
               
               
               
               
               return;
               
          }
     }
     
     /*
      
      
      NSURL *audiooutputURL = [[NSURL alloc] initFileURLWithPath:AudioPath];
      NSURL *VideoURL = [[NSURL alloc] initFileURLWithPath:Videopath];
      
      AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:audiooutputURL options:nil];
      AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:VideoURL options:nil];
      
      AVMutableComposition* mixComposition = [AVMutableComposition composition];
      
      AVMutableCompositionTrack *compositionCommentaryTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
      preferredTrackID:kCMPersistentTrackID_Invalid];
      [compositionCommentaryTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration)
      ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
      atTime:kCMTimeZero error:nil];
      
      AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
      preferredTrackID:kCMPersistentTrackID_Invalid];
      [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
      ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
      atTime:kCMTimeZero error:nil];
      CMTime duration = videoAsset.duration;
      float seconds = CMTimeGetSeconds(duration);
      
      if (seconds == 0.0)
      {
      
      UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"oops!" message: @"Generated video is currupte . Please Try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert1 show];
      [self stopTransparentIndicator];
      [audioActivity stopAnimating];
      _video_downloadbtn.enabled = true;
      _Whatsbtn.enabled = true;
      __facebookBtn.enabled = true;
      self._googleBtn.enabled = true;
      __googleBtn.enabled = true;
      __facebookBtn.enabled = true;
      __mailBtn.enabled = true;
      __twitterBtn.enabled = true;
      self._twitterBtn.enabled = true;
      [self.video_downloadbtn setSelected:NO];
      _video_downloadbtn.enabled = true;
      
      }
      else
      {
      [compositionCommentaryTrack setPreferredTransform:[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] preferredTransform]];
      
      
      AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition
      presetName:AVAssetExportPresetHighestQuality];
      
      NSString* videoName = @"export.mov";
      
      NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:videoName];
      NSURL    *exportUrl = [NSURL fileURLWithPath:exportPath];
      
      if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath])
      {
      [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
      }
      
      _assetExport.outputFileType = @"com.apple.quicktime-movie";
      _assetExport.outputURL = exportUrl;
      _assetExport.shouldOptimizeForNetworkUse = YES;
      
      [_assetExport exportAsynchronouslyWithCompletionHandler:
      
      ^(void ) {
      
      
      Main_videoPath = exportPath;
      
      [[NSUserDefaults standardUserDefaults] setObject:Main_videoPath forKey:@"shreingvideosocial"];
      [[NSUserDefaults standardUserDefaults] synchronize];
      dispatch_async(dispatch_get_main_queue(), ^{
      if (Videotempsave == true )
      {
      
      Main_videoPath = exportPath;
      
      }
      else
      {
      if (videofbshare == true)
      {
      Main_videoPath = exportPath;
      if (videotwittershare == true )
      {
      [[NSUserDefaults standardUserDefaults] setObject:Main_videoPath forKey:@"shreingvideosocial"];
      [[NSUserDefaults standardUserDefaults] synchronize];
      [self stopTransparentIndicator];
      
      [self ShareContentOnTwitterUsingSLComposer :exportPath];
      
      }
      else
      {
      [[NSUserDefaults standardUserDefaults] setObject:Main_videoPath forKey:@"shreingvideosocial"];
      [[NSUserDefaults standardUserDefaults] synchronize];
      [self stopTransparentIndicator];
      
      [self ShareContentOnFBUsingSLComposer:exportPath];
      
      }
      
      }
      else
      {
      Main_videoPath = exportPath;
      NSURL *movieURL = [NSURL fileURLWithPath:exportPath];
      
      ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
      [library writeVideoAtPathToSavedPhotosAlbum:movieURL
      completionBlock:^(NSURL *assetURL, NSError *error)
      {
      
      
      if (error)
      {
      
      
      
      dispatch_async(dispatch_get_main_queue(), ^{
      [audioActivity stopAnimating];
      UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"Fail!" message: @"The user has denied the application access to their media" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert1 show];
      [audioActivity stopAnimating];
      _video_downloadbtn.enabled = true;
      _Whatsbtn.enabled = true;
      __facebookBtn.enabled = true;
      self._googleBtn.enabled = true;
      __googleBtn.enabled = true;
      __facebookBtn.enabled = true;
      __mailBtn.enabled = true;
      __twitterBtn.enabled = true;
      self._twitterBtn.enabled = true;
      [self.video_downloadbtn setSelected:NO];
      _video_downloadbtn.enabled = true;
      });
      
      }
      else
      {
      
      if ([assetURL isEqual:NULL] || [assetURL isEqual:nil]||[assetURL isEqual:@""])
      {
      dispatch_async(dispatch_get_main_queue(), ^{
      [audioActivity stopAnimating];
      UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"Fail!" message:@"Video Fail To save in Galley!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert1 show];
      [audioActivity stopAnimating];
      _video_downloadbtn.enabled = true;
      _Whatsbtn.enabled = true;
      __facebookBtn.enabled = true;
      self._googleBtn.enabled = true;
      __googleBtn.enabled = true;
      __facebookBtn.enabled = true;
      __mailBtn.enabled = true;
      __twitterBtn.enabled = true;
      self._twitterBtn.enabled = true;
      [self.video_downloadbtn setSelected:NO];
      _video_downloadbtn.enabled = true;
      });
      }
      else
      {
      
      
      dispatch_async(dispatch_get_main_queue(), ^{
      [audioActivity stopAnimating];
      UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Video saved to the gallery" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert1 show];
      [audioActivity stopAnimating];
      _video_downloadbtn.enabled = true;
      _Whatsbtn.enabled = true;
      __facebookBtn.enabled = true;
      __googleBtn.enabled = true;
      __facebookBtn.enabled = true;
      self._googleBtn.enabled = true;
      self._twitterBtn.enabled = true;
      __twitterBtn.enabled = true;
      __mailBtn.enabled = true;
      [self.video_downloadbtn setSelected:NO];
      _video_downloadbtn.enabled = true;
      
      
      });
      }
      
      }
      
      
      }];
      
      
      
      }
      }
      
      });
      
      
      
      }
      
      ];
      }
      
      */
}

//-(void)videosaveinlibrarury:(NSString*)Videopath:(NSString*)AudioPath
//{
//
//     NSURL *audiooutputURL = [[NSURL alloc] initFileURLWithPath:AudioPath];
//     NSURL *VideoURL = [[NSURL alloc] initFileURLWithPath:Videopath];
//
//     AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:audiooutputURL options:nil];
//     AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:VideoURL options:nil];
//
//     AVMutableComposition* mixComposition = [AVMutableComposition composition];
//
//     AVMutableCompositionTrack *compositionCommentaryTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
//                                                                                         preferredTrackID:kCMPersistentTrackID_Invalid];
//
//     [compositionCommentaryTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration)
//                                         ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
//                                          atTime:kCMTimeZero error:nil];
//     CMTime duration = videoAsset.duration;
//     float seconds = CMTimeGetSeconds(duration);
//
//     if (seconds == 0.0)
//     {
//
//          UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"oops!" message: @"Generated video is currupte . Please Try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//          [alert1 show];
//          [self stopTransparentIndicator];
//          [audioActivity stopAnimating];
//          _video_downloadbtn.enabled = true;
//          _Whatsbtn.enabled = true;
//          __facebookBtn.enabled = true;
//          self._googleBtn.enabled = true;
//          __googleBtn.enabled = true;
//          __facebookBtn.enabled = true;
//          __mailBtn.enabled = true;
//          __twitterBtn.enabled = true;
//          self._twitterBtn.enabled = true;
//          [self.video_downloadbtn setSelected:NO];
//          _video_downloadbtn.enabled = true;
//
//     }
//     else
//     {
//          [compositionCommentaryTrack setPreferredTransform:[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] preferredTransform]];
//
//          AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
//                                                                                         preferredTrackID:kCMPersistentTrackID_Invalid];
//
//
//          CGSize videoSized = [videoAsset naturalSize];
//
//          UIImage *myImage = [UIImage imageNamed:@"Vokiicon.png"];
//          CALayer *aLayer = [CALayer layer];
//          aLayer.contents = (id)myImage.CGImage;
//          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
//          {
//
//               aLayer.frame = CGRectMake(videoSized.width - 100, 700, 85, 85);
//          }
//          else
//          {
//               aLayer.frame = CGRectMake(videoSized.width - 100, 290, 85, 85);
//               //aLayer.frame = CGRectMake(0, 0, 0, 0);
//          }
//
//          aLayer.opacity = 1; //Feel free to alter the alpha here
//          if (seconds == 0.0)
//          {
//
//               UIAlertView * alert1 = [[UIAlertView alloc] initWithTitle:@"oops!" message: @"Generated video is currupted . Please Try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//               [alert1 show];
//               [audioActivity stopAnimating];
//               _video_downloadbtn.enabled = true;
//               _Whatsbtn.enabled = true;
//               __facebookBtn.enabled = true;
//               self._googleBtn.enabled = true;
//               __googleBtn.enabled = true;
//               __facebookBtn.enabled = true;
//               __mailBtn.enabled = true;
//               __twitterBtn.enabled = true;
//               self._twitterBtn.enabled = true;
//               [self.video_downloadbtn setSelected:NO];
//               _video_downloadbtn.enabled = true;
//          }
//          else
//          {
//
//               [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
//                                              ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
//                                               atTime:kCMTimeZero error:nil];
//
//               AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition
//                                                                                     presetName:AVAssetExportPresetHighestQuality];
//
//               NSString* videoName = @"export.mov";
//               NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:videoName];
//               NSURL    *exportUrl = [NSURL fileURLWithPath:exportPath];
//
//               if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath])
//               {
//                    [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
//               }
//
//               _assetExport.outputFileType = @"com.apple.quicktime-movie";
//               _assetExport.outputURL = exportUrl;
//               _assetExport.shouldOptimizeForNetworkUse = YES;
//               [_assetExport exportAsynchronouslyWithCompletionHandler:
//                ^(void ) {
//
//                     CGSize videoSize = [videoAsset naturalSize];
//                     CALayer *parentLayer = [CALayer layer];
//                     CALayer *videoLayer = [CALayer layer];
//                     parentLayer.frame = CGRectMake(0, 0, 65, 60);
//                     videoLayer.frame = CGRectMake(0, 0, videoSize.width, videoSize.height);
//                     [parentLayer addSublayer:videoLayer];
//
//                     aLayer.opacity = 0.65;
//                     [parentLayer addSublayer:aLayer];
//                     AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition] ;
//                     videoComp.renderSize = videoSize;
//                     videoComp.frameDuration = CMTimeMake(1, 30);
//                     videoComp.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
//
//                     /// instruction
//                     AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
//                     instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
//                     AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
//                     AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
//                     instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
//                     videoComp.instructions = [NSArray arrayWithObject: instruction];
//                     AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];//AVAssetExportPresetPassthrough
//                     _assetExport.videoComposition = videoComp;
//
//                     if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath])
//                     {
//                          [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
//                     }
//
//                     _assetExport.outputFileType = AVFileTypeQuickTimeMovie;
//                     _assetExport.outputURL = exportUrl;
//                     _assetExport.shouldOptimizeForNetworkUse = YES;
//
//                     [_assetExport exportAsynchronouslyWithCompletionHandler:
//                      ^(void )
//                      {
//                           Main_videoPath = exportPath;
//                           [[NSUserDefaults standardUserDefaults] setObject:Main_videoPath forKey:@"shreingvideosocial"];
//                           [[NSUserDefaults standardUserDefaults] synchronize];
//                           dispatch_async(dispatch_get_main_queue(), ^{
//                                if (Videotempsave == true )
//                                {
//
//                                     Main_videoPath = exportPath;
//
//
//                                }
//                                else
//                                {
//                                     if (videofbshare == true)
//                                     {
//                                          Main_videoPath = exportPath;
//                                          if (videotwittershare == true )
//                                          {
//                                               [[NSUserDefaults standardUserDefaults] setObject:Main_videoPath forKey:@"shreingvideosocial"];
//                                               [[NSUserDefaults standardUserDefaults] synchronize];
//                                               [self stopTransparentIndicator];
//
//                                               [self ShareContentOnTwitterUsingSLComposer :exportPath];
//
//                                          }
//                                          else
//                                          {
//                                               [[NSUserDefaults standardUserDefaults] setObject:Main_videoPath forKey:@"shreingvideosocial"];
//                                               [[NSUserDefaults standardUserDefaults] synchronize];
//                                               [self stopTransparentIndicator];
//
//                                               [self ShareContentOnFBUsingSLComposer:exportPath];
//
//                                          }
//
//                                     }
//                                     else
//                                     {
//                                          Main_videoPath = exportPath;
//                                          [self Addthumbnail:exportPath addfile:Videopath];
//
//                                     }
//                                }
//
//                           });
//
//                      }
//                      ];
//
//                     // your completion code here
//                }];
//
//          }
//     }
//}

-(UIImage *)loadThumbNail:(NSURL *)urlVideo
{
     NSString *strUrl = [urlVideo absoluteString];
     if (![[strUrl substringToIndex:1]  isEqual: @"f"]) {
          urlVideo = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"file://",strUrl]];
          
     }
     
     
     AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:urlVideo options:nil];
     AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
     generate.appliesPreferredTrackTransform=TRUE;
     NSError *err = NULL;
     CMTime time1 = CMTimeMake(1, 60);
     CGImageRef imgRef = [generate copyCGImageAtTime:time1 actualTime:NULL error:&err];
     NSLog(@"err==%@, imageRef==%@", err, imgRef);
     return [[UIImage alloc] initWithCGImage:imgRef];
}



-(UIImage*)generateThumbnail:(AVAsset*)asset
{
     
     AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
     NSError *error = NULL;
     CMTime time = CMTimeMake(1, 1);
     CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
     NSLog(@"error==%@, Refimage==%@", error, refImg);
     UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
     return FrameImage;
}


- (CVPixelBufferRef) pixelBufferFromCGImage: (UIImage*) image
{
     @autoreleasepool
     {
          CGImageRef cgimage = [image CGImage];
          
          NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                                   [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                                   nil];
          
          CVPixelBufferRef pxbuffer = NULL;
          
          float width =  CGImageGetWidth(cgimage);
          float height = CGImageGetHeight(cgimage);
          
          
          CVPixelBufferCreate(kCFAllocatorDefault,width,
                              height, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef)(options),
                              &pxbuffer);
          
          CVPixelBufferLockBaseAddress(pxbuffer, 0);
          
          void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
          
          NSParameterAssert(pxdata != NULL);
          
          CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
          CGContextRef context = CGBitmapContextCreate(pxdata, width,
                                                       height, 8, 4*width, rgbColorSpace,
                                                       (CGBitmapInfo)kCGImageAlphaNoneSkipFirst);
          
          
          CGContextConcatCTM(context, CGAffineTransformMakeRotation(-M_PI/2));
          
          CGContextDrawImage(context, CGRectMake(-height, 0, height, width), cgimage);
          
          CGColorSpaceRelease(rgbColorSpace);
          CGContextRelease(context);
          
          CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
          
          return pxbuffer;
     }
}




#pragma mark - Email sharing

- (IBAction)MailAction:(id)sender
{
     
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
     }
     else
     {
          
          if (saveSceneUrl == nil)
          {
               Googleshared = NO;
               whatsappshared = NO;
               Mailshared = YES;
               [self createsharedLink];
               
          }
          else
          {
               [self sharingonMail];
               
          }
     }
}

-(void)sharingonMail
{
     [self.view setUserInteractionEnabled:false];
     [self DisableButtonSelection];
     [self.video_downloadbtn setSelected:NO];
     [__mailBtn setSelected:YES];
     [self ShareWithEmail];
     [self.view setUserInteractionEnabled:true];
     [audioActivity stopAnimating];
}




- (IBAction)whatsppAction:(id)sender
{
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
     }
     else
     {
          if (saveSceneUrl == nil)
          {
               Mailshared = NO;
               Googleshared = NO;
               whatsappshared = YES;
               [self createsharedLink];
          }
          else
          {
               [self Performwhatsappaction ];
          }
     }
     
}

-(void)Performwhatsappaction
{
     [self DisableButtonSelection];
     [self.Whatsbtn setSelected:YES];
     [self whatsappActn];
}
-(void)whatsappActn
{
     [self.view setUserInteractionEnabled:true];
     [audioActivity stopAnimating];
     NSString *url = @" https://itunes.apple.com/in/app/voki-for-education/id1106010700?mt=8";
     
     NSString *temp =[NSString stringWithFormat:@"%@%@%@%@%@",@"My Voki has something to say - check it out:",@"   ",saveSceneUrl,@"  ",@"To create your own Voki go to - www.voki.com"];
     temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
     
     NSURL *whatsappURL = [NSURL URLWithString:@"whatsapp://send?text=Hello%2C%20World!"];
     //    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
     
     
     NSMutableArray *array_Object = [[NSMutableArray alloc]initWithObjects:temp, nil];
     
     
     
     UIActivityViewController *obj_activity = [[UIActivityViewController alloc] initWithActivityItems:array_Object applicationActivities:nil];
     
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          
          if ([obj_activity respondsToSelector:@selector(popoverPresentationController)]) {
               obj_activity.popoverPresentationController.sourceView = self.view;
               
          }
     }
     
     
     [self presentViewController:obj_activity animated:YES completion:nil];
     
     
     
     
}
#pragma mark - MailView Delegate Methods
-(void)ShareWithEmail
{
     [self stopTransparentIndicator];
     if ([MFMailComposeViewController canSendMail])
     {
          sharewithemail = true;
          MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
          [mailController setMailComposeDelegate:self];
          [mailController setSubject:@"Check out my Voki"];
          [mailController setToRecipients:[NSArray arrayWithObjects:@"email1", @"email2", nil]];
          NSString *temp =[NSString stringWithFormat:@"%@%@%@%@%@%@",@"My Voki has something to say - check it out:",@"\n",saveSceneUrl,@"\n",@"\n",@"To create your own Voki go to - www.voki.com"];
          [mailController setMessageBody:temp isHTML:NO];
          [self presentViewController:mailController animated:YES completion:nil];
     }
     else
     {
          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please setup mail in your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
          [alert show];
     }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
     [self dismissViewControllerAnimated:YES completion:nil];
     
     
     
     if (result != 0)
     {
          sharewithfacebook = false;
          sharewithemail = false;
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Mail" message:@"Your video was successfully shared"  delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
          [alert show];
          alert.tag = 22;
          sharecount++;
          [self saveCharApiTrack];
     }
     
}

#pragma mark - CharacterCtoR

-(void) CharacterMove
{
     if ( thirdView == false)
     {
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               float t = playCharacterAud1.frame.origin.x-260;
               int move = (int)t;
               NSString *str = [NSString stringWithFormat:@"%d",move];
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",str,ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width -10 - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8+__mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 105, 95, 95);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
               
               
          }
          else
          {
               if (_canvasWidth == 480)
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"190",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8+__mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         
                         
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               
               else if (_canvasWidth == 736)
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"320",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+9+__mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         
                         
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else if(_canvasWidth == 667)
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"250",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width-12+__mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         
                         
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else
               {
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                     }];
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSLog(@"%@",ypos);
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"200",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width-7+__mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         
                         
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
          }
          
     }
     else
     {
          
          [self centerPositionChar];
     }
}

-(void) movecharact
{
     @autoreleasepool
     {
          if ( __colorPickerView.hidden == false)
          {
               issecopen = false;
               isvoicepanopen = true;
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"435",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    
                    
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - 15  + __colorPickerView.frame.size.width+8+__mainFirstStrip.frame.size.width)/2 - playCharacterAud1.frame.size.width/1.5 , self.view.frame.size.height - 105, 95, 95);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
               else if(_canvasWidth == 667)
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"420",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - 5 + __colorPickerView.frame.size.width+__mainFirstStrip.frame.size.width)/2 - playCharacterAud1.frame.size.width/1.5 , self.view.frame.size.height - 75, playCharacterAud1.frame.size.width, playCharacterAud1.frame.size.height);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          
                          
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
               
               
               else if (_canvasWidth == 736)
                    
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"470",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - 5 + __colorPickerView.frame.size.width+__mainFirstStrip.frame.size.width)/2 - playCharacterAud1.frame.size.width/1.5 , self.view.frame.size.height - 75, playCharacterAud1.frame.size.width, playCharacterAud1.frame.size.height);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          
                          
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
               else
               {
                    
                    
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"340",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width -10 + __colorPickerView.frame.size.width+8+__mainFirstStrip.frame.size.width)/2 - playCharacterAud1.frame.size.width/1.5 , self.view.frame.size.height - 75, playCharacterAud1.frame.size.width, playCharacterAud1.frame.size.height);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
               
               
          }
          
          else
          {
               [self centerPositionChar];
          }
     }
     
}
-(void) movecharactVoice
{
     issecopen = false;
     isright = false;
     isvoicepanopen = true;
     if ( __colorPickerView.hidden == false ||  __voiceTextView.hidden==false)
     {
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"415",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width-27+ __colorPickerView.frame.size.width+__mainFirstStrip.frame.size.width)/2 - playCharacterAud1.frame.size.width/1.5 , self.view.frame.size.height - 105, 95, 95);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                          
                          
                          
                          [UIView animateWithDuration:1.2 animations:^
                           {
                                playCharacterAud1.alpha = 1;
                           }];
                     });
                     
                }];
          }
          else if(_canvasWidth == 667)
          {
               
               
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"420",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+90+__mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                     
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
          }
          else if(_canvasWidth == 736)
          {
               
               
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"480",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+140+__mainFirstStrip.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                     
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
          }
          
          
          else
          {
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"360",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               
               [UIView animateWithDuration:0.5 animations:^
                
                {
                     playCharacterAud1.alpha = 0;
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width-10 + __colorPickerView.frame.size.width+__mainFirstStrip.frame.size.width)/2 - playCharacterAud1.frame.size.width/1.5 , self.view.frame.size.height - 75, playCharacterAud1.frame.size.width, playCharacterAud1.frame.size.height);
                     
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
               
          }
          
          
          
          
     }
     else
     {
          [self centerPositionChar];
     }
     
     
}

# pragma mark - swipeGesture
-(void)swipeHandlerLeft:(id)sender
{
     if (thirdView == false)
     {
          _isAccessoriesSelected = NO;
          [self CharacterMove];
          [self secondScrollerDeselectedButton];
          [UIView animateWithDuration:0.5 animations:^
           {
                __shareView.alpha = 0;
                _deleteGalleryBtn.alpha = 0;
                __thirdView.alpha = 0;
                _CloeseThirdStripBtn.alpha = 0;
                thirdView = true;
                __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8 , __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
                _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
                __shareView.frame = CGRectMake(__mainFirstStrip.frame.size.width + 8,__shareView.frame.origin.y ,__shareView.frame.size.width,  __shareView.frame.size.height);
                _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
                __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
                
                
           }];
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0 ;
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 105, 95, 95);
                     
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
               
          }
          else
          {
               if (_canvasWidth == 480)
               {
                    
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0 ;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                          
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               
               else if (_canvasWidth == 736)
               {
                    
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0 ;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.2 , self.view.frame.size.height - 75, 65, 65);
                          
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else if(_canvasWidth == 667)
               {
                    
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0 ;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else
               {
                    
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          playCharacterAud1.alpha = 0 ;
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                          
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
          }
          
          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"0.09",ypos];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          _CloeseThirdStripBtn.hidden=true;
          _CloseSecondStripBtn.hidden=false;
     }
     
     else if ((__vokiView.alpha == 0)&&(__clothingView.alpha ==0)&&(__ImageView.alpha == 0)&&(__shareView.alpha == 0)&(__recordVoiceView.alpha == 0)&&(__colorPickerView.alpha == 0)&&(__mainFirstStrip.alpha == 1))
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                __mainFirstStrip.frame = CGRectMake(self.view.frame.origin.x - __mainFirstStrip.frame.size.width, __mainFirstStrip.frame.origin.y ,__mainFirstStrip.frame.size.width,__mainFirstStrip.frame.size.height);
                __mainFirstStrip.alpha = 0;
                _closemainStripBtn.alpha = 1;
                
                
                
           }];
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0 ;
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width)/2 - playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 105, 95, 95);
                     
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          else
          {
               [UIView animateWithDuration:0.5 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width)/2 - playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, playCharacterAud1.frame.size.width, playCharacterAud1.frame.size.height);
                     
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          
          
          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
          
          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"",ypos];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
     }
     else
     {
          [self HideAllViews];
          [self DisableMainButtons];
     }
}
-(void)swipeHandlerRight:(id)sender
{
     menuViewOpenClose = YES;
     shareViewOpenClose = YES;
     [gestureRecognizer setEnabled:YES];
     __rightVokiBtn.enabled = YES;
     [UIView animateWithDuration:0.8 animations:^
      {
           
           
           __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
           __closerightMenuNewBtn.frame = CGRectMake(self.view.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
           
           __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y ,__rightMenuView.frame.size.width,__rightMenuView.frame.size.height);
           __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
      }];
     
     _isRightShareOpen = NO;
     _isRightMenuOpen = NO;
     rightmenu = false;
     [self centerPositionChar];
     [__rightShareBtn setEnabled:YES];
     
}

-(void)moveRightMenu
{
     isright = false;
     issecopen = true;
     [UIView animateWithDuration:0.8 animations:^
      {
           
           
           __rightShareView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightShareView.frame.origin.y ,__rightShareView.frame.size.width,__rightShareView.frame.size.height);
           __closerightMenuNewBtn.frame = CGRectMake(self.view.frame.size.width, __closerightMenuNewBtn.frame.origin.y, __closerightMenuNewBtn.frame.size.width, __closerightMenuNewBtn.frame.size.height);
           
           __rightMenuView.frame = CGRectMake(self.view.frame.size.width+__closerightMenuNewBtn.frame.size.width, __rightMenuView.frame.origin.y ,__rightMenuView.frame.size.width,__rightMenuView.frame.size.height);
           __closeRightViewBtn.frame = CGRectMake(self.view.frame.size.width, __closeRightViewBtn.frame.origin.y, __closeRightViewBtn.frame.size.width, __closeRightViewBtn.frame.size.height);
           
      }];
     
     _isRightShareOpen = NO;
     _isRightMenuOpen = NO;
     rightmenu = false;
     
     [__rightShareBtn setEnabled:YES];
}

-(void)characterLeftMoveShare
{
     isvoicepanopen = false;
     issecopen = false;
     if ( _isRightShareOpen == YES)
     {
          
          if (__mainFirstStrip.frame.origin.x != self.view.frame.origin.x)
          {
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               
               if (_isRightShareOpen )
               {
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                         [UIView animateWithDuration:0.2 animations:^
                          {
                               _isRightShareOpen = false;
                               
                               playCharacterAud1.alpha = 0;
                               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-325",ypos];
                               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                          }];
                         
                    }
                    
                    else if (_canvasWidth == 667)
                    {
                         [UIView animateWithDuration:0.2 animations:^
                          {
                               _isRightShareOpen = false;
                               
                               playCharacterAud1.alpha = 0;
                               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-300",ypos];
                               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                          }];
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         [UIView animateWithDuration:0.2 animations:^
                          {
                               _isRightShareOpen = false;
                               
                               playCharacterAud1.alpha = 0;
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-325",ypos];
                               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                          }];
                    }
                    
                    
                    else
                    {
                         [UIView animateWithDuration:0.2 animations:^
                          {
                               _isRightShareOpen = false;
                               
                               playCharacterAud1.alpha = 0;
                               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-280",ypos];
                               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                          }];
                         
                    }
                    
               }
               else
               {
                    
                    
                    
               }
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
               {
                    [UIView animateWithDuration:0.1 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                          
                     }];
                    
                    [UIView animateWithDuration:0.2 animations:^
                     {
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/1.9 , self.view.frame.size.height - 105, 95, 95);
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          
                     }];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.30 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.5 animations:^
                          {
                               
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
               else
               {
                    if (_canvasWidth == 480)
                    {
                         
                         
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                               
                          }];
                         
                         [UIView animateWithDuration:0.2 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__mainFirstStrip.frame.size.width-__rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                               
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.30 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              [UIView animateWithDuration:1.5 animations:^
                               {
                                    
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                         
                         
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               
                               playCharacterAud1.alpha = 0 ;
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width+30 - __mainFirstStrip.frame.size.width - __mainFirstStrip.frame.size.width - __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                               
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.30 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else if (_canvasWidth == 667)
                    {
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               
                               playCharacterAud1.alpha = 0 ;
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width +20 - __mainFirstStrip.frame.size.width-__mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.3 , self.view.frame.size.height - 75, 65, 65);
                               
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.30 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else
                    {
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               
                               playCharacterAud1.alpha = 0 ;
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/3 , self.view.frame.size.height - 75, 65, 65);
                               
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.30 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                         
                    }
               }
               
          }
          else
          {
               
               
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               if (_isRightShareOpen )
               {
                    
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                    {
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 0 ;
                               
                               _isRightShareOpen = false;
                               
                               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-245",ypos];
                               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                          }];
                    }
                    else
                    {
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 0 ;
                               
                               _isRightShareOpen = false;
                               
                               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-220",ypos];
                               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                          }];
                    }
               }
               else
               {
                    
                    
               }
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    [UIView animateWithDuration:0.1 animations:^
                     {
                          
                          playCharacterAud1.alpha = 0 ;
                     }];
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.2 , self.view.frame.size.height - 105, 95, 95);
                          
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
                    
               }
               else
               {
                    if (_canvasWidth == 480)
                    {
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               
                               playCharacterAud1.alpha = 0 ;
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width+40 - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else if(_canvasWidth == 667)
                    {
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0 ;
                          }];
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width+20 - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.7 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else
                    {
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                         
                    }
               }
               
               
          }
          
     }
     else
     {
          [self centerPositionChar];
     }
     
}

-(void)characterLeftMoveMenu
{
     if ( _isRightShareOpen == NO)
     {
          
          if (__mainFirstStrip.frame.origin.x != self.view.frame.origin.x)
          {
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
               {
                    
                    
                    [UIView animateWithDuration:0.1 animations:^
                     {
                          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-300",ypos];
                          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                          
                          playCharacterAud1.alpha = 0;
                          
                     }];
                    
                    [UIView animateWithDuration:0.0 animations:^
                     {
                          
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width+10 - __mainFirstStrip.frame.size.width- __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/1.9 , self.view.frame.size.height - 105, 95, 95);
                          
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 40.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               // playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else
               {
                    if (_canvasWidth == 480)
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-190",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__mainFirstStrip.frame.size.width-__rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                               
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-260",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width - __mainFirstStrip.frame.size.width - __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                               
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else if (_canvasWidth == 667)
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-330",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         
                         
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.3 , self.view.frame.size.height - 75, 65, 65);
                               
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else
                    {  NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-220",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/3 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                         
                    }
               }
               
               
               
          }
          else
          {
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
               {
                    NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                    NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-250",ypos];
                    [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                    
                    [UIView animateWithDuration:0.1 animations:^
                     {
                          playCharacterAud1.alpha = 0;
                          
                     }];
                    
                    [UIView animateWithDuration:0.5 animations:^
                     {
                          
                          
                          playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.2 , self.view.frame.size.height - 105, 95, 95);\
                          _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     }];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [UIView animateWithDuration:1.2 animations:^
                          {
                               playCharacterAud1.alpha = 1;
                          }];
                    });
               }
               else
               {
                    if (_canvasWidth == 480)
                    {      NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-180",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width-__rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    
                    else if (_canvasWidth == 736)
                    {
                         
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-280",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                               
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else if(_canvasWidth == 667)
                    {
                         
                         
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-250",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         
                         [UIView animateWithDuration:0.1 animations:^
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/2.7 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                    }
                    else
                    {
                         NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
                         NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"-220",ypos];
                         [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
                         
                         
                         [UIView animateWithDuration:0.2 animations:^
                          
                          {
                               playCharacterAud1.alpha = 0;
                               
                          }];
                         [UIView animateWithDuration:0.5 animations:^
                          {
                               
                               
                               playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width- __rightMenuView.frame.size.width)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                               _PlayCharAud1dup.frame = playCharacterAud1.frame;
                          }];
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                              
                              
                              
                              [UIView animateWithDuration:1.2 animations:^
                               {
                                    playCharacterAud1.alpha = 1;
                               }];
                         });
                         
                    }
                    
               }
          }
          
     }
     else
     {
          [self centerPositionChar];
     }
     
}
-(void)startTransparentIndicator
{
     LoaderView.hidden=NO;
     [transparentIndicator startAnimating];
     self.view.userInteractionEnabled = NO;
     playCharacterAud1.userInteractionEnabled = NO;
}
-(void)stopTransparentIndicator
{
     LoaderView.hidden=YES;
     
     [transparentIndicator stopAnimating];
     self.view.userInteractionEnabled = YES;
     playCharacterAud1.userInteractionEnabled = true;

}

-(void)moveCharacterFirstView
{
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          float t = playCharacterAud1.frame.origin.x-330;
          int move = (int)t;
          NSString *str = [NSString stringWithFormat:@"%d",move];
          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",str,ypos];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          [UIView animateWithDuration:0.5 animations:^
           {
                playCharacterAud1.alpha = 0 ;
                
                playCharacterAud1.frame = CGRectMake((self.view.frame.size.width -36- __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 105, 95, 95);
                _PlayCharAud1dup.frame = playCharacterAud1.frame;
           }];
          
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
               [UIView animateWithDuration:0.3 animations:^
                {
                     playCharacterAud1.alpha = 1;
                }];
          });
     }
     else
     {
          if (_canvasWidth == 480)
          {
               
               float t = playCharacterAud1.frame.origin.x-140;
               int move = (int)t;
               NSString *str = [NSString stringWithFormat:@"%d",move];
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",str,ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.3 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width-15 - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/4.4 , self.view.frame.size.height - 75, 55, 55);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.4 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          
          else if (_canvasWidth == 736)
          {
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"200",yposvalue];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.3 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.2 , self.view.frame.size.height - 75, 65, 65);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          else if(_canvasWidth == 667)
          {
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"185",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [UIView animateWithDuration:0.3 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width -
                                                           __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/2.7 , self.view.frame.size.height - 75, 65, 65);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          else
          {
               float t = playCharacterAud1.frame.origin.x-100;
               int m = (int)t;
               NSString *str = [NSString stringWithFormat:@"%d",m];
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",str,ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               
               [UIView animateWithDuration:0.3 animations:^
                {
                     
                     playCharacterAud1.alpha = 0;
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width+__mainFirstStrip.frame.size.width+8)/2 + playCharacterAud1.frame.size.width/4 , self.view.frame.size.height - 75, 65, 65);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
          }
     }
}


-(void)animateThirdView
{
     [UIView animateWithDuration:0.8 animations:^
      {
           
           _CloeseThirdStripBtn.alpha = 0;
           __thirdView.alpha = 0;
           __thirdView.hidden=true;
           _CloeseThirdStripBtn.hidden = true;
           __thirdView.frame = CGRectMake(__mainFirstStrip.frame.size.width+8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
           _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
      }];
}


#pragma mark - HextoUIcolor

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
     // Convert hex string to an integer
     unsigned int hexint = [self intFromHexString:hexStr];
     
     // Create color object, specifying alpha as well
     UIColor *color =
     [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                     green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                      blue:((CGFloat) (hexint & 0xFF))/255
                     alpha:alpha];
     return color;
}
- (unsigned int)intFromHexString:(NSString *)hexStr
{
     unsigned int hexInt = 0;
     // Create scanner
     NSScanner *scanner = [NSScanner scannerWithString:hexStr];
     // Tell scanner to skip the # character
     [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
     // Scan hex value
     [scanner scanHexInt:&hexInt];
     return hexInt;
}
#pragma mark - Get Image Process
- (IBAction)getImageBtn:(id)sender
{
     saveSceneUrl = nil;
     Main_videoPath = @"";
     sameaudio = false;
     outurltweet = nil ;
     sharewithfacebook = false;
     sharewithemail = false;
     [_getImageBtn setSelected:YES];
     
     [self loaderposition];
     
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Choose Option" delegate:self cancelButtonTitle:@"Gallery" otherButtonTitles:@"Camera", nil];
          alert.tag = 444;
          [alert show];
     }
     else
     {
          actnSht = [[UIActionSheet alloc]initWithTitle:@"Choose Option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
          [actnSht showInView:self.view];
     }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
     [audioActivity stopAnimating];
     [self stopTransparentIndicator];
     if(alertView.tag == 1)
     {
          [self stopTransparentIndicator];
          if(buttonIndex == 0)
          {
               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
          }
          
     }
     
     if (alertView.tag == 22)
     {
          if (buttonIndex == 0)
          {
               NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
               // getting an NSString
               NSString *myString = [prefs stringForKey:@"reviewapp"];
               [[NSUserDefaults standardUserDefaults] setInteger:sharecount forKey:@"sharecount"];
               
               //prabhjot
               if (sharecount%2 == 0)
               {
                    
               }
               else
                    if ( Dontaskagain == false && ![myString isEqualToString:@"yes"])
               {
                    [blurViewCalled removeFromSuperview];
                    [blurViewCalled1 removeFromSuperview];
                    [blurViewCalled2 removeFromSuperview];
                    [blurViewCalled3 removeFromSuperview];
                    alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
                    blurViewCalled3 = [[[alertVc showPopUp] subviews] objectAtIndex:0];
                    alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
                    alertVc.ratealert.hidden = false;
                    alertVc.deleteView.hidden = true;
                    alertVc.blackView.hidden = true;
                    alertVc.blackview1.hidden = true;
                    alertVc.unblockLearnmore.hidden = true;
                    alertVc.unblockvookichar.hidden = true;
                    alertVc.ratealert.hidden = false;
                    [alertVc.reviewbtn addTarget:self action:@selector(reviewapp:) forControlEvents:UIControlEventTouchUpInside];
                    [alertVc.remindlaterbtn addTarget:self action:@selector(remindlater) forControlEvents:UIControlEventTouchUpInside];
                    [alertVc.dontaskagainbtn addTarget:self action:@selector(DontAskAgain) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:blurViewCalled3];
                    
               }
          }
     }
     
     if (alertView.tag == 444)
     {
          
          if(buttonIndex == 0)
          {
               GalleryViewController *gallery = [self.storyboard instantiateViewControllerWithIdentifier:@"gallery"];
               gallery.galleryCamera = false;
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [self presentViewController:gallery animated:YES completion:nil];
                    });
               });
          }
          else
          {
               GalleryViewController *gallery = [self.storyboard instantiateViewControllerWithIdentifier:@"gallery"];
               gallery.galleryCamera = true;
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [self presentViewController:gallery animated:YES completion:nil];
                    });
               });
               
          }
          
     }
     
     
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
     
     
     if (buttonIndex == 0)
     {
          
          AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
          
          if(status == AVAuthorizationStatusAuthorized) {
               
               
               GalleryViewController *gallery = [self.storyboard instantiateViewControllerWithIdentifier:@"gallery"];
               gallery.galleryCamera = true;
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [self presentViewController:gallery animated:YES completion:nil];
                    });
               });
               
               
               
          }
          
          else if(status == AVAuthorizationStatusDenied)
          {
               // denied
               
               UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please allow voki to access Gallery from settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [alert show];
               
          }
          else if(status == AVAuthorizationStatusRestricted)
          {
               // restricted
          }
          
          else if(status == AVAuthorizationStatusNotDetermined){
               // not determined
               [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if(granted){
                         
                         
                         GalleryViewController *gallery = [self.storyboard instantiateViewControllerWithIdentifier:@"gallery"];
                         gallery.galleryCamera = true;
                         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                              dispatch_async(dispatch_get_main_queue(), ^{
                                   
                                   [self presentViewController:gallery animated:YES completion:nil];
                              });
                         });
                         
                         
                         
                    } else
                    {
                         
                    }
               }];
          }
          
          
          
          
          
          
     }
     if (buttonIndex == 1)
     {
          GalleryViewController *gallery = [self.storyboard instantiateViewControllerWithIdentifier:@"gallery"];
          gallery.galleryCamera = false;
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
               dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self presentViewController:gallery animated:YES completion:nil];
               });
          });
          
     }
     
     
     
     
}

#pragma mark - LongPressGestures

- (IBAction)vokiBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Head";
           }];
          
     }
     
}

- (IBAction)accessoriesBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          
          
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
          
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Accessories";
           }];
          
     }
     
}

- (IBAction)imageBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          
          
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Background";
           }];
          
     }
     
}

- (IBAction)colorBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          
          
          
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Color";
           }];
     }
     
}

- (IBAction)voiceBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Voice";
           }];
     }
     
}

- (IBAction)shareBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
          
     }
     else
     {
          
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Library";
           }];
          
          
     }
     
}

- (IBAction)addBackgroundBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Add Background";
           }];
     }
     
}

- (IBAction)saveTiLibBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Save to Library";
           }];
     }
     
}
- (IBAction)clothBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          
          
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Clothing";
           }];
          
     }
     
}
- (IBAction)hairBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
     }
     else
     {
          
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Hair";
           }];
          
          
     }
     
}
- (IBAction)capBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Hat";
           }];
     }
     
}
- (IBAction)glassBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Glasses";
           }];
     }
     
}
- (IBAction)mouthBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Mouth";
           }];
     }
     
}
- (IBAction)blingBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Bling";
           }];
     }
     
}
- (IBAction)facialBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Facial Hair";
           }];
     }
     
}
- (IBAction)propBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Props";
           }];
     }
     
}

- (IBAction)colorMouthBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Mouth";
           }];
     }
     
}

- (IBAction)colorEyeBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
          
     }
     else
     {
          
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Eye";
           }];
     }
     
}

- (IBAction)colorHandBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Skin";
           }];
     }
     
}

- (IBAction)colorHairBtnGesture:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
                _itemName.text = @"Hair";
           }];
     }
}

-(void)userLongPressedVokigroup:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
           }];
          
          _itemName.text = @"All";
     }
}

- (void)userLongPressedVokiCharacter:(UILongPressGestureRecognizer *)sender
{
     if (sender.state == UIGestureRecognizerStateEnded)
     {
          [UIView animateWithDuration:1.0 animations:^
           {
                _itemName.alpha=0;
           }];
     }
     else
     {
          NSMutableArray *vokiCharacter = [[NSMutableArray alloc]initWithObjects:@"Animals",@"Anime",@"Beach",@"Cats",@"Classic",@"Digimon",@"Dogs",@"Edgy",@"Fantasy",@"Folk",@"World",@"Holiday",@"Monsters",@"Oddballs",@"Politics",@"Randoms",@"Smilies",@"Symbols",@"Toons",@"VIP",@"3D",nil];
          [UIView animateWithDuration:0.5 animations:^
           {
                _itemName.alpha=1;
           }];
          
          UILongPressGestureRecognizer* gesture=(UILongPressGestureRecognizer*)sender;
          NSInteger i = gesture.view.tag - 555;
          NSString *charname = [vokiCharacter objectAtIndex:i];
          
          _itemName.text = charname;
     }
}


- (IBAction)panGestureToMoveChart:(UIPanGestureRecognizer *)sender
{
     
     if (sender.state == UIGestureRecognizerStateChanged)
     {
          if (isright == YES )
          {
               gesturePanBool = false;
               CGPoint p1 = [sender locationInView:self.view];
               
               CGPoint p2 = [___CharacterWebView center];
               p1.x= p1.x+67;
               CGFloat f = [self pointPairToBearingDegrees:p2 secondPoint:p1];
               
               NSString *floatstr = [NSString stringWithFormat:@"%f",f];
               NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",floatstr,@"1",@"200"];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
          }
          
          else if (  issecopen == true)
          {
               
               gesturePanBool = false;
               CGPoint p1 = [sender locationInView:self.view];
               CGPoint p2 = [___CharacterWebView center];
               p1.x= p1.x-72;
               CGFloat f = [self pointPairToBearingDegrees:p2 secondPoint:p1];
               
               NSString *floatstr = [NSString stringWithFormat:@"%f",f];
               NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",floatstr,@"1",@"200"];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
               
          }
          else if (isvoicepanopen == true)
          {
               gesturePanBool = false;
               CGPoint p1 = [sender locationInView:self.view];
               CGPoint p2 = [___CharacterWebView center];
               p1.x= p1.x-153;
               CGFloat f = [self pointPairToBearingDegrees:p2 secondPoint:p1];
               NSString *floatstr = [NSString stringWithFormat:@"%f",f];
               NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",floatstr,@"1",@"200"];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
               
               
          }
          else
          {
               gesturePanBool = false;
               CGPoint p1 = [sender locationInView:self.view];
               CGPoint p2 = [___CharacterWebView center];
               p1.x= p1.x-10;
               CGFloat f = [self pointPairToBearingDegrees:p2 secondPoint:p1];
               
               NSString *floatstr = [NSString stringWithFormat:@"%f",f];
               NSString *jsString = [NSString stringWithFormat:@"setGaze('%@','%@','%@')",floatstr,@"1",@"200"];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
               
          }
     }
     else
     {
          gesturePanBool = true;
     }
     
}


#pragma mark - Get Degree
- (CGFloat) pointPairToBearingDegrees:(CGPoint)startingPoint secondPoint:(CGPoint) endingPoint
{
     
     CGPoint originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y); // get origin point to origin by subtracting end from start
     float bearingRadians = atan2f(originPoint.y, originPoint.x); // get bearing in radians
     float bearingDegrees = bearingRadians * (180.0 / M_PI);// convert to degrees
     
     if (bearingDegrees < 270)
     {
          bearingDegrees = bearingDegrees + 90;
     }
     
     bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
     return bearingDegrees;
}
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
     return YES;
}
#pragma mark - Dice Process

//-(void)slidingcalled
//{
//     [sliding_collectionview layoutIfNeeded];
//     NSArray *visibleItems = [sliding_collectionview indexPathsForVisibleItems];
//     NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
//     NSIndexPath *nextItem = [NSIndexPath indexPathForItem:1 inSection:currentItem.section];
//     [sliding_collectionview scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
//}

-(void)timerCalled
{
     
     self._vokiDiceBtn.enabled = false;
     self._saveToLibBtn.enabled = false;
     rotateDiceCount++;
     if (rotateDiceCount > 10)
     {
          __vokiDiceBtn.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 0);
          [_VokiDiceImage stopAnimating];
          [recordTimer invalidate];
          self.view.userInteractionEnabled = YES;
          NSMutableArray* arrayOfArr1 = [NSMutableArray new];
          [arrayOfArr1 addObjectsFromArray:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char"]]];
          NSInteger l = arc4random_uniform(arrayOfArr1.count);
          
          NSArray* Array = [self RemoveIconImage:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",[arrayOfArr1 objectAtIndex:l]]]];
          int temp = l;
          UIButton *buttonm = (UIButton *)[__vokiScroller viewWithTag:temp+555];
          
          
          
          NSInteger r = arc4random_uniform(Array.count);
          thirdScrollerCharacterIndex = r;
          NSString  *xml_Path = [NSString stringWithFormat:@"assets/img/char/%@/%@",[arrayOfArr1 objectAtIndex:l],[Array objectAtIndex:r]];
          NSString *key = [arrayOfArr1 objectAtIndex:l];
          NSArray * characters = [Allproductid objectForKey:key];
          
          
          if (temp == /* DISABLES CODE */ (6) && temp == 5)
          {
               __vokiScroller.contentOffset = CGPointMake(0,  buttonm.frame.origin.y-50);
               
          }
          else if (temp > 7)
          {
               
               __vokiScroller.contentOffset = CGPointMake(0,  buttonm.frame.origin.y);
               
          }
          else
          {
               [UIView animateWithDuration:0.7 animations:^{
                    __vokiScroller.contentOffset = CGPointMake(0, 0);
               }];
          }
          
          Productforbuy = [characters objectAtIndex:r];
          
          if (![Productforbuy  isEqual: @"Purchased"])
          {
               [self fetchAvailableProducts];
          }
          
          
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
          NSString *islogin = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"islogin"];
          
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcters containsObject:xml_Path] || [freecharacetr containsObject:xml_Path] )
                    {
                         dollar.hidden = true;
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         
                         
                    }
                    else
                    {
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         _sidedollar.enabled = true;
                         _Purchasedollerimg.hidden = false;
                         _sidedollar.hidden = false;
                    }
               }
               else
               {
                    dollar.hidden = true;
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
                    // _sidedollar.enabled= false;
                    
               }
          }
          else
          {
               dollar.hidden = true;
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
               //_sidedollar.enabled= false;
               
          }
          
          selectedCategory = [NSString stringWithFormat:@"assets/img/char/%@/",[arrayOfArr1 objectAtIndex:l]];
          [[NSUserDefaults standardUserDefaults]setValue:selectedCategory forKey:@"selectedCategory"];
          [[NSUserDefaults standardUserDefaults]setValue:spriteSheetSelectedChar forKey:@"spriteSheetSelectedChar"];
          
          [[NSUserDefaults standardUserDefaults]synchronize];
          RUNNING_CHARACTER = xml_Path;
          NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
          NSMutableArray *arr = [[ParseObject sharedMethod] CallDescXML:1 :xml_Path];
          if(arr.count != 0)
          {
               spriteSheetSelectedChar = [[arr objectAtIndex:0] valueForKey:@"spriteSheet"];
               NSString *Costume,*fhair,*Glasses,*Hair,*Hat,*mouth,*Necklace,*Props;
               for(int i = 0; i < [[[arr objectAtIndex:0] valueForKey:@"Types"] count]; i ++)
               {
                    for(NSString *key in [[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i])
                    {
                         if([key isEqualToString:@"Costume"])
                         {
                              NSArray  *CostumeArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              
                              NSInteger c = arc4random_uniform(CostumeArray.count);
                              Costume = [CostumeArray objectAtIndex:c];
                         }
                         else if([key isEqualToString:@"fhair"])
                         {
                              NSArray  *fhairArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              NSInteger f = arc4random_uniform(fhairArray.count);
                              fhair = [fhairArray objectAtIndex:f];
                         }
                         else if([key isEqualToString:@"Hair"])
                         {
                              NSArray  *hairArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              
                              NSInteger h = arc4random_uniform(hairArray.count);
                              Hair = [hairArray objectAtIndex:h];
                         }
                         else if([key isEqualToString:@"Hat"])
                         {
                              
                              NSArray  *hatArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              
                              NSInteger h = arc4random_uniform(hatArray.count);
                              Hat = [hatArray objectAtIndex:h];
                              
                         }
                         else if([key isEqualToString:@"mouth"])
                         {
                              
                              NSArray  *mouthArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              
                              NSInteger m = arc4random_uniform(mouthArray.count);
                              mouth = [mouthArray objectAtIndex:m];
                         }
                         else if([key isEqualToString:@"Necklace"])
                         {
                              
                              NSArray  *NecklaceArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              
                              NSInteger n = arc4random_uniform(NecklaceArray.count);
                              Necklace = [NecklaceArray objectAtIndex:n];
                              
                         }
                         else if([key isEqualToString:@"Glasses"])
                         {
                              
                              NSArray  *GlassesArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              
                              NSInteger g = arc4random_uniform(GlassesArray.count);
                              Glasses = [GlassesArray objectAtIndex:g];
                              
                         }
                         else if([key isEqualToString:@"Props"])
                         {
                              
                              NSArray  *PropsArray = [[[[[arr objectAtIndex:0] valueForKey:@"Types"] objectAtIndex:i] valueForKey:key] componentsSeparatedByString:@","];
                              NSInteger p = arc4random_uniform(PropsArray.count);
                              Props = [PropsArray objectAtIndex:p];
                              
                         }
                    }
               }
               
               //NSString *output_path = [NSString stringWithFormat:@"%@/VokiContent",output];
               NSString *output_path = [NSString stringWithFormat:@"%@",output];
               
               NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
               
               LibPath = [myBundle pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",xml_Path] ofType:@"png" inDirectory:@""];
               
               [self openThirdScroller:l];
               NSString *jsString = [NSString stringWithFormat:@"RunXMLfile('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",BundlePath,[[arr objectAtIndex:0] valueForKey:@"spriteSheet"],[[arr objectAtIndex:0] valueForKey:@"colorString"],Costume ?: @"0",fhair?: @"0",Glasses?: @"0",Hair?: @"0",Hat?: @"0",mouth?: @"0",Necklace?: @"969",Props?: @"0"];
               
               jsString = [jsString stringByReplacingOccurrencesOfString:@"World" withString:@"Historical Figures"];
               
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
               [self._saveToLibBtn setEnabled:YES];
               [__vokiDiceBtn setEnabled:YES];
               
               xpos = [[arr objectAtIndex:0] valueForKey:@"XPOS"];
               yposvalue = [[arr objectAtIndex:0] valueForKey:@"YPOS"];
               scalevalue = [[arr objectAtIndex:0] valueForKey:@"SCALE"];
               
               NSString *pos = [NSString stringWithFormat:@"positionChar('%@','%@','%@')",@"0.14",yposvalue,scalevalue];
               [[NSUserDefaults standardUserDefaults] setObject:scalevalue forKey:@"scale"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
               
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:pos];
               genderID = [[arr objectAtIndex:0] valueForKey:@"genderID"];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"enableScreen()"];
               
               if ([__LanguageLbl.text isEqualToString: @"English"])
               {
                    
               }
               [self loaderposition];
          }
     }
}


-(void)openThirdScroller:(int)tag
{
     
     [__shareButton setSelected:NO];
     
     if(_previousVokiTag != 0)
     {
          UIButton *button = (UIButton *)[__vokiScroller viewWithTag:_previousVokiTag];
          [button setSelected:NO];
     }
     
     UIButton *button1 = (UIButton *)[__vokiScroller viewWithTag:tag+555];
     [button1 setSelected:YES];
     
     
     NSString *image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"assets/img/char/%@/%@",[VokiScrollerArray objectAtIndex:tag],[self FetchImageIcon:@"as+++++++++++++++++++++++++++++++++sets/img/char" :[VokiScrollerArray objectAtIndex:tag]]] ofType:@"png" inDirectory:@""];
     
     
     NSLog(@"%@",image_Path);
     
     
     NSString *replaceOver = [image_Path stringByReplacingOccurrencesOfString:@"base" withString:@"over"];
     if(image_Path == nil)
     {
          [button1 setBackgroundImage:[UIImage imageNamed:@"btn_img_unSel"] forState:UIControlStateNormal];
     }
     else
     {
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
               UIImage *image = [UIImage imageWithContentsOfFile:replaceOver];
               dispatch_async(dispatch_get_main_queue(), ^{
                    [button1 setBackgroundImage:image forState:UIControlStateSelected];
               });
          });
     }
     _previousVokiTag = tag+555;
     [UIView animateWithDuration:0.5 animations:^
      {
           _deleteGalleryBtn.alpha =0;
           __shareView.alpha = 0;
           _CloeseThirdStripBtn.alpha = 1;
           __thirdView.alpha = 1;
           __vokiDiceBtn.alpha = 1;
           __vokiDiceBtn.hidden = false;
           _VokiDiceImage.alpha = 1;
           _VokiDiceImage.hidden = false;
           __thirdView.hidden=false;
           thirdView = false;
           __vokiDiceBtn.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 0);
           __thirdView.frame = CGRectMake(__vokiView.frame.origin.x+__vokiView.frame.size.width + 8, __thirdView.frame.origin.y, __thirdView.frame.size.width, __thirdView.frame.size.height);
           _CloeseThirdStripBtn.frame = CGRectMake(__thirdView.frame.origin.x+__thirdView.frame.size.width , _CloeseThirdStripBtn.frame.origin.y, _CloeseThirdStripBtn.frame.size.width, _CloeseThirdStripBtn.frame.size.height);
           __shareView.frame = CGRectMake(__vokiView.frame.origin.x, __shareView.frame.origin.y, __shareView.frame.size.width, __shareView.frame.size.height);
           _deleteGalleryBtn.frame = CGRectMake(__shareView.frame.origin.x + __shareView.frame.size.width, self.view.frame.size.height - _deleteGalleryBtn.frame.size.width+10, _deleteGalleryBtn.frame.size.width, _deleteGalleryBtn.frame.size.height);
           __vokiDiceBtn.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
           _VokiDiceImage.frame = CGRectMake(__thirdView.frame.origin.x + __thirdView.frame.size.width + 8,10, __vokiDiceBtn.frame.size.width, __vokiDiceBtn.frame.size.height);
      }];
     
     [self CharacterMove];
     [self CallThirdStrip];
     
     NSArray* Array = [self RemoveIconImage:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",[VokiScrollerArray objectAtIndex:tag]]]];
     [[NSUserDefaults standardUserDefaults] setValue:Array forKey:@"SaveCharacterPath"];
     [[NSUserDefaults standardUserDefaults]synchronize];
     [self LoadVokiThirdScrollerDice:[NSString stringWithFormat:@"assets/img/char/%@",[VokiScrollerArray objectAtIndex:tag]] :Array];
     
     
}


-(void)LoadVokiThirdScrollerDice:(NSString*)path :(NSArray*)arrayOfString
{
     //NSString *output_path = [NSString stringWithFormat:@"%@/VokiContent",output];
     NSString *output_path = [NSString stringWithFormat:@"%@",output];
     
     for(UIView *subview in __thirdContentScroller.subviews)
     {
          [subview removeFromSuperview];
     }
     int x = 0;
     int y = 0;
     _previousVokiScrollerTag = 0;
     
     int count = 0;
     if(arrayOfString.count == 0)
     {
          count = 1;
     }
     else
     {
          count = [arrayOfString count];
          randomArrayCount = count;
          VokiContentArray = [[NSMutableArray alloc]init];
          [VokiContentArray addObjectsFromArray:arrayOfString];
     }
     float buttonHeight = 0.0;
     for(int i = 0; i<count; i++)
     {
          UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
          
          [button addTarget:self action:@selector(PickVokiContent:) forControlEvents:UIControlEventTouchUpInside];
          dollar =[[UIImageView alloc]init];
          dollar.image = [UIImage imageNamed:@"dollar.png"];
          
          if (i==0)
          {
               dollar.hidden = YES;
               
          }
          NSString *image_Path = @"";
          if(arrayOfString.count == 0)
          {
               image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/icons/base/btn_128x128",path] ofType:@"png" inDirectory:@""];
               
               
          }
          else
          {
               
               NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
               image_Path = [myBundle pathForResource:[NSString stringWithFormat:@"%@/%@/thumb_128x128",path,[arrayOfString objectAtIndex:i]] ofType:@"png" inDirectory:@""];
          }
          
          if(image_Path == nil)
          {
               [button setBackgroundImage:[UIImage imageNamed:@"btn_img_unSel"] forState:UIControlStateNormal];
          }
          else
          {
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    UIImage *image = [UIImage imageWithContentsOfFile:image_Path];
                    UIImage *image1  = [self resizeimage:image];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [button setBackgroundImage:image1 forState:UIControlStateNormal];
                         [button setBackgroundColor:[UIColor whiteColor]];
                         button.layer.cornerRadius = 5;
                         button.clipsToBounds= YES;
                    });
               });
          }
          
          
          button.frame = CGRectMake(x+8, y, __thirdContentScroller.frame.size.width-16, _stripWidth-14);
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               button.frame = CGRectMake(x+8, y, __thirdContentScroller.frame.size.width-16, _stripWidth-23);
               dollar.frame = CGRectMake(60, y+70, 30, 30);
          }
          else
          {
               if (_canvasWidth == 667)
               {
                    dollar.frame = CGRectMake(35, y+35, 20, 20);
               }
               else if (_canvasWidth == 736)
               {
                    dollar.frame = CGRectMake(40, y+40, 22, 22);
                    
               }
               else
               {
                    dollar.frame = CGRectMake(32, y+31, 18, 18);
               }
          }
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
          NSString *islogin = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"islogin"];
          NSString *xml_Path = [NSString stringWithFormat:@"%@/%@",path,[arrayOfString objectAtIndex:i]];
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcters containsObject:xml_Path] || [freecharacetr containsObject:xml_Path] )
                    {
                         dollar.hidden = true;
                         
                         
                         
                    }
                    else
                    {
                         dollar.hidden = false;
                         
                         
                    }
               }
               else
               {
                    dollar.hidden = true;
                    
               }
               
          }
          
          else
          {
               dollar.hidden = true;
               
          }
          
          button.tag = i + 777;
          NSLog(@"button tag are%ld@",button.tag);
          buttonHeight = button.frame.size.height;
          UILongPressGestureRecognizer *CharacterName = [[UILongPressGestureRecognizer alloc] init];
          [CharacterName addTarget:self action:@selector(userLongPressedCharacterName:)];
          [button addGestureRecognizer:CharacterName];
          [__thirdContentScroller addSubview:button];
          [__thirdContentScroller addSubview:dollar];
          
          y = y + _stripWidth;
     }
     [__thirdContentScroller setContentSize:CGSizeMake(0, (buttonHeight+18)*count)];
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          [__thirdContentScroller setContentSize:CGSizeMake(0, (buttonHeight+28)*count)];
     }
     
     for(UIView *subview in __thirdContentScroller.subviews)
     {
          if ([subview isKindOfClass:[UIButton class]])
          {
               UIButton* myButton1 = (UIButton*)subview;
               myButton1.layer.borderWidth = 0;
               myButton1.layer.borderColor = [[UIColor clearColor]CGColor];
          }
     }
     UIButton *loadvokibutton = (UIButton *)[__thirdContentScroller viewWithTag:thirdScrollerCharacterIndex+777];
     loadvokibutton.layer.borderWidth = 3;
     loadvokibutton.layer.borderColor = [[UIColor orangeColor]CGColor];
     
     if (thirdScrollerCharacterIndex > 4)
     {
          [UIView animateWithDuration:0.7 animations:^{
               
               // [self._thirdContentScroller scrollRectToVisible:loadvokibutton.frame animated:YES];
               __thirdContentScroller.contentOffset = CGPointMake(0,  loadvokibutton.frame.origin.y );
               
               [self LoadVokiScroller];
               // [__thirdContentScroller setContentOffset:CGPointMake(0, loadvokibutton.frame.origin.y)];
               
          }];
          
     }
     else
     {
          [UIView animateWithDuration:0.7 animations:^{
               [__thirdContentScroller setContentOffset:CGPointMake(0, 0)];
               [self LoadVokiScroller];
               
          }];
     }
}

-(void)getSessionApi
{
     @autoreleasepool
     {
          NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://vhss.oddcast.com/vhss_editors/getSession.php"]];
          [request setHTTPMethod:@"GET"];
          
          [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
           {
                dispatch_async(dispatch_get_main_queue(), ^{
                     
                     if (data)
                     {
                          NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                          NSArray *StrArr = [charlieSendString componentsSeparatedByString:@"="];
                          if (StrArr.count >= 1)
                          {
                               SessionID = [StrArr objectAtIndex:1];
                          }
                          
                     }
                     else if (error)
                     {
                          
                     }
                     
                });
           }];
     }
}

-(void)fxaudiowithfile
{
     
}

-(void)saveScene
{
     NSString *storePath;
     if ( WithFxvalue == true)
     {
          NSString *AudioUrl = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"FXRecorded"];
          
          
          // NSString *str = [NSUserDefaults standardUserDefaults]ge
          
          NSURL *URL = [NSURL URLWithString:[AudioUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
          NSData *data = [NSData dataWithContentsOfURL:URL];
          
          if (data == nil) {
               NSLog(@"error");
               
          }
          
          NSString *appDocDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
          storePath = [appDocDir stringByAppendingPathComponent:@"Mp3File1.mp3"];
          
          if ([[NSFileManager defaultManager] fileExistsAtPath:storePath])
          {
               [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
          }
          
          // My comment : make sure that you check whether this file exists at above path???
          // I run this code, and it can save data.
          BOOL success = [data writeToFile:storePath atomically:YES];
          
          if (success)
          {
               fxaudioapicall = true;
               [[NSUserDefaults standardUserDefaults] setObject:storePath forKey:@"AudioPath1"];
               [[NSUserDefaults standardUserDefaults] synchronize];
          }
          else
          {
               [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AudioPath1"];
               [[NSUserDefaults standardUserDefaults] synchronize];
          }
     }
     
     if (saveSceneUrl == nil)
     {
          [audioActivity stopAnimating];
          audioActivity= [[UIActivityIndicatorView alloc]
                          initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               audioActivity.frame = CGRectMake(playCharacterAud1.frame.origin.x+20,playCharacterAud1.frame.origin.y-80,70,70);
               
          }
          else
          {
               audioActivity.frame = CGRectMake(playCharacterAud1.frame.origin.x,playCharacterAud1.frame.origin.y-80,70,70);
               
          }
          if(_isRightVokiOpened)
          {
               audioActivity.backgroundColor = [UIColor blackColor];
          }
          else
          {
               audioActivity.color = [UIColor blackColor];
          }
          // audioActivity.color = [UIColor blackColor];
          [audioActivity startAnimating];
          [self.view addSubview:audioActivity];
          
          
          [[NSUserDefaults standardUserDefaults]setObject:characterCSURL forKey:@"characterCSURL"];
          [[NSUserDefaults standardUserDefaults]synchronize];
          if([__descriptionTextView.text isEqualToString:@"Enter message"]||[__descriptionTextView.text isEqualToString:@""])
          {
               
          }
          else
          {
               
               [[NSUserDefaults standardUserDefaults]setValue:__descriptionTextView.text forKey:@"soundTTS"];
               [[NSUserDefaults standardUserDefaults]synchronize];
               
          }
          characterCSURL = [characterCSURL stringByReplacingOccurrencesOfString:@"ohv2.png" withString:@"oh.swf"];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"hideLoader()"];
          
          NSString *TTS;
          NSString *audioID;
          if (audioID == nil)
          {
               //               UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Audio id is nil" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               //               [alert show];
          }
          if (issavescean == true)
          {
               
               TTS = [[NSUserDefaults standardUserDefaults]valueForKey:@"defaultTTS"];
               
          }
          else
          {
               
               TTS = [[NSUserDefaults standardUserDefaults]valueForKey:@"soundTTS"];
               
               
          }
          audioID = [[NSUserDefaults standardUserDefaults]valueForKey:@"audioID"];
          NSString *xml;
          NSString *fxmain = [NSString stringWithFormat:@"%@%@",fxtype,fxlevel];
          if ([TTS containsString:@"http://vhss"])
          {
               
               TTSText = audioID;
               xml = [NSString stringWithFormat:@"<SAVESCENE PHPSESSID=\"%@\" TYPE=\"3\" CID=\"6\" ACCOUNTID=\"37533\" PARTNERID=\"16\"><PARTNER partner_thw=\"267\" partner_thh=\"267\" /><SCENE TITLE=\"%@\" ID=\"0\"><SKIN CATID=\"57\" ID=\"124\" /><HOST CVERSION=\"2\" XPOS=\"%@\" YPOS=\"%@\" SCALE=\"%@\" URL=\"%@\"/><BG CATID=\"53\" ID=\"%@\"/><AUDIO ID=\"%@\" curFX=\"%@\" PLAY =\"load\"><VOKI CEL=\"0\" CHT=\"0\" EML=\"0\" ABT=\"0\" HST=\"0\" CMT=\"0\" ONLOAD=\"1\" CNT=\"0\"/></SCENE></SAVESCENE>",SessionID,@"myvoki",xpos,yposvalue,scalevalue,
                      characterCSURL,backgroundID,TTSText,recordFXtypeLevel];
          }
          else if (TTS != nil)
          {
               
               if (fxtype == nil  || fxtype == @"")
               {
                    TTSText = TTS;
                    xml = [NSString stringWithFormat:@"<SAVESCENE PHPSESSID=\"%@\" TYPE=\"3\" CID=\"6\" ACCOUNTID=\"37533\" PARTNERID=\"16\"><PARTNER partner_thw=\"267\" partner_thh=\"267\" /><SCENE TITLE=\"%@\" ID=\"0\"><SKIN CATID=\"57\" ID=\"124\" /><HOST CVERSION=\"2\" XPOS=\"%@\" YPOS=\"%@\" SCALE=\"%@\" URL=\"%@\"/><BG CATID=\"53\" ID=\"%@\"/><AUDIO PLAY =\"load\"><TTS VOICE=\"%d\" LANG=\"%d\" ENGINE=\"%d\">%@</TTS></AUDIO><VOKI CEL=\"0\" CHT=\"0\" EML=\"0\" ABT=\"0\" HST=\"0\" CMT=\"0\" ONLOAD=\"1\" CNT=\"0\"/></SCENE></SAVESCENE>",SessionID,@"myvoki",xpos,yposvalue,scalevalue,characterCSURL,backgroundID,_textVoiceID,_textLanguageID,engineID,TTSText];
                    
               }
               else
               {
                    
                    TTSText = TTS;
                    NSString *fxmain = [NSString stringWithFormat:@"%@%@",fxtype,fxlevel];
                    xml = [NSString stringWithFormat:@"<SAVESCENE PHPSESSID=\"%@\" TYPE=\"3\" CID=\"6\" ACCOUNTID=\"37533\" PARTNERID=\"16\"><PARTNER partner_thw=\"267\" partner_thh=\"267\" /><SCENE TITLE=\"%@\" ID=\"0\"><SKIN CATID=\"57\" ID=\"124\" /><HOST CVERSION=\"2\" XPOS=\"%@\" YPOS=\"%@\" SCALE=\"%@\" URL=\"%@\"/><BG CATID=\"53\" ID=\"%@\"/><AUDIO PLAY =\"load\"><TTS FX=\"%@\" VOICE=\"%d\" LANG=\"%d\" ENGINE=\"%d\">%@</TTS></AUDIO><VOKI CEL=\"0\" CHT=\"0\" EML=\"0\" ABT=\"0\" HST=\"0\" CMT=\"0\" ONLOAD=\"1\" CNT=\"0\"/></SCENE></SAVESCENE>",SessionID,@"myvoki",xpos,yposvalue,scalevalue,characterCSURL,backgroundID,fxmain,
                           _textVoiceID,_textLanguageID,engineID,TTSText];
                    
               }
          }
          else
          {
               
               TTSText = @"voki.com";
               
               xml = [NSString stringWithFormat:@"<SAVESCENE PHPSESSID=\"%@\" TYPE=\"3\" CID=\"6\" ACCOUNTID=\"37533\" PARTNERID=\"16\"><PARTNER partner_thw=\"267\" partner_thh=\"267\" /><SCENE TITLE=\"%@\" ID=\"0\"><SKIN CATID=\"57\" ID=\"124\" /><HOST CVERSION=\"2\" XPOS=\"%@\" YPOS=\"%@\" SCALE=\"%@\" URL=\"%@\"/><BG CATID=\"53\" ID=\"%@\"/><AUDIO PLAY =\"load\"><TTS VOICE=\"%d\" LANG=\"%d\" ENGINE=\"3\">%@</TTS></AUDIO><VOKI CEL=\"0\" CHT=\"0\" EML=\"0\" ABT=\"0\" HST=\"0\" CMT=\"0\" ONLOAD=\"1\" CNT=\"0\"/></SCENE></SAVESCENE>",SessionID,@"myvoki",xpos,yposvalue,scalevalue,characterCSURL,backgroundID,_textVoiceID,_textLanguageID,TTSText];
               
               
          }
          
          NSLog(@"%@",xml);
          NSString *str = [NSString stringWithFormat:@"Createimage()"];
          NSString *Base64 = [___CharacterWebView stringByEvaluatingJavaScriptFromString:str];
          NSString *post = [NSString stringWithFormat:@"saveXML=%@&dataStr=%@&height=%@&width=%@",xml,Base64,@"267",@"267"];
          NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
          NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://vhss.oddcast.com/vhss_editors/saveVokiScene.php"]];
          request.timeoutInterval = 20;
          [request setHTTPMethod:@"POST"];
          [request setHTTPBody:postData];
          [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
           {
                dispatch_async(dispatch_get_main_queue(), ^{
                     if (data)
                     {
                          [audioActivity stopAnimating];
                          
                          NSError *error;
                          NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                          NSDictionary *dict = [XMLReader dictionaryForXMLString:charlieSendString
                                                                         options:XMLReaderOptionsProcessNamespaces
                                                                           error:&error];
                          NSDictionary *DictXml = [dict valueForKey:@"SAVESCENE"];
                          chsm = [DictXml valueForKey:@"CHSM"];
                          Scid = [DictXml valueForKey:@"ID"];
                          saveSceneUrl = [NSString stringWithFormat:@"http://www.voki.com/pickup.php?scid=%@&height=330&width=575",Scid];
                          [self.view setUserInteractionEnabled:true];
                          [audioActivity stopAnimating];
#pragma mark
                          //To Move Character ....
                          
                          
                          
                          if (Googleshared == YES)
                          {
                               [self GooglePlussharing];
                               
                          }
                          else if (whatsappshared == YES)
                          {
                               [self Performwhatsappaction];
                          }
                          else
                          {
                               [self sharingonMail];
                          }
                          
                          
                     }
                     else if (error)
                     {
                          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                          [alert show];
                          _sidedollar.enabled = true;
                          _sidedollar.hidden = false;
                          _sidedollar.alpha = 1;
                          vokilogo_imgagess.hidden = true;
                          _isRightVokiOpened = NO;
                          
                          if (menuViewOpenClose == NO)
                          {
                               _isRightShareOpen = NO;
                               _isRightMenuOpen = YES;
                               rightmenu = true;
                               
                               [self characterLeftMoveMenu];
                          }
                          else
                          {
                               
                               _isRightShareOpen = NO;
                               _isRightMenuOpen = NO;
                               rightmenu = false;
                               
                               
                          }
                          
                          
                          __aboutView.hidden=true;
                          __menuDescriptionView.hidden=true;
                          __rightShareBtn.hidden = false;
                          
                          [__rightShareBtn setEnabled:YES];
                          [__rightVokiBtn setEnabled:YES];
                          [audioActivity stopAnimating];
                          [audioActivity removeFromSuperview];
                     }
                     
                });
                
           }];
          
     }
     else
     {
     }
}

#pragma mark -ConvertAudioToMp3
- (void) toMp3
{
    // [self startTransparentIndicator];
     
     [self.view setUserInteractionEnabled:true];
     
     NSString *cafFilePath =[NSTemporaryDirectory() stringByAppendingString:@"RecordedFile"];
     NSString *mp3FileName = @"Mp3File";
     mp3FileName = [mp3FileName stringByAppendingString:@".mp3"];
     NSString *mp3FilePath = [[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"] stringByAppendingPathComponent:mp3FileName];
     //BOOL result = [[NSFileManager defaultManager] fileExistsAtPath:mp3FilePath];
     
     dispatch_async(dispatch_get_main_queue(), ^{
    
     
     @try {
          int read, write;
          FILE *pcm = fopen([cafFilePath cStringUsingEncoding:1], "rb");  //source
          fseek(pcm, 4*1024, SEEK_CUR);                                   //skip file header
          FILE *mp3 = fopen([mp3FilePath cStringUsingEncoding:1], "wb");  //output
          const int PCM_SIZE = 8192;
          const int MP3_SIZE = 8192;
          short int pcm_buffer[PCM_SIZE*2];
          unsigned char mp3_buffer[MP3_SIZE];
          lame_t lame = lame_init();
          lame_set_in_samplerate(lame, 44100.0);
          lame_set_VBR(lame, vbr_default);
          lame_init_params(lame);
          
          do {
               read = fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
               if (read == 0)
                    write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
               else
                    write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
               
               fwrite(mp3_buffer, write, 1, mp3);
               
          } while (read != 0);
          lame_close(lame);
          fclose(mp3);
          fclose(pcm);
     }
     @catch (NSException *exception) {
     }
     @finally {
          [self performSelectorOnMainThread:@selector(convertMp3Finish)
                                 withObject:nil
                              waitUntilDone:YES];
     }
     
     });
     
     
     
}


- (void) convertMp3Finish
{
     [[UIApplication sharedApplication] endIgnoringInteractionEvents];
     NSInteger fileSize =  [self getFileSize:[NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", @"Mp3File.mp3"]];
     NSString *path = [NSString stringWithFormat:@"%ld kb", fileSize/1024];
     
}


- (NSInteger) getFileSize:(NSString*) path
{
     NSString *str = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", @"Mp3File.mp3"];
     recordWithFXPath = str;
     [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"AudioPath"];
     [[NSUserDefaults standardUserDefaults] synchronize];
     [self audioApi:str];
     NSFileManager * filemanager = [[NSFileManager alloc]init];
     if([filemanager fileExistsAtPath:path]){
          NSDictionary * attributes = [filemanager attributesOfItemAtPath:path error:nil];
          NSNumber *theFileSize;
          if ( (theFileSize = [attributes objectForKey:NSFileSize]) )
               return  [theFileSize intValue];
          else
               return -1;
     }
     else
     {
          return -1;
     }
}
-(void)centerPositionChar
{
     
     if ( _isRightShareOpen  || _isRightMenuOpen )
     {
          NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"",ypos];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          
     }
     else
     {
          if (constant==0)
          {
               constant++;
               NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"50",yposvalue];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               
          }
          
          else
          {
               constant++;
               NSString *ypos = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"getYPos()"];
               NSString  *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"30",ypos];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
               [self loaderposition];
               //For iphone 5
               CGFloat xload = ((playCharacterAud1.frame.origin.x)*2.4);
               CGFloat yload = self.view.frame.size.height*2;
               NSString *Xvale = [NSString stringWithFormat:@"updateLoaderPosition('%f','%f')",xload,yload];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvale];
               
          }
     }
     
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
     {
          
          [UIView animateWithDuration:0.1 animations:^
           {
                playCharacterAud1.alpha = 0;
                
                
           }];
          
          [UIView animateWithDuration:0.2 animations:^
           {
                playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width)/2 - 19 + playCharacterAud1.frame.size.width/2 - 19 , self.view.frame.size.height - 105, 95, 95);
                _PlayCharAud1dup.frame = playCharacterAud1.frame;
                
           }];
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.30 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
               [UIView animateWithDuration:1.5 animations:^
                {
                     
                     playCharacterAud1.alpha = 1;
                }];
          });
     }
     else
     {
          if (_canvasWidth == 480)
          {
               
               [UIView animateWithDuration:0.1 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                }];
               
               [UIView animateWithDuration:0.5 animations:^
                {
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width)/2 -15 + playCharacterAud1.frame.size.width/4.4 - 20, self.view.frame.size.height - 75, 55, 55);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          
          else if (_canvasWidth == 736)
          {
               
               [UIView animateWithDuration:0.1 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                }];
               [UIView animateWithDuration:0.5 animations:^
                {
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width)/2 -18 + playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, 65, 65);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
          }
          else if(_canvasWidth == 667)
          {
               [UIView animateWithDuration:0.1 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                }];
               [UIView animateWithDuration:0.5 animations:^
                {
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width - __mainFirstStrip.frame.size.width)/2 - 18 + playCharacterAud1.frame.size.width/2.5 , self.view.frame.size.height - 75, 65, 65);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     
                     
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          playCharacterAud1.alpha = 1;
                     }];
               });
          }
          else
          {
               
               [UIView animateWithDuration:0.1 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                }];
               [UIView animateWithDuration:0.5 animations:^
                {
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width  - __mainFirstStrip.frame.size.width)/2 - 15 + playCharacterAud1.frame.size.width/4, self.view.frame.size.height - 75, 65, 65);
                     
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:1.2 animations:^
                     {
                          
                          
                          playCharacterAud1.alpha = 1;
                     }];
               });
               
               
          }
     }
     
     
     if (__mainFirstStrip.frame.origin.x != self.view.frame.origin.x)
     {
          
          
          NSString *Xvalue = [NSString stringWithFormat:@"updateBoxPosition('%@','%@')",@"",@"-0.15"];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               
               
               [UIView animateWithDuration:0.1 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                }];
               [UIView animateWithDuration:0.0 animations:^
                {
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width)/2 - playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 105, 95, 95);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                     
                }];
               
          }
          else
          {
               
               [UIView animateWithDuration:0.1 animations:^
                {
                     playCharacterAud1.alpha = 0;
                     
                }];
               [UIView animateWithDuration:0.5 animations:^
                {
                     
                     playCharacterAud1.frame = CGRectMake((self.view.frame.size.width)/2 + 5
                                                          - playCharacterAud1.frame.size.width/2 , self.view.frame.size.height - 75, playCharacterAud1.frame.size.width, playCharacterAud1.frame.size.height);
                     _PlayCharAud1dup.frame = playCharacterAud1.frame;
                }];
               
          }
          
          
          
     }
}
-(void)secondScrollerDeselectedButton
{
     for(UIView *subview in _MouthViewScroller.subviews)
     {
          if ([subview isKindOfClass:[UIButton class]])
          {
               UIButton* myButton1 = (UIButton*)subview;
               [myButton1 setSelected:NO];
          }
          
     }
}

-(void)audioApi:(NSString*)urlStr
{
     [self stopTransparentIndicator];
     if ( issavescean == true)
     {
          
          
     }
     else
     {
          //prabhjot record
          
          [audioActivity stopAnimating];
          audioActivity= [[UIActivityIndicatorView alloc]
                          initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
          audioActivity.center=self.view.center;
          audioActivity.frame = CGRectMake(playCharacterAud1.frame.origin.x+20,playCharacterAud1.frame.origin.y-30,35,35);
          audioActivity.color = [UIColor blackColor];
          [audioActivity startAnimating];
          [self.view addSubview:audioActivity];
          
     }
     
     [self.view setUserInteractionEnabled:NO];
     NSDictionary *para = [NSDictionary dictionaryWithObjectsAndKeys:@"audio",@"type",
                           SessionID,@"PHPSESSID",
                           nil];
     NSString *urlString = @"http://vhss.oddcast.com/vhss_editors/upload.php";
     NSData *fileData = [[NSData alloc] initWithContentsOfFile:urlStr];
     AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     manager.responseSerializer = [AFHTTPResponseSerializer serializer];
     [manager POST:urlString parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
          [formData appendPartWithFileData:fileData
                                      name:@"Filedata"
                                  fileName:@"Mp3File.mp3" mimeType:@"audio/mp3"];
     }
          progress:nil success:^(NSURLSessionDataTask *task, id responseObject)
      {
           NSString *returnString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] ;
           NSError *error;
           NSDictionary *dict = [XMLReader dictionaryForXMLString:returnString
                                                          options:XMLReaderOptionsProcessNamespaces
                                                            error:&error];
           NSDictionary *DictXml = [dict valueForKey:@"UPLOAD"];
           NSString *audioID  = [DictXml valueForKey:@"ID"];
           [[NSUserDefaults standardUserDefaults]setValue:audioID forKey:@"audioID"];
           [[NSUserDefaults standardUserDefaults]synchronize];
           [self fxAudio:audioID];
           //[audioActivity stopAnimating];
           
      } failure:^(NSURLSessionDataTask *task, NSError *error)
      {
           
           __rightShareBtn.enabled = true;
           NSString *sond =   [[NSUserDefaults standardUserDefaults] objectForKey: @"AudioPath"];
           SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",sond];
           [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
           [self.view setUserInteractionEnabled:YES];
           
           UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
           [alert show];
           [audioActivity stopAnimating];
           
      }];
}

-(void)fxAudio:(NSString*)audioid
{
     
     NSString *urlStr = [NSString stringWithFormat:@"http://vhss.oddcast.com/admin/saveFXAudio.php?curFX=%@&audioId=%@&buttonPressed=1&acc_id=37533",recordFXtypeLevel,audioid];
     
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:urlStr]];
     [request setHTTPMethod:@"POST"];
     [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
      {
           dispatch_async(dispatch_get_main_queue(), ^{
                if (data)
                {
                     NSError *error;
                     NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                     NSDictionary *dict = [XMLReader dictionaryForXMLString:charlieSendString
                                                                    options:XMLReaderOptionsProcessNamespaces
                                                                      error:&error];
                     saveCharAudio = false;
                     NSDictionary *DictXml = [dict valueForKey:@"PREVIEWSWF"];
                     NSString *AudioUrl  = [DictXml valueForKey:@"FULLNAME"];
                     __descriptionTextView.text = @"Enter message";
                     
                     if( issavescean == true)
                     {
                          [audioActivity stopAnimating];
                          [[NSUserDefaults standardUserDefaults]setValue:AudioUrl forKey:@"defaultTTS"];
                          [[NSUserDefaults standardUserDefaults]setValue:AudioUrl forKey:@"defaultTTSLink"];
                          [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
                          NSString *sond =   [[NSUserDefaults standardUserDefaults] objectForKey: @"AudioPath"];
                          SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",sond];
                          [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
                          characterCSURL = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"fetchAPILink()"];
                          [self.view setUserInteractionEnabled:YES];
                          
                     }
                     else
                     {
                          
                          // [audioActivity stopAnimating];
                          [_playBtn setBackgroundImage:[UIImage imageNamed:@"playSel.png"] forState:UIControlStateNormal];
                          [_stopBtn setBackgroundImage:[UIImage imageNamed:@"playUnsel.png"] forState:UIControlStateNormal];
                          [[NSUserDefaults standardUserDefaults]setValue:AudioUrl forKey:@"soundTTS"];
                          [[NSUserDefaults standardUserDefaults]setValue:AudioUrl forKey:@"saveTTSLink"];
                          SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",AudioUrl];
                          [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
                          [[NSUserDefaults standardUserDefaults] setObject:AudioUrl forKey:@"FXRecorded"];
                          [[NSUserDefaults standardUserDefaults] synchronize];
                          NSLog(@"%@",AudioUrl);
                          
                          NSURL *URL = [NSURL URLWithString:[AudioUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
                          NSLog(@"%@",URL);
                          NSData *data = [NSData dataWithContentsOfURL:URL];
                          
                          if (data == nil)
                          {
                               
                               //  [NSThread detachNewThreadSelector:@selector(toMp3) toTarget:self withObject:nil];
                               
                               //                              SoundLink = [NSString stringWithFormat:@"play_mp3_1('%@')",AudioUrl];
                               //                              [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
                               
                          }
                          NSString *appDocDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                          NSString *storePath = [appDocDir stringByAppendingPathComponent:@"Mp3File2.mp3"];
                          
                          
                          
                          if ([[NSFileManager defaultManager] fileExistsAtPath:storePath])
                          {
                               [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
                          }
                          
                          // My comment : make sure that you check whether this file exists at above path???
                          // I run this code, and it can save data.
                          BOOL success = [data writeToFile:storePath atomically:YES];
                          
                          if (success)
                          {
                               
                               [[NSUserDefaults standardUserDefaults] setObject:storePath forKey:@"AudioPath"];
                               [[NSUserDefaults standardUserDefaults] synchronize];
                               
                               [self audiopath:storePath];
                               
                          }
                          
                          
                          __rightShareBtn.enabled = true;
                          [self.view setUserInteractionEnabled:YES];
                          
                     }
                     
                     UIImage *img1 = [UIImage imageNamed:@"playRoundBtn.png"];
                     UIImage *img = [UIImage imageNamed:@"pauseRoundBtn.png"];
                     [playCharacterAud1 setBackgroundImage:img1 forState:UIControlStateNormal];
                     [playCharacterAud1 setBackgroundImage:img forState:UIControlStateSelected];
                     return;
                     
                }
                else
                {
                     NSString *sond =   [[NSUserDefaults standardUserDefaults] objectForKey: @"AudioPath"];
                     SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",sond];
                     [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
                     
                }
                
                __rightShareBtn.enabled = true;
                [self.view setUserInteractionEnabled:YES];
                [audioActivity stopAnimating];
           });
      }];
}


-(void)checklaypause
{
#define kRefreshTimeInSeconds 1
     
}

-(void)audiopath:(NSString*)urlStr
{
     
     [self.view addSubview:audioActivity];
     NSDictionary *para = [NSDictionary dictionaryWithObjectsAndKeys:@"audio",@"type",
                           SessionID,@"PHPSESSID",
                           nil];
     NSString *urlString = @"http://vhss.oddcast.com/vhss_editors/upload.php";
     NSData *fileData = [[NSData alloc] initWithContentsOfFile:urlStr];
     AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     manager.responseSerializer = [AFHTTPResponseSerializer serializer];
     [manager POST:urlString parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
      {
           [formData appendPartWithFileData:fileData
                                       name:@"Filedata"
                                   fileName:@"Mp3File.mp3" mimeType:@"audio/mp3"];
      }
          progress:nil success:^(NSURLSessionDataTask *task, id responseObject)
      {
           NSString *returnString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] ;
           NSError *error;
           NSDictionary *dict = [XMLReader dictionaryForXMLString:returnString
                                                          options:XMLReaderOptionsProcessNamespaces
                                                            error:&error];
           NSDictionary *DictXml = [dict valueForKey:@"UPLOAD"];
           NSString *audioID  = [DictXml valueForKey:@"ID"];
           if (audioID != nil)
           {
                
                [[NSUserDefaults standardUserDefaults]setValue:audioID forKey:@"audioID"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
           }
           
           
           NSString *AudioUrl = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"FXRecorded"];
           
           //           SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",AudioUrl];
           //           [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
           [ audioActivity stopAnimating];
           
           return;
      }
      
           failure:^(NSURLSessionDataTask *task, NSError *error)
      {
           NSString *AudioUrl = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"FXRecorded"];
           
           SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",AudioUrl];
           [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
           
           [ audioActivity stopAnimating];
           __rightShareBtn.enabled = true;
           [audioActivity stopAnimating];
           
      }];
     
}

-(void)playfile
{
     NSString *AudioUrl = [[NSUserDefaults standardUserDefaults]valueForKey:@"soundTTS"];
     SoundLink = [NSString stringWithFormat:@"play_mp3_2('%@')",AudioUrl];
     [___CharacterWebView stringByEvaluatingJavaScriptFromString:SoundLink];
}

-(void)loaderposition
{
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
     {
          CGFloat xload = ((playCharacterAud1.frame.origin.x*2.1));
          CGFloat yload = self.view.frame.size.height*1.3;
          NSString *Xvalue = [NSString stringWithFormat:@"updateLoaderPosition('%f','%f')",xload,yload];
          [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
     }
     else
     {
          if (_canvasWidth == 480)
          {
               CGFloat xload = (playCharacterAud1.frame.origin.x*3.2);
               CGFloat yload = self.view.frame.size.height*2;
               NSString *Xvalue = [NSString stringWithFormat:@"updateLoaderPosition('%f','%f')",xload,yload];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          }
          
          else if (_canvasWidth == 736)
          {
               CGFloat xload = ((playCharacterAud1.frame.origin.x)*3.2);
               CGFloat yload = self.view.frame.size.height*2;
               NSString *Xvalue = [NSString stringWithFormat:@"updateLoaderPosition('%f','%f')",xload,yload];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          }
          else if(_canvasWidth == 667)
          {
               CGFloat xload = (playCharacterAud1.frame.origin.x)*3.2;
               CGFloat yload = self.view.frame.size.height*2;
               NSString *Xvalue = [NSString stringWithFormat:@"updateLoaderPosition('%f','%f')",xload,yload];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          }
          else
          {
               CGFloat xload = ((playCharacterAud1.frame.origin.x)*3.2);
               CGFloat yload = self.view.frame.size.height*2;
               NSString *Xvalue = [NSString stringWithFormat:@"updateLoaderPosition('%f','%f')",xload,yload];
               [___CharacterWebView stringByEvaluatingJavaScriptFromString:Xvalue];
          }
     }
}
-(void)startEventApi
{
     @autoreleasepool
     {
          NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://data.oddcast.com/event.php/?apt=W&acc=1334&emb=0&uni=1&sm=0&et=0&ev[0][]=tss"]];
          [request setHTTPMethod:@"POST"];
          [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
           {
                dispatch_async(dispatch_get_main_queue(), ^{
                     if (data)
                     {
                          NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                     }
                     else if (error)
                          NSLog(@"%@",error);
                });
           }];
     }
     
}
-(void)saveCharApiTrack
{
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://data.oddcast.com/event.php/?apt=W&acc=1334&emb=0&uni=1&sm=0&et=0&ev[0][]=ce1&cnt[ce1]=1"]];
     [request setHTTPMethod:@"POST"];
     [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
      {
           dispatch_async(dispatch_get_main_queue(), ^{
                if (data)
                {
                     NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                }
                else if (error)
                     NSLog(@"%@",error);
           });
      }];
     
}
#pragma mark - Audio Api
-(void)audioEffectApi
{
     @autoreleasepool
     {
          NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://vhss-d.oddcast.com/vhss_editors/xml/getVokiFX.xml"]];
          [request setHTTPMethod:@"POST"];
          [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
           {
                dispatch_async(dispatch_get_main_queue(), ^{
                     if (data)
                     {
                          NSError *error;
                          NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                          
                          NSDictionary *dict = [XMLReader dictionaryForXMLString:charlieSendString
                                                                         options:XMLReaderOptionsProcessNamespaces
                                                                           error:&error];
                          
                          NSMutableDictionary *fx = [dict valueForKey:@"VOKIFX"];
                          
                          fxmenuItems = [fx valueForKey:@"FX"];
                          
                          
                          for (int i = 0; i<fxmenuItems.count; i++)
                          {
                               NSMutableString *str = [fxmenuItems[i]valueForKey:@"PARAM"];
                               
                               NSString *oneIndex;
                               NSString *twoIndex;
                               
                               if (![str isEqualToString:@""])
                               {
                                    
                                    if ([str containsString:@"-"])
                                    {
                                         oneIndex = [NSString stringWithFormat:@"%c", [str characterAtIndex:0]];
                                         [fxTypes addObject:oneIndex];
                                         
                                         twoIndex = [NSString stringWithFormat:@"-%@",[str substringFromIndex:2]];
                                         
                                         [fxLevels addObject:twoIndex];
                                    }
                                    else
                                    {
                                         oneIndex = [NSString stringWithFormat:@"%c", [str characterAtIndex:0]];
                                         [fxTypes addObject:oneIndex];
                                         
                                         twoIndex = [NSString stringWithFormat:@"%@",[str substringFromIndex:1]];
                                         
                                         [fxLevels addObject:twoIndex];
                                    }
                               }
                               else
                                    
                               {
                                    [fxTypes addObject:@""];
                                    [fxLevels addObject:@""];
                               }
                               
                          }
                          
                          
                          [_fxTableView reloadData];
                          
                     }
                     else if (error)
                          NSLog(@"%@",error);
                });
           }];
          
     }
     
}

#pragma mark - Check Devices

- (NSString *) platformType:(NSString *)platform
{
     
     if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
     if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
     if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
     if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
     if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
     if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
     if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
     if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
     if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
     if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
     if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
     if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
     if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
     if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
     if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
     if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
     if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
     if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
     if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
     if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
     if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
     if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
     if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
     if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
     if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
     if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
     if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
     if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
     if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
     if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
     if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
     if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
     if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
     if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
     if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
     if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
     if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
     if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
     if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
     if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
     if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
     if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
     if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
     if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
     if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
     if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
     if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
     if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
     if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
     if ([platform isEqualToString:@"i386"])         return @"Simulator";
     if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
     return platform;
}
-(void)ownbackgroundApi:(NSData*)fileData
{
     
     [audioActivity stopAnimating];
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
          
          [self.view setUserInteractionEnabled:false];
          
          audioActivity= [[UIActivityIndicatorView alloc]
                          initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
          audioActivity.center=self.view.center;
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               audioActivity.frame = CGRectMake(playCharacterAud1.frame.origin.x+20,playCharacterAud1.frame.origin.y-80,70,70);
               
          }
          else
          {
               audioActivity.frame = CGRectMake(playCharacterAud1.frame.origin.x,playCharacterAud1.frame.origin.y-80,70,70);
               
          }
          if(_isRightVokiOpened)
          {
               audioActivity.backgroundColor = [UIColor blackColor];
          }
          else
          {
               audioActivity.color = [UIColor blackColor];
          }
          
          
          dispatch_async(dispatch_get_main_queue(), ^{
               
               
               [audioActivity startAnimating];
               [self.view addSubview:audioActivity];
               NSDictionary *para = [NSDictionary dictionaryWithObjectsAndKeys:@"bg",@"type",
                                     @"Koala",@"name",
                                     SessionID,@"PHPSESSID",
                                     nil];
               NSString *urlString = @"http://vhss.oddcast.com/vhss_editors/upload.php";
               AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
               [manager.requestSerializer setTimeoutInterval:20.0];
               manager.responseSerializer = [AFHTTPResponseSerializer serializer];
               [manager POST:urlString parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                    [formData appendPartWithFileData:fileData
                                                name:@"Filedata"
                                            fileName:@"Koala.jpg" mimeType:@"image/jpg"];
               }
                    progress:nil success:^(NSURLSessionDataTask *task, id responseObject)
                {
                     NSString *returnString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] ;
                     NSError *error;
                     NSDictionary *dict = [XMLReader dictionaryForXMLString:returnString
                                                                    options:XMLReaderOptionsProcessNamespaces
                                                                      error:&error];
                     
                     NSDictionary *sub = [dict valueForKey:@"UPLOAD"];
                     backgroundID = [sub valueForKey:@"ID"];
                     
                     [audioActivity stopAnimating];
                     characterCSURL = [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"fetchAPILink()"];
                     NSString *saveCharAc = [[NSUserDefaults standardUserDefaults]valueForKey:@"characterCSURL"];
                     [self saveScene];
                     [self.view setUserInteractionEnabled:true];
                     
                }
                     failure:^(NSURLSessionDataTask *task, NSError *error)
                {
                     [self.view setUserInteractionEnabled:true];
                     [audioActivity stopAnimating];
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                     [alert show];
                     
                }];
          });
     });
}
#pragma mark Textfeild delegate
//------//-----BeginEditing------///
-(void)textFieldDidBeginEditing:(UITextField *)textField {
     
     
     if (textField.tag == 0)
     {
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               
          }
          else
          {
               [UIView animateWithDuration:0.25 animations:^{
                    self.Accountview.frame =  myframe;
               }];
               
               [UIView animateWithDuration:0.25 animations:^{
                    CGRect oldTextfieldframe =  self.Accountview.frame;
                    oldTextfieldframe.origin.y = self.Accountview.frame.origin.y - 50 ;
                    self.Accountview.frame =  oldTextfieldframe;
               }];
          }
          
     }
     else
     {
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               
          }
          else
          {
               [UIView animateWithDuration:0.25 animations:^{
                    self.Accountview.frame =  myframe;
               }];
               
               [UIView animateWithDuration:0.25 animations:^{
                    CGRect oldTextfieldframe =  self.Accountview.frame;
                    oldTextfieldframe.origin.y = self.Accountview.frame.origin.y - 80 ;
                    self.Accountview.frame =  oldTextfieldframe;
               }];
          }
          
     }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
     
     [UIView animateWithDuration:0.25 animations:^{
          [textField resignFirstResponder];
          self.Accountview.frame =  myframe;
     }];
     
     return  YES;
     
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
     
     
     if ([touches isKindOfClass:[UIButton class]])
     {
          
          
     }
     else
     {
          [UIView animateWithDuration:0.25 animations:^{
               [ _twxtUserName resignFirstResponder];
               [_txtPassword resignFirstResponder];
               self.Accountview.frame =  myframe;
          }];
     }
     
}

-(void)onKeyboardHide
{
     [UIView animateWithDuration:0.25 animations:^{
          [ _twxtUserName resignFirstResponder];
          [_txtPassword resignFirstResponder];
          self.Accountview.frame =  myframe;
     }];
}

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
     return CGRectInset(bounds, 10, 10);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
     return CGRectInset(bounds, 10, 10);
}

#pragma mark inApp Purchase Action

-(void)inapppurchaseAction:(id)sender
{
     [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
          
     }
                      completion:^(BOOL finished){
                           
                           if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
                           {
                                self.Purchasedollerimg.alpha = 1;
                                self.sidedollar.alpha =1;
                                self.sidedollar.enabled = true;
                                alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
                                blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
                                alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
                                alertVc.deleteView.hidden = true;
                                alertVc.ratealert.hidden = true;
                                alertVc.blackview1.hidden = true;
                                alertVc.unblockLearnmore.hidden = true;
                                alertVc.unblockvookichar.hidden = true;
                                alertVc.whiteborderView.hidden = false;
                                [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
                                [self.view addSubview:blurViewCalledbuy];
                                
                                
                                
                           }
                           
                           else
                           {
                                alertVc1.Learnmorebuyallbtn.enabled = false;
                                alertVc1.closeunblocklearnmore.enabled = false;
                                alertVc.unblockrestoreAction.enabled = false;
                                alertVc.vokiblockclose.enabled = false;
                                alertVc.BuyProduct.enabled = false;
                                alertVc.unblocklearnmore.enabled = false;
                                alertVc.unlockbuyallbtn.enabled = false;
                                buyall = false;
                                inappprogressview = [[UIActivityIndicatorView alloc]
                                                     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                                inappprogressview.center=self.view.center;
                                [inappprogressview setBackgroundColor:[UIColor blackColor]];
                                inappprogressview.layer.cornerRadius = 10;
                                [inappprogressview startAnimating];
                                [self.view addSubview:inappprogressview];
                                if (validProducts.count == 0)
                                {
                                     [self fetchAvailableProducts];
                                     [self performSelector:@selector(callinapp)  withObject:nil afterDelay:5.0];
                                }
                                else
                                {
                                     [self fetchAvailableProducts];
                                     [self performSelector:@selector(callinapp)  withObject:nil afterDelay:1.0];
                                }
                           }
                           
                           
                      }];
     
}



-(void)callinapp
{
     
     [self purchaseMyProduct:[validProducts objectAtIndex:0]];
}


-(void)fetchAvailableProducts
{
     if (buyall == true)
     {
          NSSet *productIdentifiers = [NSSet
                                       setWithObjects:BundleID,nil];
          
          NSLog(@"productIdentifiers = %@",productIdentifiers);
          
          _productsRequest = [[SKProductsRequest alloc]
                              initWithProductIdentifiers:productIdentifiers];
          _productsRequest.delegate = self;
          [_productsRequest start];
          _purchasebuyall.enabled = true;
     }
     else
     {
          NSSet *productIdentifiers = [NSSet
                                       setWithObjects:Productforbuy,nil];
          
          NSLog(@"productIdentifiers = %@",productIdentifiers);
          
          _productsRequest = [[SKProductsRequest alloc]
                              initWithProductIdentifiers:productIdentifiers];
          _productsRequest.delegate = self;
          [_productsRequest start];
     }
     
     
}

- (BOOL)canMakePurchases
{
     return [SKPaymentQueue canMakePayments];
}


- (void)purchaseMyProduct:(SKProduct*)product{
     
     
     if (product == nil || product == NULL)
     {
          UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                    @"Purchases are disabled in your device" message:nil delegate:
                                    self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
          [alertView show];
          _purchasebuyall.enabled = true;
          _Purchasebuyallbtn.enabled = true;
     }
     
     else
     {
          _isRightShareOpen = NO;
          [inappprogressview stopAnimating];
          if ([self canMakePurchases]) {
               SKPayment *payment = [SKPayment paymentWithProduct:product];
               [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
               [[SKPaymentQueue defaultQueue] addPayment:payment];
          }
          else{
               UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                         @"Purchases are disabled in your device" message:nil delegate:
                                         self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
               [alertView show];
               _purchasebuyall.enabled = true;
               _Purchasebuyallbtn.enabled = true;
          }
     }
     
}

#pragma -
#pragma Purchase helpers

//
// saves a record of the transaction by storing the receipt to disk
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
     
}

//
// enable pro features
//
- (void)provideContent:(NSString *)productId
{
     
}


- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
     [self recordTransaction:transaction];
     [self provideContent:transaction.payment.productIdentifier];
     [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
     [self recordTransaction:transaction.originalTransaction];
     [self provideContent:transaction.originalTransaction.payment.productIdentifier];
     
}

- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
     _purchasebuyall.enabled = true;
     _Purchasebuyallbtn.enabled = true;
     _isRightShareOpen = NO;
     [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
     // remove the transaction from the payment queue.
     [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
     //   NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
     
     if (wasSuccessful)
     {
          [blurViewCalled1 removeFromSuperview];
          [blurViewCalled removeFromSuperview];
          [blurViewCalled2 removeFromSuperview];
          [blurViewCalled3 removeFromSuperview];
          
          alertVc.unblockrestoreAction.enabled = true;
          alertVc.vokiblockclose.enabled = true;
          alertVc.unlockbuyallbtn.enabled = true;
          alertVc.BuyProduct.enabled = true;
          alertVc.unblocklearnmore.enabled = true;
          alertVc.vokiblockclose.enabled = true;
          // send out a notification that we’ve finished the transaction
          
     }
     else
     {
          [blurViewCalled1 removeFromSuperview];
          [blurViewCalled removeFromSuperview];
          [blurViewCalled2 removeFromSuperview];
          [blurViewCalled3 removeFromSuperview];
          
          alertVc.unblockrestoreAction.enabled = true;
          alertVc.vokiblockclose.enabled = true;
          alertVc.unlockbuyallbtn.enabled = true;
          alertVc.BuyProduct.enabled = true;
          alertVc.unblocklearnmore.enabled = true;
          alertVc.vokiblockclose.enabled = true;
          
          
//          _Purchasedollerimg.alpha = 0;
//          _sidedollar.alpha = 0;
//          _Purchasedollerimg.alpha = 0;
//          _sidedollar.alpha = 0;
//          _sidedollar.enabled = true;
//          _Purchasedollerimg.hidden = true;
//          _sidedollar.hidden = true;
         
               isChangeRegin = true;
          
          
         
          
//          UIAlertView *tmp = [[UIAlertView alloc]
//                              initWithTitle:@"Purchase not successful"
//                              message:nil
//                              delegate:self
//                              cancelButtonTitle:nil
//                              otherButtonTitles:@"Ok", nil];
//          [tmp show]; // send out a notification for the failed transaction
          
     }
}


- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
     if (transaction.error.code != SKErrorPaymentCancelled)
     {
          [blurViewCalled1 removeFromSuperview];
          alertVc1.Learnmorebuyallbtn.enabled = true;
          alertVc1.closeunblocklearnmore.enabled = true;
          alertVc.unblockrestoreAction.enabled = true;
          alertVc.vokiblockclose.enabled = true;
          alertVc.unlockbuyallbtn.enabled = true;
          alertVc.BuyProduct.enabled = true;
          alertVc.ratealert.hidden = true;
          alertVc.unblocklearnmore.enabled = true;
          alertVc.vokiblockclose.enabled = true;
          
          
          _restorebtn.enabled=true;
          
          // error!
          [self finishTransaction:transaction wasSuccessful:NO];
     }
     else
     {
          
          [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
          [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
     }
}





- (IBAction)RestoreAction:(id)sender
{
     purchaseopen = true;
     isitunesconnect = false;
     
     _restorebtn.enabled = false;
     _purchasebuyall.enabled = false;
     //  self.Purchaselearnmore.alpha = 0;
     [_Purchaseview setUserInteractionEnabled:false];
     [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
     inappprogressview  = [[UIActivityIndicatorView alloc]
                           initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
     [inappprogressview setBackgroundColor:[UIColor blackColor]];
     inappprogressview.layer.cornerRadius = 10;
     inappprogressview.center=self.Purchaseview.center;
     [inappprogressview startAnimating];
     [self.Purchaseview addSubview:inappprogressview];
     [self displaypurchased];
     
}

-(void)displaypurchased
{
     _ReceiptRequest.delegate = self;
     [_ReceiptRequest start];
     [inappprogressview stopAnimating];
     [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
     [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(void)restorecharacters
{
     Restoreprogressview  = [[UIActivityIndicatorView alloc]
                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
     [Restoreprogressview setBackgroundColor:[UIColor blackColor]];
     Restoreprogressview.layer.cornerRadius = 10;
     Restoreprogressview.center=self.view.center;
     [Restoreprogressview startAnimating];
     alertVc1.Learnmorebuyallbtn.enabled = false;
     alertVc1.closeunblocklearnmore.enabled = false;
     alertVc.unlockbuyallbtn.enabled = false;
     alertVc.vokiblockclose.enabled = false;
     alertVc.unblockrestoreAction.enabled = false;
     alertVc.BuyProduct.enabled = false;
     alertVc.unblocklearnmore.enabled = false;
     _Purchasebuyallbtn.enabled = true;
     [self.view addSubview:Restoreprogressview];
     [self displaypurchased];
}



- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
     alertshown = false;
     _purchasebuyall.enabled = true;
     _Purchasebuyallbtn.enabled = true;
     _purchasebuyall.enabled = true;
     _Purchaselearnmorebtn.enabled = true;
     _Purchaselearnmorebtn.alpha = 1;
     isitunesconnect = true;
     _restorebtn.enabled = true;
     [_Purchaseview setUserInteractionEnabled:true];
     [blurViewCalled1 removeFromSuperview];
     [inappprogressview stopAnimating];
     [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
     NSString *paths = RUNNING_CHARACTER;
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     NSArray *soldcharcterss = [userDefaults objectForKey:@"solditems"];
     NSString *islogin = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"islogin"];
     if(_isRightVokiOpened)
          
     {
          _Purchasedollerimg.alpha = 0;
          _sidedollar.alpha = 0;
          SharingAvailble = true;
     }
     else
     {
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcterss containsObject:paths]|| [freecharacetr containsObject:paths])
                    {
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         SharingAvailble = true;
                         
                    }
                    else
                    {
                         _sidedollar.enabled = true;
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         SharingAvailble = false;
                    }
               }
               else
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
                    SharingAvailble = true;
               }
               
          }
          
          else
          {
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
               SharingAvailble = true;
          }
     }
     UIAlertView *tmp = [[UIAlertView alloc]
                         initWithTitle:@"Alert!"
                         message:@"Could not connect to itunes"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Ok", nil];
     [tmp show];
     _Purchasebuyallbtn.enabled = true;
     [Restoreprogressview stopAnimating];
     isChangeRegin = false;
     
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
     _Purchasebuyallbtn.enabled = true;
   
     
     if (isitunesconnect == false)
     {
          purchasedItemIDs = [[NSMutableArray alloc] init];
          
          NSLog(@"received restored transactions: %i", queue.transactions.count);
          
          for (SKPaymentTransaction *transaction in queue.transactions)
          {
               NSString *productID = transaction.payment.productIdentifier;
               
               if ([purchasedItemIDs containsObject:productID])
               {
                    
               }
               else
               {
                    [purchasedItemIDs addObject:productID];
                    
               }
               
          }
          
          if (queue.transactions.count == 0)
          {
               // check is change the country regin
               NSString *message;
               if (isChangeRegin == false) {
                   message = @"You have not purchased any character yet";
               }else{
                    isChangeRegin = false;
                    message = @"Please try again";
               }
               UIAlertView *tmp = [[UIAlertView alloc]
                                   initWithTitle:@"Alert!"
                                   message:message
                                   delegate:self
                                   cancelButtonTitle:nil
                                   otherButtonTitles:@"Ok", nil];
               [tmp show];
               [blurViewCalled1 removeFromSuperview];
               [blurViewCalled removeFromSuperview];
               [blurViewCalled2 removeFromSuperview];
               [blurViewCalled3 removeFromSuperview];
               _restorebtn.enabled = true;
               _purchasebuyall.enabled = true;
               [_Purchaseview setUserInteractionEnabled:true];
          }
          
          else
          {
               isChangeRegin = false;

               UIAlertView *alertView1 = [[UIAlertView alloc]initWithTitle:
                                          @"Your purchases have been restored." message:nil delegate:
                                          self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
               [alertView1 show];
               [blurViewCalled3 removeFromSuperview];
               [blurViewCalled2 removeFromSuperview];
               [blurViewCalled removeFromSuperview];
               [blurViewCalled1 removeFromSuperview];
             

               [Restoreprogressview stopAnimating];
               [self performSelector:@selector(loadRestorescrooler)  withObject:nil afterDelay:10.0];
               
          }
          
          
          if ([purchasedItemIDs containsObject:@"com.Gil.VokiEd.buyall"])
          {
               isbuyall = true;
               [[NSUserDefaults standardUserDefaults]setValue: @"yes" forKey:@"isbuyall"];
               [[NSUserDefaults standardUserDefaults]synchronize];
               
               _Purchasedollerimg.hidden = true;
               [self DisableButtonSelection];
               if (purchaseopen == true)
               {
                    _purchasedallview.hidden = false;
                    self.Purchaseview.hidden=true;
                    [__syncMainBtn setSelected:NO];
                    [__syncBtn setSelected:NO];
                    _Whyloginview.hidden = YES;
                    self.Accountview.hidden = YES;
                    [__termsBtn setSelected:YES];
                    [purchasebtn setSelected:YES];
                    [__termsMainBtn setSelected:NO];
                    self.Accountview.hidden = true;
                    __aboutView.hidden=true;
                    __syncView.hidden=true;
                    __termsView.hidden=true;
                    __faqView.hidden=true;
                    __menuDescriptionView.hidden=false;
                    
                    [blurViewCalled3 removeFromSuperview];
                    [blurViewCalled2 removeFromSuperview];
                    [blurViewCalled removeFromSuperview];
                    [blurViewCalled1 removeFromSuperview];
               }
               
          }
          else
          {
               isbuyall = false;
               [Restoreprogressview stopAnimating];
          }
     }
     
     
     
     else
     {
          isChangeRegin = false;

          [Restoreprogressview stopAnimating];

          _restorebtn.enabled = true;
          _purchasebuyall.enabled = true;
          _Purchasebuyallbtn.enabled = true;
          [_Purchaseview setUserInteractionEnabled:true];
          _Purchaselearnmorebtn.enabled = true;
     }
     
     
     
     
}

-(void)loadRestorescrooler
{
     
     alertshown = false;
     _restorebtn.enabled = true;
     _purchasebuyall.enabled = true;
     _Purchasebuyallbtn.enabled = true;
     [_Purchaseview setUserInteractionEnabled:true];
     _Purchaselearnmorebtn.enabled = true;
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
     int x = 35;
     int y = 10;
     NSString *Productid;
     NSArray *allproduct;
     NSMutableArray * charArray= [[NSMutableArray alloc]init];
     for (int i =0 ;i<[purchasedItemIDs count]; i++)
     {
          Productid = [purchasedItemIDs objectAtIndex:i];
          NSString *product;
          NSRange equalRange = [ Productid rangeOfString:@"VokiEd." options:NSBackwardsSearch];
          if (equalRange.location != NSNotFound)
          {
               product = [Productid substringFromIndex:equalRange.location + equalRange.length];
               
          }
          
          if ([product rangeOfString:@"."].location != NSNotFound );
          {
               NSRange equalRange1 = [ product rangeOfString:@"." options:NSBackwardsSearch];
               
               if (equalRange1.location != NSNotFound)
               {
                    NSString* key = [product substringToIndex:[product rangeOfString:@"."].location];
                    
                    if ([key  isEqual: @"HistoricalFigures"])
                    {
                         key = @"Historical Figures";
                    }
                    allproduct = [ Allproductid objectForKey:key];
                    
                    charArray =[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",key]];
                    
                    
                    for ( int j =0 ;j< [allproduct count];j++)
                    {
                         NSString *str = [allproduct objectAtIndex:j];
                         if ([str isEqualToString:Productid])
                              
                         {
                              NSString *path =   [NSString stringWithFormat:@"assets/img/char/%@",key];
                              NSString *storedchr = [NSString stringWithFormat:@"%@/%@",path,[charArray objectAtIndex:j]];
                              if (![soldcharcters containsObject:storedchr])
                              {
                                   [solditems addObject:storedchr];
                                   [userDefaults setObject:solditems forKey:@"solditems"];
                                   [userDefaults synchronize];
                              }
                              
                              
                              
                         }
                    }
                    
                    
               }
          }
          
     }
     [self loadpurchasescrooler];
     [inappprogressview stopAnimating];
     [blurViewCalled1 removeFromSuperview];
     NSString *paths = RUNNING_CHARACTER;
     NSArray *soldcharcterss = [userDefaults objectForKey:@"solditems"];
     NSString *islogin = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"islogin"];
     if(_isRightVokiOpened)
          
     {
          _Purchasedollerimg.alpha = 0;
          _sidedollar.alpha = 0;
          SharingAvailble = true;
     }
     else
     {
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcterss containsObject:paths]|| [freecharacetr containsObject:paths])
                    {
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         SharingAvailble = true;
                         
                    }
                    else
                    {
                         _sidedollar.enabled = true;
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         SharingAvailble = false;
                    }
               }
               else
               {
                    _Purchasedollerimg.alpha = 0;
                    _sidedollar.alpha = 0;
                    SharingAvailble = true;
               }
               
          }
          
          else
          {
               _Purchasedollerimg.alpha = 0;
               _sidedollar.alpha = 0;
               SharingAvailble = true;
          }
     }
     
}

-(void)loadpurchasescrooler
{
     alertVc.unlockbuyallbtn.enabled = true;
     alertVc.vokiblockclose.enabled = true;
     alertVc.unblockrestoreAction.enabled = true;
     
     int x = 35;
     int y = 10;
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     
     NSMutableArray *items = [userDefaults objectForKey:@"solditems"];
     for ( int j =0 ;j< [items count];j++)
     {
          
          NSString *image_Path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",[items objectAtIndex:j]] ofType:@"png" inDirectory:@""];
          
          [restore_products addObject:image_Path];
          
          UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x,y,60,60)];
          
          UIImage *image = [UIImage imageWithContentsOfFile:image_Path];
          
          [imageView setImage:image];
          
          [_Purchasescroolview addSubview:imageView];
          x = x+80;
          
          if ( x >= _Purchasescroolview.frame.size.width-60 )
          {
               y = y + 80 ;
               x = 35;
               _Purchasescroolview.frame = CGRectMake(_Purchasescroolview.frame.origin.x, _Purchasescroolview.frame.origin.y, _Purchasescroolview.frame.size.width,_Purchasescroolview.frame.size.height+y+60 );
               _purchasemainscroolview.contentSize = CGSizeMake(_purchasemainscroolview.frame.size.width, _purchasemainscroolview.frame.size.height+y+60);
               
          }
          
     }
}
#pragma mark Purchasing
#pragma mark StoreKit Delegate
-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
     alertshown = false ;
     _Purchasebuyallbtn.enabled = true;
     for (SKPaymentTransaction *transaction in transactions) {
          switch (transaction.transactionState)
          {
               case SKPaymentTransactionStatePurchasing:
                    NSLog(@"Purchasing");
                    
                    break;
               case SKPaymentTransactionStatePurchased:
                    if ([transaction.payment.productIdentifier
                         isEqualToString:Productforbuy]||[transaction.payment.productIdentifier
                                                          isEqualToString:BundleID] )
                    {
                         [blurViewCalled removeFromSuperview];
                         [blurViewCalled2 removeFromSuperview];
                         [blurViewCalled1 removeFromSuperview];
                         _purchasebuyall.enabled = true;
                         _restorebtn.enabled = true;
                         alertVc1.Learnmorebuyallbtn.enabled = true;
                         alertVc1.closeunblocklearnmore.enabled = true;
                         alertVc.unblockrestoreAction.enabled = true;
                         alertVc.vokiblockclose.enabled = true;
                         alertVc.unlockbuyallbtn.enabled = true;
                         alertVc.BuyProduct.enabled = true;
                         alertVc.unblocklearnmore.enabled = true;
                         _Purchasedollerimg.alpha = 0;
                         _sidedollar.alpha = 0;
                         
                         [inappprogressview stopAnimating];
                         if (buyall == true)
                         {
                              [blurViewCalled removeFromSuperview];
                              [blurViewCalled2 removeFromSuperview];
                              [blurViewCalled1 removeFromSuperview];
                              isbuyall = YES;
                              [[NSUserDefaults standardUserDefaults]setValue: @"yes" forKey:@"isbuyall"];
                              [[NSUserDefaults standardUserDefaults]synchronize];
                              
                              if (purchaseopen == true)
                              {
                                   purchaseopen = false;
                                   [self DisableButtonSelection];
                                   _purchasedallview.hidden = false;
                                   self.Purchaseview.hidden=true;
                                   [__syncMainBtn setSelected:NO];
                                   [__syncBtn setSelected:NO];
                                   _purchaselearnmore.hidden = true;
                                   _Whyloginview.hidden = YES;
                                   self.Accountview.hidden = YES;
                                   [__termsBtn setSelected:YES];
                                   [purchasebtn setSelected:YES];
                                   [__termsMainBtn setSelected:NO];
                                   self.Accountview.hidden = true;
                                   __aboutView.hidden=true;
                                   __syncView.hidden=true;
                                   __termsView.hidden=true;
                                   __faqView.hidden=true;
                                   __menuDescriptionView.hidden=false;
                              }
                              
                         }
                         else
                         {
                              
                              [blurViewCalled removeFromSuperview];
                              [blurViewCalled2 removeFromSuperview];
                              [blurViewCalled1 removeFromSuperview];
                              NSMutableString *paths = RUNNING_CHARACTER;
                              [solditems addObject:paths];
                              NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                              NSMutableArray *items = [userDefaults objectForKey:@"solditems"];
                              for (int i =0 ; i<items.count ; i++)
                              {
                                   NSString *path = [ items objectAtIndex:i];
                                   if (![solditems containsObject:path])
                                   {
                                        [solditems addObject:path];
                                   }
                                   
                                   
                              }
                              [userDefaults setObject:solditems forKey:@"solditems"];
                              [userDefaults synchronize];
                              [self loadpurchasescrooler];
                              
                         }
                    }
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                    _restorebtn.enabled = false;
                    break;
               case SKPaymentTransactionStateRestored:
                    if (isitunesconnect == true)
                    {
                         //                         UIAlertView *alertVieww = [[UIAlertView alloc]initWithTitle:
                         //                                                    @"Your purchases have been restored." message:nil delegate:
                         //                                                    self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         //                         [alertVieww show];
                         [Restoreprogressview stopAnimating];
                    }
                    
                    alertVc1.Learnmorebuyallbtn.enabled = true;
                    alertVc1.closeunblocklearnmore.enabled = true;
                    alertVc.unblockrestoreAction.enabled = true;
                    alertVc.vokiblockclose.enabled = true;
                    alertVc.unlockbuyallbtn.enabled = true;
                    alertVc.BuyProduct.enabled = true;
                    alertVc.unblocklearnmore.enabled = true;
                    [blurViewCalled1 removeFromSuperview];
                    [blurViewCalled2 removeFromSuperview];
                    [blurViewCalled removeFromSuperview];
                    break;
                    
               case SKPaymentTransactionStateFailed:
                    
                    [blurViewCalled removeFromSuperview];
                    [blurViewCalled1 removeFromSuperview];
                    [blurViewCalled2 removeFromSuperview];
                    [self failedTransaction:transaction];
                    
                    if(transaction.error.code != SKErrorPaymentCancelled && transaction.error.code != SKErrorPaymentNotAllowed )
                    {
                         _Purchasedollerimg.alpha = 1;
                         _sidedollar.alpha = 1;
                         _sidedollar.enabled = true;
                         _purchasebuyall.enabled = true;
                    }
                    else
                    {
                         [blurViewCalled removeFromSuperview];
                         [blurViewCalled1 removeFromSuperview];
                         [blurViewCalled2 removeFromSuperview];
                         _sidedollar.enabled = true;
                         _sidedollar.alpha = 1;
                         _restorebtn.enabled = true;
                         alertVc1.Learnmorebuyallbtn.enabled = true;
                         alertVc1.closeunblocklearnmore.enabled = true;
                         alertVc.unblockrestoreAction.enabled = true;
                         alertVc.vokiblockclose.enabled = true;
                         alertVc.unlockbuyallbtn.enabled = true;
                         alertVc.BuyProduct.enabled = true;
                         alertVc.unblocklearnmore.enabled = true;
                         UIAlertView *alertView1 = [[UIAlertView alloc]initWithTitle:
                                                    @"Purchase Canceled" message:@"Voki can not be shared until purchased." delegate:
                                                    self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alertView1 show];
                         _purchasebuyall.enabled = true;
                    }
                    
                    break;
                    
          }
     }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
     _Purchasebuyallbtn.enabled = true;
     _purchasebuyall.enabled = true;
     self.sidedollar.enabled = true;
     self.sidedollar.alpha = 1;
     alertshown = false ;
     _restorebtn.enabled = true;
     _purchasebuyall.enabled = true;
     _Purchaselearnmorebtn.enabled = true;
     isitunesconnect = true;
     _restorebtn.enabled = true;
     _purchasebuyall.enabled = true;
     [_Purchaseview setUserInteractionEnabled:true];
     [blurViewCalled1 removeFromSuperview];
     [inappprogressview stopAnimating];
     [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
     
}


-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
     _Purchasebuyallbtn.enabled = true;
     alertshown = false ;
     SKProduct *validProduct = nil;
     int count = [response.products count];
     
     if (count == 0)
     {
          inAppPurchaseCount_bool = YES;
     }
     
     if (count>0)
     {
          validProducts = response.products;
          validProduct = [response.products objectAtIndex:0];
          if ([validProduct.productIdentifier
               isEqualToString:Productforbuy])
          {
               
          }
     }
     else
     {
          
          
     }
}

#pragma mark collectionview Delegate and Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
     if (collectionView ==  allcatacollectionview)
     {
          return Allcaracters.count;
     }
     else
     {
          return  [splashArray count];
     }
     
}





- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
     if (collectionView ==  allcatacollectionview)
     {
          NSString  *xml_Path;
          static NSString *identifier = @"Cell";
          
          viewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
          cell.dollarimg.image = nil;
          NSString *image_Path = [Allcaracters objectAtIndex:indexPath.row];
          
          
          if ([selecteimagpath isEqualToString:image_Path])
          {
               cell.selected = YES;
               cell.backgroundColor = [UIColor orangeColor];
          }
          else
          {
               cell.selected = NO;
               cell.backgroundColor = [UIColor whiteColor];
          }
          NSString* username = [image_Path substringToIndex:[image_Path rangeOfString:@"/thumb_128x128.png"].location];
          NSRange equalRange = [username rangeOfString:@"unZipDirName/" options:NSBackwardsSearch];
          if (equalRange.location != NSNotFound)
          {
               xml_Path = [username substringFromIndex:equalRange.location + equalRange.length];
               
          }
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
          
          
          NSString *islogin = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"islogin"];
          
          if (![islogin  isEqual: @"yes"] )
          {
               if (isbuyall == NO)
               {
                    if ([soldcharcters containsObject:xml_Path] || [freecharacetr containsObject:xml_Path] )
                    {
                         
                         cell.dollarimg.image = nil;
                         
                         
                    }
                    else
                    {
                         cell.dollarimg.image = [UIImage imageNamed:@"dollar.png"];
                         
                    }
               }
               else
               {
                    cell.dollarimg.image = nil;
               }
               
          }
          
          else
          {
               cell.dollarimg.image = nil;
          }
          
          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
          {
               
               cell.dollarimg.frame = CGRectMake(cellWidth-30, cellWidth-20, 25, 20);
          }
          if (_canvasWidth <= 570 )
          {
               cell.dollarimg.frame = CGRectMake(cellWidth-18, cellWidth-20, 20, 18);
               
          }
          
          UIImage *image = [UIImage imageWithContentsOfFile:image_Path];
          NSString *image_Pathsel = [image_Path stringByReplacingOccurrencesOfString:@"base" withString:@"over"];
          UIImage *imagesel = [UIImage imageWithContentsOfFile:image_Pathsel];
          UIImage *image1  = [self resizeimage:image];
          cell.characterimg.image = image1;
          cell.layer.shouldRasterize = YES;
          cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
          cell.layer.masksToBounds = YES;
          cell.layer.cornerRadius = 4;
          cell.layer.shadowRadius = 8.0f;
          cell.layer.shadowOpacity= 0.2f;
          return cell;
     }
     else
     {
          static NSString *identifier = @"cell";
          viewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
          cell.splashimg.image = [UIImage imageNamed:[splashArray objectAtIndex:indexPath.row]];
          
          return cell;
          
     }
}


-(void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
     if (collectionView ==  allcatacollectionview)
     {
          [self.view setUserInteractionEnabled:false];
          
          saveSceneUrl = nil;
          Main_videoPath = @"";
          sameaudio = false;
          outurltweet = nil;
          thirdView = true;
          isgroupopen = false;
          
          
          UICollectionViewCell *datasetCell =[collectionView cellForItemAtIndexPath:indexPath];
          
          dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
               
               dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    datasetCell.backgroundColor = [UIColor orangeColor];
                    NSString *xml_Path;
                    NSString *image_Path = [Allcaracters objectAtIndex:indexPath.row];
                    
                    NSString* username = [image_Path substringToIndex:[image_Path rangeOfString:@"/thumb_128x128.png"].location];
                    NSRange equalRange = [username rangeOfString:@"unZipDirName/" options:NSBackwardsSearch];
                    if (equalRange.location != NSNotFound) {
                         xml_Path = [username substringFromIndex:equalRange.location + equalRange.length];
                         
                         
                         RUNNING_CHARACTER = xml_Path;
                         
                    }
                    
                    NSString  *selcat;
                    NSRange equalRange1 = [xml_Path rangeOfString:@"/" options:NSBackwardsSearch];
                    if (equalRange1.location != NSNotFound) {
                         selcat = [xml_Path substringFromIndex:equalRange1.location + equalRange1.length];
                         
                    }
                    
                    selectedCategory = [xml_Path substringToIndex:[xml_Path rangeOfString:selcat].location];
                    [[NSUserDefaults standardUserDefaults]setValue:selectedCategory forKey:@"selectedCategory"];
                    
                    for ( int i=0 ;i<[VokiScrollerArray count]; i++)
                    {
                         NSString *cat = [NSString stringWithFormat:@"assets/img/char/%@/",[VokiScrollerArray objectAtIndex:i]];
                         if ([cat isEqualToString:selectedCategory])
                              
                         {
                              NSString *key = [VokiScrollerArray objectAtIndex:i ];
                              NSArray *Producttarray = [ Allproductid objectForKey:key];
                              
                              NSArray* Array = [self RemoveIconImage:[[FetchFilePath sharedMethod] FetchAssestData:[NSString stringWithFormat:@"assets/img/char/%@",[VokiScrollerArray objectAtIndex:i]]]];
                              
                              for (int j =0 ; j<[Array count ]; j++)
                                   
                              {
                                   NSString *charac = [Array objectAtIndex:j];
                                   if ([charac isEqualToString:selcat])
                                        
                                   {
                                        
                                        Productforbuy = [Producttarray objectAtIndex:j];
                                        [self fetchAvailableProducts];
                                   }
                              }
                              
                         }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                         NSString *BundlePath = [[NSBundle mainBundle] resourcePath];
                         
                         NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                         NSArray *soldcharcters = [userDefaults objectForKey:@"solditems"];
                         NSString *islogin = [[NSUserDefaults standardUserDefaults]
                                              stringForKey:@"islogin"];
                         if (![islogin  isEqual: @"yes"] )
                         {
                              if (isbuyall == NO)
                              {
                                   if ([soldcharcters containsObject:xml_Path]|| [freecharacetr containsObject:xml_Path])
                                   {
                                        _Purchasedollerimg.alpha = 0;
                                        _sidedollar.alpha = 0;
                                        
                                   }
                                   else
                                   {
                                        _Purchasedollerimg.alpha = 1;
                                        _sidedollar.alpha = 1;
                                        _Purchasedollerimg.alpha = 1;
                                        _sidedollar.alpha = 1;
                                        _sidedollar.enabled = true;
                                        _Purchasedollerimg.hidden = false;
                                        _sidedollar.hidden = false;
                                   }
                              }
                              else
                              {
                                   _Purchasedollerimg.alpha = 0;
                                   _sidedollar.alpha = 0;
                              }
                              
                         }
                         
                         else
                         {
                              _Purchasedollerimg.alpha = 0;
                              _sidedollar.alpha = 0;
                         }
                         
                         
                         dispatch_async(dispatch_get_main_queue(), ^(void){
                              
                              NSMutableArray *arr = [[ParseObject sharedMethod] CallDescXML:1 :xml_Path];
                              if(arr.count != 0)
                              {
                                   spriteSheetSelectedChar = [[arr objectAtIndex:0] valueForKey:@"spriteSheet"];
                                   [[NSUserDefaults standardUserDefaults]setValue:spriteSheetSelectedChar forKey:@"spriteSheetSelectedChar"];
                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                   
                                   NSString *Costume,*fhair,*Glasses,*Hair,*Hat,*mouth,*Necklace,*Props;
                                   
                                   for(int i = 0; i < [[[arr objectAtIndex:0] valueForKey:@"Type"] count]; i ++)
                                   {
                                        for(NSString *key in [[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i])
                                        {
                                             if([key isEqualToString:@"Costume"])
                                             {
                                                  Costume = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             }
                                             else if([key isEqualToString:@"fhair"])
                                             {
                                                  fhair = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             }
                                             else if([key isEqualToString:@"Hair"])
                                             {
                                                  Hair = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             }
                                             else if([key isEqualToString:@"Hat"])
                                             {
                                                  Hat = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             }
                                             else if([key isEqualToString:@"mouth"])
                                             {
                                                  mouth = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                                  
                                             }
                                             else if([key isEqualToString:@"Necklace"])
                                             {
                                                  Necklace = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             }
                                             else if([key isEqualToString:@"Glasses"])
                                             {
                                                  Glasses = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             }
                                             else if([key isEqualToString:@"Props"])
                                             {
                                                  Props = [[[[arr objectAtIndex:0] valueForKey:@"Type"] objectAtIndex:i] valueForKey:key];
                                             }
                                        }
                                   }
                                   
                                   // NSString *output_path = [NSString stringWithFormat:@"%@/VokiContent",output];
                                   NSString *output_path = [NSString stringWithFormat:@"%@",output];
                                   NSBundle *myBundle = [NSBundle bundleWithPath:output_path];
                                   
                                   LibPath = [myBundle pathForResource:[NSString stringWithFormat:@"%@/thumb_128x128",xml_Path] ofType:@"png" inDirectory:@""];
                                   
                                   NSString *jsString = [NSString stringWithFormat:@"RunXMLfile('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",BundlePath,[[arr objectAtIndex:0] valueForKey:@"spriteSheet"],[[arr objectAtIndex:0] valueForKey:@"colorString"],Costume ?: @"0",fhair?: @"0",Glasses?: @"0",Hair?: @"0",Hat?: @"0",mouth?: @"0",Necklace?: @"969",Props?: @"0"];
                                   
                                   
                                   jsString = [jsString stringByReplacingOccurrencesOfString:@"World" withString:@"Historical Figures"];
                                   
                                   [___CharacterWebView stringByEvaluatingJavaScriptFromString:jsString];
                                   
                                   xpos = [[arr objectAtIndex:0] valueForKey:@"XPOS"];
                                   yposvalue = [[arr objectAtIndex:0] valueForKey:@"YPOS"];
                                   scalevalue = [[arr objectAtIndex:0] valueForKey:@"SCALE"];
                                   
                                   NSString *pos = [NSString stringWithFormat:@"positionChar('%@','%@','%@')",@"0.14",yposvalue,scalevalue];
                                   
                                   
                                   
                                   [___CharacterWebView stringByEvaluatingJavaScriptFromString:pos];
                                   
                                   genderID = [[arr objectAtIndex:0] valueForKey:@"genderID"];
                                   
                                   [___CharacterWebView stringByEvaluatingJavaScriptFromString:@"enableScreen()"];
                                   
                                   //                    if ([__LanguageLbl.text isEqualToString: @"English"])
                                   //                    {
                                   //                         engineID = 3;
                                   //                         if ([genderID isEqualToString:@"1"])
                                   //                         {
                                   //                              _textVoiceID=3;
                                   //
                                   //                         }
                                   //                         if ([genderID isEqualToString:@"2"])
                                   //                         {
                                   //                              _textVoiceID=2;
                                   //
                                   //                         }
                                   //                         if ([genderID isEqualToString:@"4"])
                                   //                         {
                                   //                              _textVoiceID=3;
                                   //
                                   //                         }
                                   //                         if ([genderID isEqualToString:@"3"])
                                   //                         {
                                   //                              _textVoiceID=3;
                                   //
                                   //                         }
                                   //                    }
                                   [self loaderposition];
                                   [self LoadVokiScroller];
                                   
                                   [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"VideoPath"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                   
                              }
                              
                         });
                         
                    });
               });
          });
          [self.view setUserInteractionEnabled:true];
     }
}

#pragma  mark collectionview Size for Row

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
     if (collectionView ==  allcatacollectionview)
     {
          CGFloat frame =  allcatagoryview.frame.size.width;
          frame = frame;
          cellWidth = frame / 7 ;
          CGSize size = CGSizeMake(cellWidth, cellWidth);
          return size;
     }
     else
     {
          return CGSizeMake(collectionView.bounds.size.width, collectionView.bounds.size.height);
          
     }
}

#pragma  mark collectionview up and Down ButtonACtion
- (IBAction)collectionUpAction:(id)sender
{
     [self DisableButtonSelection];
     [Groupup setSelected:YES];
     [Groupdown setSelected:NO];
     
     if (allcatacollectionview.contentOffset.y > 0)
     {
          [Groupdown setEnabled:YES];
          if (allcatacollectionview.contentOffset.y < cellWidth)
          {
               [allcatacollectionview setContentOffset:CGPointMake(0, 0) animated:YES];
          }
          else
          {
               [allcatacollectionview setContentOffset:CGPointMake(0, allcatacollectionview.contentOffset.y-cellWidth) animated:YES];
          }
     }
     else
     {
          [Groupup setSelected:NO];
          [Groupup setEnabled:NO];
          [Groupdown setEnabled:YES];
          
     }
}

- (IBAction)collectiondownAction:(id)sender
{
     [self DisableButtonSelection];
     [Groupdown setSelected:YES];
     [Groupup setSelected:NO];
     
     if(allcatacollectionview.contentOffset.y+allcatacollectionview.frame.size.height< allcatacollectionview.contentSize.height+8 )
     {
          [Groupup setEnabled:YES];
          if(allcatacollectionview.contentOffset.y+cellWidth +allcatacollectionview.frame.size.height > allcatacollectionview.contentSize.height )
          {
               [Groupdown setSelected:NO];
               [Groupdown setEnabled:NO];
               [Groupup setEnabled:YES];
               [allcatacollectionview setContentOffset:CGPointMake(0, allcatacollectionview.contentOffset.y+cellWidth) animated:YES];
          }
          else
          {
               [allcatacollectionview setContentOffset:CGPointMake(0, allcatacollectionview.contentOffset.y+cellWidth) animated:YES];
          }
     }
     else
     {
          [Groupdown setSelected:NO];
          [Groupdown setEnabled:NO];
          [Groupup setEnabled:YES];
     }
}

- (IBAction)purshaseBuyallaction:(id)sender

{
     purchaseopen = true;
     _purchasebuyall.enabled = false;
     _restorebtn.enabled = false;
     [self unlockbuyallbtnAction];
}

- (IBAction)termsconditnbtn:(id)sender {
     
     _TermsandPrivacyteextview.hidden = false;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
     
     
     for (UICollectionViewCell *cell in [allcatacollectionview visibleCells])
          
     {
          
          NSIndexPath *indexPath = [allcatacollectionview indexPathForCell:cell];
          if (indexPath.row <= 20)
          {
               currentIndexPath = 30;
          }
          
          else
          {
               currentIndexPath = indexPath.row;
               currentIndexPath = currentIndexPath;
          }
     }
}

#pragma mark Rate Alert button Action
-(void)remindlater
{
     
     [blurViewCalled3 removeFromSuperview];
}
-(void)reviewapp:(id)sender
{
     [blurViewCalled3 removeFromSuperview];
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     [prefs setObject:@"yes" forKey:@"reviewapp"];
     [blurViewCalled removeFromSuperview];
     NSString *appid = @"1106010700";
     
   //  NSString* url = [NSString stringWithFormat:  @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8", appid];
     
     NSString* url = [NSString stringWithFormat:  @"itms://itunes.apple.com/us/app/apple-store/id%@?action=write-review&mt=8", appid];

//      NSString* url = [NSString stringWithFormat:  @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software&action=write-review", appid];
//     
     [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url ]];

}
-(void)DontAskAgain
{
     Dontaskagain = true;
     [blurViewCalled3 removeFromSuperview];
}
-(void) showreviewAlert
{
     
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     // getting an NSString
     NSString *myString = [prefs stringForKey:@"reviewapp"];
     
     usagecount =  usagecount + 10;
     if (usagecount == 600 && Dontaskagain == false && ![myString isEqualToString:@"yes"])
     {
          [blurViewCalled3 removeFromSuperview];
          usagecount = 0;
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalled3 = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.ratealert.hidden = false;
          alertVc.deleteView.hidden = true;
          alertVc.blackView.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.ratealert.hidden = false;
          [alertVc.reviewbtn addTarget:self action:@selector(reviewapp:) forControlEvents:UIControlEventTouchUpInside];
          [alertVc.remindlaterbtn addTarget:self action:@selector(remindlater) forControlEvents:UIControlEventTouchUpInside];
          [alertVc.dontaskagainbtn addTarget:self action:@selector(DontAskAgain) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalled3];
     }
     
}

#pragma Timer Method
-(void) checkLogin
{
     
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     NSString *islogin = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"islogin"];
     if ([islogin  isEqual: @"yes"] )
     {
          NSString *currentDeviceId = [[NSUserDefaults standardUserDefaults]
                                       stringForKey:@"currentDeviceId"];
          
          NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
          NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
          NSURL *url = [NSURL URLWithString:@"https://vhss.oddcast.com/mng/mobile/mngMobileCheckLogin.php"];
          NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
          [urlRequest setTimeoutInterval:20];
          NSString * params = [NSString stringWithFormat:@"DeviceID=%@",currentDeviceId];
          [urlRequest setHTTPMethod:@"POST"];
          [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          
          NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                            {
                                                 if (data == nil)
                                                 {
                                                      
                                                 }
                                                 else
                                                 {
                                                      
                                                      NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                      
                                                      int status = [[jsonDict valueForKey:@"IsLoggedIn"]intValue];
                                                      NSString *errormesage = [jsonDict valueForKey:@"Error"];
                                                      if (status == 0)
                                                      {
                                                           
                                                           [UIView animateWithDuration:0.2 animations:^{
                                                                
                                                                [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"islogin"];
                                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                                _Loginusrdetailview.hidden = true;
                                                                self.MultipleLogins.hidden = true;
                                                                self.Accountview.hidden = false;
                                                                self.MainLoginview.hidden = false;
                                                                self.Whyloginview.hidden  = true;
                                                                self.twxtUserName.text = @"";
                                                                self.txtPassword.text = @"";
                                                           }];
                                                           
                                                           
                                                      }
                                                      
                                                      else
                                                      {
                                                           
                                                      }
                                                      
                                                 }
                                            }];
          [dataTask resume];
          
     }
     
}

-(void)RecordAudio
{
     @autoreleasepool
     {
          NSURL *outputFileURL= [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"RecordedFile"]];
          [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
          AVAudioSession *session = [AVAudioSession sharedInstance];
          [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
          // Define the recorder setting
          NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
          [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
          [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
          [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
          // Initiate and prepare the recorder
          recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
          recorder.delegate = self;
          recorder.meteringEnabled = YES;
          [recorder prepareToRecord];
     }
     
}

- (void) recordingFinished:(NSString*)outputPathOrNil
{
     
}
#pragma mark Download MP3 Files From Server in Background

-(void)saveMp3Files
{
     if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
     {
          [self.view setUserInteractionEnabled:true];
          alertVc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertScreen"];
          blurViewCalledbuy = [[[alertVc showPopUp] subviews] objectAtIndex:0];
          alertVc.lblText.text=@"This operation requires an internet connection. Your connection seems to be unavailable.";
          alertVc.deleteView.hidden = true;
          alertVc.ratealert.hidden = true;
          alertVc.unblockLearnmore.hidden = true;
          alertVc.unblockvookichar.hidden = true;
          alertVc.blackview1.hidden = true;
          alertVc.whiteborderView.hidden = false;
          [alertVc.closeBtn addTarget:self action:@selector(dismissinternet:) forControlEvents:UIControlEventTouchUpInside];
          [self.view addSubview:blurViewCalledbuy];
          __rightShareBtn.enabled = true;
          [self hideplayCharacterAud1];
          
     }
     
     else
     {
          [self.backgroundTask cancel];
          self.progressview.hidden = false;
          self.downloadview.hidden = false;
          [self.view bringSubviewToFront:self.downloadview];
          // unzip the local Politics file
          NSFileManager *fileManager = [NSFileManager defaultManager];
          NSArray *URLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
          NSURL *documentsDirectory = URLs[0];
          [self unzipPoliticsFile:documentsDirectory];
          
          self.progressview.progressValue = 2.0f;
          NSString *url = @"http://content.oddcast.com/ccs2/voki/mobile/ios-data-v2.zip";
         // NSString *url = @"http://beta.brstdev.com/swipr/ios-data-v1.zip";
          
          NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
          self.backgroundTask = nil;
          self.backgroundTask = [self.backgroundSession downloadTaskWithRequest:request];
          [self.backgroundTask resume];
          
          
          
     }
}

- (NSURLSession *)backgroundSession
{
     static NSURLSession *backgroundSession = nil;
     
     
     if (isdownloading == false)
     {
          static dispatch_once_t onceToken;
          dispatch_once(&onceToken, ^{
               NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.Gil.VokiEd"];
               backgroundSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
               isdownloading = true;
          });
          
     }
     return backgroundSession;
}

#pragma mark - NSURLSessionDownloadDelegate methods
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
     
     double currentProgress;
     currentProgress = totalBytesWritten / (double)totalBytesExpectedToWrite;
     int Progress = (int)(currentProgress * 100);
     Progress = Progress/2;
     
     dispatch_async(dispatch_get_main_queue(), ^{
          
          self.progressview.progressValue = Progress;
          
     });
     
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
     // Leave this for now
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
     
     // We've successfully finished the download. Let's save the file
     NSFileManager *fileManager = [NSFileManager defaultManager];
     NSArray *URLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
     NSURL *documentsDirectory = URLs[0];
     NSURL *destinationPath = [documentsDirectory URLByAppendingPathComponent:@"ios-data-v1.zip"];
     NSError *error;
     // Make sure we overwrite anything that's already there
     [fileManager removeItemAtURL:destinationPath error:NULL];
     BOOL success = [fileManager copyItemAtURL:location toURL:destinationPath error:&error];
     
     if (success == true)
     {
          dispatch_async(dispatch_get_main_queue(), ^{
               
               self.progressview.progressValue = 50.0f;
               self.unzipview.hidden = false;
               if (_backgroundTask.state == NSURLSessionTaskStateRunning) {
                    [_backgroundTask cancel];
               }
               
               [self unzipFile:documentsDirectory];
          });
          
          
     }
     else
     {
          //[[UIApplication sharedApplication] setIdleTimerDisabled: NO]; // Keep alive

          self.progressview.hidden = true;
          [self removeTimer];
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!!!" message:@"some error while downloading Assest.. Please Try Again!" delegate: nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
          [alert show];
          NSLog(@"%@",error);
     }
     
     
     
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    // [[UIApplication sharedApplication] setIdleTimerDisabled: NO]; // Keep alive

     
     self.progressview.progressValue = 1.0f;
     self.progressview.hidden = true;
     self.backgroundTask  = nil;
     if (_backgroundTask.state == NSURLSessionTaskStateRunning) {
          [_backgroundTask cancel];
     }
     
     
}

-(void) unzipFile:(NSURL*)documentsDirectory
{
     dispatch_async(dispatch_get_main_queue(), ^{
          // [[UIApplication sharedApplication] setIdleTimerDisabled: NO]; // Keep alive
          
          self.progressview.hidden = true;
          self.progressview.progressValue = 50.0f;
          
          download_installimg.image = [UIImage imageNamed:@"installing"];
          
     });
     dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
          //Background Thread
          NSURL *destinationPathss = [documentsDirectory URLByAppendingPathComponent:@"ios-data-v1.zip"];
          
          NSString *zipFilePath = [destinationPathss path];
          
          NSURL *destinationPathssss = [documentsDirectory URLByAppendingPathComponent:@"unZipDirName"];
          
          output = [ destinationPathssss path];
          
          ZipArchive* za = [[ZipArchive alloc] init];
          
          dispatch_async(dispatch_get_main_queue(), ^{
               
               self.progressview.hidden = false;
               
               self.progressview.progressValue = 50.0f;
               
          });
          
          ZipArchiveProgressUpdateBlock progressBlock = ^ (int percentage, int filesProcessed, int numFiles)
          
          {
               dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.progressview.hidden = false;
                    self.progressview.progressValue =  50 +  percentage/2;
                    
               });
               
          };
          
          za.progressBlock = progressBlock;
          
          if( [za UnzipOpenFile:zipFilePath])
          {
               if([za UnzipFileTo:output overWrite:YES] != NO)
               {
                    dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSString * sorce = [NSString stringWithFormat:@"%@/POLITICS",[documentsDirectory path]];
                         
                         NSString * dest = [NSString stringWithFormat:@"%@/assets/img/char/POLITICS",output];
                         
                         BOOL save = [self moveFile:sorce :dest];
                         NSLog(@"-----%d",save);
                         
                         [self savecharactersimage];
                         [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.0];
                         
                        
                         
                    });
               }
               else
               {
                    [inappprogressview stopAnimating];
               }
               
               [za UnzipCloseFile];
               [self hideplayCharacterAud1];
               self.view.userInteractionEnabled = true;
               [inappprogressview stopAnimating];
          }
          
     });
     
     
}



-(BOOL) moveFile:(NSString *)srcPath :(NSString *)dstPath
{
     NSLog(@"moving %@ -> %@", srcPath, dstPath);
     
    NSFileManager *fm = [NSFileManager defaultManager];
     if ([fm fileExistsAtPath:dstPath])
     {
          // in my usecase this is a hard error, bolt to prevent overwriting
          return NO;
     }
     
     if ([fm fileExistsAtPath:srcPath])
     {
          NSError *error = nil;
          NSString *destDir = [dstPath stringByDeletingLastPathComponent];
          [fm createDirectoryAtPath:destDir withIntermediateDirectories:YES attributes:nil error:nil];
          
          if ([[NSFileManager defaultManager] copyItemAtPath:srcPath toPath:dstPath error:&error]==NO)
          {
               NSLog(@"failure declassing %@", srcPath);
               return NO;
          }
          else
          {
               [fm removeItemAtPath:srcPath error:NULL]; // gr8t success
               return YES;
          }
     }
     
     return NO;
}

#pragma Mark  UNZIP FIle:-

-(void) unzipPoliticsFile:(NSURL*)documentsDirectory
{
//     dispatch_async(dispatch_get_main_queue(), ^{
//         // [[UIApplication sharedApplication] setIdleTimerDisabled: NO]; // Keep alive
//
//          self.progressview.hidden = true;
//          self.progressview.progressValue = 50.0f;
//          
//          download_installimg.image = [UIImage imageNamed:@"installing"];
//          
//     });
     dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
          //Background Thread
          NSString *zipFilePath = [[NSBundle mainBundle] pathForResource:@"POLITICS" ofType:@"zip"];
          
//           NSURL *destinationPathssss = [documentsDirectory URLByAppendingPathComponent:@"Politics"];
          
         NSString *unZipFilePath = [documentsDirectory path];
          
        //  NSURL *destinationPathssss = [documentsDirectory URLByAppendingPathComponent:@"unZipDirName"];
          
       //   output = [ destinationPathssss path];
          
          
          
          ZipArchive* za = [[ZipArchive alloc] init];
          
          dispatch_async(dispatch_get_main_queue(), ^{
               
//               self.progressview.hidden = false;
//               
//               self.progressview.progressValue = 50.0f;
//               
          });
          
          ZipArchiveProgressUpdateBlock progressBlock = ^ (int percentage, int filesProcessed, int numFiles)
          
          {
               dispatch_async(dispatch_get_main_queue(), ^{
                    
//                    self.progressview.hidden = false;
//                    self.progressview.progressValue =  50 +  percentage/2;
//                    
               });
               
          };
          
          za.progressBlock = progressBlock;
          
          if( [za UnzipOpenFile:zipFilePath])
          {
               if([za UnzipFileTo:unZipFilePath overWrite:YES] != NO)
               {
                    dispatch_async(dispatch_get_main_queue(), ^{
                         
//                         [self savecharactersimage];
//                         [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.0];
                         
                    });
               }
               else
               {
                    //[inappprogressview stopAnimating];
               }
               
               [za UnzipCloseFile];
//               [self hideplayCharacterAud1];
//               self.view.userInteractionEnabled = true;
//               [inappprogressview stopAnimating];
          }
          
     });
     
     
}

-(void)updateUI
{
     [[UIApplication sharedApplication] setIdleTimerDisabled: NO];
     vokilogo_img.hidden = false;
     _downloadview.hidden = true;
     Sliding_view.hidden = true;
     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunched"];
     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isOldData"];

     [self hideplayCharacterAud1];
     self.view.userInteractionEnabled = true;
     self.unzipview.hidden = true;
     [self removeTimer];
}


- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
     UITextView *textView = (UITextView *)recognizer.view;
     
     // Location of the tap in text-container coordinates
     
     NSLayoutManager *layoutManager = textView.layoutManager;
     CGPoint location = [recognizer locationInView:textView];
     location.x -= textView.textContainerInset.left;
     location.y -= textView.textContainerInset.top;
     
     // Find the character that's been tapped on
     
     NSUInteger characterIndex;
     characterIndex = [layoutManager characterIndexForPoint:location
                                            inTextContainer:textView.textContainer
                   fractionOfDistanceBetweenInsertionPoints:NULL];
     
     if (characterIndex >= range.location-2 && characterIndex <= range.location+2)
          
     {
          [self AccountAction];
          
     }
     
     
}

- (void)termsTapGesture:(UITapGestureRecognizer *)recognizer
{
     UITextView *textView = (UITextView *)recognizer.view;
     
     
     // Location of the tap in text-container coordinates
     
     NSLayoutManager *layoutManager = textView.layoutManager;
     CGPoint location = [recognizer locationInView:textView];
     location.x -= textView.textContainerInset.left;
     location.y -= textView.textContainerInset.top;
     
     // Find the character that's been tapped on
     
     NSUInteger characterIndex;
     characterIndex = [layoutManager characterIndexForPoint:location
                                            inTextContainer:textView.textContainer
                   fractionOfDistanceBetweenInsertionPoints:NULL];
     
     if (characterIndex >= termsrange.location-20 && characterIndex <= termsrange.location+9)
          
     {
          _Privacyview.hidden = true;
          
          self._termsServicesTextView.hidden = true;
          
          _Termsprivacyview.hidden = false;
          
     }
     else if (characterIndex >= Privacyrange.location-2 && characterIndex <= Privacyrange.location+18)
          
     {
          
          
          self._termsServicesTextView.hidden = true;
          
          _Termsprivacyview.hidden = true;
          
          _Privacyview.hidden = false;
          
     }
}

- (IBAction)closeTermsprivacyview:(id)sender
{
     _Termsprivacyview.hidden = true;
     _Privacyview.hidden = true;
     self._termsServicesTextView.hidden = false;
     
}
- (IBAction)closeprivacyview:(id)sender {
     
     _Termsprivacyview.hidden = true;
     _Privacyview.hidden = true;
     self._termsServicesTextView.hidden = false;
}

- (IBAction)Purchaserestoreall:(id)sender
{
     buyall = true;
     _Purchasebuyallbtn.enabled = false;
     purchaseopen = true;
     [self unlockbuyallbtnAction];
}


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
     
     float pageWidth = sliding_collectionview.bounds.size.width; // width + space
     
     float currentOffset = scrollView.contentOffset.x;
     float targetOffset = targetContentOffset->x;
     float newTargetOffset = 0;
     
     if (targetOffset > currentOffset)
          newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
     else
          newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
     
     if (newTargetOffset < 0)
          newTargetOffset = 0;
     else if (newTargetOffset > scrollView.contentSize.width)
          newTargetOffset = scrollView.contentSize.width;
     
     targetContentOffset->x = currentOffset;
     [scrollView setContentOffset:CGPointMake(newTargetOffset, scrollView.contentOffset.y) animated:YES];
}



@end
