//
//  AppDelegate.m
//  VokiApp
//
//  Created by brst on 12/8/15.
//  Copyright © 2015 brst. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <CoreData/CoreData.h>
#import "GalleryViewController.h"
#import <GooglePlus/GooglePlus.h>
#import "ViewController.h"
#import <StoreKit/StoreKit.h>
#import "ViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TwitterKit/TwitterKit.h>
#define GOOGLE_PLUS_CLIENT_ID @"944406190708-9h5fd0gof4nf6mo3g01glbm1g51rjfsu.apps.googleusercontent.com"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize google_str,google_url,isFacebookCalled,isRecording,isTwitterCalled;

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.isRecording = NO;
     [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    [Fabric with:@[[Crashlytics class]]];
  
    [[NSUserDefaults standardUserDefaults] setObject:currentDeviceId forKey:@"currentDeviceId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults]setValue:@"https://vhss.oddcast.com/admin/getAudioByNameMP3.php?audioname=kara_4&acc=237929&play=1" forKey:@"SOUND_LINK"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults]setValue:@"/assets/img/char/Anime/Aiko_217" forKey:@"RUNNING_CHARACTER"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self initGooglePlusSignIn];
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"soundTTS"];
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"characterCSURL"];
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"saveTTSLink"];
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"galleryImage"];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES]; // Keep alive


     [[Twitter sharedInstance] startWithConsumerKey:@"YaPyr2VY8RkhGHsT84BSej2MN" consumerSecret:@"8x1s0OmnAyemtKuwk4sv75x47psPmaAklk6jVhfWq3jVQLjByZ"];

    return YES;
}
- (void)initGooglePlusSignIn
{
    [GPPSignIn sharedInstance].clientID = GOOGLE_PLUS_CLIENT_ID;
       
}
//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
//{
//    return [[Twitter sharedInstance] application:app openURL:url options:options];
//}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if(isFacebookCalled)
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    else if (isTwitterCalled)
    {
         return [[Twitter sharedInstance] application:application openURL:url options:sourceApplication];
    }
  
    else
    {
        return [GPPURLHandler handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
    }
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([self.window.rootViewController.presentedViewController isKindOfClass: [GalleryViewController class]])
    {
        GalleryViewController *secondController = (GalleryViewController *) self.window.rootViewController.presentedViewController;
        
        if (secondController.isPresented)
            return UIInterfaceOrientationMaskPortrait;
        else return UIInterfaceOrientationMaskLandscape;
    }
    else return UIInterfaceOrientationMaskLandscape;

}
#pragma  mark - Check Network Connection

-(BOOL)ReturnConnection
{
    BOOL Connection;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        //my web-dependent code
        Connection = true;
    }
    else
    {
        //there-is-no-connection warning
        Connection = false;
    }
    
    return Connection;
}

-(BOOL)internetActive
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if(internetStatus == NotReachable)
    {
        return NO;
    }//END if(internetStatus == NotReachable)
    
    else {
        return YES;
    }//END Else Statement
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotification:[NSNotification notificationWithName:@"appDidEnterbackground" object:nil]];
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"backgroundenter"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotification:[NSNotification notificationWithName:@"appDidEnterForeground" object:nil]];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4.00 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [activityView stopAnimating];
//        [self.window setUserInteractionEnabled:true];
//    });
    
  
   
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FetchProgressValue)  name:@"PROGRESSVALUE" object:nil];
    
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSDKAppEvents activateApp];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
