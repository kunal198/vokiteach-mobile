//
//  ServerConnection.h
//  VokiApp
//
//  Created by brst on 1/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AppDelegate;

@interface ServerConnection : NSObject
{
    AppDelegate *appDelegate_obj;
    
    
}

+ (ServerConnection *)sharedMethod;

-(NSString*)SendDataToServer:(NSString*)EID :(NSString*)LID :(NSString*)VID :(NSString*)TXT :(NSString*)EXT :(NSString*)FX_TYPE :(NSString*)FX_LEVEL :(NSString*)ACC :(NSString*)SESSION :(NSString*)CS;

@end
