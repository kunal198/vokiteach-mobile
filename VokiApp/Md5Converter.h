//
//  Md5Converter.h
//  VokiApp
//
//  Created by brst on 1/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Md5Converter : NSObject

+ (Md5Converter *)sharedMethod;
- (NSString *)MD5String :(NSString *) input;

@end
