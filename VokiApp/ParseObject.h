//
//  ParseObject.h
//  VokiApp
//
//  Created by brst on 1/21/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParseObject : NSObject<NSXMLParserDelegate>
{
    NSXMLParser *xmlParser;
    NSString *LastElement;
    
    NSMutableArray *_costumeArray;
    NSMutableArray *_fhairArray;
    NSMutableArray *_glassesArray;
    NSMutableArray *_hairArray;
    NSMutableArray *_hatArray;
    NSMutableArray *_mouthArray;
    NSMutableArray *_necklaceArray;
    NSMutableArray *_propsArray;
    
    NSMutableArray *_mainArray;
    NSString* _hostColorString,*_hostSpritSheet,*_hostAccString,*XPOS,*YPOS,*SCALE,*genderID;
    NSString*_typeIDString,*_typeIDKey,*_accIDString,*_accIDKey;
    NSString *_accString,*_accTypeString;
    
    NSMutableArray *_dataArray;
    NSMutableArray *_typeArray;
    NSMutableArray *_AccIDArray;
}

+ (ParseObject *)sharedMethod;
-(NSMutableArray*)ParseFile;
-(NSMutableArray*)CallDescXML:(int)IndexNo :(NSString*)XML_Path;

@end
