//
//  GalleryViewController.m
//  VokiApp
//
//  Created by mrinal khullar on 3/8/16.
//  Copyright © 2016 brst. All rights reserved.
//
/*
if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
    TWTRComposerViewController *composer = [[TWTRComposerViewController  emptyComposer] initWithInitialText:@"Voki" image:nil videoURL:filename] ;
    composer.delegate = self;
    [self presentViewController:composer animated:YES completion:nil];
} else {
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            TWTRComposerViewController *composer = [[TWTRComposerViewController  emptyComposer] initWithInitialText:@"Voki" image:nil videoURL:filename ] ;
            [self presentViewController:composer animated:YES completion:nil];
        } else {
            
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Twitter Accounts Available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"cancel", nil];
            alert.tag = 1;
            
        }
    }];
}
*/



#import "GalleryViewController.h"

@interface GalleryViewController ()

@end

@implementation GalleryViewController
@synthesize isPresented,galleryCamera;
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil];
    
    if (self)
    {
        isPresented = YES;
    }
    
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    isPresented = YES;
   
    
 
}


-(void)viewWillAppear:(BOOL)animated
{

}

-(void)viewDidAppear:(BOOL)animated
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
        //imagePicker.allowsEditing = YES;
            if (galleryCamera == true)
        {
            UIDeviceOrientation Orientation=[[UIDevice currentDevice]orientation];
            if (Orientation == UIDeviceOrientationLandscapeRight)
            {
                orientation = true;
            }
            
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
        }
        else
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
      
        
            [self presentViewController:imagePicker animated:YES completion:nil];
        
        

        
  });
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * img = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    UIImage *image = img;
    
    switch (img.imageOrientation)
    {
        case UIImageOrientationUp:
             orientation = false;
             break;
        case UIImageOrientationDown:
            orientation = true;
            break;
        case UIImageOrientationLeft:
            orientation = false;
            break;
        case UIImageOrientationRight:
             orientation = false;
             break;
        default:
             break;
    }
    
    UIImage *rotatedImage = [self imageRotatedByDegrees:img deg:180];
   if (orientation == true)
   {
       [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(rotatedImage) forKey:@"galleryImage"];

   }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:@"galleryImage"];

    }
    
    isPresented = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self dismissViewControllerAnimated:true completion:nil];
        [self.presentingViewController dismissViewControllerAnimated: YES completion: nil];
        
    });
    
    
}

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    //Calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,oldImage.size.width, oldImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    //Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    //Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //Rotate the image context
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    //Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"galleryImage"];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self dismissViewControllerAnimated:true completion:nil];
        isPresented = NO;
        [self.presentingViewController dismissViewControllerAnimated: YES completion: nil];
    });

   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
