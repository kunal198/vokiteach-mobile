REM requires Image Magick

SET INPUT_DIR=L:\Voki\_Avatar_Creator\_Avatar_Creator_Mobile\comps\icons\PNGs

md assets\vokiapp\img\ui\icons\randomize_anim\base

rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim03

rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim04
rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim02
rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim06
rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim08
rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim07
rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim05
rem call :SUB_convert %INPUT_DIR% \randomize_anim\base\btn_randomize_anim01

md assets\vokiapp\img\ui\icons\randomize_anim\over
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim03
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim04
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim02
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim06
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim08
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim07
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim05
rem call :SUB_convert %INPUT_DIR% \randomize_anim\over\btn_randomize_anim01

pause

rem call :SUB_charTypes %INPUT_DIR% btn_animals Animals
rem call :SUB_charTypes %INPUT_DIR% btn_anime Anime
rem call :SUB_charTypes %INPUT_DIR% btn_cats Cats
rem call :SUB_charTypes %INPUT_DIR% btn_classic Classic
rem call :SUB_charTypes %INPUT_DIR% btn_digimon Digimon
rem call :SUB_charTypes %INPUT_DIR% btn_dogs Dogs
rem call :SUB_charTypes %INPUT_DIR% btn_edgy Edgy
rem call :SUB_charTypes %INPUT_DIR% btn_fantasy Fantasy
rem call :SUB_charTypes %INPUT_DIR% btn_world Folk
rem call :SUB_charTypes %INPUT_DIR% btn_holiday Holiday
rem call :SUB_charTypes %INPUT_DIR% btn_monsters Monsters
rem call :SUB_charTypes %INPUT_DIR% btn_oddballs Oddballs
rem call :SUB_charTypes %INPUT_DIR% btn_random Randoms
rem call :SUB_charTypes %INPUT_DIR% btn_smilies Smilies
rem call :SUB_charTypes %INPUT_DIR% btn_toons Toons
rem call :SUB_charTypes %INPUT_DIR% btn_vip VIP
rem call :SUB_charTypes %INPUT_DIR% btn_historical World
rem call :SUB_charTypes %INPUT_DIR% btn_3D 3D
rem call :SUB_charTypes %INPUT_DIR% btn_beach Beach
rem call :SUB_charTypes %INPUT_DIR% btn_politics POLITICS
rem call :SUB_charTypes %INPUT_DIR% btn_symbols Symbols

md assets\vokiapp\img\ui\icons\over
pause
call :SUB_convert %INPUT_DIR% \over\btn_color
pause
call :SUB_convert %INPUT_DIR% \over\btn_library
call :SUB_convert %INPUT_DIR% \over\btn_mouth
call :SUB_convert %INPUT_DIR% \over\btn_TTS
call :SUB_convert %INPUT_DIR% \over\utility_item_box_over
call :SUB_convert %INPUT_DIR% \over\btn_props
call :SUB_convert %INPUT_DIR% \over\btn_share_email
call :SUB_convert %INPUT_DIR% \over\btn_accessories
call :SUB_convert %INPUT_DIR% \over\btn_hair
call :SUB_convert %INPUT_DIR% \over\btn_background
call :SUB_convert %INPUT_DIR% \over\btn_share_blogger
call :SUB_convert %INPUT_DIR% \over\btn_facial_hair
call :SUB_convert %INPUT_DIR% \over\btn_mic
call :SUB_convert %INPUT_DIR% \over\btn_share_facebook
call :SUB_convert %INPUT_DIR% \over\btn_randomize
call :SUB_convert %INPUT_DIR% \over\btn_share_wiki
call :SUB_convert %INPUT_DIR% \over\btn_share_gplus
call :SUB_convert %INPUT_DIR% \over\btn_background_add
call :SUB_convert %INPUT_DIR% \over\btn_head
call :SUB_convert %INPUT_DIR% \over\btn_hat
call :SUB_convert %INPUT_DIR% \over\btn_glasses
call :SUB_convert %INPUT_DIR% \over\utility_btn_X_1
call :SUB_convert %INPUT_DIR% \over\utility_logo_voki
call :SUB_convert %INPUT_DIR% \over\btn_skin
call :SUB_convert %INPUT_DIR% \over\btn_save_to_library
call :SUB_convert %INPUT_DIR% \over\btn_voice
call :SUB_convert %INPUT_DIR% \over\btn_trashcan
call :SUB_convert %INPUT_DIR% \over\btn_eye
call :SUB_convert %INPUT_DIR% \over\btn_clothing
call :SUB_convert %INPUT_DIR% \over\btn_share_twitter
call :SUB_convert %INPUT_DIR% \over\btn_menu
call :SUB_convert %INPUT_DIR% \over\btn_nightlife
call :SUB_convert %INPUT_DIR% \over\btn_head2
call :SUB_convert %INPUT_DIR% \over\btn_sports
call :SUB_convert %INPUT_DIR% \over\btn_bling
call :SUB_convert %INPUT_DIR% \over\btn_city
call :SUB_convert %INPUT_DIR% \over\btn_share

md assets\vokiapp\img\ui\icons\base

call :SUB_convert %INPUT_DIR% \base\btn_color
call :SUB_convert %INPUT_DIR% \base\utility_item_box
call :SUB_convert %INPUT_DIR% \base\btn_library
call :SUB_convert %INPUT_DIR% \base\btn_mouth
call :SUB_convert %INPUT_DIR% \base\btn_TTS
call :SUB_convert %INPUT_DIR% \base\btn_props
call :SUB_convert %INPUT_DIR% \base\btn_share_email
call :SUB_convert %INPUT_DIR% \base\utility_color_arrow
call :SUB_convert %INPUT_DIR% \base\btn_accessories
call :SUB_convert %INPUT_DIR% \base\btn_hair
call :SUB_convert %INPUT_DIR% \base\btn_background
call :SUB_convert %INPUT_DIR% \base\btn_share_blogger
call :SUB_convert %INPUT_DIR% \base\btn_facial_hair
call :SUB_convert %INPUT_DIR% \base\btn_mic
call :SUB_convert %INPUT_DIR% \base\btn_share_facebook
call :SUB_convert %INPUT_DIR% \base\btn_play
call :SUB_convert %INPUT_DIR% \base\utility_color_selector
call :SUB_convert %INPUT_DIR% \base\btn_randomize
call :SUB_convert %INPUT_DIR% \base\btn_pause
call :SUB_convert %INPUT_DIR% \base\btn_share_wiki
call :SUB_convert %INPUT_DIR% \base\btn_share_gplus
call :SUB_convert %INPUT_DIR% \base\utility_arrow
call :SUB_convert %INPUT_DIR% \base\btn_background_add
call :SUB_convert %INPUT_DIR% \base\btn_head
call :SUB_convert %INPUT_DIR% \base\btn_hat
call :SUB_convert %INPUT_DIR% \base\btn_glasses
call :SUB_convert %INPUT_DIR% \base\utility_btn_X_1
call :SUB_convert %INPUT_DIR% \base\utility_logo_voki
call :SUB_convert %INPUT_DIR% \base\btn_skin
call :SUB_convert %INPUT_DIR% \base\btn_save_to_library
call :SUB_convert %INPUT_DIR% \base\btn_voice
call :SUB_convert %INPUT_DIR% \base\btn_trashcan
call :SUB_convert %INPUT_DIR% \base\utility_dropdown
call :SUB_convert %INPUT_DIR% \base\btn_eye
call :SUB_convert %INPUT_DIR% \base\btn_clothing
call :SUB_convert %INPUT_DIR% \base\btn_share_twitter
call :SUB_convert %INPUT_DIR% \base\btn_menu
call :SUB_convert %INPUT_DIR% \base\btn_nightlife
call :SUB_convert %INPUT_DIR% \base\btn_head2
call :SUB_convert %INPUT_DIR% \base\btn_sports
call :SUB_convert %INPUT_DIR% \base\btn_bling
call :SUB_convert %INPUT_DIR% \base\btn_city
call :SUB_convert %INPUT_DIR% \base\btn_share
pause
exit /b

:SUB_convert
	convert "%1%2.png" -resize 128x128 "assets\vokiapp\img\ui\icons%2_128x128.png"
exit /b

:SUB_charTypes
	mkdir assets\vokiapp\img\char\%3\icons\base\
	convert %1/base/%2.png -resize 128x128 assets\vokiapp\img\char\%3\icons\base\%2_128x128.png
	mkdir assets\vokiapp\img\char\%3\icons\over\
	convert %1/over/%2.png -resize 128x128 assets\vokiapp\img\char\%3\icons\over\%2_128x128.png
exit /b